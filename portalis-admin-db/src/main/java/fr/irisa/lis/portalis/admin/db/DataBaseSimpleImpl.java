package fr.irisa.lis.portalis.admin.db;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.UnknownHostException;
import java.util.Set;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import com.mongodb.DB;
import com.mongodb.MongoClient;

import fr.irisa.lis.portalis.commons.DOMUtil;
import fr.irisa.lis.portalis.shared.admin.ClientConstants;
import fr.irisa.lis.portalis.shared.admin.PortalisError;
import fr.irisa.lis.portalis.shared.admin.PortalisException;
import fr.irisa.lis.portalis.shared.admin.RequestException;
import fr.irisa.lis.portalis.shared.admin.RightValue;
import fr.irisa.lis.portalis.shared.admin.data.LoginErr;
import fr.irisa.lis.portalis.shared.admin.data.LoginResult;
import fr.irisa.lis.portalis.shared.admin.data.PasswordUser;
import fr.irisa.lis.portalis.shared.admin.data.Site;
import fr.irisa.lis.portalis.shared.admin.data.StandardUser;
import fr.irisa.lis.portalis.shared.admin.data.SuperUser;
import fr.irisa.lis.portalis.shared.admin.data.UserData;
import fr.irisa.lis.portalis.shared.admin.http.AdminHttp;

@SuppressWarnings("serial")
public class DataBaseSimpleImpl extends Site implements DataBase {

	private static final Logger LOGGER = LoggerFactory
			.getLogger(DataBaseSimpleImpl.class.getName());

	private static final String DB_SITE = AdminHttp.WEBAPP_PATH
			+ "/portalisSite.xml";

	/** Holder */
	private static class SingletonHolder {
		/** Instance unique non préinitialisée */
		private final static DataBaseSimpleImpl instance = new DataBaseSimpleImpl();
	}

	private DataBaseSimpleImpl() {
		super();
		try {
			load();
		} catch (Exception e) {
			String mess = "impossible de charger la base de données du site : "
					+ e.getMessage();
			LOGGER.error(mess);
			throw new PortalisError(mess);
		}
	}

	/** Point d'accès pour l'instance unique du singleton */
	public static DataBaseSimpleImpl getInstance() {
		return SingletonHolder.instance;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see fr.irisa.lis.portalis.admin.db.DataB#init()
	 */
	@Override
	public void load() throws FileNotFoundException, SAXException, IOException,
			ParserConfigurationException, RequestException, PortalisException {
		Document doc = DOMUtil.readFile(new FileInputStream(DB_SITE));
		ClientConstants.ADMIN_DATA_XML_READER.setSite(doc.getDocumentElement(),
				this);

	}

	public static Site load(String fileName) throws RequestException,
			PortalisException, SAXException, IOException,
			ParserConfigurationException {

		Document doc = DOMUtil.readFile(new FileInputStream(fileName));
		Site site = ClientConstants.ADMIN_DATA_XML_READER.getSite(doc
				.getDocumentElement());
		return site;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see fr.irisa.lis.portalis.admin.db.DataB#getUserInfo(java.lang.String,
	 * java.lang.String)
	 */
	@Override
	public LoginResult getUserInfo(String email, String password) {
		UserData userCore = users.get(email);
		LoginErr err = null;
		if (userCore == null) {
			err = LoginErr.unknown;
		} else if (userCore instanceof StandardUser) {
			if (((StandardUser) userCore).getPassword().equals(password)) {
				err = LoginErr.ok;
			} else {
				err = LoginErr.incorrectPassword;
			}
		} else if (userCore instanceof SuperUser) {
			if (((PasswordUser) userCore).getPassword().equals(password)) {
				err = LoginErr.ok;
			} else {
				err = LoginErr.incorrectPassword;
			}
		} else {
			err = LoginErr.unexpected;
		}
		return new LoginResult(userCore, err);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see fr.irisa.lis.portalis.admin.db.DataB#save(java.lang.String)
	 */
	@Override
	public void save() throws IOException, TransformerException {
		save(DB_SITE);
	}

	@Override
	public void save(String fileName) throws IOException, TransformerException {
		DOMUtil.writeFile(ClientConstants.PORTALIS_DATA_XML_WRITER.visit(this)
				.getOwnerDocument(), fileName);
	}

	@Override
	public void isAuthorized(UserData userData, String serviceFullName,
			RightValue requiredRole) throws PortalisException {
		RightValue max = null;
		if (userData instanceof SuperUser)
			max = RightValue.SUPER_ADMIN;
		else
			max = getRole(userData.getEmail(), serviceFullName).getRight();
		if (!RightValue.greaterEq(max, requiredRole)) {
			String mess = new StringBuffer(requiredRole.name())
					.append(" permission denied for user ")
					.append(userData.getEmail()).append(", it has ")
					.append(max.name()).append(" permission").toString();
			LOGGER.warn(mess);
			throw new PortalisException(mess);
		}
	}

}

package com.androlis.networkCommunication;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import android.os.AsyncTask;

import com.androlis.memorizator.Memory;

import fr.irisa.lis.portalis.shared.admin.PortalisException;
import fr.irisa.lis.portalis.shared.camelis.http.CamelisHttp;
import fr.irisa.lis.portalis.shared.camelis.reponse.GetValuedFeaturesReponse;

public class GetFeatures extends AsyncTask<String[], Void, GetValuedFeaturesReponse> {
	private static final Logger logger = LoggerFactory.getLogger(GetFeatures.class.getName());

	@Override
	protected GetValuedFeaturesReponse doInBackground(String[]... params) {
		logger.info("Start GetFeatures Task");

		GetValuedFeaturesReponse valuedFeatures = new GetValuedFeaturesReponse();
		
		try {
			CamelisHttp camelisHttp = new CamelisHttp(Memory.getActiveUser(),
					Memory.getActiveLisService());

			if (params.length == 2) {
				int[] oidArray = new int[params[0].length];
				for (int i = 0; i < params[0].length; i++)
					oidArray[i] = Integer.valueOf(params[0][i]);

				valuedFeatures = camelisHttp.getValuedFeatures(oidArray, params[1]);
			}
		} catch (PortalisException e) {
			logger.warn("Exception raised when creating CamelisHttp object");
			logger.warn(e.getStackTrace().toString());
		}
		
		return valuedFeatures;
	}

	@Override
	protected void onPostExecute(GetValuedFeaturesReponse result) {
		super.onPostExecute(result);
	}
}

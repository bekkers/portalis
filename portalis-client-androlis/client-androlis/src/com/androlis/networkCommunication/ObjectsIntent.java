package com.androlis.networkCommunication;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import android.os.AsyncTask;

import com.androlis.memorizator.Memory;

import fr.irisa.lis.portalis.shared.camelis.http.CamelisHttp;
import fr.irisa.lis.portalis.shared.camelis.reponse.IntentReponse;

public class ObjectsIntent extends AsyncTask<String, Void, IntentReponse> {
	private static final Logger logger = LoggerFactory.getLogger(ObjectsIntent.class.getName());

	@Override
	protected IntentReponse doInBackground(String... params) {
		logger.info("Start ObjectsIntent Task");
	
		CamelisHttp camelisHttp = Memory.getCamelisHttp();
		IntentReponse intentReponse = camelisHttp.intent(params);

		return intentReponse;
	}
	
	@Override
	protected void onPostExecute(IntentReponse intentReponse) {
		super.onPostExecute(intentReponse);
	}
}

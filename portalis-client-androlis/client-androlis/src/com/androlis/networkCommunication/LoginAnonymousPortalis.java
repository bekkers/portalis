package com.androlis.networkCommunication;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import android.os.AsyncTask;
import fr.irisa.lis.portalis.shared.admin.http.AdminHttp;
import fr.irisa.lis.portalis.shared.admin.reponse.LoginReponse;

public class LoginAnonymousPortalis extends AsyncTask<Void, Void, LoginReponse> {
	private static final Logger logger = LoggerFactory.getLogger(LoginAnonymousPortalis.class.getName());

	@Override
	protected LoginReponse doInBackground(Void... params) {
		logger.info("Start LoginAnonymousPortalis Task");
		LoginReponse loginReponse = AdminHttp.anonymousLogin();
		return loginReponse;
	}
	
	@Override
	protected void onPostExecute(LoginReponse result) {
		super.onPostExecute(result);
	}
}

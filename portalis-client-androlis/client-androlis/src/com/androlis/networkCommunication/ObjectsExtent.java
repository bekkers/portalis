package com.androlis.networkCommunication;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import android.os.AsyncTask;

import com.androlis.memorizator.Memory;

import fr.irisa.lis.portalis.shared.camelis.http.CamelisHttp;
import fr.irisa.lis.portalis.shared.camelis.reponse.ExtentReponse;

public class ObjectsExtent extends AsyncTask<Integer, Void, ExtentReponse> {
	private static final Logger logger = LoggerFactory.getLogger(ObjectsExtent.class.getName());

	@Override
	protected ExtentReponse doInBackground(Integer... params) {
		logger.info("Start ObjectsExtent Task");

		String request = Memory.getRequestMemorizator().getState();
		if(request==null || request.length()==0)
			request="all";

		CamelisHttp camelisHttp = Memory.getCamelisHttp();
		ExtentReponse extentReponse = new ExtentReponse();
		
		if(params==null || params.length!=2)
			extentReponse = camelisHttp.extent(request);
		else
			extentReponse = camelisHttp.extent(request, params[0], params[1]);

		return extentReponse;
	}

	@Override
	protected void onPostExecute(ExtentReponse extentReponse) {
		super.onPostExecute(extentReponse);
	}
}

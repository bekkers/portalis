package com.androlis;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.webkit.WebView;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.CheckBox;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;

import com.androlis.component.FlyOutContainer;
import com.androlis.javascriptCommunication.JSMapsLoader;
import com.androlis.list.Doublet;
import com.androlis.list.List_MyCustomAdapter;
import com.androlis.memorizator.Memory;
import com.androlis.networkCommunication.AttributesGetZoom;
import com.androlis.networkCommunication.ObjectsExtent;
import com.androlis.networkCommunication.ObjectsIntent;

import fr.irisa.lis.portalis.shared.camelis.data.LisExtent;
import fr.irisa.lis.portalis.shared.camelis.data.LisIncrement;
import fr.irisa.lis.portalis.shared.camelis.data.LisIncrementSet;
import fr.irisa.lis.portalis.shared.camelis.data.LisIntent;
import fr.irisa.lis.portalis.shared.camelis.data.LisObject;
import fr.irisa.lis.portalis.shared.camelis.data.Property;
import fr.irisa.lis.portalis.shared.camelis.reponse.ExtentReponse;
import fr.irisa.lis.portalis.shared.camelis.reponse.IntentReponse;
import fr.irisa.lis.portalis.shared.camelis.reponse.ZoomReponse;

@SuppressLint("SetJavaScriptEnabled")
public class JSMaps extends Activity{
	private static final Logger logger = LoggerFactory.getLogger(JSMaps.class.getName());
	private WebView webView=null;
	public static ArrayList<MarkerData> markerArray = null;
	FlyOutContainer root;
	private ListView listItems;
	private List_MyCustomAdapter adapter;
	private ListView listObjects;
	private List_MyCustomAdapter adapterObjects;
	private Dialog dialog;
	private ListView listSelectedObjects;
	private List_MyCustomAdapter adapterSelectedObjects;
	private List<LisObject> objectsList;

	private Double[][]positions={};

	LocationManager locationManager;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		logger.info("JSMaps activity start");
		setContentView(R.layout.activity_jsmaps);

		root = (FlyOutContainer) findViewById(R.id.JSMaps_flyout);

		dialog = new Dialog(this);
		dialog.setContentView(R.layout.jsmaps_dialog_objects);
		dialog.setTitle(R.string.options);
		listSelectedObjects = (ListView) dialog.findViewById(R.id.jsmaps_dialog_objects_list);

		((TextView) findViewById(R.id.header_title)).setText("Visu | ");
		((TextView) findViewById(R.id.header_subtitle)).setText(Memory.getCamelisName());
		findViewById(R.id.header_arrow).setVisibility(View.VISIBLE);

		listObjects = (ListView) findViewById(R.id.objects_comp_list_view);
		adapterObjects = new List_MyCustomAdapter(getApplicationContext());
		listObjects.setAdapter(adapterObjects);
		addMarkers();

		//Create a WebView which is able to display JavaScript
		webView = (WebView)this.findViewById(R.id.jsmaps__webView); 
		webView.getSettings().setJavaScriptEnabled(true);
		webView.loadUrl("file:///android_asset/www/jsmaps.html");
		webView.addJavascriptInterface(new JSMapsLoader(this), "mapData");

		//KMLManager
		//Add KMLs to a list
		//TODO : présentation des KMLS. Doit être fait grâce aux getFeatures
		//Présents dans la mémoire
		listItems = (ListView) findViewById(R.id.kml_manager_list);
		adapter= new List_MyCustomAdapter(this);
		adapter.addItem("massifs arbustifs");
		adapter.addItem("pelouses");
		adapter.addItem("surfaces minerales");
		adapter.addItem("massifs floraux");
		adapter.addItem("rosiers");
		listItems.setAdapter(adapter);
		//Listener kmls list to show or hide them when they are clicked
		((ListView)findViewById(R.id.kml_manager_list)).setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				int pos = 0; 
				String name = adapter.getItem(position).getItem();

				Doublet[] names = Memory.getKmlsNames();

				//Search the kml clicked in the Memory array
				while(pos<name.length() && name != names[pos].arg0)
					pos++;

				//If the boolean was true in the Memory array, the kml was hidden and must be shown
				if((Boolean) names[pos].arg1) {
					parent.getChildAt(position).setBackgroundColor(getResources().getColor(R.color.blue_start));
					webView.loadUrl("javascript:displayKML("+(pos)+")");
					names[pos].arg1 = false;
				} else {
					parent.getChildAt(position).setBackgroundColor(getResources().getColor(R.color.white));
					webView.loadUrl("javascript:hideKML("+(pos)+")");
					names[pos].arg1 = true;
				}
			}
		});

		//Check the checkbox if the user click on the ListItem (not only on the checkbox)
		listObjects.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				CheckBox checkbox = (CheckBox) view.findViewById(R.id.list_checkable_item_checkbox);
				checkbox.setChecked(!checkbox.isChecked());
			}
		});

		//Contextual Menu Button Listener
		ImageButton options = (ImageButton) findViewById(R.id.objects_comp_configuration_button);
		options.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				v.showContextMenu();
			}
		});
		//Registering context menu for the listview
		registerForContextMenu(options);

		//Header
		//Listeners on header icons to show or hide the sliding menu
		findViewById(R.id.header_logo).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				root.toggleMenu();
			}
		});

		findViewById(R.id.header_arrow).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				root.toggleMenu();
				
			}
		});

		locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
	}

	/**
	 * Add markers from Objects which have à gps Property, from the current request
	 */
	/*Début de fonction avec le getFeatures
	private void addMarkers() {
		//Retrieve all the objects which have a gps position thanks to an Extent
		if(Memory.getRequestMemorizator().size()==0)
			Memory.getRequestMemorizator().add("gps ?");
		else
			Memory.getRequestMemorizator().add("("+Memory.getRequestMemorizator().getState()+")"+"and gps ?");

		ObjectsExtent objectsExtent = new ObjectsExtent();
		ExtentReponse extentReponse=null;

		try {
			extentReponse=objectsExtent.execute().get();
		} catch (InterruptedException e) {
			e.printStackTrace();
		} catch (ExecutionException e) {
			e.printStackTrace();
		}
		LisObject[] lisObjects = extentReponse.getExtent().toArray();

		//Add them to the map
		addMarkers(lisObjects);

		Memory.getRequestMemorizator().clean();
		listObjects.setAdapter(adapterObjects);
	}
	 */

	private void addMarkers() {
		objectsList = new ArrayList<LisObject>();

		//Retrieve all the gps positions in properties thanks to "gps ?"
		AttributesGetZoom attributesGetZoom = new AttributesGetZoom();
		ZoomReponse zoomReponse=null;
		String[] params = {Memory.getRequestMemorizator().getState(), "gps ?"};
		try {
			zoomReponse = attributesGetZoom.execute(params).get();
		} catch (InterruptedException e) {
			e.printStackTrace();
		} catch (ExecutionException e) {
			e.printStackTrace();
		}

		LisIncrementSet receivedIncrs = zoomReponse.getIncrements();
		LisIncrement[] increments = receivedIncrs.toArray();

		markerArray = new ArrayList<JSMaps.MarkerData>();

		//Retrieve all the Objects located at the gps position and add them to the map
		for(LisIncrement lisIncrement : increments) {
			addMarkers(lisIncrement);
		}

		Memory.setArrayObjects(objectsList.toArray(new LisObject[objectsList.size()]));
		Memory.getRequestMemorizator().clean();
		listObjects.setAdapter(adapterObjects);
	}


	/**
	 * Add Marker for Objects of LisObjects
	 */
	/*Début de fonction avec le getFeatures
	private void addMarkers(LisObject[] lisObjects) {
		String[] oidArray = new String[lisObjects.length];

		//Create a string Array from int Array
		for(int i=0;i<lisObjects.length;i++)
			oidArray[i]=String.valueOf(lisObjects[i].getOid());

		//Use getValuedFeatures to fin gps position of each object
		String[][] params={oidArray,{"gps"}};
		GetFeatures features = new GetFeatures();
		GetValuedFeaturesReponse valuedFeaturesReponse = null;
		try {
			valuedFeaturesReponse=features.execute(params).get();
		} catch (InterruptedException e) {
			e.printStackTrace();
		} catch (ExecutionException e) {
			e.printStackTrace();
		}

		//Add each object to the map and to the objects list
		for(int i=0;i<lisObjects.length;i++){
			adapterObjects.addCheckableItem(lisObjects[i].getName());

			String position = valuedFeaturesReponse.getValue(lisObjects[i].getOid(), "gps");
			Double[] pos = parseLocation(position);
			markerArray.add(new MarkerData(pos[0], pos[1], lisObjects[i].getName()));
		}
	}
	 */

	private void addMarkers(LisIncrement lisIncrement) {
		//Create the new request to find all the Objects to the position contains in lisIncrement
		String request = Memory.getRequestMemorizator().getState();
		if(!(request.length()==0)) request = "("+request+") and ";
		request += lisIncrement.getName();
		Memory.getRequestMemorizator().add(request);

		//Retrieve all the Objects located to the gps position thanks to the request
		ObjectsExtent listExtent = new ObjectsExtent();
		ExtentReponse extentReponse=null;
		try {
			extentReponse = listExtent.execute().get();
		} catch (InterruptedException e) {
			e.printStackTrace();
		} catch (ExecutionException e) {
			e.printStackTrace();
		}
		LisExtent receivedExtent = extentReponse.getExtent();
		LisObject[] arrayExtent = receivedExtent.toArray();

		Double[] pos = parseLocation(lisIncrement.getName());
		for(LisObject objectExtent : arrayExtent){
			markerArray.add(new MarkerData(pos[0], pos[1], objectExtent.getName()));
			adapterObjects.addCheckableItem(objectExtent.getName());
			objectsList.add(objectExtent);
		}

		//Undo the last request to be able to use it for the next position
		Memory.getRequestMemorizator().undo();
	}

	private Double[] parseLocation(String name) {
		String delims = "[ ;,\"]+";
		String[] tokens = name.split(delims);
		Double[] result = {Double.parseDouble(tokens[3]), Double.parseDouble(tokens[2])};
		return result;
	}

	public class MarkerData{
		public double lat, lon;
		public String title;

		public MarkerData(double pos, double pos2, String title) {
			this.lon=pos2;
			this.lat=pos;
			this.title=title;
		}

	}

	/***
	 * Contextual Object Menu
	 */

	/** This will be invoked when an item in the listview is long pressed
	 * Make the contextual menu appears */
	@Override
	public void onCreateContextMenu(ContextMenu menu, View v, ContextMenuInfo menuInfo) {
		super.onCreateContextMenu(menu, v, menuInfo);    	
		getMenuInflater().inflate(R.menu.contextual_menu_jsmaps_objects_comp , menu);

		menu.setHeaderTitle(getString(R.string.options));
	}


	/** This will be invoked when a menu item is selected
	 * Use to assign actions to the items from the contextual menu */
	@Override
	public boolean onContextItemSelected(MenuItem item) {

		int itemId = item.getItemId();
		if (itemId == R.id.contextual_menu_jsmaps_objects_comp_display_selection) {
			//Retrieve Oid of checked items
			ArrayList<Integer> itemChecked=adapterObjects.getChecked();
			if(itemChecked.size()!=0){
				adapterSelectedObjects = new List_MyCustomAdapter(getApplicationContext());
				for(Integer position : itemChecked)
					adapterSelectedObjects.addItem(Memory.getArrayObjects()[position].getName());

				listSelectedObjects.setAdapter(adapterSelectedObjects);
				dialog.show();
			}

		} else if (itemId == R.id.contextual_menu_jsmaps_objects_comp_uncheck) {
			adapterObjects.unCheck();
			listObjects.invalidate();

		} else if (itemId == R.id.contextual_menu_jsmaps_objects_comp_selection) {
			//Retrieve Oid of checked items
			ArrayList<Integer> itemChecked=adapterObjects.getChecked();
			if(itemChecked.size()!=0){
				//Positions will contain LatLng for selected points and the beginning and ending point
				positions= new Double[itemChecked.size()+2][];

				Location currentLocation = getLastBestLocation();
				Double[] currentLatLng = {currentLocation.getLatitude(), currentLocation.getLongitude()};
				positions[0]=currentLatLng;

				//Add selected points location to the positions array
				for(int i=0; i<itemChecked.size();i++) {
					Integer position = itemChecked.get(i);

					//Retrieve the gps location of a object from the oid
					ObjectsIntent objectsIntent = new ObjectsIntent();
					IntentReponse intentReponse=null;
					try {
						intentReponse = objectsIntent.execute(String.valueOf(Memory.getArrayObjects()[position].getOid())).get();
					} catch (InterruptedException e) {
						e.printStackTrace();
					} catch (ExecutionException e) {
						e.printStackTrace();
					}
					LisIntent receivedIntent = intentReponse.getIntent();
					Property[] arrayIntent = receivedIntent.toArray();

					int gpsPos=0;
					//Search if the object has a gps position
					while(gpsPos<arrayIntent.length && !arrayIntent[gpsPos].getName().startsWith("gps is"))
						gpsPos++;

					//Add the gps position of the object, if it does not have one, add the previous one
					if(gpsPos<arrayIntent.length)
						positions[i+1]=parseLocation(arrayIntent[gpsPos].getName());
					else
						positions[i+1]=positions[i];
				}
				//Add the last point
				positions[itemChecked.size()+1]=currentLatLng;

				//Call the javascript function which optimize the path
				webView.loadUrl("javascript:calcRoute("+(JSMapsLoader.sendPositions(positions))+")");
			}
		}
		return true;
	}

	//Call googleMaps activity with the optimized path
	public void googleMapsIntent(){
		runOnUiThread(new Runnable() {
			public void run() {
				//clean checked items in the objects list
				adapterObjects.unCheck();
				listObjects.invalidate();

				int[] order=Memory.getWaypointOrder();

				//Add the first point
				StringBuffer sBuffer = new StringBuffer();
				sBuffer.append("http://maps.google.com/maps?saddr=");
				sBuffer.append(positions[0][0]+","+positions[0][1]+"&daddr=");

				//Add point which are optimized by javascript call (from:position[1] to:position[position.length-1])
				//We want the first optimized point, it have the rank 0 in the order array
				int currentRank=0;
				while(currentRank<order.length){
					int iter=0;
					while(iter<order.length && order[iter]!=currentRank)
						iter++;

					if(order[iter]==currentRank)
						sBuffer.append(positions[iter+1][0]+","+positions[iter+1][1]+"+to:+");

					currentRank++;
				}

				//Add the last point
				sBuffer.append(positions[positions.length-1][0]+","+positions[positions.length-1][1]);

				System.out.println(sBuffer.toString());

				//Call google maps activity
				Intent intent = new Intent(android.content.Intent.ACTION_VIEW, Uri.parse(sBuffer.toString()));
				startActivity(intent);	
			}
		});
	}

	/**
	 * @return the last know best location
	 */
	private Location getLastBestLocation() {
		Location locationGPS = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
		Location locationNet = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);

		long GPSLocationTime = 0;
		if (null != locationGPS)
			GPSLocationTime = locationGPS.getTime();

		long NetLocationTime = 0;

		if (null != locationNet) 
			NetLocationTime = locationNet.getTime();

		if (locationGPS !=null && 0 < GPSLocationTime - NetLocationTime )
			return locationGPS;

		else if (locationNet!=null)
			return locationNet;

		else 
			return new Location("default");
	}
}
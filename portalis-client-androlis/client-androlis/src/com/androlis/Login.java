package com.androlis;

import java.util.concurrent.ExecutionException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.androlis.memorizator.Memory;
import com.androlis.networkCommunication.LoginAnonymousPortalis;
import com.androlis.networkCommunication.LoginPortalis;

import fr.irisa.lis.portalis.shared.admin.data.ActiveUser;
import fr.irisa.lis.portalis.shared.admin.data.PortalisService;
import fr.irisa.lis.portalis.shared.admin.reponse.LoginReponse;

public class Login extends Activity {
	private static final Logger logger = LoggerFactory.getLogger(Login.class);
	private Button connectButton;
	private TextView error;
	private String userId;
	private String userPwd;


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		logger.info("Login activity start");

		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_login);
		
		((TextView) findViewById(R.id.header_title)).setText("Login");
		((TextView) findViewById(R.id.header_subtitle)).setVisibility(View.GONE);

		
		// load default values for user settings if this has never been done before
		PreferenceManager.setDefaultValues(this, R.xml.preferences, false);
		
		SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(this);
		
		// init extentPageSize in Memory from preferences
		int extentPageSize = Integer.parseInt(sharedPrefs.getString("pref_objs_per_page", ""));
		Memory.setExtentPageSize(extentPageSize);
		logger.debug("extentPageSize is " + Memory.getExtentPageSize());
		
		// initialize portalis service
		String serverName = sharedPrefs.getString("pref_server_name", "");
		String serverPort = sharedPrefs.getString("pref_server_port", "");
		
		// PortalisService.init() makes a network lookup
		// Since 3.0, Android forbids network operations in a main thread
		// so we have to wrap this call in an AsyncTask, or else Android will raise an exception
		// See https://developer.android.com/reference/android/os/StrictMode.ThreadPolicy.Builder.html#penaltyDeathOnNetwork()
		new AsyncTask<String, Void, Void>() {
			@Override
			protected Void doInBackground(String... params) {
				String name = params[0];
				int port = Integer.parseInt(params[1]);
				PortalisService.getInstance().init(name, port, "portalis");
				return null;
			}
		}.execute(serverName, serverPort);
		
		
		//Check if the user can access to camelis on click on connectButton
		error = (TextView) findViewById(R.id.login_error); 
		connectButton = (Button) findViewById(R.id.login_connectButton);
		connectButton.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				//Hides the soft keyboard when connect button is clicked
				InputMethodManager inputManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
				inputManager.toggleSoftInput(0, 0);

				EditText editTextId=(EditText) findViewById(R.id.login_userId); 
				userId=editTextId.getText().toString();

				EditText editTextPwd=(EditText) findViewById(R.id.login_passwd); 
				userPwd=editTextPwd.getText().toString();

				logger.info("Connection tried with userID :"+userId+", pwd :"+userPwd);

				//Ask portalis if informations entered by user refer to an allowed account
				LoginPortalis loginTask = new LoginPortalis();
				LoginReponse loginReponse = null;
				try {
					String [] att = {userId, userPwd};
					loginReponse = loginTask.execute(att).get();
				} catch (InterruptedException e) {
					e.printStackTrace();
				} catch (ExecutionException e) {
					e.printStackTrace();
				}


				if(loginReponse.isOk()) {
					logger.info("Connection succeeded");

					error.setVisibility(View.INVISIBLE);

					ActiveUser au = loginReponse.getActiveUser();
					Memory.setActiveUser(au);

					Intent intent = new Intent(Login.this, Services.class);
					startActivity(intent);
				} 

				else {
					logger.info("Connection failed");
					error.setVisibility(View.VISIBLE);
					error.setText(loginReponse.getMessagesAsString());
				}
			}
		});
		
		findViewById(R.id.login_connectAnonymousButton).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				logger.info("Login quick Access");

				//Hides the soft keyboard when quick access button is clicked
				InputMethodManager inputManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
				inputManager.toggleSoftInput(0, 0);
				
				//Create an anonymous user on portalis
				LoginAnonymousPortalis loginTask = new LoginAnonymousPortalis();
				LoginReponse loginReponse = null;
				try {
					loginReponse = loginTask.execute().get();
				} catch (InterruptedException e) {
					e.printStackTrace();
				} catch (ExecutionException e) {
					e.printStackTrace();
				}
				
				if (loginReponse.isOk()) {
					ActiveUser au = loginReponse.getActiveUser();
					Memory.setActiveUser(au);
				
					Intent intent = new Intent(Login.this, Services.class);
					startActivity(intent);
				} else {
					logger.info("Connection failed");
					error.setVisibility(View.VISIBLE);
					error.setText(loginReponse.getMessagesAsString());	
				}
			}
		});
		
		findViewById(R.id.login_settings).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				logger.info("Login: settings button clicked");

				//Toast.makeText(Login.this, "Should show some settings page", Toast.LENGTH_SHORT).show();

				// switch to the preferences activity
				Intent intent = new Intent(Login.this, Settings.class);
				startActivity(intent);
			}
		});
	}
}

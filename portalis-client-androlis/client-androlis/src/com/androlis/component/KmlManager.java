package com.androlis.component;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.RelativeLayout;

import com.androlis.R;

public class KmlManager extends RelativeLayout  {
	private enum State {SHOW, HIDE};
	private State state = State.HIDE;


	public KmlManager(Context context, AttributeSet attrs) {
		super(context, attrs);
		LayoutInflater layoutInflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		layoutInflater.inflate(R.layout.comp_kml_manager, this);


		findViewById(R.id.kml_manager_header).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				changeState();
			}
		});

		findViewById(R.id.kml_manager_expand).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				changeState();
			}
		});

		findViewById(R.id.kml_manager_reduce).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				changeState();
			}
		});
	}

	public void changeState() {
		switch(state){
		case  SHOW:
			findViewById(R.id.kml_manager_reduce).setVisibility(View.GONE);
			findViewById(R.id.kml_manager_list).setVisibility(View.GONE);
			findViewById(R.id.kml_manager_expand).setVisibility(View.VISIBLE);
			state=State.HIDE;
			break;

		case HIDE:
			findViewById(R.id.kml_manager_reduce).setVisibility(View.VISIBLE);
			findViewById(R.id.kml_manager_list).setVisibility(View.VISIBLE);
			findViewById(R.id.kml_manager_expand).setVisibility(View.GONE);
			state=State.SHOW;
			break;
		}
	}
}

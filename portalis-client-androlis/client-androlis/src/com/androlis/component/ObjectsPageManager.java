package com.androlis.component;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.androlis.R;

public class ObjectsPageManager extends RelativeLayout  {
	private int currentPage = 0;
	private int maxPage = 0;
	private int shownPage = 1;
	private int shownMaxPage = 1;
	
	public ObjectsPageManager(Context context, AttributeSet attrs) {
		super(context, attrs);
		LayoutInflater layoutInflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		layoutInflater.inflate(R.layout.comp_objects_page_manager, this);
	}

	public void beginning() {
		currentPage = 0;
		shownPage = 1;
	}
	public void previous() {
		if (currentPage > 0)
			currentPage--;
		shownPage = currentPage + 1;
	}

	public void next() {
		if (currentPage < maxPage)
			currentPage++;
		shownPage = currentPage + 1;
	}

	public void ending() {
		currentPage = maxPage;
		shownPage = currentPage + 1;
	}
	
	public void setMaxPage(int maxPage) {
		this.maxPage = maxPage;
		shownMaxPage = maxPage + 1;
	}
	
	public void updateTextView() {
		((TextView) findViewById(R.id.page_manager_text_view))
			.setText(shownPage + "/" + shownMaxPage);
	}
	
	public int getCurrentPage() {
		return currentPage;
	}
}

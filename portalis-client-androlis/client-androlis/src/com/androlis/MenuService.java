package com.androlis;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

import com.androlis.component.Request;
import com.androlis.memorizator.Memory;

public class MenuService extends Activity {
	private static final Logger logger = LoggerFactory.getLogger(MenuService.class);
	private Button object;
	private Button attribute;
	private Button wordCloud;
	private Button jsmaps;
	private Button timeline;
	private Button pieChart;
	private Button test;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		logger.info("MenuService activity start");

		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_menu_service);

		((TextView) findViewById(R.id.header_title)).setText("Menu | ");
		((TextView) findViewById(R.id.header_subtitle)).setText(Memory.getCamelisName());

		((Request) findViewById(R.id.request)).hideButtons();
		//click listener from the compound component
		//(need to be in an Activity)
		findViewById(R.id.request_text).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(MenuService.this, RequestEdition.class);
				startActivity(intent);
			}
		});

		findViewById(R.id.request_reload).setVisibility(View.GONE);

		//Buttons listeners
		object = (Button) findViewById(R.id.menu_service_objet);
		object.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(MenuService.this, Objects.class);
				startActivity(intent);
			}
		});

		attribute= (Button) findViewById(R.id.menu_service_attribute);
		attribute.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(MenuService.this, Attributes.class);
				startActivity(intent);
			}
		});

		wordCloud = (Button) findViewById(R.id.menu_service_word_cloud);
		wordCloud.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(MenuService.this, WordCloud.class);
				startActivity(intent);		
			}
		});

		jsmaps = (Button) findViewById(R.id.menu_service_jsmaps);
		jsmaps.setVisibility(View.VISIBLE);
		jsmaps.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Memory.initKmls();
				Intent intent = new Intent(MenuService.this, JSMaps.class);
				startActivity(intent);	
			}
		});

		timeline = (Button) findViewById(R.id.menu_service_timeline);
		timeline.setVisibility(View.GONE);
		//		timeline.setOnClickListener(new OnClickListener() {
		//			@Override
		//			public void onClick(View v) {
		//				Intent intent = new Intent(MenuService.this, Timeline.class);
		//				startActivity(intent);					
		//			}
		//		});

		pieChart = (Button) findViewById(R.id.menu_service_pie_chart);
		pieChart.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(MenuService.this, PieChart.class);
				startActivity(intent);					
			}
		});

		test = (Button) findViewById(R.id.menu_service_test);
		test.setVisibility(View.GONE);
		//		test.setOnClickListener(new OnClickListener() {
		//			
		//			@Override
		//			public void onClick(View v) {
		//				Intent intent = new Intent(MenuService.this, MainView.class);
		//				startActivity(intent);
		//			}
		//		});
	}

	@Override
	protected void onResume() {
		super.onResume();
		((Request) findViewById(R.id.request)).reload();
	}
}

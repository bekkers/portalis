package com.androlis.spinner;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;


public class Spinner_MyCustomSpinner {
	private static final Logger logger = LoggerFactory.getLogger(Spinner_MyCustomSpinner.class.getName());

	public Spinner_MyCustomSpinner() {}

	public void initSpinner(final Spinner spinner, final Context context, final int pos, final String[] items) {
		logger.info("function initSpinner called.");

		spinner.setVisibility(View.VISIBLE);

		//Create an adapter which is able to hide spinner items
		ArrayAdapter<String> adapter = new ArrayAdapter<String>(context,android.R.layout.simple_dropdown_item_1line, items) {
			@Override
			public View getDropDownView(int position, View convertView, ViewGroup parent)
			{
				View v = null;

				//Hide the item if it is in a position to hide
				if (position == pos) {
					TextView tv = new TextView(getContext());
					tv.setHeight(0);
					tv.setVisibility(View.GONE);
					v = tv;
				}
				else {
					v = super.getDropDownView(position, null, parent);
				}

				// Hide scroll bar because it appears sometimes unnecessarily
				parent.setVerticalScrollBarEnabled(false);
				return v;
			}
		};
		
		spinner.setAdapter(adapter);

		//Set the item show 
		spinner.setSelection(pos);

		spinner.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
				if(position!=pos)
					initSpinner(spinner, context, position, items);
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
			}

		});
	}
}


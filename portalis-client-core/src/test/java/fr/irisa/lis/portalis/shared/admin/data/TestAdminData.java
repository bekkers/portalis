package fr.irisa.lis.portalis.shared.admin.data;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.net.InetAddress;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Element;

import fr.irisa.lis.portalis.commons.DOMUtil;
import fr.irisa.lis.portalis.commons.core.CommonsUtil;
import fr.irisa.lis.portalis.shared.admin.ClientConstants;
import fr.irisa.lis.portalis.shared.admin.RightValue;
import fr.irisa.lis.portalis.shared.admin.Util;
import fr.irisa.lis.portalis.test.CoreTestConstants;


public class TestAdminData {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(TestAdminData.class.getName());
	
	

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		final String host = InetAddress.getLocalHost().getCanonicalHostName();
		PortalisService.getInstance().init(host, 8080, "portalis");
		Util.clientInitLogPropertiesFile();
		LOGGER.info(""
				+ "\n            ----------------------------------------------------"
				+ "\n            |            @BeforeClass : TestPortalisCore        |"
				+ "\n            ----------------------------------------------------\n");
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		LOGGER.info(""
				+ "\n            ----------------------------------------------------"
				+ "\n            |            @AfterClass : TestPortalisCore         |"
				+ "\n            ----------------------------------------------------\n");
	}

	@Test
	public void testUIPreference() {
		try {
			UIPreference in = new UIPreference();
			in.setHasPictures(true);
			String[][] columns = {{"machin",null}, {"truc", "http://machin.irisa.fr/essai"}, {"blanc", "http://machin.irisa.fr/blanc"}};
			in.addExtraColumn(columns);
			in.setEntryShown(40);
			in.setMinimumSupport(32);
			String[] hiddenAttributes = {"toto","INGT ?"};
			in.addHiddenAttribute(hiddenAttributes);
			assertNotNull(in);
			Element elem = ClientConstants.PORTALIS_DATA_XML_WRITER.visit(in);
			assertNotNull(elem);
			
			UIPreference out = ClientConstants.ADMIN_DATA_XML_READER.getUIPreference(elem);
			assertTrue(
					"les deux objets UIPreference doivent être égaux\nin = "
							+ in + "\nout = " + out, in.equals(out));
			
		} catch (Exception e) {
			fail("Exception rencontrée lors du test : " + e.getMessage()
					+ "\nStack :\n" + CommonsUtil.stack2string(e));
		}
	}
	
	@Test
	public void testSparqlApplication() {
		try {
			SparqlApplication in = CoreTestConstants.SPARQL_APPLICATION;
			Element elem = ClientConstants.PORTALIS_DATA_XML_WRITER.visit(in);
			assertNotNull(elem);
			
			SparqlApplication out = ClientConstants.ADMIN_DATA_XML_READER.getSparqlApplication(elem);
			assertTrue(
					"les deux objets SparqlApplication doivent être égaux\nin = "
							+ in + "\nout = " + out, in.equals(out));
			
		} catch (Exception e) {
			fail("Exception rencontrée lors du test : " + e.getMessage()
					+ "\nStack :\n" + CommonsUtil.stack2string(e));
		}
	}
	
	@Test
	public void testQuery() {
		try {
			Query in = CoreTestConstants.QUERY_1;
			Element elem = ClientConstants.PORTALIS_DATA_XML_WRITER.visit(in);
			assertNotNull(elem);
			
			Query out = ClientConstants.ADMIN_DATA_XML_READER.getQuery(elem);
			assertTrue(
					"les deux objets Query doivent être égaux\nin = "
							+ in + "\nout = " + out, in.equals(out));
			
		} catch (Exception e) {
			fail("Exception rencontrée lors du test : " + e.getMessage()
					+ "\nStack :\n" + CommonsUtil.stack2string(e));
		}
	}
	
	@Test
	public void testSparqlService() {
		try {
			SparqlService in = CoreTestConstants.SPARQL_SERVICE;
			Element elem = ClientConstants.PORTALIS_DATA_XML_WRITER.visit(in);
			assertNotNull(elem);
			
			SparqlService out = ClientConstants.ADMIN_DATA_XML_READER.getSparqlService(elem);
			assertTrue(
					"les deux objets SparqlService doivent être égaux\nin = "
							+ in + "\nout = " + out, in.equals(out));
			
		} catch (Exception e) {
			fail("Exception rencontrée lors du test : " + e.getMessage()
					+ "\nStack :\n" + CommonsUtil.stack2string(e));
		}
	}
	
	@Test
	public void testLoginResult() {
		try {
			LoginResult in = new LoginResult(CoreTestConstants.SUPER_USER, LoginErr.ok);
			assertNotNull(in);
			Element elem = ClientConstants.PORTALIS_DATA_XML_WRITER.visit(in);
			assertNotNull(elem);

			UserDataContent out = ClientConstants.ADMIN_DATA_XML_READER.getLoginResult(elem);
			assertTrue(
					"les deux objets LoginResult doivent être égaux\nin = "
							+ in + "\nout = " + out, in.equals(out));
			
			LoginResult in1 = new LoginResult(CoreTestConstants.SUPER_USER, LoginErr.unknown);
			assertNotNull(in1);
			Element elem1 = ClientConstants.PORTALIS_DATA_XML_WRITER.visit(in1);
			assertNotNull(elem1);

			UserDataContent out1 = ClientConstants.ADMIN_DATA_XML_READER.getLoginResult(elem1);
			assertTrue(
					"les deux objets LoginResult doivent être égaux\nin1 = "
							+ in1 + "\nout1 = " + out1, in1.equals(out1));
			
			LoginResult in2 = new LoginResult(CoreTestConstants.STANDARD_USER, LoginErr.ok);
			assertNotNull(in2);
			Element elem2 = ClientConstants.PORTALIS_DATA_XML_WRITER.visit(in2);
			assertNotNull(elem2);

			UserDataContent out2 = ClientConstants.ADMIN_DATA_XML_READER.getLoginResult(elem2);
			assertTrue(
					"les deux objets LoginResult doivent être égaux\nin2 = "
							+ in2 + "\nout2 = " + out2, in2.equals(out2));
			
			LoginResult in3 = new LoginResult(new AnonymousUser(), LoginErr.ok);
			assertNotNull(in3);
			Element elem3 = ClientConstants.PORTALIS_DATA_XML_WRITER.visit(in3);
			assertNotNull(elem3);

			UserDataContent out3 = ClientConstants.ADMIN_DATA_XML_READER.getLoginResult(elem3);
			assertTrue(
					"les deux objets LoginResult doivent être égaux\nin3 = "
							+ in3 + "\nout3 = " + out3, in3.equals(out3));
			
		} catch (Exception e) {
			fail("Exception rencontrée lors du test : " + e.getMessage()
					+ "\nStack :\n" + CommonsUtil.stack2string(e));
		}
	}

	@Test
	public void testCamelisServiceCore1() {
		try {
			LOGGER.info("--------------> @Test : testServiceCore1\n");
			CamelisServiceCore in = new CamelisServiceCore("appli", "service", RightValue.EDITOR);
			LOGGER.info("\nin  = " + in);
			CamelisServiceCore in1 = new CamelisServiceCore("appli1", "service", RightValue.EDITOR);
			CamelisServiceCore in2 = new CamelisServiceCore("appli", "service1", RightValue.EDITOR);
			CamelisServiceCore in3 = new CamelisServiceCore("appli", "service", RightValue.READER);
			assertTrue(
					"les deux objets ServiceCore doivent être différents\nin = "
							+ in + "\nin1 = " + in1, !in.equals(in1));
			assertTrue(
					"les deux objets ServiceCore doivent être différents\nin = "
							+ in + "\nin3 = " + in3, !in.equals(in3));
			assertTrue(
					"les deux objets ServiceCore doivent être différents\nin = "
							+ in2 + "\nin2 = " + in2, !in.equals(in2));
			assertTrue(
					"les deux objets ServiceCore doivent être différents\nin1 = "
							+ in1 + "\nin2 = " + in2, !in1.equals(in2));

			Element elem = ClientConstants.PORTALIS_DATA_XML_WRITER.visit(in);
			assertNotNull(elem);

			LisServiceCore out = ClientConstants.ADMIN_DATA_XML_READER.getServiceCore(elem);
			LOGGER.info("\nout = " + out);

			assertTrue("les deux objets ServiceCore doivent être égaux\nin = "
					+ in + "\nout = " + out, in.equals(out));

		} catch (Exception e) {
			fail("Exception rencontrée lors du test : " + e.getMessage()
					+ "\nStack :\n" + CommonsUtil.stack2string(e));
		}
	}

	@Test
	public void testCamelisServiceCore2() {
		try {
			LOGGER.info("--------------> @Test : testServiceCore2\n");
			CamelisServiceCore in = new CamelisServiceCore("appli", "service", RightValue.READER);
			LOGGER.info("\nin  = " + in);
			CamelisServiceCore in1 = new CamelisServiceCore("appli", "service", RightValue.READER);
			CamelisServiceCore in2 = new CamelisServiceCore("appli1", "service", RightValue.READER);
			CamelisServiceCore in3 = new CamelisServiceCore("appli", "service1", RightValue.READER);
			assertTrue("les deux objets ServiceCore doivent être égaux\nin = "
					+ in + "\nin1 = " + in1, in.equals(in1));
			assertTrue("les deux objets ServiceCore doivent être égaux\nin1 = "
					+ in1 + "\nin = " + in, in1.equals(in));
			assertTrue("les deux objets ServiceCore doivent être égaux\nin1 = "
					+ in1 + "\nin1 = " + in1, in1.equals(in1));
			assertTrue(
					"les deux objets ServiceCore doivent être différents\nin3 = "
							+ in3 + "\nin1 = " + in1, !in3.equals(in1));
			assertTrue(
					"les deux objets ServiceCore doivent être différents\nin1 = "
							+ in1 + "\nin3 = " + in3, !in1.equals(in3));
			assertTrue(
					"les deux objets ServiceCore doivent être différents\nin2 = "
							+ in2 + "\nin1 = " + in1, !in2.equals(in1));
			assertTrue(
					"les deux objets ServiceCore doivent être différents\nin1 = "
							+ in1 + "\nin2 = " + in2, !in1.equals(in2));

			List<LisServiceCore> list1 = new ArrayList<LisServiceCore>();
			list1.add(in3);
			list1.add(in);
			list1.add(in1);
			List<LisServiceCore> list2 = new ArrayList<LisServiceCore>();
			list2.add(in);
			list2.add(in3);
			assertTrue("les deux objets HashSet doivent être égaux",
					new HashSet<LisServiceCore>(list1)
							.equals(new HashSet<LisServiceCore>(list2)));
			list2.add(in3);
			assertTrue("les deux objets HashSet doivent être égaux",
					new HashSet<LisServiceCore>(list1)
							.equals(new HashSet<LisServiceCore>(list2)));
			list2.add(in2);
			assertTrue("les deux objets HashSet doivent être différents",
					!new HashSet<LisServiceCore>(list1)
							.equals(new HashSet<LisServiceCore>(list2)));

		} catch (Exception e) {
			fail("Exception rencontrée lors du test : " + e.getMessage()
					+ "\nStack :\n" + CommonsUtil.stack2string(e));
		}
	}
	

	@Test
	public void testAnonymousUser() {
		try {
			LOGGER.info("--------------> @Test : testAnonymousUser\n");
			AnonymousUser in = new AnonymousUser();

			Element elem = ClientConstants.PORTALIS_DATA_XML_WRITER.visit(in);
			assertNotNull(elem);
			LOGGER.debug(DOMUtil.prettyXmlString(elem));

			AnonymousUser out = ClientConstants.ADMIN_DATA_XML_READER.getAnonymousUser(elem);
			assertTrue(out!=null);
			assertTrue("in et ou doivent être égaux\nin="+in+"ou=\n"+out,in.equals(out));
		
		} catch (Exception e) {
			fail("Exception rencontrée lors du test : " + e.getMessage()
					+ "\nStack :\n" + CommonsUtil.stack2string(e));
		}
	}

	@Test
	public void testSuperUser() {
		try {
			LOGGER.info("--------------> @Test : testSuperUser\n");
			SuperUser in = CoreTestConstants.SUPER_USER;

			Element elem = ClientConstants.PORTALIS_DATA_XML_WRITER.visit(in);
			assertNotNull(elem);
			LOGGER.debug(DOMUtil.prettyXmlString(elem));

			PasswordUser out = ClientConstants.ADMIN_DATA_XML_READER.getSuperUser(elem);
			assertTrue(out!=null);
			assertTrue("in et ou doivent être égaux\nin="+in+"ou=\n"+out,in.equals(out));
		
		} catch (Exception e) {
			fail("Exception rencontrée lors du test : " + e.getMessage()
					+ "\nStack :\n" + CommonsUtil.stack2string(e));
		}
	}


	@Test
	public void testStandardUser() {
		try {
			LOGGER.info("--------------> @Test : testStandardUser\n");
			StandardUser in = new StandardUser(CoreTestConstants.MAIL_YVES, 
					CoreTestConstants.PSEUDO_YVES, CoreTestConstants.PASSWORD_YVES);

			StandardUser in2 = new StandardUser(CoreTestConstants.MAIL_BENJAMIN, 
					CoreTestConstants.PSEUDO_BENJAMIN, CoreTestConstants.PASSWORD_BENJAMIN);
			assertTrue(
					"les deux objets UserCore doivent être différents\nin = "
							+ in + "\nin2 = " + in2, !in.equals(in2));
			assertTrue(
					"les deux objets UserCore doivent être différents\nin = "
							+ in + "\nin2 = " + in2, !in2.equals(in));


			StandardUser in4 = new StandardUser(CoreTestConstants.MAIL_YVES, 
					CoreTestConstants.PSEUDO_YVES, CoreTestConstants.PASSWORD_YVES);
			assertTrue("les deux objets UserCore doivent être égaux\nin = "
					+ in + "\nin4 = " + in4, in.equals(in4));
			assertTrue("les deux objets UserCore doivent être égaux\nin = "
					+ in + "\nin4 = " + in4, in4.equals(in));

			Element elem = ClientConstants.PORTALIS_DATA_XML_WRITER.visit(in);
			assertNotNull(elem);

			StandardUser out = ClientConstants.ADMIN_DATA_XML_READER.getStandardUser(elem);
			assertTrue("les deux objets UserCore doivent être égaux\nin = "
					+ in + "\nout = " + out, in.equals(out));

		} catch (Exception e) {
			fail("Exception rencontrée lors du test : " + e.getMessage()
					+ "\nStack :\n" + CommonsUtil.stack2string(e));
		}
	}

	@Test
	public void testSite() {
		try {
			LOGGER.info("--------------> @Test : testSite\n");
			Site in = CoreTestConstants.PORTALIS_SITE;

			Element elem = ClientConstants.PORTALIS_DATA_XML_WRITER.visit(in);
			assertNotNull(elem);

			SiteInterface out = ClientConstants.ADMIN_DATA_XML_READER.getSite(elem);
			assertTrue("les deux objets Site doivent être égaux\nin1 = "
					+ in + "\nout = " + out, in.equals(out));
			assertEquals(3, out.getApplications().length);
			assertEquals(CoreTestConstants.APPLI1, out.getApplication(CoreTestConstants.APPLI_ID1));
			assertEquals(CoreTestConstants.PLANETS_PLANETS, out.getService(CoreTestConstants.PLANETS_PLANETS.getFullName()));
			assertEquals(CoreTestConstants.SPARQL_APPLICATION, out.getSparqlEndPoints());
			assertEquals(CoreTestConstants.ROLE, out.getRole(CoreTestConstants.StandardActiveUser.getUserCore().getEmail(), 
					CoreTestConstants.PLANETS_PLANETS.getFullName()));

		} catch (Exception e) {
			fail("Exception rencontrée lors du test : " + e.getMessage()
					+ "\nStack :\n" + CommonsUtil.stack2string(e));
		}
	}

	@Test
	public void testActiveSite() {
		try {
			LOGGER.info("--------------> @Test : testActiveSite\n");
			List<ActiveLisService> activeServices = new ArrayList<ActiveLisService>();
			activeServices.add(CoreTestConstants.cam1);
			activeServices.add(CoreTestConstants.cam2);
			activeServices.add(CoreTestConstants.cam3);
			ActiveSite in = new ActiveSite(CoreTestConstants.PORTALIS_SITE, activeServices);

			Element elem = ClientConstants.PORTALIS_DATA_XML_WRITER.visit(in);
			assertNotNull(elem);

			ActiveSite out = ClientConstants.ADMIN_DATA_XML_READER.getActiveSite(elem);
			assertTrue("les deux objets ActiveSite doivent être égaux\nin1 = "
					+ in + "\nout = " + out, in.equals(out));
			assertEquals(3, out.getApplications().length);
			assertEquals(CoreTestConstants.APPLI1, out.getApplication(CoreTestConstants.APPLI_ID1));
			CoreTestConstants.cam1.getActiveId();
			assertEquals(CoreTestConstants.cam1, 
					out.getActiveLisService(CoreTestConstants.cam1.getActiveId()));

		} catch (Exception e) {
			fail("Exception rencontrée lors du test : " + e.getMessage()
					+ "\nStack :\n" + CommonsUtil.stack2string(e));
		}
	}


	@Test
	public void testApplication() {
		try {
			LOGGER.info("--------------> @Test : testApplication\n");
			LisApplication in1 = CoreTestConstants.PLANETS;
			in1.add(new CamelisServiceCore(CoreTestConstants.PLANETS_ID, "photo", RightValue.READER));
			LisApplication in2 = CoreTestConstants.APPLI1;
			LisApplication in3 = CoreTestConstants.APPLI2;
			LisApplication in4 = new LisApplication(CoreTestConstants.APPLI_ID1);
			in4.add(new CamelisServiceCore(CoreTestConstants.APPLI_ID1, "truc", RightValue.READER));
			LisApplication in5 = new LisApplication(CoreTestConstants.APPLI_ID1);
			in5.add(new CamelisServiceCore(CoreTestConstants.APPLI_ID1, CoreTestConstants.SERVICE_NAME1, RightValue.READER));
			assertTrue(
					"les deux objets Application doivent être différents\nin1 = "
							+ in1 + "\nin2 = " + in2, !in1.equals(in2));
			assertTrue(
					"les deux objets Application doivent être différents\nin1 = "
							+ in1 + "\nin3 = " + in3, !in1.equals(in3));
			assertTrue(
					"les deux objets Application doivent être différents\nin4 = "
							+ in4 + "\nin2 = " + in2, !in4.equals(in2));
			assertTrue("les deux objets UserData doivent être égaux\nin2 = "
					+ in2 + "\nin5 = " + in5, in2.equals(in5));
			
			Element elem = ClientConstants.PORTALIS_DATA_XML_WRITER.visit(in1);
			assertNotNull(elem);
			
			LisApplication out = ClientConstants.ADMIN_DATA_XML_READER.getLisApplication(elem);
			

			assertTrue("les deux objets Application doivent être égaux\nin1 = "
					+ in1 + "\nout = " + out, in1.equals(out));


		} catch (Exception e) {
			fail("Exception rencontrée lors du test : " + e.getMessage()
					+ "\nStack :\n" + CommonsUtil.stack2string(e));
		}
	}

	@Test
	public void TestApplication() {
		try {
			LOGGER.info("--------------> @Test : TestApplication\n");

			LisApplication in = CoreTestConstants.PLANETS;
			
			Element elem = ClientConstants.PORTALIS_DATA_XML_WRITER.visit(in);
			assertNotNull(elem);
			
			LisApplication out = ClientConstants.ADMIN_DATA_XML_READER.getLisApplication(elem);
			

			assertTrue("les deux objets ActiveApplication doivent être égaux\nin1 = "
					+ in + "\nout = " + out, in.equals(out));
		} catch (Exception e) {
			fail("Exception rencontrée lors du test : " + e.getMessage()
					+ "\nStack :\n" + CommonsUtil.stack2string(e));
		}
	}

	@Test
	public void TestActiveLisService() {
		try {
			LOGGER.info("--------------> @Test : TestActiveLisService\n");
			
			ActiveLisService in =
					new ActiveLisService(
							CoreTestConstants.MAIL_BENJAMIN,
							CoreTestConstants.HOST,
							CoreTestConstants.PORT, new Date(), new Date(), 1234, 567, CoreTestConstants.SC1.getFullName());
			final String BLABLA = "blabla";
			in.addFeature(BLABLA);
				
			Element elem = ClientConstants.PORTALIS_DATA_XML_WRITER.visit(in);
			assertNotNull(elem);
			
			ActiveLisService out = ClientConstants.ADMIN_DATA_XML_READER.getActiveLisService(elem);
			
			assertTrue("les deux objets ActiveLisService doivent être égaux\nin = "
					+ in + "\nout = " + out, in.equals(out));
			
			assertTrue("l'objet ActiveLisService doit doit comporter une feature '"+BLABLA+"'\nin = "
					+ in, out.getFeatures().size()==1 && out.getFeatures().contains(BLABLA));
			
			
		} catch (Exception e) {
			fail("Exception rencontrée lors du test : " + e.getMessage()
					+ "\nStack :\n" + CommonsUtil.stack2string(e));
		}
	}

	@Test
	public void testUserDataSuper() {
		try {
			LOGGER.info("--------------> @Test : testUserDataSuper\n");
			SuperUser in = new SuperUser(CoreTestConstants.MAIL_YVES, 
					CoreTestConstants.PSEUDO_YVES, CoreTestConstants.PASSWORD_YVES);

			StandardUser in2 = new StandardUser(CoreTestConstants.MAIL_YVES, 
					CoreTestConstants.PSEUDO_BENJAMIN, CoreTestConstants.PASSWORD_BENJAMIN);
			assertTrue(
					"les deux objets UserData doivent être différents\nin = "
							+ in + "\nin2 = " + in2, !in.equals(in2));
			assertTrue(
					"les deux objets UserData doivent être différents\nin = "
							+ in + "\nin2 = " + in2, !in2.equals(in));

			StandardUser in3 = new StandardUser(CoreTestConstants.MAIL_YVES, 
					CoreTestConstants.PSEUDO_YVES, CoreTestConstants.PASSWORD_YVES);
			assertTrue("les deux objets UserData doivent être différents\nin = "
					+ in + "\nin3 = " + in3, !in.equals(in3));

			StandardUser in4 = new StandardUser(CoreTestConstants.MAIL_YVES, 
					CoreTestConstants.PSEUDO_YVES, CoreTestConstants.PASSWORD_YVES);
			assertTrue("les deux objets UserData doivent être différents\nin = "
					+ in + "\nin4 = " + in4, !in.equals(in4));
			assertTrue("les deux objets UserData doivent être différents\nin = "
					+ in + "\nin4 = " + in4, !in4.equals(in));

			Element elem = ClientConstants.PORTALIS_DATA_XML_WRITER.visit(in);
			assertNotNull(elem);

			UserData out = ClientConstants.ADMIN_DATA_XML_READER.getUserData(elem);
			assertTrue("les deux objets UserCore doivent être égaux\nin = "
					+ in + "\nout = " + out, in.equals(out));

		} catch (Exception e) {
			fail("Exception rencontrée lors du test : " + e.getMessage()
					+ "\nStack :\n" + CommonsUtil.stack2string(e));
		}
	}
	
	@Test
	public void testUserDataAnonymous() {
		try {
			LOGGER.info("--------------> @Test : testUserDataAnonymous\n");
			AnonymousUser in = new AnonymousUser();

			AnonymousUser in2 = new AnonymousUser();
			assertTrue(
					"les deux objets AnonymousUser doivent être différents\nin = "
							+ in + "\nin2 = " + in2, !in.equals(in2));
			assertTrue(
					"les deux objets AnonymousUser doivent être différents\nin = "
							+ in + "\nin2 = " + in2, !in2.equals(in));

			AnonymousUser in3 = new AnonymousUser(in.getPseudo());
			assertTrue("les deux objets AnonymousUser doivent être égaux\nin = "
					+ in + "\nin3 = " + in3, in.equals(in3));
			assertTrue("les deux objets UserData doivent être égaux\nin = "
					+ in + "\nin4 = " + in, in3.equals(in));

			Element elem = ClientConstants.PORTALIS_DATA_XML_WRITER.visit(in);
			assertNotNull(elem);

			AnonymousUser out = ClientConstants.ADMIN_DATA_XML_READER.getAnonymousUser(elem);
			assertTrue("les deux objets AnonymousUser doivent être égaux\nin = "
					+ in + "\nout = " + out, in.equals(out));

		} catch (Exception e) {
			fail("Exception rencontrée lors du test : " + e.getMessage()
					+ "\nStack :\n" + CommonsUtil.stack2string(e));
		}
	}
	
	@Test
	public void testUserDataStandard() {
		try {
			LOGGER.info("--------------> @Test : testUserDataStandard\n");
			StandardUser in = new StandardUser(CoreTestConstants.MAIL_YVES, 
					CoreTestConstants.PSEUDO_YVES, CoreTestConstants.PASSWORD_YVES);

			StandardUser in2 = new StandardUser(CoreTestConstants.MAIL_YVES, 
					CoreTestConstants.PSEUDO_BENJAMIN, CoreTestConstants.PASSWORD_BENJAMIN);
			assertTrue(
					"les deux objets UserData doivent être différents\nin = "
							+ in + "\nin2 = " + in2, !in.equals(in2));
			assertTrue(
					"les deux objets UserData doivent être différents\nin = "
							+ in + "\nin2 = " + in2, !in2.equals(in));

			StandardUser in3 = new StandardUser(CoreTestConstants.MAIL_YVES, 
					CoreTestConstants.PSEUDO_YVES, CoreTestConstants.PASSWORD_YVES);
			assertTrue("les deux objets UserData doivent être égaux\nin = "
					+ in + "\nin3 = " + in3, in.equals(in3));

			StandardUser in4 = new StandardUser(CoreTestConstants.MAIL_YVES, 
					CoreTestConstants.PSEUDO_YVES, CoreTestConstants.PASSWORD_YVES);
			assertTrue("les deux objets UserData doivent être égaux\nin = "
					+ in + "\nin4 = " + in4, in.equals(in4));
			assertTrue("les deux objets UserData doivent être égaux\nin = "
					+ in + "\nin4 = " + in4, in4.equals(in));

			Element elem = ClientConstants.PORTALIS_DATA_XML_WRITER.visit(in);
			assertNotNull(elem);

			UserData out = ClientConstants.ADMIN_DATA_XML_READER.getUserData(elem);
			assertTrue("les deux objets UserCore doivent être égaux\nin = "
					+ in + "\nout = " + out, in.equals(out));

		} catch (Exception e) {
			fail("Exception rencontrée lors du test : " + e.getMessage()
					+ "\nStack :\n" + CommonsUtil.stack2string(e));
		}
	}
	
	@Test
	public void testRightProperty() {
		try {
			LOGGER.info("--------------> @Test : testUserData\n");
			Role in = new Role(CoreTestConstants.MAIL_YVES, 
					CoreTestConstants.SERVICE_FULL_NAME1, RightValue.NONE);
			
			Role in1 = new Role(CoreTestConstants.MAIL_YVES,
					CoreTestConstants.SERVICE_FULL_NAME2, RightValue.NONE);
			assertTrue(
					"les deux objets RightProperty doivent être différents\nin = "
							+ in + "\nin2 = " + in1, !in.equals(in1));
			
			Role in2 = new Role(CoreTestConstants.MAIL_YVES,
					CoreTestConstants.SERVICE_FULL_NAME1, RightValue.ADMIN);
			assertTrue("les deux objets RightProperty doivent être différents\nin = "
					+ in + "\nout = " + in2, !in.equals(in2));

			Element elem = ClientConstants.PORTALIS_DATA_XML_WRITER.visit(in);
			assertNotNull(elem);

			Role out = ClientConstants.ADMIN_DATA_XML_READER.getRightProperty(elem);
			assertTrue("les deux objets RightProperty doivent être égaux\nin = "
					+ in + "\nout = " + out, in.equals(out));

		} catch (Exception e) {
			fail("Exception rencontrée lors du test : " + e.getMessage()
					+ "\nStack :\n" + CommonsUtil.stack2string(e));
		}
	}

	@Test
	public void testPortActif() {
		LOGGER.info("--------------> @Test : testPortActif\n");
		try {
			String SERVER = PortalisService.getInstance().getHost();
			int PORT = 8090;
			int PID = 12345;
			PortActif in = new PortActif(SERVER, PORT, PID);

			Element elem = ClientConstants.PORTALIS_DATA_XML_WRITER.visit(in);
			assertNotNull(elem);

			PortActif out = ClientConstants.ADMIN_DATA_XML_READER.getPortActif(elem);
			LOGGER.info("\nout = " + out);

			assertTrue("les deux objets ServiceCore doivent être égaux\nin = "
					+ in + "\nout = " + out, in.equals(out));

		} catch (Exception e) {
			fail("Exception rencontrée lors du test : " + e.getMessage()
					+ "\nStack :\n" + CommonsUtil.stack2string(e));
		}
	}
	
	@Test
	public void testServiceId() {
		try {
			LOGGER.info("--------------> @Test : testServiceId\n");
			ActiveService service = PortalisService.getInstance();
			service.getActiveId();
	
		} catch (Exception e) {
			fail("Exception rencontrée lors du test : " + e.getMessage()
					+ "\nStack :\n" + CommonsUtil.stack2string(e));
		}
	}
	

}

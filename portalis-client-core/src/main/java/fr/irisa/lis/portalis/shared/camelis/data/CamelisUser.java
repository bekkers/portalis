package fr.irisa.lis.portalis.shared.camelis.data;

import java.util.Date;

import com.flipthebird.gwthashcodeequals.EqualsBuilder;
import com.flipthebird.gwthashcodeequals.HashCodeBuilder;

import fr.irisa.lis.portalis.commons.DOMUtil;
import fr.irisa.lis.portalis.shared.admin.ClientConstants;
import fr.irisa.lis.portalis.shared.admin.RightValue;

@SuppressWarnings("serial")
public class CamelisUser implements CamelisDataObject {
	
	@Override
	public String toString() {
		return DOMUtil.prettyXmlString(ClientConstants.CAMELIS_DATA_XML_WRITER.visit(this));
	}

	@Override
	public <T> T accept(CamelisDataWriterInterface<T> visitor) {
		return visitor.visit(this);
	}

	private String key;
	private RightValue role;
	// TODO: use jodatime DateTime instead
	private Date expires;

	public CamelisUser() {

	}

	public CamelisUser(String key, RightValue role) {
		this(key, role, null);
	}

	@Override
	public boolean equals(Object aThat){
		  if ( this == aThat ) return true;

		  if ( !(aThat instanceof CamelisUser) ) return false;
		  //you may prefer this style, but see discussion in Effective Java
		  //if ( aThat == null || aThat.getClass() != this.getClass() ) return false;

		  final CamelisUser that = (CamelisUser)aThat;
			return new EqualsBuilder()
			//.appendSuper(super.equals(aThat))
			.append(this.key, that.key)
			.append(this.role, that.role)
			.append(this.expires, that.expires)
			.isEquals();
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder(17, 31). // two randomly chosen prime numbers
				//appendSuper(super.hashCode()).
				append(key).
				append(role).
				append(expires).
				toHashCode();
	}

	public CamelisUser(String key, RightValue role, Date expires) {
		this.key = key;
		this.role = role;
		this.expires = expires;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public RightValue getRole() {
		return role;
	}

	public void setRole(RightValue role) {
		this.role = role;
	}

	public Date getExpires() {
		return expires;
	}

	public void setExpires(Date expires) {
		this.expires = expires;
	}

}

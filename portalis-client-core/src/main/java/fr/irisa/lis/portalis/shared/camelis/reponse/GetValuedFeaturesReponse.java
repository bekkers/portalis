package fr.irisa.lis.portalis.shared.camelis.reponse;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

import com.flipthebird.gwthashcodeequals.EqualsBuilder;
import com.flipthebird.gwthashcodeequals.HashCodeBuilder;

import fr.irisa.lis.portalis.commons.DOMUtil;
import fr.irisa.lis.portalis.shared.admin.ClientConstants;
import fr.irisa.lis.portalis.shared.admin.Err;
import fr.irisa.lis.portalis.shared.admin.reponse.LisReponse;
import fr.irisa.lis.portalis.shared.camelis.CamelisXmlName;

@SuppressWarnings("serial")
public class GetValuedFeaturesReponse extends LisReponse implements
		CamelisReponseObject {

	private HashMap<Integer, HashMap<String, Set<String>>> values =
			new HashMap<Integer, HashMap<String, Set<String>>>();  
	
	private final String xmlName = CamelisXmlName.GET_VALUED_FEATURES_REPONSE;
	
	public GetValuedFeaturesReponse() {
		super();
	}

	public GetValuedFeaturesReponse(Err err) {
		super(err);
	}

	public Set<Integer> getOids() {
		return values.keySet();
	}
	
	public Set<String> getFeatnames(int oid) {
		if (!values.containsKey(oid)) {
			return new HashSet<String>();
		}
		return values.get(oid).keySet();
	}
	
	
	public Set<String> getValues(int oid, String featname) {
		HashMap<String, Set<String>> map = null;
		if (!values.containsKey(oid)) {
			map = new HashMap<String, Set<String>>();
			values.put(oid, map);
		} else {
			map = values.get(oid);
		}
		Set<String> result = map.get(featname);	
		if (result==null) {
			result = new HashSet<String>();
			map.put(featname, result);
		}
		return result;
	}
	
	public void setValue(int oid, String featname, String val) {
			Set<String> old = getValues(oid, featname);
			old.add(val);
	}
	
	
	public String toString() {
		return DOMUtil.prettyXmlString(ClientConstants.CAMELIS_REPONSE_XML_WRITER
						.visit(this));
	}

	
	public <T> T accept(CamelisReponseWriterInterface<T> visitor) {
		return visitor.visit(this);
	}
	
	
	public boolean equals(Object aThat) {
		if (this == aThat)
			return true;

		if (!(aThat instanceof GetValuedFeaturesReponse))
			return false;

		final GetValuedFeaturesReponse that = (GetValuedFeaturesReponse) aThat;
		return new EqualsBuilder().appendSuper(super.equals(aThat))
				.append(this.values, that.values).isEquals();
	}


	public int hashCode() {
		return new HashCodeBuilder(17, 31). // two randomly chosen prime numbers
				appendSuper(super.hashCode()).append(values).toHashCode();
	}


	public String getXmlName() {
		return xmlName;
	}

	

}

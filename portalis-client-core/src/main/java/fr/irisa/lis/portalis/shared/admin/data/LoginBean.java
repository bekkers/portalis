package fr.irisa.lis.portalis.shared.admin.data;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import com.flipthebird.gwthashcodeequals.EqualsBuilder;
import com.flipthebird.gwthashcodeequals.HashCodeBuilder;

import fr.irisa.lis.portalis.commons.DOMUtil;
import fr.irisa.lis.portalis.shared.admin.ClientConstants;
import fr.irisa.lis.portalis.shared.admin.PortalisException;

@LoginConstraint
@SuppressWarnings("serial")
public class LoginBean implements AdminDataObject {
	private String id;
	private String md5Password;
	protected boolean isGeneralAdmin;

	public LoginBean(String id, String password) {
		this.id = id;
		this.md5Password = password;
	}

	@Override
	public String toString() {
		return DOMUtil.prettyXmlString(ClientConstants.PORTALIS_DATA_XML_WRITER.visit(this));
	}

	public LoginBean() {
		super();
	}

	@Override
	public boolean equals(Object aThat){
		  if ( this == aThat ) return true;

		  if ( !(aThat instanceof LoginBean) ) return false;
		  //you may prefer this style, but see discussion in Effective Java
		  //if ( aThat == null || aThat.getClass() != this.getClass() ) return false;

		  final LoginBean that = (LoginBean)aThat;
	        return new EqualsBuilder().
	                // if deriving: appendSuper(super.equals(obj)).
	                append(this.id, that.id).
	                append(this.md5Password, that.md5Password).
	                append(this.isGeneralAdmin, that.isGeneralAdmin).
	                isEquals();
		}

	@Override
    public int hashCode() {
        return new HashCodeBuilder(17, 31). // two randomly chosen prime numbers
            // if deriving: appendSuper(super.hashCode()).
                append(id).
                append(md5Password).
                append(isGeneralAdmin).
            toHashCode();
    }

	@NotNull
	@Size(min=2)
	@Pattern(regexp="^[\\w\\-]([\\.\\w])+[\\w]+@([\\w\\-]+\\.)+[A-Z]{2,4}$", message="Ceci n'est pas une adresse email")
	public String getId() {
		return this.id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public void setIsGeneralAdmin(boolean isGeneralAdmin) {
		this.isGeneralAdmin = isGeneralAdmin;
	}

	public void setPassword(String password) throws PortalisException {
		this.md5Password = password;
	}

	@NotNull
	@Size(min=8, max=50)
	public String getPassword() {
		return md5Password;
	}


	@Override
	public <T> T accept(AdminDataWriterInterface<T> visitor) {
		return visitor.visit(this);
	}

}
package fr.irisa.lis.portalis.shared.admin.data;

import java.io.Serializable;


public interface AdminDataObject extends Serializable {

	public abstract <T> T accept(AdminDataWriterInterface<T> visitor);
	
	public abstract String toString();

}

package fr.irisa.lis.portalis.shared.camelis.reponse;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Element;

import fr.irisa.lis.portalis.commons.DOMWriter;
import fr.irisa.lis.portalis.shared.admin.ClientConstants;
import fr.irisa.lis.portalis.shared.admin.PortalisError;
import fr.irisa.lis.portalis.shared.admin.PortalisException;
import fr.irisa.lis.portalis.shared.admin.Util;
import fr.irisa.lis.portalis.shared.admin.XmlIdentifier;
import fr.irisa.lis.portalis.shared.admin.data.ActiveLisService;
import fr.irisa.lis.portalis.shared.admin.data.AdminDataXmlWriter;
import fr.irisa.lis.portalis.shared.admin.data.Role;
import fr.irisa.lis.portalis.shared.admin.reponse.AdminReponseXmlWriter;
import fr.irisa.lis.portalis.shared.camelis.CamelisXmlName;
import fr.irisa.lis.portalis.shared.camelis.data.CamelisDataXmlWriter;
import fr.irisa.lis.portalis.shared.camelis.data.CamelisUser;
import fr.irisa.lis.portalis.shared.camelis.data.LisIncrementSet;
import fr.irisa.lis.portalis.shared.camelis.data.LisIntent;

public class CamelisReponseXmlWriter extends CamelisDataXmlWriter implements
		CamelisReponseWriterInterface<Element> {

	@SuppressWarnings("unused")private static final Logger LOGGER = LoggerFactory.getLogger(CamelisReponseXmlWriter.class.getName());

	static private CamelisReponseXmlWriter instance;
	
	public static CamelisReponseXmlWriter getInstance() {
		if (instance == null) {
			instance = new CamelisReponseXmlWriter();
		}
		return instance;
	}
	
	
	public Element visit(PingReponse reponse) {
		Element root = buildRootElement(
				XmlIdentifier.PING_REPONSE(), reponse);
		if (reponse.isOk()) {
			List<ActiveLisService> lesServices = reponse.getLesServices();
			if (lesServices.size() == 1) {
				ActiveLisService activeLisService;
				try {
					activeLisService = reponse
							.getFirstService();
					DOMWriter.addAttribute(root, XmlIdentifier.ACTIVE_SERVICE_ID(),
								activeLisService.getActiveId());

				} catch (PortalisException e) {
					// should nerver append
					throw new PortalisError(e.getMessage());
				}
				setActiveLisService(root, activeLisService);
				DOMWriter.addAttribute(root, 
						XmlIdentifier.LAST_UPDATE(),
						Util.writeDate(reponse.getLastUpdate()));
			} else {
				List<Element> lesElements = visit(lesServices);
				for (Element elem : lesElements) {
					addChild(root, elem);
				}
			}
		}
		return root;
	}


	private void setActiveLisService(Element reponseElem,
			ActiveLisService activeLisService) {
		addChild(reponseElem, visit(activeLisService));		
	}


	private List<Element> visit(List<ActiveLisService> activeLisServices) {
		List<Element> reponse = new ArrayList<Element>();
		for (ActiveLisService activeLisService : activeLisServices) {
			reponse.add(ClientConstants.PORTALIS_DATA_XML_WRITER.visit(activeLisService));
		}
		return reponse;
	}


	public Element visit(StartReponse startReponse) {
		Element reponseElem = AdminReponseXmlWriter.buildRootElement(
				XmlIdentifier.START_REPONSE(), startReponse);
		if (startReponse.isOk()) {
			DOMWriter.addChild(reponseElem, ClientConstants.PORTALIS_DATA_XML_WRITER.visit(startReponse.getActiveLisService()));
		}
		return reponseElem;
	}

	
	public Element visit(ExtentReponse extentReponse) {
		Element elem = AdminReponseXmlWriter.buildRootElement(
				CamelisXmlName.EXTENT_REPONSE, extentReponse);
		if (extentReponse.isOk()) {
			DOMWriter.addChild(elem, visit(extentReponse.getExtent()));
			DOMWriter.addAttribute(elem,
					CamelisXmlName.EXTENT_NB_OBJECTS,
					Integer.toString(extentReponse.getNbObjects()));
			DOMWriter.addAttribute(elem, 
					XmlIdentifier.LAST_UPDATE(),
					Util.writeDate(extentReponse.getLastUpdate()));
		}
		return elem;
	}

	
	public Element visit(IntentReponse intentReponse) {
		Element elem = AdminReponseXmlWriter.buildRootElement(
				CamelisXmlName.INTENT_REPONSE, intentReponse);
		if (intentReponse.isOk()) {
			LisIntent intent = intentReponse.getIntent();
			if (intent != null) {
				DOMWriter.addChild(elem, visit(intent));
			}
			DOMWriter.addAttribute(elem, 
					XmlIdentifier.LAST_UPDATE(),
					Util.writeDate(intentReponse.getLastUpdate()));
		}
		return elem;
	}

	
	public Element visit(ZoomReponse zoomReponse) {
		Element elem = AdminReponseXmlWriter.buildRootElement(
				CamelisXmlName.ZOOM_REPONSE, zoomReponse);
		if (zoomReponse.isOk()) {
			DOMWriter.addAttribute(elem,
					CamelisXmlName.NB_OBJECTS,
					Integer.toString(zoomReponse.getNbObjects()));
			DOMWriter.addAttribute(elem,
					CamelisXmlName.NB_INCRS,
					Integer.toString(zoomReponse.getNbIncrs()));
			DOMWriter.addAttribute(elem,
					CamelisXmlName.NEW_WQ,
					zoomReponse.getNewWorkingQuery());
			LisIncrementSet incrs = zoomReponse.getIncrements();
			if (incrs != null) {
				Element xmlIncrs = visit(incrs);
				DOMWriter.addChild(elem, xmlIncrs);
			}
			DOMWriter.addAttribute(elem, 
					XmlIdentifier.LAST_UPDATE(),
					Util.writeDate(zoomReponse.getLastUpdate()));
		}
		return elem;
	}

	
	public Element visit(PropertyTreeReponse reponse) {
		Element elem = AdminReponseXmlWriter.buildRootElement(
				CamelisXmlName.PROPERTY_TREE_REPONSE, reponse);
		if (reponse.isOk()) {
			DOMWriter.addChild(elem, visit(reponse.getTree()));
			DOMWriter.addAttribute(elem, 
					XmlIdentifier.LAST_UPDATE(),
					Util.writeDate(reponse.getLastUpdate()));
		}
		return elem;
	}

	
	public Element visit(PingUsersReponse reponse) {

		Element root = AdminReponseXmlWriter.buildRootElement(
				CamelisXmlName.PING_USERS_REPONSE, reponse);
		if (reponse.isOk()) {
			DOMWriter.addTextChild(root, CamelisXmlName.STATUS, CamelisXmlName.OK);
			DOMWriter.addAttribute(root, CamelisXmlName.CREATOR,
							reponse.getCreator());

			Element rightListElems = DOMWriter.createRootElement(CamelisXmlName.ROLES);
			for (Role prop : reponse.getUsers()) {
				DOMWriter.addChild(rightListElems, new AdminDataXmlWriter().visit(prop));
				DOMWriter.addChild(root, rightListElems);
			}
			DOMWriter.addAttribute(root, 
				XmlIdentifier.LAST_UPDATE(),
				Util.writeDate(reponse.getLastUpdate()));
		}

		return root;
	}

	
	public Element visit(RightPropertyReponse rightPropReponse) {
		// TODO à implémenter
		throw new NoSuchMethodError("RightPropertyReponse not yet emplemented");
	}

	
	public Element visit(CamelisUserReponse reponse) {
		Element reponseElem = AdminReponseXmlWriter.buildRootElement(
				reponse.getElemName(), reponse);
		if (reponse.isOk()) {
			CamelisUser user = reponse.getUser();
			if (user!=null)
				DOMWriter.addChild(reponseElem, visit(user));

		}
		return reponseElem;
	}

	
	public Element visit(ImportCtxReponse reponse) {
		Element root = AdminReponseXmlWriter.buildRootElement(
				CamelisXmlName.IMPORT_CTX_REPONSE, reponse);
		if (reponse.isOk()) {
			DOMWriter.addAttribute(root, CamelisXmlName.CTX_FILE, reponse.getCtxFile());
		}
		DOMWriter.addAttribute(root, 
				XmlIdentifier.LAST_UPDATE(),
				Util.writeDate(reponse.getLastUpdate()));
		return root;
	}


	
	public Element visit(ImportLisReponse reponse) {
		Element root = AdminReponseXmlWriter.buildRootElement(
				CamelisXmlName.IMPORT_LIS_REPONSE, reponse);
		if (reponse.isOk()) {
			DOMWriter.addAttribute(root, CamelisXmlName.LIS_FILE, reponse.getLisFile());
		}
		DOMWriter.addAttribute(root, 
				XmlIdentifier.LAST_UPDATE(),
				Util.writeDate(reponse.getLastUpdate()));
		return root;
	}


	public Element visit(DelKeyReponse reponse) {
		Element root = AdminReponseXmlWriter.buildRootElement(
				CamelisXmlName.DEL_KEY_REPONSE, reponse);
		DOMWriter.addAttribute(root,
				XmlIdentifier.LAST_UPDATE(),
				Util.writeDate(reponse.getLastUpdate()));
		return root;
	}
	

	public Element visit(ResetCreatorReponse reponse) {
		Element root = AdminReponseXmlWriter.buildRootElement(
				CamelisXmlName.RESET_CREATOR_REPONSE, reponse);
		DOMWriter.addAttribute(root,
				XmlIdentifier.LAST_UPDATE(),
				Util.writeDate(reponse.getLastUpdate()));
		return root;
	}

	
	public Element visit(DelObjectsReponse reponse) {
		Element root = AdminReponseXmlWriter.buildRootElement(
				reponse.getXmlName(), reponse);
		if (reponse.isOk()) {
			DOMWriter.addChild(root, visit(reponse.getExtent()));
		}
		DOMWriter.addAttribute(root, 
				XmlIdentifier.LAST_UPDATE(),
				Util.writeDate(reponse.getLastUpdate()));
		return root;
	}


	
	public Element visit(DelFeatureReponse reponse) {
		Element root = AdminReponseXmlWriter.buildRootElement(
				reponse.getXmlName(), reponse);
		if (reponse.isOk()) {
			DOMWriter.addChild(root, visit(reponse.getIncrements()));
		}
		DOMWriter.addAttribute(root, 
				XmlIdentifier.LAST_UPDATE(),
				Util.writeDate(reponse.getLastUpdate()));
		return root;		
	}


	public Element visit(AddAxiomReponse reponse) {
		Element root = AdminReponseXmlWriter.buildRootElement(
				reponse.getXmlName(), reponse);
		DOMWriter.addAttribute(root, 
				XmlIdentifier.LAST_UPDATE(),
				Util.writeDate(reponse.getLastUpdate()));
		return root;
	}


	public Element visit(ResetCamelisReponse reponse) {
		Element root = AdminReponseXmlWriter.buildRootElement(
				reponse.getXmlName(), reponse);
		DOMWriter.addAttribute(root, 
				XmlIdentifier.LAST_UPDATE(),
				Util.writeDate(reponse.getLastUpdate()));
		return root;
	}


	public Element visit(GetValuedFeaturesReponse reponse) {
		Element root = AdminReponseXmlWriter.buildRootElement(
				reponse.getXmlName(), reponse);
		if (reponse.isOk()) {
			for (int oid: reponse.getOids()) {
				Element obj = DOMWriter.createRootElement("object");
				DOMWriter.addAttribute(obj, "oid", Integer.toString(oid));
				for (String featname: reponse.getFeatnames(oid)) {
					for (String value : reponse.getValues(oid,  featname)) {
						Element prop = DOMWriter.createRootElement("property");
						DOMWriter.addAttribute(prop, "name", featname);
						DOMWriter.addAttribute(prop, "value", value);
						DOMWriter.addChild(obj,  prop);
					}
				}
				DOMWriter.addChild(root, obj);
			}
		}
		DOMWriter.addAttribute(root, 
				XmlIdentifier.LAST_UPDATE(),
				Util.writeDate(reponse.getLastUpdate()));
		return root;
	}


	@Override
	public Element visit(LoadContextReponse reponse) {
		Element root = AdminReponseXmlWriter.buildRootElement(
				reponse.getXmlName(), reponse);
		if (reponse.isOk()) {
			DOMWriter.addAttribute(root, CamelisXmlName.CONTEXT_NAME, reponse.getContextName());
		}
		DOMWriter.addAttribute(root, 
				XmlIdentifier.LAST_UPDATE(),
				Util.writeDate(reponse.getLastUpdate()));
		return root;		
	}


}

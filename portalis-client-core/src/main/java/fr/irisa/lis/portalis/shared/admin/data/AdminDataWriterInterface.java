package fr.irisa.lis.portalis.shared.admin.data;


public interface AdminDataWriterInterface<T> {

	public T visit(boolean bool);
	public T visit(String s);
	public T visit(Integer[] intTable);
	public T visit(Role userRights);
	public T visit(PortActif portActif);
	public T visit(ActiveUser loggedInUser);
	public T visit(ActiveUser[] loggedInUser);
	public T visit(SuperUser superUser);
	public T visit(StandardUser user);
	public T visit(StandardUser[] users);
	public T visit(LisServiceCore serviceCore);
	public T visit(ActiveLisService lisService);
	public T visit(LisApplication application);
	public T visit(LoginBean loginBean);
	public T visit(PortalisService portalisService);
	public T visit(LoginResult loginResult);
	public T visit(ActiveSite activeSite);
	public T visit(AnonymousUser anonymousUser);
	public T visit(UIPreference uiPreference);
	public T visit(SiteInterface portalisSite);
	public T visit(Property property);
	public T visit(SparqlService sparqlService);
	public T visit(Query query);
	public T visit(SparqlApplication sparqlApplication);

}

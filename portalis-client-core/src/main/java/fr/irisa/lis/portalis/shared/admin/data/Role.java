package fr.irisa.lis.portalis.shared.admin.data;

import com.flipthebird.gwthashcodeequals.EqualsBuilder;
import com.flipthebird.gwthashcodeequals.HashCodeBuilder;

import fr.irisa.lis.portalis.commons.DOMUtil;
import fr.irisa.lis.portalis.shared.admin.ClientConstants;
import fr.irisa.lis.portalis.shared.admin.RightValue;

@SuppressWarnings("serial")
public class Role implements AdminDataObject {

	@Override
	public String toString() {
		return DOMUtil.prettyXmlString(ClientConstants.PORTALIS_DATA_XML_WRITER.visit(this));
	}

	@Override
	public <T> T accept(AdminDataWriterInterface<T> visitor) {
		return visitor.visit(this);
	}

	private String serviceFullName;
	protected String userEmail;
	
	private RightValue right;

	public Role() {
	}

	public Role(String userEmail, String serviceName, RightValue right) {
		this.userEmail = userEmail;
		this.serviceFullName = serviceName;
		this.right = right;
	}

	@Override
	public boolean equals(Object aThat){
		  if ( this == aThat ) return true;

		  if ( !(aThat instanceof Role) ) return false;
		  //you may prefer this style, but see discussion in Effective Java
		  //if ( aThat == null || aThat.getClass() != this.getClass() ) return false;

		  final Role that = (Role)aThat;
	        return new EqualsBuilder().
	                // if deriving: appendSuper(super.equals(obj)).
	                append(this.userEmail, that.userEmail).
	                append(this.serviceFullName, that.serviceFullName).
	                append(this.right, that.right).
	                isEquals();
		}

	@Override
	public int hashCode() {
	    return new HashCodeBuilder(17, 31). // two randomly chosen prime numbers
	        // if deriving: appendSuper(super.hashCode()).
	            append(userEmail).
	            append(serviceFullName).
	            append(right).
	        toHashCode();
	}

	public String getUserEmail() {
		return userEmail;
	}

	public void setUserEmail(String userEmail) {
		this.userEmail = userEmail;
	}

	public String getServiceFullName() {
		return serviceFullName;
	}

	public void setServiceFullName(String serviceName) {
		this.serviceFullName = serviceName;
	}

	public String getId() {
		return buildId(userEmail, serviceFullName);
	}

	public RightValue getRight() {
		return right;
	}

	public void setRight(RightValue right) {
		this.right = right;
	}

	public static String buildId(String email, String fullName) {
		return email+"::"+fullName;
	}
}

package fr.irisa.lis.portalis.shared.admin.data;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.mongodb.BasicDBObject;

import fr.irisa.lis.jquery.TreetableNode;
import fr.irisa.lis.portalis.shared.admin.PortalisException;

public abstract class Application<T extends ServiceCoreInterface> extends BasicDBObject {

	protected List<T> services = new ArrayList<T>();
	protected String id;

	public Application() {}
	
	public Application(String appliName) {
		this.id = appliName;
	}

	public Application(String appliName, List<T> services) {
		this.id = appliName;
		this.services = services;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getId() {
		return id;
	}
	
	public void setServices(List<T> services) {
		this.services = services;
	}

	public List<T> getServices() {
		return services;
	}
	
	public T getService(String name) throws PortalisException {
		T result = null;
		for (T service : services) {
			if (service.getServiceName().equals(name)) {
				result = service;
				break;
			}
		}
		return result;
	}
	
	public Map<String, T> getServiceMap() {
		Map<String, T> result = new HashMap<String, T>();
		for (T service : services) {
			result.put(service.getServiceName(), service);
		}
		return result;
	}
	
	public void add(T service) {
		this.services.add(service);
	}

	public void removeService(T service) {
		this.services.remove(service);
	}
	
	public TreetableNode toTreetableNode() {
		TreetableNode result = new TreetableNode(id);
		for (T service : services) {
			TreetableNode child = new TreetableNode(service.getServiceName());
			child.addProperty("language", service.getReqLanguage().name());
			result.addChild(child);
		}
		return result;
	}


}
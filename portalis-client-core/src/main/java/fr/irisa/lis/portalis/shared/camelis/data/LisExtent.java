package fr.irisa.lis.portalis.shared.camelis.data;

import java.util.ArrayList;
import java.util.Arrays;

import com.flipthebird.gwthashcodeequals.HashCodeBuilder;

import fr.irisa.lis.portalis.commons.DOMUtil;
import fr.irisa.lis.portalis.shared.admin.ClientConstants;
import fr.irisa.lis.portalis.shared.camelis.CamelisXmlName;

@SuppressWarnings("serial")
public class LisExtent implements CamelisDataObject {

	private ArrayList<LisObject> lisObjects;
	private static final String xmlName = CamelisXmlName.EXTENT;

	public ArrayList<LisObject> getLisObjects() {
		return lisObjects;
	}

	public void setLisObjects(ArrayList<LisObject> lisObjects) {
		this.lisObjects = lisObjects;
	}

	public LisExtent() {
		this.lisObjects = new ArrayList<LisObject>();
	}

	public LisExtent(ArrayList<LisObject> objs) {
		this.lisObjects = objs;
	}

	public LisExtent(LisObject[] objs) {
		this.lisObjects = new ArrayList<LisObject>(Arrays.asList(objs));
	}

	public LisObject[] toArray() {
		return lisObjects.toArray(new LisObject[lisObjects.size()]);
	}

	public boolean contains(int oid) {
		for (LisObject o : lisObjects) {
			if (o.getOid() == oid)
				return true;
		}
		return false;
	}

	@Override
	public boolean equals(Object aThat) {
		if (this == aThat) return true;

		if (!(aThat instanceof LisExtent)) return false;

		final LisExtent that = (LisExtent) aThat;
		return this.lisObjects.size() == that.lisObjects.size()
				&& this.lisObjects.containsAll(that.lisObjects);
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder(17, 31). // two randomly chosen prime numbers
				// appendSuper(super.hashCode()).
				append(lisObjects).toHashCode();
	}

	@Override
	public <T> T accept(CamelisDataWriterInterface<T> visitor) {
		return visitor.visit(this);
	}

	@Override
	public String toString() {
		return DOMUtil.prettyXmlString(ClientConstants.CAMELIS_DATA_XML_WRITER
						.visit(this));
	}

	public int card() {
		return this.lisObjects.size();
	}

	public String getXmlName() {
		return xmlName;
	}

	public void add(LisObject o) {
		this.lisObjects.add(o);
	}
	
	public int[] getOids() {
		LisObject[] lisObjects = toArray();
		int[] oids = new int[lisObjects.length];
		for (int i = 0; i<lisObjects.length; i++) {
			oids[i] = lisObjects[i].getOid();
		}
		return oids;
	}

}

package fr.irisa.lis.portalis.shared.camelis.reponse;

import com.flipthebird.gwthashcodeequals.EqualsBuilder;
import com.flipthebird.gwthashcodeequals.HashCodeBuilder;

import fr.irisa.lis.portalis.commons.DOMUtil;
import fr.irisa.lis.portalis.shared.admin.ClientConstants;
import fr.irisa.lis.portalis.shared.admin.Err;
import fr.irisa.lis.portalis.shared.admin.data.Role;
import fr.irisa.lis.portalis.shared.admin.reponse.LisReponse;

@SuppressWarnings("serial")
public class PingUsersReponse extends LisReponse implements CamelisReponseObject  {
	
	private Role[] usersRightProp;
	private String creator;
	
	public PingUsersReponse() {
		super();
	};

	public PingUsersReponse(Role[] users, String creator) {
		super();
		this.usersRightProp= users;
		this.creator = creator;
	};

	public PingUsersReponse(Err err) {
		super(err);
	}
	
	@Override
	public String toString() {
		return DOMUtil.prettyXmlString(ClientConstants.CAMELIS_REPONSE_XML_WRITER.visit(this));
	}

	@Override
	public <T> T accept(CamelisReponseWriterInterface<T> visitor) {
		return visitor.visit(this);
	}
	
	@Override
	public boolean equals(Object aThat){
		  if ( this == aThat ) return true;

		  if ( !(aThat instanceof PingUsersReponse) ) return false;
		  //you may prefer this style, but see discussion in Effective Java
		  //if ( aThat == null || aThat.getClass() != this.getClass() ) return false;

		  final PingUsersReponse that = (PingUsersReponse)aThat;
			return new EqualsBuilder()
			.appendSuper(super.equals(aThat))
			.append(this.usersRightProp, that.usersRightProp)
			.append(this.creator, that.creator)
			.isEquals();
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder(17, 31). // two randomly chosen prime numbers
				appendSuper(super.hashCode()).
				append(usersRightProp).
				append(creator).
				toHashCode();
	}


	public void setUsers(Role[] users) {
		this.usersRightProp = users;
	}


	public Role[] getUsers() {
		return usersRightProp;
	}


	public void setCreator(String creator) {
		this.creator = creator;
	}


	public String getCreator() {
		return creator;
	}
	
}

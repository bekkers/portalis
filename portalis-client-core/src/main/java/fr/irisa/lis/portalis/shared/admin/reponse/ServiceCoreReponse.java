package fr.irisa.lis.portalis.shared.admin.reponse;

import java.io.Serializable;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.flipthebird.gwthashcodeequals.EqualsBuilder;
import com.flipthebird.gwthashcodeequals.HashCodeBuilder;

import fr.irisa.lis.portalis.commons.DOMUtil;
import fr.irisa.lis.portalis.shared.admin.ClientConstants;
import fr.irisa.lis.portalis.shared.admin.Err;
import fr.irisa.lis.portalis.shared.admin.data.LisServiceCore;

@SuppressWarnings("serial")
public class ServiceCoreReponse extends VoidReponse implements Serializable,
		AdminReponseVisitedObject {

	@SuppressWarnings("unused")
	private static final Logger LOGGER = LoggerFactory
			.getLogger(ServiceCoreReponse.class.getName());
	
	private LisServiceCore serviceCore;


	public ServiceCoreReponse() {
		super();
	}

	public ServiceCoreReponse(LisServiceCore serviceCore)  {
		super();
		this.serviceCore = serviceCore;
	}

	public ServiceCoreReponse(Err err) {
		super(err);
	}

	@Override
	public boolean equals(Object aThat) {
		if (this == aThat)
			return true;

		if (!(aThat instanceof ServiceCoreReponse))
			return false;
		// you may prefer this style, but see discussion in Effective Java
		// if ( aThat == null || aThat.getClass() != this.getClass() ) return
		// false;

		final ServiceCoreReponse that = (ServiceCoreReponse) aThat;
		return new EqualsBuilder().appendSuper(super.equals(aThat))
				.append(this.serviceCore, that.serviceCore).isEquals();
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder(17, 31). // two randomly chosen prime numbers
				appendSuper(super.hashCode()).append(serviceCore).toHashCode();
	}

	@Override
	public String toString() {
		return DOMUtil.prettyXmlString(ClientConstants.ADMIN_REPONSE_XML_WRITER
				.visit(this));
	}

	@Override
	public <T> T accept(AdminReponseWriterInterface<T> visitor) {
		return visitor.visit(this);
	}

	public LisServiceCore getServiceCore() {
		return serviceCore;
	}

	public void setServiceCore(LisServiceCore serviceCore) {
		this.serviceCore = serviceCore;
	}


}

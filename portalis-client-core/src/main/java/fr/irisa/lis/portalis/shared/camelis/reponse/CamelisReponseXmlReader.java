package fr.irisa.lis.portalis.shared.camelis.reponse;

import java.util.Iterator;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Element;

import fr.irisa.lis.portalis.commons.DOMUtil;
import fr.irisa.lis.portalis.shared.admin.Err;
import fr.irisa.lis.portalis.shared.admin.PortalisException;
import fr.irisa.lis.portalis.shared.admin.RequestException;
import fr.irisa.lis.portalis.shared.admin.Util;
import fr.irisa.lis.portalis.shared.admin.XmlIdentifier;
import fr.irisa.lis.portalis.shared.admin.data.Property;
import fr.irisa.lis.portalis.shared.admin.reponse.LisReponse;
import fr.irisa.lis.portalis.shared.camelis.CamelisException;
import fr.irisa.lis.portalis.shared.camelis.CamelisXmlName;
import fr.irisa.lis.portalis.shared.camelis.data.CamelisDataXmlReader;
import fr.irisa.lis.portalis.shared.camelis.data.LisExtent;
import fr.irisa.lis.portalis.shared.camelis.data.LisIncrement;
import fr.irisa.lis.portalis.shared.camelis.data.LisIncrementSet;
import fr.irisa.lis.portalis.shared.camelis.data.LisIntent;

public class CamelisReponseXmlReader extends CamelisDataXmlReader implements
		CamelisReponseReaderInterface<Element> {private static final Logger LOGGER = LoggerFactory.getLogger(CamelisReponseXmlReader.class.getName());

	static private CamelisReponseXmlReader instance;

	public static CamelisReponseXmlReader getInstance() {
		if (instance == null) {
			instance = new CamelisReponseXmlReader();
		}
		return instance;
	}

	protected CamelisReponseXmlReader() {
		super();
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see fr.irisa.lis.portalis.client.camelis.dom4j.reader.
	 * CamelisReponseXmlReaderInterface#getStartReponse(org.dom4j.Element)
	 */
	@Override
	public StartReponse getStartReponse(Element root) throws PortalisException,
			RequestException {
		LOGGER.debug("getStartReponse(<" + root.getNodeName() + "> ...)");
		StartReponse reponse = new StartReponse();
		if (!checkReponseElement(root, XmlIdentifier.START_REPONSE(), reponse)) {
			return reponse;
		} else {
			reponse.setActiveLisService(getActiveLisService(getChildElement(root,
					XmlIdentifier.CAMELIS())));
			return reponse;
		}
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see fr.irisa.lis.portalis.client.camelis.dom4j.reader.
	 * CamelisReponseXmlReaderInterface#getPingReponse(org.dom4j.Element)
	 */
	@Override
	public PingReponse getPingReponse(Element root) throws PortalisException,
			RequestException {
		LOGGER.debug("getPingReponse(\nElement = " + DOMUtil.prettyXmlString(root) + ")");
		PingReponse reponse = new PingReponse();
		if (!checkReponseElement(root, XmlIdentifier.PING_REPONSE(), reponse)) {
			LOGGER.debug("retour avec erreur de getPingReponse() reponse=" + reponse);
			return reponse;
		}
		
		String lastUpdate = getAttribute(root, XmlIdentifier.LAST_UPDATE());
		if (lastUpdate != null) {
			reponse.setLastUpdate(Util.readDate(lastUpdate));
		}
		
		String creator = getAttribute(root, XmlIdentifier.CREATOR());
		if (creator != null) {
			// S'il y a un créator comme attribut de root, la réponse vient de Camelis
			reponse.addActiveLisService(getActiveLisService(root));
			return reponse;
		} else {
			// S'il n'y a pas un créator comme attribut de root, la réponse vient de Portalis
			List<Element> lesElems = getChildElements(root,
					XmlIdentifier.CAMELIS());
			if (lesElems != null) {
				for (Element elem : lesElems) {
					reponse.addActiveLisService(getActiveLisService(elem));
				}
			}
		}
		return reponse;
	}		

	/*
	 * (non-Javadoc)
	 *
	 * @see fr.irisa.lis.portalis.client.camelis.dom4j.reader.
	 * CamelisReponseXmlReaderInterface#getImportCtxReponse(org.dom4j.Element)
	 */
	@Override
	public ImportCtxReponse getImportCtxReponse(Element root)
			throws PortalisException, RequestException {
		ImportCtxReponse reponse = new ImportCtxReponse();
		if (!checkReponseElement(root, XmlIdentifier.IMPORT_CTX_REPONSE(), reponse)) {
			LOGGER.debug("retour avec erreur de getImportCtxReponse() reponse="
					+ reponse);
			return reponse;
		}

		setLastUpdate(root, reponse);

		if (getAttribute(root, CamelisXmlName.CTX_FILE) != null) {
			reponse.setCtxFile(getStringAttribute(root, CamelisXmlName.CTX_FILE));
		} else {
			String mess = String.format(
					"Attribute %s is missing in element %s",
					CamelisXmlName.CTX_FILE, DOMUtil.prettyXmlString(root));
			LOGGER.error(mess);
			reponse = new ImportCtxReponse(new Err(mess));
		}
		return reponse;
	}

	@Override
	public ImportLisReponse getImportLisReponse(Element root)
			throws PortalisException, RequestException {
		ImportLisReponse reponse = new ImportLisReponse();
		if (!checkReponseElement(root, CamelisXmlName.IMPORT_LIS_REPONSE, reponse)) {
			LOGGER.debug("retour avec erreur de getImportLisReponse() reponse="
					+ reponse);
			return reponse;
		}
		setLastUpdate(root, reponse);

		if (root.getAttribute(CamelisXmlName.LIS_FILE) != null) {
			reponse.setLisFile(getStringAttribute(root, CamelisXmlName.LIS_FILE));
		} else {
			String mess = String.format(
					"Attribute %s is missing in element %s",
					CamelisXmlName.LIS_FILE, DOMUtil.prettyXmlString(root));
			LOGGER.error(mess);
			reponse = new ImportLisReponse(new Err(mess));
		}
		return reponse;
	}


	public ExtentReponse getExtentReponse(Element root)
			throws CamelisException, PortalisException, RequestException {
		ExtentReponse reponse = new ExtentReponse();

		if (!checkReponseElement(root, CamelisXmlName.EXTENT_REPONSE, reponse)) {
			LOGGER.debug("retour avec erreur de getExtent()+Util.stack2string( reponse=" + reponse);
			return reponse;
		}

		setLastUpdate(root, reponse);

		int nbObjects = Integer.parseInt(root.getAttribute(CamelisXmlName.EXTENT_NB_OBJECTS));

		List<Element> el = getChildElements(root, CamelisXmlName.EXTENT);
		if (el == null || el.size() != 1) {
			String msg = String.format("Élément %s manquant ou non unique.",
					CamelisXmlName.EXTENT);
			LOGGER.error(msg);
			throw new PortalisException(msg);
		}

		LisExtent extent = getLisExtent(el.get(0));

		reponse.setNbObjects(nbObjects);
		reponse.setExtent(extent);
		return reponse;
	}


	public IntentReponse getIntentReponse(Element root)
			throws PortalisException, RequestException, CamelisException {
		IntentReponse reponse = new IntentReponse();

		if (!checkReponseElement(root, reponse.getXmlName(), reponse)) {
			LOGGER.debug("retour avec erreur de getIntent()+Util.stack2string( reponse=" + reponse);
			return reponse;
		}

		setLastUpdate(root, reponse);

		Element intentElem = getChildElement(root, CamelisXmlName.INTENT);

		LisIntent intent = new LisIntent();
		List<Element> xmlProps = getChildElements(intentElem, CamelisXmlName.FEATURE);
		if (xmlProps != null) {
			Iterator<Element> iter = xmlProps.iterator();
			while (iter.hasNext()) {
				Element e = iter.next();
				Property p = getProperty(e);
				intent.add(p);
			}
		}
		reponse.setIntent(intent);
		return reponse;
	}


	public ZoomReponse getZoomReponse(Element root)
			throws PortalisException, RequestException, CamelisException {
		ZoomReponse reponse = new ZoomReponse();
		LOGGER.debug("ZoomReponse elem "+DOMUtil.prettyXmlString(root));

		if (!checkReponseElement(root, CamelisXmlName.ZOOM_REPONSE, reponse)) {
			LOGGER.debug("retour avec erreur de getZoomReponse()+Util.stack2string( reponse=" + reponse);
			return reponse;
		}

		setLastUpdate(root, reponse);
		
		// Retrieve number of objects
		int n = Integer.parseInt(getAttribute(root, CamelisXmlName.NB_OBJECTS));
		reponse.setNbObjects(n);

		// Retrieve number of increments
		n = Integer.parseInt(getAttribute(root, CamelisXmlName.NB_INCRS));
		reponse.setNbIncrs(n);

		// Retrieve new working query
		String newQuery = getAttribute(root, CamelisXmlName.NEW_WQ);
		reponse.setNewWorkingQuery(newQuery);
		
		// Retrieve increments
		LisIncrementSet incrs = new LisIncrementSet();
		Element incrsElem = getChildElement(root, incrs.getXmlName());

		List<Element> xmlProps = getChildElements(incrsElem, CamelisXmlName.INCR);
		if (xmlProps != null) {
			Iterator<Element> iter = xmlProps.iterator();
			while (iter.hasNext()) {
				Element e = iter.next();
				LisIncrement i = getIncrement(e);
				incrs.add(i);
			}
		}
		reponse.setIncrements(incrs);

		return reponse;
	}


	private void setLastUpdate(Element root, LisReponse reponse) throws PortalisException {
		String dateStr = getAttribute(root, XmlIdentifier.LAST_UPDATE());
		java.util.Date d = Util.readDate(dateStr);
		reponse.setLastUpdate(d);
	}

	/*
	 * (non-Javadoc)
	 *
	 *
	 * CamelisReponseXmlReaderInterface
	 * #setCamelisUserReponse(fr.irisa.lis.portalis
	 * .client.camelis.dom4j.reponse.CamelisUserReponse, org.dom4j.Element)
	 */
	public void setCamelisUserReponse(CamelisUserReponse reponse, Element root)
			throws PortalisException, RequestException {
		if (!checkReponseElement(root, reponse.getElemName(), reponse)) {
		} else {
			Element userElem = getChildElement(root, CamelisXmlName.USER);
			reponse.setUser(getCamelisUser(userElem));
		}
	}


	public DelKeyReponse getDelKeyReponse(Element root)
			throws PortalisException, RequestException {
		DelKeyReponse reponse = new DelKeyReponse();
		if (!checkReponseElement(root, CamelisXmlName.DEL_KEY_REPONSE,
				reponse)) {
		}
		setLastUpdate(root, reponse);
		return reponse;
	}


	public ResetCreatorReponse getResetCreatorReponse(Element root)
			throws PortalisException, RequestException {
		ResetCreatorReponse reponse = new ResetCreatorReponse();
		if (!checkReponseElement(root, CamelisXmlName.RESET_CREATOR_REPONSE,
				reponse)) {
		}
		setLastUpdate(root, reponse);
		return reponse;
	}


	public DelObjectsReponse getDelObjectsReponse(Element root)
			throws PortalisException, RequestException, CamelisException {
		DelObjectsReponse reponse = new DelObjectsReponse();
		if (!checkReponseElement(root, reponse.getXmlName(), reponse)) {
			LOGGER.debug("getDelObjectsReponse() returned with an error+Util.stack2string( reponse=" + reponse);
		} else {
			setLastUpdate(root, reponse);
			LisExtent ext = new LisExtent();
			Element extElem = getChildElement(root, ext.getXmlName());
			ext = getLisExtent(extElem);
			reponse.setExtent(ext);
		}
		return reponse;
	}


	public DelFeatureReponse getDelFeatureReponse(Element root)
			throws PortalisException, RequestException, CamelisException {
		DelFeatureReponse reponse = new DelFeatureReponse();
		if (!checkReponseElement(root, reponse.getXmlName(), reponse)) {
			LOGGER.debug("getDelFeatureReponse() returned with an error+Util.stack2string( reponse=" + reponse);
		} else {
			setLastUpdate(root, reponse);
			LisIncrementSet incrs = new LisIncrementSet();
			Element incrsElem = getChildElement(root, incrs.getXmlName());
			incrs = getLisIncrementSet(incrsElem);
			reponse.setIncrements(incrs);
		}
		return reponse;
	}


	public AddAxiomReponse getAddAxiomReponse(Element root)
			throws PortalisException, RequestException, CamelisException {
		AddAxiomReponse reponse = new AddAxiomReponse();
		if (!checkReponseElement(root, reponse.getXmlName(), reponse)) {
			LOGGER.debug("getAddAxiomReponse returned with an error+Util.stack2string( reponse=" + reponse);
		}
		setLastUpdate(root, reponse);
		return reponse;

	}


	public ResetCamelisReponse getResetCamelisReponse(Element root)
			throws PortalisException, RequestException, CamelisException {
		ResetCamelisReponse reponse = new ResetCamelisReponse();
		if (!checkReponseElement(root, reponse.getXmlName(), reponse)) {
			LOGGER.debug("getResetCamelisReponse() returned with an error+Util.stack2string( reponse=" + reponse);
		}
		setLastUpdate(root, reponse);
		return reponse;
	}


	public GetValuedFeaturesReponse getGetValuedFeaturesReponse(Element root)
			throws PortalisException, RequestException, CamelisException {
		GetValuedFeaturesReponse reponse = new GetValuedFeaturesReponse();
		if (!checkReponseElement(root, reponse.getXmlName(), reponse)) {
			LOGGER.debug("getGetValuedFeaturesReponse() returned with an error+Util.stack2string( reponse = " + reponse);
			return reponse;
		}
		setLastUpdate(root, reponse);
		List<Element> objs = getChildElements(root, "object");
		for (Element objElem: objs) {
			int oid = Integer.parseInt(objElem.getAttribute("oid"));
			List<Element> props = getChildElements(objElem, "property");
			for (Element propElem: props) {
				String featname = propElem.getAttribute("name");
				String value = propElem.getAttribute("value");
				reponse.setValue(oid, featname, value);
			}
		}

		return reponse;
	}


	public LoadContextReponse getLoadContextReponse(Element root)
			throws PortalisException, RequestException, CamelisException {
		LoadContextReponse reponse = new LoadContextReponse();
		if (!checkReponseElement(root, reponse.getXmlName(), reponse)) {
			LOGGER.debug("getGetValuedFeaturesReponse() returned with an error+Util.stack2string( reponse = " + reponse);
			return reponse;
		}
		setLastUpdate(root, reponse);
		String name = root.getAttribute(CamelisXmlName.CONTEXT_NAME);
		reponse.setContextName(name);
		return reponse;
	}

}

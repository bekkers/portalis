package fr.irisa.lis.portalis.shared.camelis.reponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.flipthebird.gwthashcodeequals.EqualsBuilder;
import com.flipthebird.gwthashcodeequals.HashCodeBuilder;

import fr.irisa.lis.portalis.commons.DOMUtil;
import fr.irisa.lis.portalis.shared.admin.ClientConstants;
import fr.irisa.lis.portalis.shared.admin.Err;
import fr.irisa.lis.portalis.shared.admin.data.ActiveLisService;
import fr.irisa.lis.portalis.shared.admin.reponse.VoidReponse;

@SuppressWarnings("serial")
public class StartReponse extends VoidReponse implements CamelisReponseObject {

	@SuppressWarnings("unused")private static final Logger LOGGER = LoggerFactory.getLogger(StartReponse.class.getName());

	private ActiveLisService ActiveLisService;

	public StartReponse() {
		super();
	}

	public StartReponse(ActiveLisService ActiveLisService) {
		super();
		this.ActiveLisService = ActiveLisService;
	}

	public StartReponse(Err err) {
		super(err);
	}

	@Override
	public boolean equals(Object aThat) {
		if (this == aThat)
			return true;

		if (!(aThat instanceof StartReponse))
			return false;
		// you may prefer this style, but see discussion in Effective Java
		// if ( aThat == null || aThat.getClass() != this.getClass() ) return
		// false;

		final StartReponse that = (StartReponse) aThat;
		return new EqualsBuilder().appendSuper(super.equals(aThat))
				.append(this.ActiveLisService, that.ActiveLisService).isEquals();
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder(17, 31)
				. // two randomly chosen prime numbers
				appendSuper(super.hashCode()).append(ActiveLisService)
				.toHashCode();
	}

	@Override
	public <T> T accept(CamelisReponseWriterInterface<T> visitor) {
		return visitor.visit(this);
	}

	@Override
	public String toString() {
		return DOMUtil.prettyXmlString(ClientConstants.CAMELIS_REPONSE_XML_WRITER
						.visit(this));
	}

	public ActiveLisService getActiveLisService() {
		return ActiveLisService;
	}

	public void setActiveLisService(ActiveLisService ActiveLisService) {
		this.ActiveLisService = ActiveLisService;
	}

}

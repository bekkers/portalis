package fr.irisa.lis.portalis.shared.admin;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Precondition {

	@SuppressWarnings("unused")
	private static final Logger LOGGER = LoggerFactory.getLogger(Precondition.class.getName());
	
	private List<String> messages = new ArrayList<String>();
	
	private String[][] httpReqArgs;
	
	public Precondition() {}

	public boolean checkJavaArg(boolean cond, String format, Object... params) {
		if (!cond) {
			this.messages.add(String.format(format, params));
		}
		return cond;
	}

	public int checkIntArg(String number, String format, Object... params) {
		int n = 0;
		try {
			n = Integer.parseInt(number);
		} catch (NumberFormatException e) {
			this.messages.add(String.format(format, params));
		}
		return n;
	}


	public List<String> getMessages() {
		return this.messages;
	}

	public void setMessages(List<String> messages) {
		this.messages = messages;
	}
	
	public boolean isOk() {
		return messages.size()==0;
	}
	
	public void addMessage(String format, Object... params) {
		this.messages.add(String.format(format, params));
	}
	
	public String toString() {
		throw new UnsupportedOperationException("use method getMessagesAsString() instead");
	}

	public void setHttpReqArgs(String[][] args) {
		this.httpReqArgs = args;
		for (String[] couple : args) {
			checkJavaArg(couple[1]!=null, "Parameter "+couple[0]+" is missing");
		}
	}
	
	public String[][] getHttpReqArgs() {
		return this.httpReqArgs;
	}

	
	public String getMessagesAsString() {
		StringBuffer buff = new StringBuffer();
		for (String mess : messages) {
			buff.append("\n").append(mess);
		}
		return buff.toString();
	}
	
	public String getArgsAsLinuxCommandString() {
		StringBuffer buff = new StringBuffer();
		for (String[] couple : httpReqArgs) {
			buff.append(" -");
			buff.append(couple[0]);
			buff.append(" ");
			buff.append(couple[1]);
		}
		return buff.toString();
	}

	public String getArgsAsHttpRequestString() throws UnsupportedEncodingException {
		StringBuffer buff = new StringBuffer();
		int i = 0;
		for (String[] couple : httpReqArgs) {
			buff.append(i==0?"?":"&");
			buff.append(couple[0]);
			buff.append("=");
			buff.append(URLEncoder.encode(couple[1],"UTF-8"));
			i++;
		}
		return buff.toString();
	}

}

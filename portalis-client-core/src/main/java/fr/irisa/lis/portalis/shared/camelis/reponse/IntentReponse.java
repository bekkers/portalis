package fr.irisa.lis.portalis.shared.camelis.reponse;

import com.flipthebird.gwthashcodeequals.EqualsBuilder;
import com.flipthebird.gwthashcodeequals.HashCodeBuilder;

import fr.irisa.lis.portalis.commons.DOMUtil;
import fr.irisa.lis.portalis.shared.admin.ClientConstants;
import fr.irisa.lis.portalis.shared.admin.Err;
import fr.irisa.lis.portalis.shared.admin.data.Property;
import fr.irisa.lis.portalis.shared.admin.reponse.LisReponse;
import fr.irisa.lis.portalis.shared.camelis.CamelisXmlName;
import fr.irisa.lis.portalis.shared.camelis.data.LisIntent;


@SuppressWarnings("serial")
public class IntentReponse extends LisReponse implements CamelisReponseObject {

	protected LisIntent intent = new LisIntent();
	private final String xmlName = CamelisXmlName.INTENT_REPONSE;
	
	public IntentReponse() {
		super();
	}

	public IntentReponse(Err err) {
		super(err);
	}

    public void setIntent(LisIntent intent) {
		this.intent = intent;
	}

	public void setIntent(Property[] props) {
		this.intent = new LisIntent(props);		
	}
	
	public LisIntent getIntent() {
		return intent;
	}
	
	@Override
	public String toString() {
		return DOMUtil.prettyXmlString(ClientConstants.CAMELIS_REPONSE_XML_WRITER.visit(this));
	}

	@Override
	public <T> T accept(CamelisReponseWriterInterface<T> visitor) {
		return visitor.visit(this);
	}

	@Override
	public boolean equals(Object aThat) {
		if (this == aThat)
			return true;

		if (!(aThat instanceof IntentReponse))
			return false;
		// you may prefer this style, but see discussion in Effective Java
		// if ( aThat == null || aThat.getClass() != this.getClass() ) return
		// false;

		final IntentReponse that = (IntentReponse) aThat;
		return new EqualsBuilder().appendSuper(super.equals(aThat))
				.append(this.intent, that.intent).isEquals();
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder(17, 31). // two randomly chosen prime numbers
				appendSuper(super.hashCode()).append(intent).toHashCode();
	}

	public String getXmlName() {
		return xmlName;
	}


}

package fr.irisa.lis.portalis.shared.camelis.data;

import com.flipthebird.gwthashcodeequals.EqualsBuilder;
import com.flipthebird.gwthashcodeequals.HashCodeBuilder;

import fr.irisa.lis.portalis.commons.DOMUtil;
import fr.irisa.lis.portalis.shared.admin.ClientConstants;


@SuppressWarnings("serial")
public class PropertyTree implements CamelisDataObject {

	@Override
	public String toString() {
		return DOMUtil.prettyXmlString(ClientConstants.CAMELIS_DATA_XML_WRITER.visit(this));
	}


	@Override
	public <T> T accept(CamelisDataWriterInterface<T> visitor) {
		return visitor.visit(this);
	}

	private String feature;
	private int supp;
	private PropertyTree[] children;
	private String oids;
	
	public PropertyTree() {}

	@Override
	public boolean equals(Object aThat){
		  if ( this == aThat ) return true;

		  if ( !(aThat instanceof PropertyTree) ) return false;
		  //you may prefer this style, but see discussion in Effective Java
		  //if ( aThat == null || aThat.getClass() != this.getClass() ) return false;

		  final PropertyTree that = (PropertyTree)aThat;
			return new EqualsBuilder()
			.appendSuper(super.equals(aThat))
			.append(this.feature, that.feature)
			.append(this.supp, that.supp)
			.append(this.children, that.children)
			.append(this.oids, that.oids)
			.isEquals();
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder(17, 31). // two randomly chosen prime numbers
				appendSuper(super.hashCode()).
				append(feature).
				append(supp).
				append(children).
				append(oids).
				toHashCode();
	}

	public int getSupp() {
		return supp;
	}

	public String getOids() {
		return oids;
	}

	public PropertyTree[] getChildren() {
		return children;
	}

	public void setSupp(int supp) {
		this.supp = supp;
	}

	public void setChildren(PropertyTree[] children) {
		this.children = children;
	}

	public void setOids(String oids) {
		this.oids = oids;
	}

	public void setFeature(String feature) {
		this.feature = feature;
	}

	public String getFeature() {
		return feature;
	}

}

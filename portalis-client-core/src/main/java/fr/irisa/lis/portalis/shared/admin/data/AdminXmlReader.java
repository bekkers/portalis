package fr.irisa.lis.portalis.shared.admin.data;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import fr.irisa.lis.portalis.commons.DOMReader;
import fr.irisa.lis.portalis.commons.DOMUtil;
import fr.irisa.lis.portalis.shared.admin.PortalisException;
import fr.irisa.lis.portalis.shared.admin.RightValue;
import fr.irisa.lis.portalis.shared.admin.Util;
import fr.irisa.lis.portalis.shared.admin.XmlIdentifier;
import fr.irisa.lis.portalis.shared.admin.reponse.VoidReponse;

public class AdminXmlReader extends DOMReader implements
		AdminReaderInterface<Element> {
	static final Logger LOGGER = LoggerFactory.getLogger(AdminXmlReader.class
			.getName());

	static private AdminXmlReader instance;

	public static AdminXmlReader getInstance() {
		if (instance == null) {
			instance = new AdminDataXmlReader();
		}
		return instance;
	}

	protected AdminXmlReader() {
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see fr.irisa.lis.portalis.client.dom4j.reader.XmlReaderInterface#
	 * getMessageElements(org.dom4j.Element)
	 */
	@Override
	public List<String> getMessageElements(Element root)
			throws PortalisException {
		String name = XmlIdentifier.MESSAGE();
		isValidName(name);
		List<String> reponse = new ArrayList<String>();
		NodeList elems = root.getElementsByTagName(name);
		if (elems == null || elems.getLength() == 0) {
			String mess = String.format(
					"manque les éléments fils <%s> au sein de l'élément <%s>",
					name, DOMUtil.prettyXmlString(root));
			LOGGER.error(mess);
			throw new PortalisException(mess);
		} else {
			for (int i = 0; i < elems.getLength(); i++) {
				Element elem = (Element) elems.item(i);
				reponse.add(getTextValue(elem));
			}
		}
		return reponse;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see fr.irisa.lis.portalis.client.dom4j.reader.XmlReaderInterface#
	 * getRightValueAttribute(org.dom4j.Element, java.lang.String)
	 */
	@Override
	public RightValue getRightValueAttribute(Element root, String name)
			throws PortalisException {
		return getRightValueAttribute(root, name, false);
	}

	public RightValue getRightValueAttribute(Element root, String name,
			boolean isOptional) throws PortalisException {
		RightValue reponse = null;
		String role = getAttribute(root, name);
		if (role == null) {
			if (isOptional)
				return null;
			String mess = String.format(
					"manque l'attribut '%s' au sein de l'élément %s", name,
					DOMUtil.prettyXmlString(root));
			LOGGER.error(mess);
			throw new PortalisException(mess);
		}
		try {
			reponse = RightValue.valueOf(role.toUpperCase());
		} catch (IllegalArgumentException e) {
			throw new PortalisException("Role incorrect : " + role);
		}
		return reponse;
	}

	protected LoginErr getLoginErrAttribute(Element root, String name)
			throws PortalisException {
		LoginErr reponse = null;
		String status = getAttribute(root, name);
		if (status == null) {
			String mess = String.format(
					"manque l'attribut '%s' au sein de l'élément %s", name,
					DOMUtil.prettyXmlString(root));
			LOGGER.error(mess);
			throw new PortalisException(mess);
		}
		try {
			reponse = LoginErr.valueOf(status);
		} catch (IllegalArgumentException e) {
			throw new PortalisException("Role incorrect : " + status);
		}
		return reponse;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see fr.irisa.lis.portalis.client.dom4j.reader.XmlReaderInterface#
	 * checkReponseElement(org.dom4j.Element, java.lang.String,
	 * fr.irisa.lis.portalis.client.dom4j.reponse.VoidReponse)
	 */
	@Override
	public boolean checkReponseElement(Element root, String name,
			VoidReponse reponse) {
		LOGGER.debug("Checked ReponseElement is :\n"
				+ DOMUtil.prettyXmlString(root));
		checkElement(root, name);
		String state = getAttribute(root, XmlIdentifier.STATUS());
		if (state != null) {
			reponse.setStatus(state);
			if (state.length() == 0) {
				String mess = "checkReponseElement() attribut "
						+ XmlIdentifier.STATUS() + "=''";
				LOGGER.warn(mess);
				reponse.addMessage(mess);
				return false;
			} else if (state.equals(XmlIdentifier.OK)) {
				return true;
			} else if (state.equals(XmlIdentifier.ERROR)) {
				String mess = getAttribute(root, XmlIdentifier.MESSAGE());
				if (mess != null) {
					// le message est un attribut
					reponse.addMessage(mess);
				} else {
					// les messages sont des éléments
					try {
						reponse.setMessages(getMessageElements(root));
					} catch (PortalisException e) {
						String mes = "Message d'erreur vide !";
						LOGGER.warn(mes);
						reponse.addMessage(mes);
						return false;
					}
				}
				LOGGER.debug("fin checkReponseElement(\n"
						+ DOMUtil.prettyXmlString(root)
						+ ") erreurs détectées, " + "type = "
						+ reponse.getClass().getCanonicalName() + " reponse = "
						+ reponse.toString());
				return false;
			} else {
				String mess = String.format(
						"incorrect %s = %s, élément en cours de lecture = %s",
						XmlIdentifier.STATUS(), state,
						DOMUtil.prettyXmlString(root));
				LOGGER.error(mess);
				reponse.addMessage(mess);
				return false;
			}
		} else if (root.getNodeName().equals(XmlIdentifier.ERROR)) {
			// pas d'attribut status est equivalent à status OK
			String mess = getAttribute(root, XmlIdentifier.MESSAGE());
			LOGGER.error(mess);
			reponse.addMessage(mess);
			return false;
		} else {
			return true;
		}
	}

	public Date getDateElement(Element root, String name)
			throws PortalisException {
		isValidName(name);
		String value = getStringElement(root, name);
		return Util.readDate(value);
	}

	public Date getDateAttribute(Element root, String name)
			throws PortalisException {
		isValidName(name);
		String value = getStringAttribute(root, name);
		return Util.readDate(value);
	}

}

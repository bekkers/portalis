package fr.irisa.lis.portalis.shared.admin.reponse;




public interface AdminReponseWriterInterface<T> {
	
	public T visit(LisReponse lisReponse);
	public T visit(SiteReponse siteReponse);
	public T visit(ActiveSiteReponse siteReponse);
	public T visit(PidReponse pidReponse);
	public T visit(LoginReponse loginReponse);
	public T visit(PortsActifsReponse portsActifsReponse);
	public T visit(LogReponse logReponse);
	public T visit(VersionReponse implementationVersionReponse);
	public T visit(VoidReponse voidReponse);
	public T visit(BooleanReponse booleanReponse);
	public T visit(ServiceCoreReponse serviceCoreReponse);
}

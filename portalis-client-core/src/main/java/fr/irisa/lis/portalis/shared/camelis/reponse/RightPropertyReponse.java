package fr.irisa.lis.portalis.shared.camelis.reponse;

import java.util.Date;

import fr.irisa.lis.portalis.commons.DOMUtil;
import fr.irisa.lis.portalis.shared.admin.ClientConstants;
import fr.irisa.lis.portalis.shared.admin.Err;
import fr.irisa.lis.portalis.shared.admin.RightValue;
import fr.irisa.lis.portalis.shared.admin.data.Role;
import fr.irisa.lis.portalis.shared.admin.reponse.LisReponse;

@SuppressWarnings("serial")
public class RightPropertyReponse extends LisReponse implements CamelisReponseObject   {

	private Role rightProperty;
	private Date expire;

	public RightPropertyReponse() {
		super();
	}

	public RightPropertyReponse(String userKey, String serviceKey, RightValue right, Date expire) {
		super();
		rightProperty = new Role(userKey, serviceKey, right);
		this.expire = expire;
	}

	public RightPropertyReponse(Err err) {
		super(err);
	}
	
	@Override
	public String toString() {
		return DOMUtil.prettyXmlString(ClientConstants.CAMELIS_REPONSE_XML_WRITER.visit(this));
	}

	@Override
	public <T> T accept(CamelisReponseWriterInterface<T> visitor) {
		return visitor.visit(this);
	}

	public Date getExpire() {
		return expire;
	}

	public void setExpire(Date expire) {
		this.expire = expire;
	}

	public Role getRightProperty() {
		return rightProperty;
	}

	public void setRightProperty(Role rightProperty) {
		this.rightProperty = rightProperty;
	}


}

package fr.irisa.lis.portalis.shared.camelis.reponse;

import com.flipthebird.gwthashcodeequals.EqualsBuilder;
import com.flipthebird.gwthashcodeequals.HashCodeBuilder;

import fr.irisa.lis.portalis.commons.DOMUtil;
import fr.irisa.lis.portalis.shared.admin.ClientConstants;
import fr.irisa.lis.portalis.shared.admin.Err;
import fr.irisa.lis.portalis.shared.admin.reponse.LisReponse;
import fr.irisa.lis.portalis.shared.camelis.data.LisIncrement;
import fr.irisa.lis.portalis.shared.camelis.data.LisIncrementSet;

@SuppressWarnings("serial")
public class ZoomReponse extends LisReponse implements CamelisReponseObject {

	private LisIncrementSet increments = new LisIncrementSet();
	private int nbObjects;
	private int nbIncrs;
	private String newWorkingQuery;
	
	public ZoomReponse() {
		super();
	}

	public ZoomReponse(Err err) {
		super(err);
	}
	
	@Override
	public boolean equals(Object aThat) {
		if (this == aThat)
			return true;

		if (!(aThat instanceof ZoomReponse))
			return false;
		// you may prefer this style, but see discussion in Effective Java
		// if ( aThat == null || aThat.getClass() != this.getClass() ) return
		// false;

		final ZoomReponse that = (ZoomReponse) aThat;
		return new EqualsBuilder().appendSuper(super.equals(aThat))
				.append(this.increments, that.increments)
				.append(this.nbObjects, that.nbObjects)
				.append(this.nbIncrs, that.nbIncrs)
				.append(this.newWorkingQuery, that.newWorkingQuery)
				.isEquals();
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder(17, 31). // two randomly chosen prime numbers
				appendSuper(super.hashCode())
				.append(increments)
				.append(nbObjects)
				.append(nbIncrs)
				.append(newWorkingQuery)
				.toHashCode();
	}

	@Override
	public String toString() {
		return DOMUtil.prettyXmlString(ClientConstants.CAMELIS_REPONSE_XML_WRITER
						.visit(this));
	}

	@Override
	public <T> T accept(CamelisReponseWriterInterface<T> visitor) {
		return visitor.visit(this);
	}

	public LisIncrementSet getIncrements() {
		return increments;
	}

	public void setIncrements(LisIncrementSet increments) {
		this.increments = increments;
	}

	public void setIncrements(LisIncrement[] increments) {
		this.increments = new LisIncrementSet(increments);
	}

	public int getNbObjects() {
		return nbObjects;
	}

	public void setNbObjects(int n) {
		this.nbObjects = n;
	}

	public int getNbIncrs() {
		return nbIncrs;
	}

	public void setNbIncrs(int n) {
		this.nbIncrs = n;
	}

	public String getNewWorkingQuery() {
		return newWorkingQuery;
	}

	public void setNewWorkingQuery(String q) {
		this.newWorkingQuery = q;
	}

}

package fr.irisa.lis.portalis.shared.admin;

import java.util.ArrayList;
import java.util.List;

import fr.irisa.lis.portalis.commons.core.CommonsUtil;

public class Err {

	private List<String> errMessages = new ArrayList<String>();

	public Err(String mess) {
		errMessages.add(mess);
	}

	public Err(String mess1, String mess2) {
		errMessages.add(mess1);
		errMessages.add(mess2);
	}

	public Err(List<String> listMessages) {
		for (String message : listMessages) {
			errMessages.add(message);
		}
	}

	public Err(Throwable t) {
		errMessages.add(CommonsUtil.stack2string(t));
	}

	public Err(String mess, Throwable t) {
		errMessages.add(mess);
		errMessages.add(CommonsUtil.stack2string(t));
	}

	public List<String> getErrMessages() {
		return errMessages;
	}
}

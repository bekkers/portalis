package fr.irisa.lis.portalis.shared.admin;

public class ErrorMessages {

	public static final String TEMOIN_IS_NULL = "ne doit pas être null";
	public static final String TEMOIN_UNKNOWN_OPTION_CREATOR = "Unknown option -creator";
	public static final String TEMOIN_ERROR_DURING_HTTP_REQUEST = "Error during http request";
	public static final String TEMOIN_CONNECTION_FAILURE = "connection failure";
	public static final String TEMOIN_PORT_INTEGER = "should be an integer";
	public static final String TEMOIN_PORT_POSITIF = "strickly positive integer";
	public static final String TEMOIN_IS_MISSING = "is missing";
	public static final String TEMOIN_IS_INCORRECT = "is incorrect";
	public static final String TEMOIN_IS_NOT_REGISTERED = "is not registered";
	public static final String TEMOIN_IS_ALREADY_REGISTERED = "is already registered";
	public static final String TEMOIN_IS_ALREADY_LOGGED = " is already logged ";
	public static final String TEMOIN_SA_VALEUR_EST = "sa valeur est";
	public static final String TEMOIN_NO_CAMELIS_SERVER_LISTENING = "Pas de service Lis actif";
	public static final String TEMOIN_INCORRECT_SERVICE_NAME = "incorrect service name";
	public static final String TEMOIN_ALREADY_IN_USE = "the required port is already in use";
	public static final String TEMOIN_CONNECTION_STILL_ALLOCATED = "connection still allocated";
	public static final String TEMOIN_NO_USER_LOGGED_IN = "Sorry no user logged in";
	public static final String TEMOIN_PERMISSION_DENIED = "permission denied for user";
	public static final String TEMOIN_UNKNOWN_SERVICE_NAME_FOR_APPLICATION = " unknown service name for the application : ";
	public static final String TEMOIN_UNKNOWN_APPLICATION_NAME = "Unknown application name : ";
}

package fr.irisa.lis.portalis.shared.admin.http;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.ProtocolException;
import java.net.URL;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Element;

import fr.irisa.lis.portalis.commons.DOMUtil;
import fr.irisa.lis.portalis.commons.core.CommonsUtil;
import fr.irisa.lis.portalis.shared.admin.ClientConstants;
import fr.irisa.lis.portalis.shared.admin.PortalisException;
import fr.irisa.lis.portalis.shared.admin.XmlIdentifier;
import fr.irisa.lis.portalis.shared.admin.data.ActiveUser;
import fr.irisa.lis.portalis.shared.admin.data.AdminDataXmlWriter;
import fr.irisa.lis.portalis.shared.admin.data.AdminXmlReader;
import fr.irisa.lis.portalis.shared.admin.data.PortalisService;

public class HttpPortalisUrlConnection extends AbstractHttpUrlConnection {
	private static final Logger LOGGER = LoggerFactory
			.getLogger(HttpPortalisUrlConnection.class.getName());
	
	private static String DEFAULT_ENCODING = CommonsUtil.DEFAULT_ENCODING;


	public Element doFirstConnection(String cmd) throws PortalisException {
		return doFirstConnection(cmd, new String[0][2]);
	}

	public Element doFirstConnection(String cmd, String[][] httpReqArgs)
			throws PortalisException {
		String portalisCmd = PortalisService.getInstance().getAppliName()
				+ (cmd.startsWith("/") ? cmd : "/" + cmd);
		URL url = buildUrl(PortalisService.getInstance().getHost(),
				PortalisService.getInstance().getPort(), portalisCmd,
				httpReqArgs);
		return firstConnect(url);
	}

	public Element doConnection(ActiveUser activeUser, String cmd)
			throws PortalisException {
		return doConnection(activeUser, cmd, new String[0][2]);
	}

	public Element doConnection(ActiveUser activeUser, String cmd,
			String[][] httpReqArgs) throws PortalisException {
		if (activeUser.getCookies() == null
				|| activeUser.getCookies().size() == 0) {
			String mess = new StringBuffer("Connection '").append(cmd)
					.append("' impossible pour l'utilisateur ")
					.append(activeUser.getUserCore().getEmail())
					.append(", il n'a pas de cookies de session http")
					.toString();
			LOGGER.warn(mess);
			throw new PortalisException(mess);
		}
		String portalisCmd = PortalisService.getInstance().getAppliName()
				+ (cmd.startsWith("/") ? cmd : "/" + cmd);
		URL url = buildUrl(PortalisService.getInstance().getHost(),
				PortalisService.getInstance().getPort(), portalisCmd,
				httpReqArgs);
		Element result = reconnect(activeUser, url);
		return result;
	}

	/**
	 * Returns the output from the given URL.
	 * 
	 * I tried to hide some of the ugliness of the exception-handling in this
	 * method, and just return a high level Exception from here.
	 * 
	 * @param desiredUrl
	 * @return
	 * @throws PortalisException
	 */
	public static Element reconnect(ActiveUser activeUser, URL url)
			throws PortalisException {
		LOGGER.info("PortalisCtx -------------- > @reconnect "
				+ activeUser.getUserCore().getEmail() + " : " + url.toString());
		return connect(activeUser, url, DEFAULT_ENCODING);
	}

	private static Element firstConnect(URL url) throws PortalisException {
		LOGGER.info("PortalisCtx -------------- > @firstConnect "
				+ url.toString());
		return connect(null, url, DEFAULT_ENCODING);
	}

	public static void bip(final ActiveUser activeUser) {
		Thread t = new Thread() {
		    public void run() {
				URL targetURL = null;
				try {
					targetURL = buildUrl(PortalisService.getInstance().getHost(),
							PortalisService.getInstance().getPort(), PortalisService.getInstance().getAppliName()+"/bip.jsp",
							new String[0][2]);
					reconnect(activeUser, targetURL);
				} catch (Exception e) {
					String mess = "Error while bipping server request is "
							+ targetURL.toString();
					LOGGER.error(mess, e);
				}
		    }
		};
		t.start();
	}

	public static Element connect(ActiveUser activeUser, URL url, String charset)
			throws PortalisException {
		if (ClientConstants.isPrintHttpRequest()) {
			System.out.println("\n" + url + "\n");
		}
		BufferedReader reader = null;
		StringBuilder stringBuilder;
		String result = null;
		HttpURLConnection connection = null;
		try {
			connection = prepareConnexion(url);

			if (activeUser != null) {
				getCookiesFromUser(connection, activeUser);
			}

			connection.connect();

			int responseCode = connection.getResponseCode();
			LOGGER.info("Http responseCode = " + responseCode);
			if (responseCode != 200) {
				String mess = String
						.format("portalisConnect failure during http request, error code is : %d, error message is : %s, url is : \n%s",
								responseCode, connection.getResponseMessage(),
								url.toString());
				LOGGER.error(mess);
				throw new PortalisException(mess);
			}

			stringBuilder = new StringBuilder();
			try {
				// read the output from the server
				reader = new BufferedReader(new InputStreamReader(
						connection.getInputStream(), charset));

				String line = null;
				while ((line = reader.readLine()) != null) {
					stringBuilder.append(line + "\n");
				}
			} catch (Exception e) {
				String mess = "Error reading http response inputStream, http requête is : "
						+ url.toString();
				LOGGER.error(mess, e);
				throw new PortalisException(mess, e);
			}

			result = stringBuilder.toString();
		} catch (Exception e) {
			String mess = "Error getting http response, http requête is : "
					+ url.toString();
			LOGGER.error(mess, e);
			throw new PortalisException(mess, e);
		}

		Element resultElem = null;
		try {
			resultElem = DOMUtil.parseXMLString(result);
		} catch (Exception e) {
			String mess = "Error parsing http response, http requête is : "
					+ url.toString() + "\nstring read is :\n" + result;
			LOGGER.error(mess, e);
			throw new PortalisException(mess, e);
		}
		LOGGER.debug(" resultElem =\n"
				+ DOMUtil.prettyXmlString(resultElem));
		String ok = resultElem.getAttribute(XmlIdentifier.STATUS());
		if (ok.equals(XmlIdentifier.OK) && activeUser == null) {
			setConnexionCookiesToUser(resultElem, connection);
		}

		LOGGER.info("\nPortalisCtx @fin portalisConnect "
				+ resultElem.getNodeName()
				+ (ok.equals(XmlIdentifier.ERROR) ? resultElem
						.getAttribute(XmlIdentifier.MESSAGE()) : ""));
		return resultElem;
	}

	private static void setConnexionCookiesToUser(Element root,
			HttpURLConnection connection) throws PortalisException {
		// Gather all cookies on the first request.
		Map<String, List<String>> headers = connection.getHeaderFields();
		List<String> cookies = headers.get("Set-Cookie");
		if (cookies != null) {
			Element userElem = AdminXmlReader.getInstance().getChildElement(
					root, XmlIdentifier.ACTIF_USER());
			for (String cookie : cookies) {
				LOGGER.debug(String.format(
						"portalisConnect() saving Cookie : %s", cookie));
				AdminDataXmlWriter.addTextChild(userElem,
						XmlIdentifier.COOKIE(), cookie);
			}
		} else {
			String errMess = String
					.format("Internal error : no 'Set-Cookie' cookies returned on a login request, cookies are :\n%s",
							cookiesToString(headers));
			LOGGER.error(errMess);
			throw new PortalisException(errMess);
		}
	}

	private static void getCookiesFromUser(HttpURLConnection connection,
			ActiveUser activeUser) throws PortalisException {
		// Use the same cookies as the ones found in the first one.
		List<String> cookies = activeUser.getCookies();
		if (cookies.size() < 1) {
			String mess = "this user has no recorded Portalis session "
					+ activeUser.getPortalisSessionId();
			LOGGER.error(mess);
			throw new PortalisException(mess);
		}
		for (String cookie : cookies) {
			LOGGER.debug(String.format(
					"Setting Cookie before connection of %s : %s", activeUser
							.getUserCore().getEmail(), cookie));
			connection.addRequestProperty("Cookie", cookie.split(";", 2)[0]);
		}
	}

	private static HttpURLConnection prepareConnexion(URL url)
			throws IOException, ProtocolException {
		// create the HttpURLConnection
		HttpURLConnection connection = (HttpURLConnection) url.openConnection();
		// just want to do an HTTP GET here
		connection.setRequestMethod("GET");

		// uncomment this if you want to write output to this url
		// connection.setDoOutput(true);

		// give it 15 seconds to respond
		connection.setReadTimeout(15 * 1000);
		return connection;
	}

	private static String cookiesToString(Map<String, List<String>> headers) {
		StringBuffer buff = new StringBuffer();
		for (String key : headers.keySet()) {
			List<String> cookies = headers.get(key);
			buff.append("\n   . ")
					.append(key)
					.append(" : ")
					.append(cookies == null ? "null" : Arrays.toString(cookies
							.toArray(new String[cookies.size()])));
		}
		return buff.append("\n").toString();
	}

}

package fr.irisa.lis.portalis.shared.admin.data;

import java.util.Map;
import java.util.Set;

import fr.irisa.lis.jquery.TreetableNode;
import fr.irisa.lis.portalis.shared.admin.PortalisException;
import fr.irisa.lis.portalis.shared.admin.RightValue;

public interface SiteInterface {

	/* (non-Javadoc)
	 * @see fr.irisa.lis.portalis.shared.admin.data.SiteInterface#getLesServices()
	 */
	public abstract Set<LisServiceCore> getLesServices();

	public abstract LisApplication[] getApplications();

	public abstract TreetableNode lisApplicationsToNode();

	public abstract LisApplication getApplication(String id);

	public abstract void addApplication(LisApplication appli);

	public abstract LisServiceCore[] getServices();

	public abstract LisServiceCore getService(String fullName)
			throws PortalisException;

	public abstract PasswordUser[] getUsers();

	public abstract PasswordUser getUser(String email);

	public abstract void addUser(PasswordUser user);

	public abstract void addRole(String email, String fullName, RightValue rightValue);

	public abstract Role[] getRoles();

	public abstract Role getRole(String email, String fullName);

	public abstract void addRole(Role role);

	public abstract Map<String, PasswordUser> getUsersMap();

	public abstract Map<String, Role> getRolesMap();

	public abstract Map<String, LisApplication> getApplicationsMap();

	public abstract <T> T accept(AdminDataWriterInterface<T> visitor);
	
	public abstract boolean equals(Object aThat);
	
	public abstract SparqlApplication getSparqlEndPoints();

	public abstract void setSparqlEndPoints(SparqlApplication sparqlEndPoints);

}
package fr.irisa.lis.portalis.shared.admin;




public class XmlIdentifier {

	private static ProprietesBase ioConstants = AdminProprietes.ioConstants;
	
	public final static String OK = AdminProprietes.ioConstants.getProperty("OK");
	public static final String ERROR = AdminProprietes.ioConstants.getProperty("ERROR");
	
	public static final String CAMELIS_SESSION_ID = "camelisSessionID";

	public static String STATUS() {
		return ioConstants.getProperty("STATUS");
	}

	public static String MESSAGE() {
		return ioConstants.getProperty("MESSAGE");
	}

	public static String BOOLEAN() {
		return ioConstants.getProperty("BOOLEAN");
	}

	public static String STRING() {
		return ioConstants.getProperty("STRING");
	}

	public static String INTEGER() {
		return ioConstants.getProperty("INTEGER");
	}

	public static String INTEGER_LIST() {
		return ioConstants.getProperty("INTEGER_LIST");
	}
	
	public static String KEY() {
		return ioConstants.getProperty("KEY");
	}

	public static String ID() {
		return ioConstants.getProperty("ID");
	}

	public static String NONE() {
		return ioConstants.getProperty("NONE");
	}

	public static String DATADIR() {
		return ioConstants.getProperty("DATADIR");
	}

	public static String LOG() {
		return ioConstants.getProperty("LOG");
	}

	public static String REPONSE() {
		return ioConstants.getProperty("REPONSE");
	}

	public static String PING_REPONSE() {
		return ioConstants.getProperty("PING_REPONSE");
	}

	public static String LIS_REPONSE() {
		return ioConstants.getProperty("LIS_REPONSE");
	}

	public static String SET_ROLE_REPONSE() {
		return ioConstants.getProperty("SET_ROLE_REPONSE");
	}

	public static String PING_USERS_REPONSE() {
		return ioConstants.getProperty("PING_USERS_REPONSE");
	}

	public static String IMPORT_CTX_REPONSE() {
		return ioConstants.getProperty("IMPORT_CTX_REPONSE");
	}

	public static String GET_TREE_REQ_REPONSE() {
		return ioConstants.getProperty("GET_TREE_REQ_REPONSE");
	}

	public static String SITE() {
		return ioConstants.getProperty("SITE");
	}

	public static String ACTIVE_SITE() {
		return ioConstants.getProperty("ACTIVE_SITE");
	}

	public static String LIS_APPLICATION() {
		return ioConstants.getProperty("LIS_APPLICATION");
	}

	public static String ACTIVE_APPLICATION() {
		return ioConstants.getProperty("ACTIVE_APPLICATION");
	}

	public static String APPLICATION_LIST() {
		return ioConstants.getProperty("APPLICATION_LIST");
	}

	public static String APPLICATION_ID() {
		return ioConstants.getProperty("APPLICATION_ID");
	}

	public static String SERVICE_CORE() {
		return ioConstants.getProperty("SERVICE_CORE");
	}

	public static String SERVICE_LIST() {
		return ioConstants.getProperty("SERVICE_LIST");
	}

	public static String SERVICE_NAME() {
		return ioConstants.getProperty("SERVICE_NAME");
	}

	public static String ACTIVE_SERVICE() {
		return ioConstants.getProperty("ACTIVE_SERVICE");
	}

	public static String ACTIVE_SERVICE_LIST() {
		return ioConstants.getProperty("ACTIVE_SERVICE_LIST");
	}

	public static String ACTIVE_SERVICE_ID() {
		return ioConstants.getProperty("SERVICE_ID");
	}

	public static String CREATOR() {
		return ioConstants.getProperty("CREATOR");
	}

	public static String PID() {
		return ioConstants.getProperty("PID");
	}

	public static String HOST() {
		return ioConstants.getProperty("HOST");
	}

	public static String CONTEXT_LOADED() {
		return ioConstants.getProperty("CONTEXT_LOADED");
	}

	public static String PORT() {
		return ioConstants.getProperty("PORT");
	}

	public static String NB_OBJECT() {
		return ioConstants.getProperty("NB_OBJECT");
	}
	
	public static String FEATURE() {
		return ioConstants.getProperty("FEATURE");
	}


	public static String LAST_UPDATE() {
		return ioConstants.getProperty("LAST_UPDATE");
	}

	public static String ACTIVATION_DATE() {
		return ioConstants.getProperty("ACTIVATION_DATE");
	}

	public static String USER() {
		return ioConstants.getProperty("USER");
	}

	public static String USER_LIST() {
		return ioConstants.getProperty("USER_LIST");
	}

	public static String EMAIL() {
		return ioConstants.getProperty("EMAIL");
	}

	public static String IS_GENERAL_ADMIN() {
		return ioConstants.getProperty("IS_GENERAL_ADMIN");
	}

	public static String PSEUDO() {
		return ioConstants.getProperty("PSEUDO");
	}

	public static String PASSWORD() {
		return ioConstants.getProperty("PASSWORD");
	}

	public static String MAX_INACTIVE_INTERVAL() {
		return ioConstants.getProperty("MAX_INACTIVE_INTERVAL");
	}

	public static String ROLE() {
		return ioConstants.getProperty("ROLE");
	}

	public static String PORTALIS_ADMIN_ROLE() {
		return ioConstants.getProperty("PORTALIS_ADMIN_ROLE");
	}

	public static String ROLES() {
		return ioConstants.getProperty("ROLES");
	}

	public static String ACTIF_USER() {
		return ioConstants.getProperty("ACTIF_USER");
	}

	public static String ACTIF_USER_LIST() {
		return ioConstants.getProperty("ACTIF_USER_LIST");
	}

	public static String ACTIF_USER_KEY() {
		return ioConstants.getProperty("ACTIF_USER_KEY");
	}

	public static String CLIENT_INFO() {
		return ioConstants.getProperty("CLIENT_INFO");
	}

	public static String IP_ADDRESS_S() {
		return ioConstants.getProperty("IP_ADDRESS_S");
	}

	public static String ACTIF_ADMIN_KEY() {
		return ioConstants.getProperty("ACTIF_ADMIN_KEY");
	}

	public static String CURRENT_ACTIF_SERVICE_ID() {
		return ioConstants.getProperty("CURRENT_ACTIF_SERVICE_ID");
	}

	public static String PORT_ACTIF() {
		return ioConstants.getProperty("PORT_ACTIF");
	}

	public static String PORT_ACTIF_REPONSE() {
		return ioConstants.getProperty("PORT_ACTIF_REPONSE");
	}

	public static String START_REPONSE() {
		return ioConstants.getProperty("START_REPONSE");
	}

	public static String PID_REPONSE() {
		return ioConstants.getProperty("PID_REPONSE");
	}

	public static String VOID_REPONSE() {
		return ioConstants.getProperty("VOID_REPONSE");
	}

	public static String CAMELIS() {
		return ioConstants.getProperty("CAMELIS");
	}

	public static String SESSION() {
		return ioConstants.getProperty("SESSION");
	}

	public static String LOGIN_REPONSE() {
		return ioConstants.getProperty("LOGIN_REPONSE");
	}

	public static String VALUE() {
		return ioConstants.getProperty("VALUE");
	}

	public static String SESSION_ID() {
		return ioConstants.getProperty("SESSION_ID");
	}

	public static String VERSION() {
		return ioConstants.getProperty("VERSION");
	}

	public static String VERSION_REPONSE() {
		return ioConstants.getProperty("VERSION_REPONSE");
	}

	public static String JAVA_PID() {
		return ioConstants.getProperty("JAVA_PID");
	}

	public static String PORTALIS_SERVICE() {
		return ioConstants.getProperty("PORTALIS_SERVICE");
	}

//	public static String CAMELIS_SERVICE() {
//		return ioConstants.getProperty("CAMELIS_SERVICE");
//	}

	public static String CAMELIS_SERVICE_SHORT() {
		return ioConstants.getProperty("CAMELIS_SERVICE_SHORT");
	}

	public static String SITE_REPONSE() {
		return ioConstants.getProperty("SITE_REPONSE");
	}

	public static String ACTIVE_SITE_REPONSE() {
		return ioConstants.getProperty("ACTIVE_SITE_REPONSE");
	}

	public static String LOG_REPONSE() {
		return ioConstants.getProperty("LOG_REPONSE");
	}

	public static String LINE() {
		return ioConstants.getProperty("LINE");
	}

	public static String COOKIE() {
		return ioConstants.getProperty("COOKIE");
	}

	public static String LOGIN_RESULT() {
		return ioConstants.getProperty("LOGIN_RESULT");
	}

	public static String STANDARD_USER() {
		return ioConstants.getProperty("STANDARD_USER");
	}

	public static String ANONYMOUS_USER() {
		return ioConstants.getProperty("ANONYMOUS_USER");
	}

	public static String SUPER_USER() {
		return ioConstants.getProperty("SUPER_USER");
	}

	public static String ANSWER() {
		return ioConstants.getProperty("ANSWER");
	}

	public static String BOOLEAN_REPONSE() {
		return ioConstants.getProperty("BOOLEAN_REPONSE");
	}

	public static String HAS_PICTURE() {
		return ioConstants.getProperty("HAS_PICTURE");
	}

	public static String SERVICE_CORE_REPONSE() {
		return ioConstants.getProperty("SERVICE_CORE_REPONSE");
	}

	public static String UI_PREFERENCE() {
		return ioConstants.getProperty("UI_PREFERENCE");
	}

	public static String ENTRY_SHOWN() {
		return ioConstants.getProperty("ENTRY_SHOWN");
	}

	public static String MINIMUM_SUPPORT() {
		return ioConstants.getProperty("MINIMUM_SUPPORT");
	}

	public static String HIDDEN_ATTRIBUTE() {
		return ioConstants.getProperty("HIDDEN_ATTRIBUTE");
	}

	public static String NAME() {
		return ioConstants.getProperty("NAME");
	}

	public static String EXTRA_COLUMN() {
		return ioConstants.getProperty("EXTRA_COLUMN");
	}

	public static String SPARQL_APPLICATION() {
		return ioConstants.getProperty("SPARQL_APPLICATION");
	}

	public static String SPARQL_SERVICE() {
		return ioConstants.getProperty("SPARQL_SERVICE");
	}

	public static String QUERY() {
		return ioConstants.getProperty("QUERY");
	}

	public static String PARAM() {
		return ioConstants.getProperty("PARAM");
	}

	public static String URL() {
		return ioConstants.getProperty("URL");
	}

	public static String LABEL() {
		return ioConstants.getProperty("LABEL");
	}

	public static String REQUEST_LANGUAGE() {
		return ioConstants.getProperty("REQUEST_LANGUAGE");
	}


}

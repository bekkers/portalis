package fr.irisa.lis.portalis.shared.camelis.data;

import java.io.Serializable;


public abstract interface CamelisDataObject extends Serializable {

	public abstract <T> T accept(CamelisDataWriterInterface<T> visitor);
	
	@Override
	public abstract String toString();

}

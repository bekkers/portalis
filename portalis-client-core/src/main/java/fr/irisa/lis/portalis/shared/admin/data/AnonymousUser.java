package fr.irisa.lis.portalis.shared.admin.data;

import fr.irisa.lis.portalis.commons.DOMUtil;
import fr.irisa.lis.portalis.shared.admin.ClientConstants;

@SuppressWarnings("serial")
public class AnonymousUser extends UserData {
	
	private static String ANONYMOUS = "anonymous";
	
	private static long next = 0;

	@Override
	public String toString() {
		return DOMUtil.prettyXmlString(ClientConstants.PORTALIS_DATA_XML_WRITER.visit(this));
	}

	@Override
	public <T> T accept(AdminDataWriterInterface<T> visitor) {
		return visitor.visit(this);
	}
	
	public AnonymousUser() {
		super(ANONYMOUS+next+"@irisa.fr",ANONYMOUS+next);
		next++;
	}

	public AnonymousUser(String pseudo) {
		super(pseudo+"@irisa.fr",pseudo);

	}

	@Override
	public boolean isGeneralAdmin() {
		return false;
	}


}

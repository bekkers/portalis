package fr.irisa.lis.portalis.shared.admin.data;

import java.util.HashMap;

import com.flipthebird.gwthashcodeequals.EqualsBuilder;
import com.flipthebird.gwthashcodeequals.HashCodeBuilder;

import fr.irisa.lis.portalis.commons.DOMUtil;
import fr.irisa.lis.portalis.shared.admin.ClientConstants;
import fr.irisa.lis.portalis.shared.admin.PortalisException;

@SuppressWarnings("serial")
public class Query implements AdminDataObject {

	private static final String INT_TYPE = "xs:int ";
	private RequestLanguage reqLanguage = RequestLanguage.NONE;
	private HashMap<String, String> param = new HashMap<String, String>();
	private String id;
	private String label;
	private String query;


	public Query() {
	}

	public Query(String id, String lABEL2, String qUERY2, RequestLanguage reqLanguage2) {
		this.id = id;
		this.label = lABEL2;
		this.query = qUERY2;
		this.reqLanguage = reqLanguage2;
	}

	@Override
	public String toString() {
		return DOMUtil.prettyXmlString(ClientConstants.PORTALIS_DATA_XML_WRITER
				.visit(this));
	}

	@Override
	public <T> T accept(AdminDataWriterInterface<T> visitor) {
		return visitor.visit(this);
	}

	@Override
	public boolean equals(Object aThat) {
		if (this == aThat)
			return true;

		if (!(aThat instanceof Query))
			return false;
		// you may prefer this style, but see discussion in Effective Java
		// if ( aThat == null || aThat.getClass() != this.getClass() ) return
		// false;

		final Query that = (Query) aThat;
		return new EqualsBuilder()
				// if deriving: .appendSuper(super.equals(obj))
				.append(this.reqLanguage, that.reqLanguage)
				.append(this.param, that.param)
				.append(this.query, that.query)
				.append(this.label, that.label)
				.append(this.id, that.id)
				.isEquals();
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder(17, 31). // two randomly chosen prime numbers
				// if deriving: appendSuper(super.hashCode()).
				append(reqLanguage).
				append(param).
				append(query).
				append(label).
				append(id).
				toHashCode();
	}



	public RequestLanguage getReqLanguage() {
		return reqLanguage;
	}


	public void setReqLanguage(RequestLanguage reqLanguage) {
		this.reqLanguage = reqLanguage;
	}
	
	public HashMap<String, String> getParam() {
		return param;
	}

	public void setParam(HashMap<String, String> param) {
		this.param = param;
	}

	public void clearParam() {
		this.param = new HashMap<String, String> ();
	}

	public void addParam(String key, String value) {
		this.param.put(key, value);
	}

	public void addParam(String key, String value, String type) {
		this.param.put(key, type+" "+value);
	}

	public void addParam(String key, int value) {
		this.param.put(key, INT_TYPE+value);
	}

	public String getParamValue(String key) {
		return this.param.get(key);
	}
	
	public int getIntParamValue(String key) throws PortalisException {
		String value = this.param.get(key);
		if (value!= null && value.startsWith(INT_TYPE)) {
			try {
				return Integer.parseInt(value.replace(INT_TYPE, ""));
			} catch (NumberFormatException e) {
				String mess = "Le paramètre de nom "+key+ " n'est pas un entier, il vaut '"+
			value+"'";
				throw new PortalisException(mess);
			}
		} else {
			String mess = "pas de paramètre entier de nom "+key;
			throw new PortalisException(mess);
		}
	}
	
	public String getLabel() {
		return label;
	}


	public void setLabel(String label) {
		this.label = label;
	}

	public String getQuery() {
		return query;
	}

	public void setQuery(String query) {
		this.query = query;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}



}

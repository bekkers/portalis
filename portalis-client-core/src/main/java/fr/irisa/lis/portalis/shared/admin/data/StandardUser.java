package fr.irisa.lis.portalis.shared.admin.data;

import com.flipthebird.gwthashcodeequals.EqualsBuilder;
import com.flipthebird.gwthashcodeequals.HashCodeBuilder;

import fr.irisa.lis.portalis.commons.DOMUtil;
import fr.irisa.lis.portalis.shared.admin.ClientConstants;
import fr.irisa.lis.portalis.shared.admin.RightValue;

@SuppressWarnings("serial")
public class StandardUser extends PasswordUser {

	private RightValue portalisAdminRole = RightValue.READER;

	public StandardUser() {
		super();
	}

	public StandardUser(String email, String pseudo, String password) {
		super(email, pseudo, password);
	}
		
	public StandardUser(String email, String pseudo, String password, RightValue portalisRight) {
		super(email, pseudo, password);
		this.portalisAdminRole = portalisRight;
	}

	/* (non-Javadoc)
	 * @see fr.irisa.lis.portalis.shared.admin.data.userCore#toString()
	 */
	@Override
	public String toString() {
		return DOMUtil.prettyXmlString(ClientConstants.PORTALIS_DATA_XML_WRITER.visit(this));
	}

	/* (non-Javadoc)
	 * @see fr.irisa.lis.portalis.shared.admin.data.userCore#accept(fr.irisa.lis.portalis.shared.admin.data.AdminDataWriterInterface)
	 */
	@Override
	public <T> T accept(AdminDataWriterInterface<T> visitor) {
		return visitor.visit(this);
	}

	/* (non-Javadoc)
	 * @see fr.irisa.lis.portalis.shared.admin.data.userCore#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object aThat){
		  if ( this == aThat ) return true;

		  if ( !(aThat instanceof StandardUser) ) return false;
		  //you may prefer this style, but see discussion in Effective Java
		  //if ( aThat == null || aThat.getClass() != this.getClass() ) return false;

		  final StandardUser that = (StandardUser)aThat;
	        return new EqualsBuilder()
	                .appendSuper(super.equals(aThat))
	                .append(this.md5Password, that.md5Password)
	                .append(this.portalisAdminRole, that.portalisAdminRole)
	                .isEquals();
		}

	/* (non-Javadoc)
	 * @see fr.irisa.lis.portalis.shared.admin.data.userCore#hashCode()
	 */
	@Override
    public int hashCode() {
        return new HashCodeBuilder(17, 31). // two randomly chosen prime numbers
            	appendSuper(super.hashCode()).append(portalisAdminRole).
            toHashCode();
    }

	public RightValue getPortalisAdminRole() {
		return portalisAdminRole;
	}

	public void setPortalisADminRole(RightValue portalisRight) {
		this.portalisAdminRole = portalisRight;
	}

	@Override
	public boolean isGeneralAdmin() {
		return false;
	}

}
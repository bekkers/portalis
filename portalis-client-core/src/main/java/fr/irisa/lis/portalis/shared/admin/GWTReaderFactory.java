package fr.irisa.lis.portalis.shared.admin;

import org.w3c.dom.Element;

import fr.irisa.lis.portalis.shared.admin.data.AdminDataReaderInterface;
import fr.irisa.lis.portalis.shared.admin.data.AdminDataXmlReader;
import fr.irisa.lis.portalis.shared.admin.data.AdminReaderInterface;
import fr.irisa.lis.portalis.shared.admin.data.AdminXmlReader;
import fr.irisa.lis.portalis.shared.admin.reponse.AdminReponseReaderInterface;
import fr.irisa.lis.portalis.shared.admin.reponse.AdminReponseXmlReader;
import fr.irisa.lis.portalis.shared.camelis.data.CamelisDataReaderInterface;
import fr.irisa.lis.portalis.shared.camelis.data.CamelisDataXmlReader;
import fr.irisa.lis.portalis.shared.camelis.reponse.CamelisReponseReaderInterface;
import fr.irisa.lis.portalis.shared.camelis.reponse.CamelisReponseXmlReader;

public class GWTReaderFactory implements XmlReaderFactory<Element> {

	@Override
	public AdminReaderInterface<Element> getXmlReader() {
		return AdminXmlReader.getInstance();
	}

	@Override
	public AdminDataReaderInterface<Element> getPortalisDataXmlReader() {
		return AdminDataXmlReader.getInstance();
	}

	@Override
	public AdminReponseReaderInterface<Element> getAdminReponseXmlReader() {
		return AdminReponseXmlReader.getInstance();
	}

	@Override
	public CamelisDataReaderInterface<Element> getCamelisDataXmlReader() {
		return CamelisDataXmlReader.getInstance();
	}

	@Override
	public CamelisReponseReaderInterface<Element> getCamelisReponseXmlReader() {
		return CamelisReponseXmlReader.getInstance();
	}


}

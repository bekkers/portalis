package fr.irisa.lis.portalis.shared.admin;

import org.w3c.dom.Element;

import fr.irisa.lis.portalis.shared.admin.data.AdminDataWriterInterface;
import fr.irisa.lis.portalis.shared.admin.data.AdminDataXmlWriter;
import fr.irisa.lis.portalis.shared.admin.reponse.AdminReponseWriterInterface;
import fr.irisa.lis.portalis.shared.admin.reponse.AdminReponseXmlWriter;
import fr.irisa.lis.portalis.shared.camelis.data.CamelisDataWriterInterface;
import fr.irisa.lis.portalis.shared.camelis.data.CamelisDataXmlWriter;
import fr.irisa.lis.portalis.shared.camelis.reponse.CamelisReponseWriterInterface;
import fr.irisa.lis.portalis.shared.camelis.reponse.CamelisReponseXmlWriter;


public class DomWriterFactory implements XmlWriterFactory<Element> {

	@Override
	public AdminDataWriterInterface<Element> getPortalisDataVisitor() {
		return AdminDataXmlWriter.getInstance();
	}

	@Override
	public AdminReponseWriterInterface<Element> getAdminReponseVisitor() {
		return AdminReponseXmlWriter.getInstance();
	}

	@Override
	public CamelisDataWriterInterface<Element> getCamelisDataVisitor() {
		return CamelisDataXmlWriter.getInstance();
	}

	@Override
	public CamelisReponseWriterInterface<Element> getCamelisReponseVisitor() {
		return CamelisReponseXmlWriter.getInstance();
	}


}

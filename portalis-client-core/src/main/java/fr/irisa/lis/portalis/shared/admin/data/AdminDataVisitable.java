package fr.irisa.lis.portalis.shared.admin.data;

public interface AdminDataVisitable {

	public abstract <T> T accept(AdminDataWriterInterface<T> visitor);

}
package fr.irisa.lis.portalis.shared.admin.data;

import org.w3c.dom.Element;

import fr.irisa.lis.portalis.commons.DOMWriter;

public class AdminDataHtmlWriter extends DOMWriter implements
		AdminDataWriterInterface<Element> {

	@Override
	public Element visit(boolean bool) {

		throw new Error("Empty method stub");
	}

	@Override
	public Element visit(String s) {

		throw new Error("Empty method stub");
	}

	@Override
	public Element visit(Integer[] intTable) {

		throw new Error("Empty method stub");
	}

	@Override
	public Element visit(Role userRights) {

		throw new Error("Empty method stub");
	}

	@Override
	public Element visit(PortActif portActif) {

		throw new Error("Empty method stub");
	}

	@Override
	public Element visit(ActiveUser loggedInUser) {

		throw new Error("Empty method stub");
	}

	@Override
	public Element visit(ActiveUser[] loggedInUser) {

		throw new Error("Empty method stub");
	}

	@Override
	public Element visit(StandardUser user) {

		throw new Error("Empty method stub");
	}

	@Override
	public Element visit(StandardUser[] users) {

		throw new Error("Empty method stub");
	}

	@Override
	public Element visit(LisServiceCore serviceCore) {

		throw new Error("Empty method stub");
	}

	@Override
	public Element visit(ActiveLisService lisService) {

		throw new Error("Empty method stub");
	}

	@Override
	public Element visit(LisApplication application) {

		throw new Error("Empty method stub");
	}

	@Override
	public Element visit(SiteInterface portalisSite) {

		throw new Error("Empty method stub");
	}

	@Override
	public Element visit(LoginBean loginBean) {

		throw new Error("Empty method stub");
	}

	@Override
	public Element visit(PortalisService portalisService) {

		throw new Error("Empty method stub");
	}

	@Override
	public Element visit(LoginResult loginResult) {

		throw new Error("Empty method stub");
	}

	@Override
	public Element visit(ActiveSite activeSite) {
		Element bodyElem = createRootElement("body");

		return bodyElem;
	}

	protected Element htmlRoot(Element content) {
		Element rootElem = DOMWriter.createRootElement("html");
		addChild(rootElem, content);
		return rootElem;
	}

	@Override
	public Element visit(AnonymousUser anonymousUser) {

		throw new Error("Empty method stub");
	}

	@Override
	public Element visit(SuperUser superUser) {

		throw new Error("Empty method stub");
	}

	@Override
	public Element visit(UIPreference uiPreference) {
		throw new Error("Empty method stub");
	}

	@Override
	public Element visit(Property property) {
		throw new Error("Empty method stub");
	}

	@Override
	public Element visit(SparqlService sparqlService) {
		throw new Error("Empty method stub");
	}

	@Override
	public Element visit(Query query) {
		throw new Error("Empty method stub");
	}

	@Override
	public Element visit(SparqlApplication sparqlApplication) {
		throw new Error("Empty method stub");
	}

}

package fr.irisa.lis.portalis.shared.admin.reponse;

import java.io.Serializable;

import com.flipthebird.gwthashcodeequals.EqualsBuilder;
import com.flipthebird.gwthashcodeequals.HashCodeBuilder;

import fr.irisa.lis.portalis.commons.DOMUtil;
import fr.irisa.lis.portalis.shared.admin.ClientConstants;
import fr.irisa.lis.portalis.shared.admin.Err;
import fr.irisa.lis.portalis.shared.admin.data.ActiveSite;

@SuppressWarnings("serial")
public class ActiveSiteReponse extends VoidReponse implements Serializable, AdminReponseVisitedObject {

	private ActiveSite site;

    public ActiveSiteReponse() {
    	super();
    }

	public ActiveSiteReponse(ActiveSite site) {
    	super();
		this.site = site;
	}

	public ActiveSiteReponse(Err err) {
		super(err);
	}

	@Override
	public boolean equals(Object aThat){
		  if ( this == aThat ) return true;

		  if ( !(aThat instanceof ActiveSiteReponse) ) return false;
		  //you may prefer this style, but see discussion in Effective Java
		  //if ( aThat == null || aThat.getClass() != this.getClass() ) return false;

		  final ActiveSiteReponse that = (ActiveSiteReponse)aThat;
			return new EqualsBuilder()
			.appendSuper(super.equals(aThat))
			.append(this.site, that.site)
			.isEquals();
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder(17, 31). // two randomly chosen prime numbers
				appendSuper(super.hashCode()).
				append(site).
				toHashCode();
	}


	@Override
	public String toString() {
		return DOMUtil.prettyXmlString(ClientConstants.ADMIN_REPONSE_XML_WRITER.visit(this));
	}

	@Override
	public <T> T accept(AdminReponseWriterInterface<T> visitor) {
		return visitor.visit(this);
	}

	public ActiveSite getSite() {
		return site;
	}

	public void setSite(ActiveSite site) {
		this.site = site;
	}


}

package fr.irisa.lis.portalis.shared.admin.reponse;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.flipthebird.gwthashcodeequals.EqualsBuilder;
import com.flipthebird.gwthashcodeequals.HashCodeBuilder;

import fr.irisa.lis.portalis.commons.DOMUtil;
import fr.irisa.lis.portalis.shared.admin.ClientConstants;
import fr.irisa.lis.portalis.shared.admin.Err;

@SuppressWarnings("serial")
public class LogReponse extends VoidReponse implements Serializable, AdminReponseVisitedObject {
	private List<String> lines = new ArrayList<String>();;
	public LogReponse() {}
	
	public LogReponse(List<String> lines) {
		this.lines = lines;
	}

	public LogReponse(Err err) {
		super(err);
	}

	@Override
	public String toString() {
		return DOMUtil.prettyXmlString(ClientConstants.ADMIN_REPONSE_XML_WRITER.visit(this));
	}

	@Override
	public boolean equals(Object aThat){
		  if ( this == aThat ) return true;

		  if ( !(aThat instanceof LogReponse) ) return false;
		  //you may prefer this style, but see discussion in Effective Java
		  //if ( aThat == null || aThat.getClass() != this.getClass() ) return false;

		  final LogReponse that = (LogReponse)aThat;
			return new EqualsBuilder()
			.appendSuper(super.equals(aThat))
			.append(this.lines, that.lines)
			.isEquals();
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder(17, 31). // two randomly chosen prime numbers
				appendSuper(super.hashCode()).
				append(lines).
				toHashCode();
	}
	
	public List<String>getLines() {
		return lines;
	}

	public void setLines(List<String> lines) {
		this.lines = lines;
	}
	
	public void addLine(String line) {
		lines.add(line);
	}

}

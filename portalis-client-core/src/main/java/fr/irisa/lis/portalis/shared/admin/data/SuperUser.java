package fr.irisa.lis.portalis.shared.admin.data;

import com.flipthebird.gwthashcodeequals.EqualsBuilder;
import com.flipthebird.gwthashcodeequals.HashCodeBuilder;

import fr.irisa.lis.portalis.commons.DOMUtil;
import fr.irisa.lis.portalis.shared.admin.ClientConstants;

@SuppressWarnings("serial")
public class SuperUser extends PasswordUser {

	public SuperUser() {
		super();
	}

	public SuperUser(String email, String pseudo, String password) {
		super(email, pseudo, password);
	}

	@Override
	public String toString() {
		return DOMUtil.prettyXmlString(ClientConstants.PORTALIS_DATA_XML_WRITER.visit(this));
	}

	@Override
	public <T> T accept(AdminDataWriterInterface<T> visitor) {
		return visitor.visit(this);
	}

	@Override
	public boolean equals(Object aThat){
		  if ( this == aThat ) return true;

		  if ( !(aThat instanceof SuperUser) ) return false;

//		  final SuperUser that = (SuperUser)aThat;
	        return new EqualsBuilder().
	                appendSuper(super.equals(aThat)).
	                isEquals();
		}


	@Override
    public int hashCode() {
        return new HashCodeBuilder(17, 31). // two randomly chosen prime numbers
            appendSuper(super.hashCode()).
            toHashCode();
    }

	@Override
	public boolean isGeneralAdmin() {
		return true;
	}
}

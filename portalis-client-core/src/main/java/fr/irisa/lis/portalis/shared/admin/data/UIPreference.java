package fr.irisa.lis.portalis.shared.admin.data;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;

import com.flipthebird.gwthashcodeequals.EqualsBuilder;
import com.flipthebird.gwthashcodeequals.HashCodeBuilder;

import fr.irisa.lis.portalis.commons.DOMUtil;
import fr.irisa.lis.portalis.shared.admin.ClientConstants;

@SuppressWarnings("serial")
public class UIPreference implements AdminDataObject {

	private int minimumSupport = 1;
	private int entryShown = 20;
	private Set<String> hiddenAttribute = new HashSet<String>();
	private Map<String, String> extraColumn = new HashMap<String, String>();
	private boolean hasPictures = false;
	private boolean camelisID = true;
	private boolean camelisLabel = true;

	public UIPreference() {

	}

	public UIPreference(int minimumSupport, int entryShown,
			Set<String> hiddenAttribute, Map<String, String> extraColumn,
			boolean hasPictures) {
		this.minimumSupport = minimumSupport;
		this.entryShown = entryShown;
		if (hiddenAttribute != null) {
			this.hiddenAttribute = hiddenAttribute;
		}
		if (extraColumn != null) {
			this.extraColumn = extraColumn;
		}
		this.hasPictures = hasPictures;
	}

	public UIPreference(int minimumSupport, int entryShown,
			String[] hiddenAttribute, String[][] extraColumn,
			boolean hasPictures) {
		this.minimumSupport = minimumSupport;
		this.entryShown = entryShown;
		if (hiddenAttribute != null) {
			addHiddenAttribute(hiddenAttribute);
		}
		if (extraColumn != null) {
			addExtraColumn(extraColumn);
		}
		this.hasPictures = hasPictures;
	}

	public static UIPreference fromJson (String jsonPreferences) throws JsonParseException, JsonMappingException, IOException {
		//create ObjectMapper instance
        ObjectMapper objectMapper = new ObjectMapper();
         
        //convert json string to object
        UIPreference emp = objectMapper.readValue(jsonPreferences, UIPreference.class);
        return emp;
    }

	@Override
	public String toString() {
		return DOMUtil.prettyXmlString(ClientConstants.PORTALIS_DATA_XML_WRITER
				.visit(this));
	}

	@Override
	public <T> T accept(AdminDataWriterInterface<T> visitor) {
		return visitor.visit(this);
	}

	@Override
	public boolean equals(Object aThat) {
		if (this == aThat)
			return true;

		if (!(aThat instanceof UIPreference))
			return false;
		// you may prefer this style, but see discussion in Effective Java
		// if ( aThat == null || aThat.getClass() != this.getClass() ) return
		// false;

		final UIPreference that = (UIPreference) aThat;
		boolean extraColumnOk = extraColumnOk(that);
		if (!extraColumnOk) {
			return false;
		}
		;
		return new EqualsBuilder()
				.
				// if deriving: appendSuper(super.equals(obj)).
				append(this.minimumSupport, that.minimumSupport)
				.append(this.entryShown, that.entryShown)
				.append(this.hiddenAttribute, that.hiddenAttribute)
				// .append(this.extraColumn, that.extraColumn)
				.append(this.hasPictures, that.hasPictures)
				.append(this.camelisID, that.camelisID)
				.append(this.camelisLabel, that.camelisLabel).isEquals();
	}

	private boolean extraColumnOk(final UIPreference that) {
		for (Entry<String, String> thisKey : this.extraColumn.entrySet()) {
			boolean present = false;
			for (Entry<String, String> thatKey : that.extraColumn.entrySet()) {
				if (thatKey.getKey().equals(thisKey.getKey())
						& thatKey.getValue().equals(thisKey.getValue())) {
					present = true;
					break;
				}
			}
			if (!present) {
				return false;
			}
		}
		for (Entry<String, String> thatKey : that.extraColumn.entrySet()) {
			boolean present = false;
			for (Entry<String, String> thisKey : this.extraColumn.entrySet()) {
				if (thatKey.getKey().equals(thisKey.getKey())
						& thatKey.getValue().equals(thisKey.getValue())) {
					present = true;
					break;
				}
			}
			if (!present) {
				return false;
			}
		}
		return true;
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder(17, 31)
				. // two randomly chosen prime numbers
					// if deriving: appendSuper(super.hashCode()).
				append(minimumSupport).append(entryShown)
				.append(hiddenAttribute).append(extraColumn)
				.append(camelisID).append(camelisLabel)
				.append(hasPictures).toHashCode();
	}

	public int getMinimumSupport() {
		return minimumSupport;
	}

	public void setMinimumSupport(int minimumSupport) {
		this.minimumSupport = minimumSupport;
	}

	public int getEntryShown() {
		return entryShown;
	}

	public void setEntryShown(int entryShown) {
		this.entryShown = entryShown;
	}

	public Set<String> getHiddenAttribute() {
		return hiddenAttribute;
	}

	public void addHiddenAttribute(String[] attrs) {
		for (String attr : attrs) {
			this.hiddenAttribute.add(attr);
		}
	}

	public void addHiddenAttribute(String attr) {
		this.hiddenAttribute.add(attr);
	}

	public void setHiddenAttribute(Set<String> hiddenAttribute) {
		this.hiddenAttribute = hiddenAttribute;
	}

	public Map<String, String> getExtraColumn() {
		return extraColumn;
	}

	public void addExtraColumn(String[][] columns) {
		for (String[] column : columns) {
			addExtraColumn(column[0], column[1]);
		}
	}

	public void addExtraColumn(String name, String value) {
		this.extraColumn.put(name, value == null ? ClientConstants.LOCAL_ATTR : value);
	}

	public void setExtraColumn(Map<String, String> extraColumn) {
		this.extraColumn = extraColumn;
	}

	public boolean isHasPictures() {
		return hasPictures;
	}

	public void setHasPictures(boolean hasPictures) {
		this.hasPictures = hasPictures;
	}

	public boolean isCamelisID() {
		return camelisID;
	}

	public void setCamelisID(boolean camelisID) {
		this.camelisID = camelisID;
	}

	public boolean isCamelisLabel() {
		return camelisLabel;
	}

	public void setCamelisLabel(boolean camelisLabel) {
		this.camelisLabel = camelisLabel;
	}
	
	@JsonIgnore
	public Set<String> getCamelisFeatures() {
		Map<String, String> extraColumns = this.getExtraColumn();

		Set<String> feats = new HashSet<String>();
		for (Entry<String, String> extraColumn : extraColumns.entrySet()) {
			if (extraColumn.getValue().equals(ClientConstants.LOCAL_ATTR)) {
				feats.add(extraColumn.getKey());
			}
		}
		return feats;
	}

	@JsonIgnore
	public List<String> getCamelisInfo() {
		List<String> feats = new ArrayList<String>();
		if (this.isCamelisID()) {
			feats.add(ClientConstants.CAMELIS_ID);
		}
		if (this.isCamelisLabel()) {
			feats.add(ClientConstants.CAMELIS_LABEL);
		}
		if (this.isHasPictures()) {
			feats.add(ClientConstants.CAMELIS_PICTURE);
		}
		return feats;
	}



}

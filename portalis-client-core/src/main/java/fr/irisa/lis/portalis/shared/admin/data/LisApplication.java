package fr.irisa.lis.portalis.shared.admin.data;

import java.util.List;

import com.flipthebird.gwthashcodeequals.EqualsBuilder;
import com.flipthebird.gwthashcodeequals.HashCodeBuilder;

import fr.irisa.lis.portalis.commons.DOMUtil;
import fr.irisa.lis.portalis.shared.admin.ClientConstants;



@SuppressWarnings("serial")
public class LisApplication extends Application<LisServiceCore> implements AdminDataObject {

	public LisApplication() {}
	
	public LisApplication(String appliName) {
		this.id = appliName;
	}

	public LisApplication(String appliName, List<LisServiceCore> services) {
		this.id = appliName;
		this.services = services;
	}

	@Override
	public String toString() {
		return DOMUtil.prettyXmlString(ClientConstants.PORTALIS_DATA_XML_WRITER.visit(this));
	}

	@Override
	public <T> T accept(AdminDataWriterInterface<T> visitor) {
		return visitor.visit(this);
	}

	@Override
	public boolean equals(Object aThat){
		  if ( this == aThat ) return true;

		  if ( !(aThat instanceof LisApplication) ) return false;
		  //you may prefer this style, but see discussion in Effective Java
		  //if ( aThat == null || aThat.getClass() != this.getClass() ) return false;

		  final LisApplication that = (LisApplication)aThat;
	        return new EqualsBuilder().
	                // appendSuper(super.equals(aThat)).
	                append(this.services, that.services).
	                append(this.id, that.id).
	                isEquals();
		}

	@Override
    public int hashCode() {
        return new HashCodeBuilder(17, 31). // two randomly chosen prime numbers
            	// appendSuper(super.hashCode()).
                append(services).
                append(id).
            toHashCode();
    }




}

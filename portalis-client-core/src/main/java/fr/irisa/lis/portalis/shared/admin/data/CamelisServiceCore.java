package fr.irisa.lis.portalis.shared.admin.data;

import fr.irisa.lis.portalis.shared.admin.RightValue;

@SuppressWarnings("serial")
public class CamelisServiceCore extends LisServiceCore {

	@Override
	public RequestLanguage getReqLanguage() {
		return RequestLanguage.CAMELIS;
	}
	
	public CamelisServiceCore() {
		super();
	}

	public CamelisServiceCore(String idAppli, String idService, RightValue defaultRightValue) {
		super(idAppli, idService, defaultRightValue);
	}

	public CamelisServiceCore(String idAppli, String idService, RightValue defaultRightValue, boolean hasPictures) {
		super(idAppli, idService, defaultRightValue, hasPictures);
	}

	public CamelisServiceCore(String fullName, RightValue DefaultRightValue) {
		super(fullName, DefaultRightValue);
	}

	public CamelisServiceCore(String fullName, RightValue DefaultRightValue, boolean hasPictures) {
		super(fullName, DefaultRightValue, hasPictures);
	}


}

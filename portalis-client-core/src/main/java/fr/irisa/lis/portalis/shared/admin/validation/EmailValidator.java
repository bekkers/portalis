package fr.irisa.lis.portalis.shared.admin.validation;

import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class EmailValidator implements PortalisValidator<String> {

	private Pattern pattern;
	private Matcher matcher;
	private static EmailValidator instance;

	private static final String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
			+ "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

	/**
	 * Singleton class use getInstance() to get the singleton instance of this class
	 */
	private EmailValidator() {
		pattern = Pattern.compile(EMAIL_PATTERN);
	}
	
	/**
	 * 
	 * @return the singleton instance of this class validator
	 */
	public static EmailValidator getInstance() {
		if (instance==null)
			instance = new EmailValidator();
		return instance;
	}

	/**
	 * Validate email with ServiceName_PATTERN regular expression
	 * 
	 * @param email
	 *            email for validation
	 * @return true valid email, false invalid email
	 */
	public boolean validate(final String email) {

		matcher = pattern.matcher(email);
		return matcher.matches();

	}
}

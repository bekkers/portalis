package fr.irisa.lis.portalis.shared.camelis.reponse;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.flipthebird.gwthashcodeequals.EqualsBuilder;
import com.flipthebird.gwthashcodeequals.HashCodeBuilder;

import fr.irisa.lis.portalis.commons.DOMUtil;
import fr.irisa.lis.portalis.shared.admin.ClientConstants;
import fr.irisa.lis.portalis.shared.admin.Err;
import fr.irisa.lis.portalis.shared.admin.ErrorMessages;
import fr.irisa.lis.portalis.shared.admin.PortalisException;
import fr.irisa.lis.portalis.shared.admin.data.ActiveLisService;
import fr.irisa.lis.portalis.shared.admin.reponse.LisReponse;

@SuppressWarnings("serial")
public class PingReponse extends LisReponse implements CamelisReponseObject {
	private static final Logger LOGGER = LoggerFactory.getLogger(PingReponse.class.getName());
	
	@Override
	public String toString() {
		return DOMUtil.prettyXmlString(ClientConstants.CAMELIS_REPONSE_XML_WRITER.visit(this));
	}

	@Override
	public <T> T accept(CamelisReponseWriterInterface<T> visitor) {
		return visitor.visit(this);
	}

	protected List<ActiveLisService> lesServices = new ArrayList<ActiveLisService>();

	public PingReponse() {
		super();
	}

	public PingReponse(ActiveLisService ActiveLisService){
		super();
		this.lesServices.add(ActiveLisService);
	}

	public PingReponse(List<ActiveLisService> lesServices2){
		super();
		this.lesServices = lesServices2;
	}

	public PingReponse(Err err) {
		super(err);
	}
	
	@Override
	public boolean equals(Object aThat){
		  if ( this == aThat ) return true;

		  if ( !(aThat instanceof PingReponse) ) return false;
		  //you may prefer this style, but see discussion in Effective Java
		  //if ( aThat == null || aThat.getClass() != this.getClass() ) return false;

		  final PingReponse that = (PingReponse)aThat;
			return new EqualsBuilder()
			.appendSuper(super.equals(aThat))
			.append(this.lesServices, that.lesServices)
			.isEquals();
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder(17, 31) // two randomly chosen prime numbers
				.appendSuper(super.hashCode())
				.append(lesServices)
				.toHashCode();
	}

	public void addActiveLisService(ActiveLisService ActiveLisService) {
		this.lesServices.add(ActiveLisService);
	}

	public boolean contains(ActiveLisService ActiveLisService) {
		return this.lesServices.contains(ActiveLisService);
	}

	public List<ActiveLisService> getLesServices() {
		return lesServices;
	}

	public void setLesServices(List<ActiveLisService> lesServices) {
		this.lesServices = lesServices;
	}

	public ActiveLisService getService(String activeServiceId) throws PortalisException {
		ActiveLisService result = null;
		for (ActiveLisService activeService : lesServices) {
			if (activeService.getActiveId().equals(activeServiceId)) {
				result = activeService;
				break;
			}
		}
		return result;
	}

	public ActiveLisService getFirstService() throws PortalisException {
		if (this.lesServices.size()==0) {
			String mess = ErrorMessages.TEMOIN_NO_CAMELIS_SERVER_LISTENING;
			LOGGER.warn(mess);
			throw new PortalisException(mess);
		}
		return lesServices.get(0);
	}
}

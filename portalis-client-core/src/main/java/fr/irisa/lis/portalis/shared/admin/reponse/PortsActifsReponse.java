package fr.irisa.lis.portalis.shared.admin.reponse;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.flipthebird.gwthashcodeequals.EqualsBuilder;
import com.flipthebird.gwthashcodeequals.HashCodeBuilder;

import fr.irisa.lis.portalis.commons.DOMUtil;
import fr.irisa.lis.portalis.shared.admin.ClientConstants;
import fr.irisa.lis.portalis.shared.admin.Err;
import fr.irisa.lis.portalis.shared.admin.data.PortActif;


@SuppressWarnings("serial")
public class PortsActifsReponse extends VoidReponse implements Serializable, AdminReponseVisitedObject {
	
	@SuppressWarnings("unused")private static final Logger LOGGER = LoggerFactory.getLogger(PortsActifsReponse.class.getName());
	
	private Set<PortActif> listPortsActifs = new HashSet<PortActif>();
	
	public PortsActifsReponse() {
		super();
	}
	
	public PortsActifsReponse(Set<PortActif> set) {
		super();
		listPortsActifs = set;
	}
		
	public PortsActifsReponse(Err err) {
		super(err);
	}

	@Override
	public boolean equals(Object aThat){
		  if ( this == aThat ) return true;

		  if ( !(aThat instanceof PortsActifsReponse) ) return false;
		  //you may prefer this style, but see discussion in Effective Java
		  //if ( aThat == null || aThat.getClass() != this.getClass() ) return false;

		  final PortsActifsReponse that = (PortsActifsReponse)aThat;
			return new EqualsBuilder()
			.appendSuper(super.equals(aThat))
			.append(this.listPortsActifs, that.listPortsActifs)
			.isEquals();
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder(17, 31). // two randomly chosen prime numbers
				appendSuper(super.hashCode()).
				append(listPortsActifs).
				toHashCode();
	}

	@Override
	public String toString() {
		return DOMUtil.prettyXmlString(ClientConstants.ADMIN_REPONSE_XML_WRITER.visit(this));
	}

	@Override
	public <T> T accept(AdminReponseWriterInterface<T> visitor)  {
		return visitor.visit(this);
	}

	public static String portActifAsString(HashSet<PortActif> set) {
		StringBuffer buff = new StringBuffer();
		for (PortActif portActf : set) {
			buff.append("\n"+portActf);
		}
		return buff.toString();
	}
	
	public String portActifAsString() {
		StringBuffer buff = new StringBuffer();
		for (PortActif portActf : listPortsActifs) {
			buff.append("\n"+portActf);
		}
		return buff.toString();
	}
	
	public Set<PortActif> getListPortsActifs() {
		return listPortsActifs;
	}


	public void setListPortsActifs(HashSet<PortActif> listPortsActifs) {
		this.listPortsActifs = listPortsActifs;
	}
	
	public int getPort(int pid) {
		int port = -1;
		for (PortActif portActif : listPortsActifs) {
			if (portActif.getPid() == pid) {
				port = portActif.getPort();
				break;
			}
		}
		return port;
	}

	public int getPid(String server, int port) {
		int pid = -1;
		for (PortActif portActif : listPortsActifs) {
			if (portActif.getPort() == port) {
				pid = portActif.getPid();
				break;
			}
		}
		return pid;
	}

}

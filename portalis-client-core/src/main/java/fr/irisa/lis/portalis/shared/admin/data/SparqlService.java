package fr.irisa.lis.portalis.shared.admin.data;

import java.util.ArrayList;
import java.util.List;

import com.flipthebird.gwthashcodeequals.EqualsBuilder;
import com.flipthebird.gwthashcodeequals.HashCodeBuilder;

import fr.irisa.lis.portalis.commons.DOMUtil;
import fr.irisa.lis.portalis.shared.admin.ClientConstants;

@SuppressWarnings("serial")
public class SparqlService extends ActiveService implements ServiceCoreInterface {
	
	private String url;
	private String serviceName;
	private static final String APPLI_NAME = "sparqlEndPoint";
	private static final RequestLanguage reqLanguage = RequestLanguage.SPARQL;
	private List<Query> query = new ArrayList<Query>();
	
	public SparqlService() {}

	public SparqlService(String url, String serviceName) {
		this.url = url;
		this.serviceName = serviceName;
	}
	
	@Override
	public boolean equals(Object aThat) {
		if (this == aThat)
			return true;

		if (!(aThat instanceof SparqlService))
			return false;
		// you may prefer this style, but see discussion in Effective Java
		// if ( aThat == null || aThat.getClass() != this.getClass() ) return
		// false;

		final SparqlService that = (SparqlService) aThat;
		return new EqualsBuilder().
//				appendSuper(super.equals(aThat)).
				append(this.query, that.query).
				append(this.url, that.url).
				append(this.serviceName, that.serviceName).
				isEquals();
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder(17, 31)
				. // two randomly chosen prime numbers
//				appendSuper(super.hashCode()).
				append(query).
				append(url).
				append(serviceName).toHashCode();
	}



	@Override
	public String toString() {
		return DOMUtil.prettyXmlString(ClientConstants.PORTALIS_DATA_XML_WRITER
				.visit(this));
	}

	@Override
	public <T> T accept(AdminDataWriterInterface<T> visitor) {
		return visitor.visit(this);
	}

	@Override
	public String getFullName() {
		return APPLI_NAME+":"+serviceName;
	}

	@Override
	public String getActiveId() {
		return getActivePartOfId()+"::"+getFullName();
	}

	@Override
	public String getActivePartOfId() {
		String result[] = url.split("://");
		return result[1];
	}

	@Override
	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	@Override
	public String getAppliName() {
		return APPLI_NAME;
	}

	@Override
	public String getServiceName() {
		return this.serviceName;
	}

	public void setServiceName(String serviceName) {
		this.serviceName = serviceName;
	}

	@Override
	public RequestLanguage getReqLanguage() {
		return reqLanguage;
	}

	public List<Query> getQuery() {
		return query;
	}

	public Query getQuery(String id) {
		Query ResultQuery = null;
		for (Query q : query) {
			if (q.getId().equals(id)) {
				ResultQuery = q;
				break;
			}
		}
		return ResultQuery;
	}

	public void setQuery(List<Query> query) {
		this.query = query;
	}

	public void addQuery(Query query) {
		this.query.add(query);
	}

	public void clearQuery() {
		this.query = new ArrayList<Query>();
	}

}

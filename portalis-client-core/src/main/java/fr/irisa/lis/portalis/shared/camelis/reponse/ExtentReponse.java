package fr.irisa.lis.portalis.shared.camelis.reponse;

import com.flipthebird.gwthashcodeequals.EqualsBuilder;
import com.flipthebird.gwthashcodeequals.HashCodeBuilder;

import fr.irisa.lis.portalis.commons.DOMUtil;
import fr.irisa.lis.portalis.shared.admin.ClientConstants;
import fr.irisa.lis.portalis.shared.admin.Err;
import fr.irisa.lis.portalis.shared.admin.reponse.LisReponse;
import fr.irisa.lis.portalis.shared.camelis.data.LisExtent;
import fr.irisa.lis.portalis.shared.camelis.data.LisObject;

@SuppressWarnings("serial")
public class ExtentReponse extends LisReponse implements CamelisReponseObject {

	private LisExtent extent = new LisExtent();
	private int nbObjects;

	public ExtentReponse() {
		super();
	}

	public ExtentReponse(Err err) {
		super(err);
	}

	public void setExtent(LisExtent extent) {
		this.extent = extent;
	}

	public void setExtent(LisObject[] objs) {
		this.extent = new LisExtent(objs);
	}

	public LisExtent getExtent() {
		return extent;
	}

	public int getNbObjects() {
		return nbObjects;
	}
	
	public void setNbObjects(int n) {
		nbObjects = n;
	}
	
	@Override
	public String toString() {
		return DOMUtil.prettyXmlString(ClientConstants.CAMELIS_REPONSE_XML_WRITER
						.visit(this));
	}

	@Override
	public <T> T accept(CamelisReponseWriterInterface<T> visitor) {
		return visitor.visit(this);
	}

	@Override
	public boolean equals(Object aThat) {
		if (this == aThat)
			return true;

		if (!(aThat instanceof ExtentReponse))
			return false;

		final ExtentReponse that = (ExtentReponse) aThat;
		return new EqualsBuilder().appendSuper(super.equals(aThat))
				.append(this.extent, that.extent)
				.append(this.nbObjects, that.nbObjects)
				.isEquals();
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder(17, 31). // two randomly chosen prime numbers
				appendSuper(super.hashCode()).append(extent).toHashCode();
	}

}

package fr.irisa.lis.portalis.commons.pipe;

import java.nio.charset.Charset;

public interface CharacterSetable {

	public abstract Charset getCharacterSet();

	public abstract void setCharacterSet(Charset characterSet);

}
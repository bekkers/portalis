package fr.irisa.lis.portalis.commons.pipe;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.charset.Charset;
 
/**
 * StringOutputStream backed by ByteArrayOutputStream that simplifies  reading of {@link OutputStream}  directly into string
 * @author Grzegorz Bugaj
 *
 */
public class StringOutputStream extends OutputStream {
	private ByteArrayOutputStream buffer = new ByteArrayOutputStream();
	private static final Charset DEFAULT_CHARACTER_SET = Charset.forName("UTF-8");
	private Charset characterSet = DEFAULT_CHARACTER_SET;
 
	@Override
	public void write(int b) throws IOException {
		buffer.write(b);
	}
 
	@Override
	public void write(byte[] b) throws IOException {
		buffer.write(b);
	}
 
	@Override
	public void write(byte[] b, int off, int len) throws IOException {
		buffer.write(b, off, len);
	}
 
	public Charset getCharacterSet() {
		return characterSet;
	}
 
	public void setCharacterSet(Charset characterSet) {
		this.characterSet = characterSet;
	}
 
 
	@Override
	public String toString() {
		return new String(buffer.toByteArray(), getCharacterSet());
	}
}
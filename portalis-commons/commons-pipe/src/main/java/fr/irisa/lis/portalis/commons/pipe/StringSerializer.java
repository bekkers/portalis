package fr.irisa.lis.portalis.commons.pipe;

import java.io.IOException;
import java.io.StringWriter;
import java.nio.charset.Charset;
 
public abstract class StringSerializer<T> extends OCharacterSetable<StringWriter> {
	private static final Charset DEFAULT_CHARACTER_SET = Charset.forName("UTF-8");
	protected Charset characterSet = DEFAULT_CHARACTER_SET;

	public StringSerializer() {
		super(new StringWriter());
	}
 
	public StringSerializer(Charset charset) {
		super(new StringWriter());
		this.characterSet = charset;
	}

	/* (non-Javadoc)
	 * @see fr.irisa.lis.portalis.commons.pipe.CharacterSetable#getCharacterSet()
	 */
	@Override
	public Charset getCharacterSet() {
		return characterSet;
	}
 
	/* (non-Javadoc)
	 * @see fr.irisa.lis.portalis.commons.pipe.CharacterSetable#setCharacterSet(java.nio.charset.Charset)
	 */
	@Override
	public void setCharacterSet(Charset characterSet) {
		this.characterSet = characterSet;
	}
	
	public void close() throws IOException {
		outputStream.close();
	}
		
	abstract protected void serialize(T t);
	
	public String toString(T t) {
		serialize(t);
		return new String(outputStream.toString().getBytes(characterSet));
	}

}
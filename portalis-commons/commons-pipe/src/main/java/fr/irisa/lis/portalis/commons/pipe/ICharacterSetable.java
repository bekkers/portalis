package fr.irisa.lis.portalis.commons.pipe;

import java.io.IOException;
import java.io.Reader;

public abstract class ICharacterSetable<T extends Reader> implements CharacterSetable {
	
	protected T reader;

	public ICharacterSetable(T inputStream) {
		this.reader = inputStream;;
	}

	public T getReader() {
		return reader;
	}
	
	public void close() throws IOException {
		reader.close();
	}

	public void read() throws IOException {
		reader.read();
	}

}

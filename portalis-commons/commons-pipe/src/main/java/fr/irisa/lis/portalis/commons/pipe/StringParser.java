package fr.irisa.lis.portalis.commons.pipe;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
 
public abstract class StringParser<T> extends ICharacterSetable<InputStreamReader> {
	private static final Charset DEFAULT_CHARACTER_SET = Charset.forName("UTF-8");
	protected Charset characterSet = DEFAULT_CHARACTER_SET;

	public StringParser(String s) {
		super(new InputStreamReader(new ByteArrayInputStream(s.getBytes(DEFAULT_CHARACTER_SET))));
	}
 
	public StringParser(String s, Charset charset) {
		super(new InputStreamReader(new ByteArrayInputStream(s.getBytes(charset))));
	}

	/* (non-Javadoc)
	 * @see fr.irisa.lis.portalis.commons.pipe.CharacterSetable#getCharacterSet()
	 */
	@Override
	public Charset getCharacterSet() {
		return characterSet;
	}
 
	/* (non-Javadoc)
	 * @see fr.irisa.lis.portalis.commons.pipe.CharacterSetable#setCharacterSet(java.nio.charset.Charset)
	 */
	@Override
	public void setCharacterSet(Charset characterSet) {
		this.characterSet = characterSet;
	}
	
	public void close() throws IOException {
		reader.close();
	}
	
	public abstract T parse() throws IOException;

}
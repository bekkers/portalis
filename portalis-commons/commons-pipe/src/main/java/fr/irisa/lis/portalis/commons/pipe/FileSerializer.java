package fr.irisa.lis.portalis.commons.pipe;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.nio.charset.Charset;
 
public abstract class FileSerializer<T> extends OCharacterSetable<OutputStreamWriter> {
	private static final Charset DEFAULT_CHARACTER_SET = Charset.forName("UTF-8");
	protected Charset characterSet = DEFAULT_CHARACTER_SET;

	public FileSerializer(FileOutputStream stream) {
		super(new OutputStreamWriter(stream, DEFAULT_CHARACTER_SET));
	}
 
	public FileSerializer(FileOutputStream stream, Charset charset) {
		super(new OutputStreamWriter(stream, charset));
		characterSet = charset;
	}

	public FileSerializer(OutputStreamWriter stream) {
		super(stream);
	}
 
	/* (non-Javadoc)
	 * @see fr.irisa.lis.portalis.commons.pipe.CharacterSetable#getCharacterSet()
	 */
	@Override
	public Charset getCharacterSet() {
		return characterSet;
	}
 
	/* (non-Javadoc)
	 * @see fr.irisa.lis.portalis.commons.pipe.CharacterSetable#setCharacterSet(java.nio.charset.Charset)
	 */
	@Override
	public void setCharacterSet(Charset characterSet) {
		this.characterSet = characterSet;
	}
	
	public void close() throws IOException {
		outputStream.close();
	}
		
	abstract protected void serialize(T t);
	

}
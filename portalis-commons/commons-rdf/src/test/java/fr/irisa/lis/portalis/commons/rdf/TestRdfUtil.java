package fr.irisa.lis.portalis.commons.rdf;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Set;

import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.DOMException;

import com.hp.hpl.jena.query.Query;
import com.hp.hpl.jena.query.QueryFactory;
import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.rdf.model.ModelFactory;
import com.hp.hpl.jena.rdf.model.NodeIterator;
import com.hp.hpl.jena.rdf.model.RDFNode;
import com.hp.hpl.jena.rdf.model.ResIterator;
import com.hp.hpl.jena.rdf.model.impl.PropertyImpl;
import com.hp.hpl.jena.rdf.model.impl.ResourceImpl;

import fr.irisa.lis.portalis.commons.core.CommonsUtil;

public class TestRdfUtil {

	private static final Logger LOGGER = LoggerFactory
			.getLogger(TestRdfUtil.class.getName());

	private static final String BLOGERS_RDF_DIR = "/local/bekkers/portalis/portalis-commons/commons-rdf/src/test/resources/";
	private static final String BLOGERS_RDF = "/bloggers.rdf";


	private static final String QUERY5 = "PREFIX foaf: <http://xmlns.com/foaf/0.1/>"
			+ " SELECT *"
			+ " WHERE {"
			+ "   ?contributor foaf:name ?name ."
			+ "}";

	private static final String QUERY4 = "PREFIX foaf: <http://xmlns.com/foaf/0.1/> "
			+ " SELECT * " + " WHERE {?s ?p ?o .}";

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		CommonsUtil.clientInitLogPropertiesFile();
	}

	@Ignore
	@Test
	public void testResultSet() {
		try {
			InputStream in = this.getClass().getResourceAsStream(BLOGERS_RDF);		
//			Model model = new IStream(in).read();
			Model model = null;
			in.close();

			NodeIterator iterator1 = model.listObjects();
			Set<RDFNode> rdfNodes = iterator1.toSet();
			assertEquals(437, rdfNodes.size());

			NodeIterator iterator = model.listObjects();
			int anonResource = 0;
			int nonAnonResource = 0;
			int literal = 0;
			int error = 0;
			while (iterator.hasNext()) {
				Object element = iterator.next();
				switch (element.getClass().getName()) {
				case "com.hp.hpl.jena.rdf.model.impl.ResourceImpl":
					if (((ResourceImpl) element).isAnon())
						anonResource++;
					else
						nonAnonResource++;
					break;
				case "com.hp.hpl.jena.rdf.model.impl.LiteralImpl":
					literal++;
					break;
				default:
					error++;
					break;

				}
			}
			
			int total = anonResource + nonAnonResource + literal + error;
			assertEquals(121, anonResource);
			assertEquals(156, nonAnonResource);
			assertEquals(160, literal);
			assertEquals(0, error);
			assertEquals(437, total);
			System.out.println("rdfNodes.size="+rdfNodes.size() + " anonResource=" + anonResource + " nonAnonResource="
					+ nonAnonResource + " literal=" + literal + " error=" + error + " total="
					+ total);

			// Liste des objets ayant la propriété dc:title
			ResIterator iter = model.listSubjectsWithProperty(new PropertyImpl(
					"http://purl.org/dc/elements/1.1/title"));
			int size = iter.toSet().size();
			assertEquals(74, size);
			System.out.println("resources with 'dc:title' ="+size);

		} catch (Exception e) {
			e.printStackTrace();
			fail("Exception rencontrée lors du test : " + e.getMessage()
					+ " : " + e.getClass().getName());
		}

	}

	@Test
	public void testExecSparqlQuery() {
		try {

			final String QUERY = "prefix foaf: <http://xmlns.com/foaf/0.1/>"
					+ " SELECT ?name WHERE {   "
					+ "   ?p a foaf:Person ."
					+ "   ?p foaf:name ?name ."
					+ "   ?p foaf:holdsAccount ?account ."
					+ "   ?account foaf:accountName \"mortenf\""
					+ "}";
			
			InputStream in = this.getClass().getResourceAsStream(BLOGERS_RDF);
			// InputStream in = new FileInputStream(new
			// File(BLOGERS_RDF_DIR+"bloggers.rdf"));

			Query query = QueryFactory.create(QUERY);
			// Output query results
			String text = RdfUtil.execSparqlQuery(in, query);
			LOGGER.debug("results =\n" + text);

			final String TEMOIN = "Morten Frederiksen";
			assertTrue(String.format(
					"Le résultat \n%s\nne contient pas la chaîne '%s'", text,
					TEMOIN), text.contains(TEMOIN));

		} catch (Exception e) {
			e.printStackTrace();
			fail("Exception rencontrée lors du test : " + e.getMessage()
					+ " : " + e.getClass().getName());
		}

	}

	@Test
	public void testExecSparqlQuery1() {
		try {

			try {
				final String QUERY = "PREFIX foaf: <http://xmlns.com/foaf/0.1/>"
						+ " SELECT ?name "
						+ " FROM   <http://planetrdf.com/bloggers.rdf>"
						+ " WHERE { "
						+ "    ?person a foaf:Agent ."
						+ "    ?person foaf:weblog <http://ivanherman.wordpress.com/tag/work-related/semantic-web/> ."
						+ "    ?person foaf:name ?name ."
						+ "}";


				Query query = QueryFactory.create(QUERY);
				// Output query results
				InputStream in = this.getClass().getResourceAsStream(BLOGERS_RDF);
				String text = RdfUtil.execSparqlQuery(in, query);
				final String TEMOIN = "Ivan Herman";
				assertTrue(String.format(
						"Le résultat \n%s\nne contient pas la chaîne '%s'",
						text, TEMOIN), text.contains(TEMOIN));

			} catch (DOMException e) {

			}

		} catch (Exception e) {
			e.printStackTrace();
			fail("Exception rencontrée lors du test : " + e.getMessage()
					+ " : " + e.getClass().getName());
		}

	}

	@Test
	public void testExecSparqlQuery2() {
		try {

			final String QUERY = "PREFIX foaf: <http://xmlns.com/foaf/0.1/>"
					+ " SELECT ?url"
					+ " FROM 	<bloggers.rdf>"
					+ " WHERE {"
					+ "   ?contributor foaf:name \"DERI Galway\" ."
					+ "   ?contributor foaf:weblog ?url ." + "}";

			try {
				InputStream in = this.getClass().getResourceAsStream(
						BLOGERS_RDF);
				// InputStream in = new FileInputStream(new
				// File(BLOGERS_RDF_DIR+"bloggers.rdf"));

				Query query = QueryFactory.create(QUERY);
				// Output query results
				String text = RdfUtil.execSparqlQuery(in, query);
				LOGGER.debug("results =\n" + text);

				final String TEMOIN = "http://blog.deri.ie";
				assertTrue(String.format(
						"Le résultat \n%s\nne contient pas la chaîne '%s'",
						text, TEMOIN), text.contains(TEMOIN));

			} catch (DOMException e) {

			}

		} catch (Exception e) {
			e.printStackTrace();
			fail("Exception rencontrée lors du test : " + e.getMessage()
					+ " : " + e.getClass().getName());
		}

	}

	@Test
	public void testExecSparqlQuery3() {
		final String QUERY = "PREFIX foaf: <http://xmlns.com/foaf/0.1/> "
				+ "prefix dc: <http://purl.org/dc/elements/1.1/> "
				+ " SELECT ?name ?url "
				+ " WHERE {"
				+ "      ?contributor foaf:name ?name . "
				+ "      ?contributor foaf:weblog ?url . "
				+ "      ?doc a foaf:Document . "
				+ "      ?doc dc:title ?name . " 
				+ " }";

		try {

			try {
				// Open the bloggers RDF graph from the filesystem
				InputStream in = this.getClass().getResourceAsStream(
						BLOGERS_RDF);

				Query query = QueryFactory.create(QUERY);
				// Output query results
				String text = RdfUtil.execSparqlQuery(in, query);
				final String TEMOIN = "<http://torrez.us/>";
				assertTrue(String.format(
						"Le résultat \n%s\nne contient pas la chaîne '%s'",
						text, TEMOIN), text.contains(TEMOIN));

			} catch (DOMException e) {

			}

		} catch (Exception e) {
			e.printStackTrace();
			fail("Exception rencontrée lors du test : " + e.getMessage()
					+ " : " + e.getClass().getName());
		}

	}

	@Ignore
	@Test
	public void testExecSparqlQuery4() {
		try {

			try {
				// Open the bloggers RDF graph from the filesystem
				// InputStream in =
				// this.getClass().getResourceAsStream(BLOGERS_RDF);
				InputStream in = new FileInputStream(new File(BLOGERS_RDF_DIR
						+ "bloggers.rdf"));

				Query query = QueryFactory.create(QUERY4);
				// Output query results
				String text = RdfUtil.execSparqlQuery(in, query);
				final String TEMOIN = "<http://foobar.xx/blog>";
				assertTrue(String.format(
						"Le résultat \n%s\nne contient pas la chaîne '%s'",
						text, TEMOIN), text.contains(TEMOIN));

			} catch (DOMException e) {

			}

		} catch (Exception e) {
			e.printStackTrace();
			fail("Exception rencontrée lors du test : " + e.getMessage()
					+ " : " + e.getClass().getName());
		}

	}

	@Ignore
	@Test
	public void testExecSparqlQuery5() {
		try {

			try {
				// Open the bloggers RDF graph from the filesystem
				// InputStream in =
				// this.getClass().getResourceAsStream(BLOGERS_RDF);
				InputStream in = new FileInputStream(new File(BLOGERS_RDF_DIR
						+ "bloggers.rdf"));

				Query query = QueryFactory.create(QUERY5);
				// Output query results
				String text = RdfUtil.execSparqlQuery(in, query);
				final String TEMOIN = "<http://foobar.xx/blog>";
				assertTrue(String.format(
						"Le résultat \n%s\nne contient pas la chaîne '%s'",
						text, TEMOIN), text.contains(TEMOIN));

			} catch (DOMException e) {

			}

		} catch (Exception e) {
			e.printStackTrace();
			fail("Exception rencontrée lors du test : " + e.getMessage()
					+ " : " + e.getClass().getName());
		}

	}

}

package fr.irisa.lis.portalis.commons.geoloc;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.InputStream;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class TestReader {
	private static final Logger LOGGER = LoggerFactory.getLogger(TestReader.class.getName());


	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		LOGGER.info(""
				+ "\n            ---------------------------------------------------"
				+ "\n            |         @BeforeClass : TestReader       |"
				+ "\n            ---------------------------------------------------\n");

	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		LOGGER.info(""
				+ "\n            ---------------------------------------------------"
				+ "\n            |         @AfterClass : TestReader        |"
				+ "\n            ---------------------------------------------------\n");

	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}
	
	@Test
	public void testRead() {
		final String fileName = "/surf_minerales_thabor.kml";
		try {
			InputStream in = this.getClass().getResourceAsStream(fileName);
			String[] lines = KlmReader.kmlToString(in).split("\n");
			assertEquals(41, lines.length);
			assertTrue("première ligne incorrecte : "+lines[0],lines[0].startsWith("mk"));
		} catch (Error e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}

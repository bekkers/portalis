package fr.irisa.lis.portalis.commons.csv;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;

import java.io.File;
import java.io.FileInputStream;
import java.net.URL;

import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import fr.irisa.lis.portalis.commons.DOMUtil;
import fr.irisa.lis.portalis.commons.core.CommonsUtil;

public class CsvReaderTest {

	private static final String encoding = CommonsUtil.DEFAULT_ENCODING;

	@Rule
	public TemporaryFolder testFolder = new TemporaryFolder();

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@Test
	public void testCvsReader1() {
		try {

			String rowName = "commune";
			String cvsSplitBy = ",";
			int headersPos = 5;
			int longHeadersPos = 4;
			
			String csvFile = "data/test1.csv";
			File tempFile = testFolder.newFile("test1.xml");

			URL resource = this.getClass().getClassLoader()
					.getResource(csvFile);
			assertNotNull(resource);
			File file = new File(resource.toURI());
			FileInputStream input = new FileInputStream(file);

			File2DocumentCsvParser cvsReader = new File2DocumentCsvParser(
					input, rowName, cvsSplitBy, headersPos, longHeadersPos,
					encoding);
			Document doc = cvsReader.parse();
			assertNotNull(doc);
			DOMUtil.prettyPrintXml(doc, tempFile);

			Element root = doc.getDocumentElement();
			assertNotNull(root);
			assertEquals(rowName + "s", root.getNodeName());

			NodeList nodeList = root.getElementsByTagName(rowName);
			assertNotNull(nodeList);
			int tailleListe = 56;
			assertEquals(tailleListe, nodeList.getLength());

		} catch (Exception e) {
			e.printStackTrace();
			fail("Exception rencontrée lors du test : " + e.getMessage()
					+ " : " + e.getClass().getName());
		}

	}

	@Test
	public void testCvsReader2() {
		try {
			String rowName = "commune";
			String cvsSplitBy = ";";
			int headersPos = 0;
			int longHeadersPos = -1;
			String csvFile = "data/test2.csv";
			File tempFile = testFolder.newFile("test2.xml");

			URL resource = this.getClass().getClassLoader()
					.getResource(csvFile);
			assertNotNull(resource);
			File file = new File(resource.toURI());
			FileInputStream input = new FileInputStream(file);

			File2DocumentCsvParser cvsReader = new File2DocumentCsvParser(
					input, rowName, cvsSplitBy, headersPos, longHeadersPos,
					encoding);
			Document doc = cvsReader.parse();
			assertNotNull(doc);
			DOMUtil.prettyPrintXml(doc, tempFile);

			Element root = doc.getDocumentElement();
			assertEquals(rowName + "s", root.getNodeName());

			NodeList nodeList = root.getElementsByTagName(rowName);
			int tailleListe = 10;
			assertEquals(tailleListe, nodeList.getLength());

		} catch (Exception e) {
			fail("Exception rencontrée lors du test : " + e.getMessage());
			e.printStackTrace();
		}
	}

}

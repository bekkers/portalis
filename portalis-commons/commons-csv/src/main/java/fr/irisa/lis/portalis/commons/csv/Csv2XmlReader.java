package fr.irisa.lis.portalis.commons.csv;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import javax.xml.bind.JAXBException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import fr.irisa.lis.portalis.commons.DOMUtil;
import fr.irisa.lis.portalis.commons.XmlBeanDAO;
import fr.irisa.lis.portalis.commons.core.CommonsUtil;

public class Csv2XmlReader<O> extends XmlBeanDAO<O> {

	private String headerNames = "headerNames";
	protected String rowName = "row";
	protected String rootName = rowName + "s";
	protected String csvSplitBy = ",";
	protected int headersPos = 0;
	protected int longHeadersPos = -1;
	protected String encoding = CommonsUtil.DEFAULT_ENCODING;

	private String[] longHeaders;
	private String[] headers;

	protected Document doc;

	public Csv2XmlReader(O bean, InputStream inputStream) throws Exception {
		super(bean);
		// A leading '/' in path indicates the working dir of your project, 
		// while no '/' indicates a relative dir to current class
		if (inputStream==null) {
			throw new FileNotFoundException("csvFile inputStream is null");
		}
		BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream, encoding));
		initDocument(reader);
	}

	public Csv2XmlReader(O bean, InputStream inputStream, String rowName,
			String csvSplitBy, int headersPos, int longHeadersPos,
			String encoding) throws Exception {
		super(bean);
		if (inputStream==null) {
			throw new FileNotFoundException("csvFile inputStream is null");
		}
//		System.out.println("available "+inputStream.available());
		this.rowName = rowName;
		this.rootName = rowName + "s";
		this.csvSplitBy = csvSplitBy;
		this.headersPos = headersPos;
		this.longHeadersPos = longHeadersPos;
		this.encoding = encoding;
		BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream, encoding));
		initDocument(reader);
	}

	public Csv2XmlReader(O bean, String fileName) throws Exception {
		super(bean);
		BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(fileName), encoding));
		initDocument(reader);
	}

	public Csv2XmlReader(O bean, String fileName, String rowName,
			String csvSplitBy, int headersPos, int longHeadersPos,
			String encoding) throws Exception {
		super(bean);
		this.rowName = rowName;
		this.rootName = rowName + "s";
		this.csvSplitBy = csvSplitBy;
		this.headersPos = headersPos;
		this.longHeadersPos = longHeadersPos;
		this.encoding = encoding;
		BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(fileName), encoding));
		initDocument(reader);
	}


	private void initHeaders(BufferedReader reader) throws IOException {
//		System.out.println("reader.ready() = "+Boolean.toString(reader.ready())+" longHeadersPos = "+longHeadersPos+" headersPos = "+headersPos);
		
		String line = "";
		if (longHeadersPos >= 0) {
			if (headersPos > longHeadersPos) {
				int i = 0;
				while ((line = reader.readLine()) != null) {
					if (i == longHeadersPos) {
						longHeaders = line.split(csvSplitBy);
						i++;
						break;
					}
					i++;
				}
				while ((line = reader.readLine()) != null) {
					if (i == headersPos) {
						headers = line.split(csvSplitBy);
						break;
					}
					i++;
				}
			} else {
				int i = 0;
				while ((line = reader.readLine()) != null) {
					if (i == headersPos) {
						headers = line.split(csvSplitBy);
						break;
					}
					i++;
				}
				i++;
				while ((line = reader.readLine()) != null) {
					if (i == longHeadersPos) {
						longHeaders = line.split(csvSplitBy);
						break;
					}
					i++;
				}

			}

		} else {
			int i = 0;
			while ((line = reader.readLine()) != null) {
				if (i == headersPos) {
					headers = line.split(csvSplitBy);
					break;
				}
				i++;
			}
		}

		if (headers==null) {
			throw new Error("headers not initialized");
		}
		// normalize headers
		for (int i = 0; i < headers.length; i++) {
			String newHeader = DOMUtil.deAccent(headers[i]).replace("-", "_")
					.replace(":", "_").replace(".", "_").replace(" ", "_")
					.replace("'", "_").replace("\"", "_");
			headers[i] = newHeader;
		}
	}

	public static String asString(String[] tab) throws IOException {
		StringBuffer buff = new StringBuffer();
		for (String header : tab) {
			buff.append(header).append("\t");
		}
		return buff.toString();
	}

	private void initDocument(BufferedReader reader) throws Exception {
		
		doc = DOMUtil.createDocument();
		Element root = doc.createElement(rootName);
		doc.appendChild(root);

		String line = "";
		try {
			initHeaders(reader);
			if (longHeaders != null) {
				putOneRow(root, longHeaders, this.headerNames);
			}

			while ((line = reader.readLine()) != null) {

				// use comma as separator
				String[] colums = line.split(csvSplitBy);
				putOneRow(root, colums, this.rowName);
			}

		} catch (Exception e) {
			throw e;
		} finally {
			if (reader != null) {
				try {
					reader.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}

	private void putOneRow(Element root, String[] colums, String rowName) {
		Document doc = root.getOwnerDocument();
		Element row = doc.createElement(rowName);
		root.appendChild(row);
		for (int j = 0; j < headers.length; j++) {
			Element property = doc.createElement(headers[j]);
			row.appendChild(property);
			if (j >= colums.length) {
				property.setTextContent("null");
			} else {
				property.setTextContent(colums[j]);
			}
		}
	}

	public String getHeaderNames() {
		return headerNames;
	}

	public void setHeaderNames(String headerNames) {
		this.headerNames = headerNames;
	}

	public String getRowName() {
		return rowName;
	}

	public void setRowName(String rowName) {
		this.rowName = rowName;
		this.rootName = rowName + "s";
	}

	public String getEncoding() {
		return encoding;
	}

	public void setEncoding(String encoding) {
		this.encoding = encoding;
	}

	public String getRootName() {
		return rootName;
	}

	public void setRootName(String rootName) {
		this.rootName = rootName;
	}

	public String getCsvSplitBy() {
		return csvSplitBy;
	}

	public void setCsvSplitBy(String csvSplitBy) {
		this.csvSplitBy = csvSplitBy;
	}

	public int getHeadersPos() {
		return headersPos;
	}

	public void setHeadersPos(int headersPos) {
		this.headersPos = headersPos;
	}

	public int getLongHeadersPos() {
		return longHeadersPos;
	}

	public void setLongHeadersPos(int longHeadersPos) {
		this.longHeadersPos = longHeadersPos;
	}
	
	public Document getDocument() throws Exception {
		return doc;
	}
	
	@Override
	public O getBeanValue() throws JAXBException {
		if (beanValue==null) {
			beanValue = loadBeanFromDom(doc);
		}
		return beanValue;
	}



}

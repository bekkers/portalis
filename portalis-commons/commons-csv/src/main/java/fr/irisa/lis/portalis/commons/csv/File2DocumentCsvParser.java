package fr.irisa.lis.portalis.commons.csv;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.charset.Charset;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import fr.irisa.lis.portalis.commons.DOMUtil;
import fr.irisa.lis.portalis.commons.core.CommonsUtil;
import fr.irisa.lis.portalis.commons.pipe.FileParser;

public class File2DocumentCsvParser extends FileParser<Document> {

	private String headerNames = "headerNames";
	protected String rowName = "row";
	protected String rootName = rowName + "s";
	protected String csvSplitBy = ",";
	protected int headersPos = 0;
	protected int longHeadersPos = -1;
	protected String encoding = CommonsUtil.DEFAULT_ENCODING;

	private String[] longHeaders;
	private String[] headers;

	public File2DocumentCsvParser(FileInputStream inputStream) throws Exception {
		super(inputStream);
	}

	public File2DocumentCsvParser(FileInputStream inputStream, String rowName,
			String csvSplitBy, int headersPos, int longHeadersPos,
			String encoding) throws Exception {
		super(inputStream, Charset.forName(encoding));
		if (inputStream==null) {
			throw new FileNotFoundException("csvFile inputStream is null");
		}
		this.rowName = rowName;
		this.rootName = rowName + "s";
		this.csvSplitBy = csvSplitBy;
		this.headersPos = headersPos;
		this.longHeadersPos = longHeadersPos;
		this.encoding = encoding;
	}

	public File2DocumentCsvParser(String fileName) throws Exception {
		super(fileName);
	}

	public File2DocumentCsvParser(String fileName, String rowName,
			String csvSplitBy, int headersPos, int longHeadersPos,
			String encoding) throws Exception {
		super(fileName, Charset.forName(encoding));
		this.rowName = rowName;
		this.rootName = rowName + "s";
		this.csvSplitBy = csvSplitBy;
		this.headersPos = headersPos;
		this.longHeadersPos = longHeadersPos;
		this.encoding = encoding;
	}


	private int initHeaders(BufferedReader reader) throws IOException {
		String line = "";
		int lineNumber = 0;
		if (longHeadersPos >= 0) { // présence de longHeaders
			if (headersPos > longHeadersPos) {  // les longHeaders sont après les shortHeaders
				int i = 0;
				while ((line = reader.readLine()) != null) { // lecture des shortHeaders
//					System.out.println("line0="+line);
					lineNumber++;
					if (i == longHeadersPos) {
						longHeaders = line.split(csvSplitBy);
						i++;
						break;
					}
					i++;
				}
				while ((line = reader.readLine()) != null) { // lecture des longHeaders
//					System.out.println("line1="+line);
					lineNumber++;
					if (i == headersPos) {
						headers = line.split(csvSplitBy);
						break;
					}
					i++;
				}
			} else {  // les longHeaders sont avant les shortHeaders
				int i = 0;
				while ((line = reader.readLine()) != null) { // lecture des longHeaders
//					System.out.println("line2="+line);
					lineNumber++;
					if (i == headersPos) {
						headers = line.split(csvSplitBy);
						break;
					}
					i++;
				}
				i++;
				while ((line = reader.readLine()) != null) { // lecture des shortHeaders
//					System.out.println("line3="+line);
					lineNumber++;
					if (i == longHeadersPos) {
						longHeaders = line.split(csvSplitBy);
						break;
					}
					i++;
				}

			}

		} else { // pas de long headers
			int i = 0;
			while ((line = reader.readLine()) != null) { // lecture des shortHeaders
//				System.out.println("line4="+line);
				lineNumber++;
				if (i == headersPos) {
					headers = line.split(csvSplitBy);
					break;
				}
				i++;
			}
		}

		if (headers==null) {
			throw new Error("headers not initialized");
		}
		// normalize headers
		for (int i = 0; i < headers.length; i++) {
			String newHeader = DOMUtil.deAccent(headers[i]).replace("-", "_")
					.replace(":", "_").replace(".", "_").replace(" ", "_")
					.replace("'", "_").replace("\"", "_");
			headers[i] = newHeader;
		}
		return lineNumber;
	}

	public static String asString(String[] tab) throws IOException {
		StringBuffer buff = new StringBuffer();
		for (String header : tab) {
			buff.append(header).append("\t");
		}
		return buff.toString();
	}

	@Override
	public Document parse() throws IOException {
		
		Document doc = DOMUtil.createDocument();
		Element root = doc.createElement(rootName);
		doc.appendChild(root);
		int lineNumber = 0;
		String line = "";
		try {
			lineNumber = initHeaders(reader);
			if (longHeaders != null) {
				putOneRow(root, longHeaders, this.headerNames);
			}

			while ((line = reader.readLine()) != null) {
				lineNumber++;

				// use comma as separator
				String[] colums = line.split(csvSplitBy);
//				System.out.println("line5="+line+"\n"+colums.length);
				putOneRow(root, colums, this.rowName);
			}

		} catch (Exception e) {
			String mess = e.getClass().getName()+" : "+e.getMessage()+" lineNb = "+lineNumber+" line = "+line;
			throw new IOException(mess);
		} finally {
			if (reader != null) {
				try {
					reader.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		return doc;
	}

	private void putOneRow(Element root, String[] colums, String rowName) {
		Document doc = root.getOwnerDocument();
		Element row = doc.createElement(rowName);
		root.appendChild(row);
		for (int j = 0; j < headers.length; j++) {
			Element property = doc.createElement(headers[j]);
			row.appendChild(property);
			if (j >= colums.length) {
				property.setTextContent("null");
			} else {
				property.setTextContent(colums[j]);
			}
		}
	}

	public String getHeaderNames() {
		return headerNames;
	}

	public void setHeaderNames(String headerNames) {
		this.headerNames = headerNames;
	}

	public String getRowName() {
		return rowName;
	}

	public void setRowName(String rowName) {
		this.rowName = rowName;
		this.rootName = rowName + "s";
	}

	public String getEncoding() {
		return encoding;
	}

	public void setEncoding(String encoding) {
		this.encoding = encoding;
	}

	public String getRootName() {
		return rootName;
	}

	public void setRootName(String rootName) {
		this.rootName = rootName;
	}

	public String getCsvSplitBy() {
		return csvSplitBy;
	}

	public void setCsvSplitBy(String csvSplitBy) {
		this.csvSplitBy = csvSplitBy;
	}

	public int getHeadersPos() {
		return headersPos;
	}

	public void setHeadersPos(int headersPos) {
		this.headersPos = headersPos;
	}

	public int getLongHeadersPos() {
		return longHeadersPos;
	}

	public void setLongHeadersPos(int longHeadersPos) {
		this.longHeadersPos = longHeadersPos;
	}
	


}

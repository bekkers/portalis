package fr.irisa.lis.portalis.commons;

import java.util.ArrayList;
import java.util.List;

import org.w3c.dom.CharacterData;
import org.w3c.dom.DOMException;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;


public class DOMReader {

	public DOMReader() {
		super();
	}

	protected void isValidName(String name) throws DOMException {
		if (name == null) {
			String mess = "isValidName() nom d'attribut ou nom d'élément null";
			throw new DOMException(DOMException.NOT_FOUND_ERR, mess);
		} else if (name.length() == 0) {
			String mess = "isValidName() nom d'attribut ou nom d'élément vide";
			throw new DOMException(DOMException.NOT_FOUND_ERR, mess);
		}
	}

	protected String getTextValue(Element elem) throws DOMException {
		if (elem == null ) {
			String mess = "erreur interne getTextValue(null)";
			throw new DOMException(DOMException.NOT_FOUND_ERR, mess);
		}
		Node textNode = elem.getFirstChild();
		if (textNode == null) {
			String mess = String.format("manque le fils text() ou cdata() au sein de l'élément <%s>", elem.getNodeName());
			throw new DOMException(DOMException.NOT_FOUND_ERR, mess);
		} else if (textNode.getNodeType()==Element.TEXT_NODE || textNode.getNodeType()==Element.CDATA_SECTION_NODE) {
			return textNode.getNodeValue();
		} else {
			String mess = String.format("L'élément <%s> ne contient pas du text, il contient %s", elem.getNodeName(), DOMUtil.prettyXmlString(textNode));
			throw new DOMException(DOMException.NOT_FOUND_ERR, mess);
			
		}
	}

	public Element getChildElement(Element root, String name)
			throws DOMException {
				return getChildElement(root, name, false);
			}

	public Element getChildElement(Element root, String name, boolean isOptional)
			throws DOMException {
				isValidName(name);
				// on prend le premier élément de nom 'name' fils de <root>
				NodeList nodeList = root.getElementsByTagName(name);
				if (nodeList==null || nodeList.getLength()== 0) {
					if (isOptional)
						 return null;
					String errMess = String.format("Manque l'élément fils <%s> au sein de l'élément <%s>", name, root.getNodeName());
					throw new DOMException(DOMException.NOT_FOUND_ERR, errMess);
				} else if (nodeList.getLength()!=1) {
					String errMess = String.format("%d éléments fils <%s> (au lieu d'un seul) au sein de l'élément\n%s", 
							nodeList.getLength(), name, DOMUtil.prettyXmlString(root));
					throw new DOMException(DOMException.NOT_FOUND_ERR, errMess);
				}
				Element s = (Element) nodeList.item(0);
			
				return s;
			}

	public List<Element> getChildElements(Element root, String name)
			throws DOMException {
				isValidName(name);
				// on prend le premier élément de nom 'name' fils de <root>
				NodeList nodeList = root.getElementsByTagName(name);
				List<Element> result = new ArrayList<Element>(); 
				if (nodeList!=null) {
					for (int i = 0; i<nodeList.getLength(); i++)  {
						result.add((Element) nodeList.item(i)); 
					}
				}
				return result;
			}

	public List<Element> getChildElements(Element root) throws DOMException {
		NodeList nodeList = root.getChildNodes();
		List<Element> result = new ArrayList<Element>(); 
		if (nodeList!=null) {
			for (int i = 0; i<nodeList.getLength(); i++)  {
				Node node = nodeList.item(i);
				if (node instanceof Element)
					result.add((Element) node); 
			}
		}
		return result;
	}

	public boolean getBooleanAttribute(Element root, String attName)
			throws DOMException {
				String boolValue = getStringAttribute(root, attName);
				return Boolean.parseBoolean(boolValue);
			}

	public String getStringElement(Element root, String name)
			throws DOMException {
				return getTextValue(getChildElement(root, name, false));
			}

	public List<String> getStringElements(Element root, String name)
			throws DOMException {
				isValidName(name);
				List<Element> listElem = getChildElements(root, name);;
				List<String> reponse = new ArrayList<String>();
			
				for (Element elem : listElem) {
					reponse.add(getTextValue(elem));
				}
			
				return reponse;
			}

	public String getStringAttribute(Element root, String name, String defaultValue)
			throws DOMException {
				isValidName(name);
				String s = getAttribute(root, name);
				if (s == null) {
					s = defaultValue;
				}
			
				return s;
			}

	public String getStringAttribute(Element root, String name)
			throws DOMException {
				isValidName(name);
				String s = getAttribute(root, name);
				if (s == null) {
					String mess = String.format("manque l'attribut '%s' pour l'élément %s", 
							name, DOMUtil.prettyXmlString(root));
					throw new DOMException(DOMException.NOT_FOUND_ERR, mess);
				}
			
				return s;
			}

	public int getIntElement(Element elem, String name)
			throws DOMException {
				isValidName(name);
				String val = getStringElement(elem, name);
				int i = 0;
				try {
					i = Integer.parseInt(val);
				} catch (NumberFormatException e) {
					String mess = String.format("Contenu non numérique = '%s', pour l'élement %s", 
							val, DOMUtil.prettyXmlString(elem));
					throw new DOMException(DOMException.NOT_FOUND_ERR, mess);
				}
				return i;
			}

	public List<Integer> getIntElements(Element root, String name)
			throws DOMException {
				isValidName(name);
				List<Element> listElem = getChildElements(root, name);
				List<Integer> reponse = new ArrayList<Integer>();
			
				for (Element elem : listElem) {
					String val = getTextValue(elem);
					int i = 0;
					try {
						i = Integer.parseInt(val);
					} catch (NumberFormatException e) {
						String mess = "Contenu d'élement <" + name
								+ "> non numérique = " + val;
						throw new DOMException(DOMException.NOT_FOUND_ERR, mess);
					}
					reponse.add(new Integer(i));
				}
			
				return reponse;
			}

	public int getIntAttribute(Element elem, String attName)
			throws DOMException {
				String val = getStringAttribute(elem, attName);
			
				int i = 0;
				try {
					i = Integer.parseInt(val);
				} catch (NumberFormatException e) {
					String mess = "Valeur d'attribut " + attName + " non numérique = "
							+ val;
					throw new DOMException(DOMException.NOT_FOUND_ERR, mess);
				}
				return i;
			}

	public void checkElement(Element root, String name)
			throws DOMException {
				isValidName(name);
				if (root == null) {
					String mess = "Erreur interne checkElement(null, "+name+")";
					throw new DOMException(DOMException.NOT_FOUND_ERR, mess);			
				} else if (!root.getNodeName().equals(name)) {
					String mess = "Le nom de l'élément réponse <" + root.getNodeName()
							+ "> incorrect, il devrait être '" + name + "'";
					throw new DOMException(DOMException.NOT_FOUND_ERR, mess);
				}
			}

	protected String getAttribute(Element root, String name) {
		String result = root.getAttribute(name);
		return result.length()==0 ? null : result;
	}
	
	  public static String getCharacterDataFromElement(Element e) throws DOMException {
		    Node child = e.getFirstChild();
		    if (child instanceof CharacterData) {
		      CharacterData cd = (CharacterData) child;
		      return cd.getData();
		    }
		    throw new DOMException(DOMException.NOT_FOUND_ERR, "pas de section CDATA dans l'élément "+e.getNodeName());
		  }




}
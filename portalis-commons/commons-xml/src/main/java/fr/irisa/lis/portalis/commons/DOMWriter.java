package fr.irisa.lis.portalis.commons;

import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;


//import org.apache.xml.serialize.XMLSerializer;

public class DOMWriter {

	static final String attributeModele = "([a-zA-z]*=\\\".*?\\\"\\s*)";

	public static void addChild(Element parent, Element child) {
		Node node = parent.getOwnerDocument().importNode(child, true);
		parent.appendChild(node);
	}

	public static Element createRootElement(String nodeName) {
		Document doc = DOMUtil.createDocument();
		Element child = doc.createElement(nodeName);
		doc.appendChild(child);
		return child;
	}

	public static Element createAddChild(Element parent, String nodeName) {
		Document doc = parent.getOwnerDocument();
		Element child = doc.createElement(nodeName);
		parent.appendChild(child);
		return child;
	}

	public static void addAttribute(Element parent, String attName,
			String attrValue) {
		Document doc = parent.getOwnerDocument();
		Attr attr = doc.createAttribute(attName);
		parent.setAttributeNode(attr);
		attr.setValue(attrValue);
	}

	public static void addTextChild(Element parent, String nodeName, String text) {
		Document doc = parent.getOwnerDocument();
		Element child = doc.createElement(nodeName);
		child.setTextContent(text);
		parent.appendChild(child);
	}

}

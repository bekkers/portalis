package fr.irisa.lis.portalis.commons;

import java.io.BufferedWriter;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.io.StringWriter;
import java.io.Writer;
import java.text.Normalizer;
import java.util.regex.Pattern;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.TransformerFactoryConfigurationError;
import javax.xml.transform.dom.DOMResult;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.xml.sax.SAXException;

import fr.irisa.lis.portalis.commons.core.CommonsUtil;

public class DOMUtil {

	private static final String MESSAGE = "message";


	public static void writeFile(Document doc, String fileName)
			throws IOException, TransformerException {
		Writer writer = new FileWriter(fileName);
		writeFile(doc, writer);
	}


	private static void writeFile(Document doc, Writer writer)
			throws TransformerFactoryConfigurationError,
			TransformerConfigurationException, TransformerException {
		DOMSource domSource = new DOMSource(doc);
		StreamResult result = new StreamResult(writer);
		TransformerFactory tf = TransformerFactory.newInstance();
		Transformer transformer = tf.newTransformer();
		transformer.setOutputProperty(OutputKeys.ENCODING, CommonsUtil.DEFAULT_ENCODING);
		transformer.setOutputProperty(OutputKeys.INDENT, "yes");
		transformer.transform(domSource, result);
	}

	public static String prettyXmlString(Node elem) {
		StringWriter writer = new StringWriter();
		prettyPrintXml(elem, writer);
		return writer.toString();
	}

	public static void prettyPrintXml(Node elem, String fileName) {
		try {
			Writer writer = new BufferedWriter(new OutputStreamWriter(
					new FileOutputStream(fileName), CommonsUtil.DEFAULT_ENCODING));
			prettyPrintXml(elem, writer);
		} catch (Exception e) {
			throw new Error(e);
		}
	}

	public static void prettyPrintXml(Node elem, File file) {
		try {
			Writer writer = new BufferedWriter(new OutputStreamWriter(
					new FileOutputStream(file), CommonsUtil.DEFAULT_ENCODING));
			prettyPrintXml(elem, writer);
		} catch (Exception e) {
			throw new Error(e);
		}
	}

	public static void prettyPrintXml(Node elem, String fileName,
			String encoding) {
		try {
			Writer writer = new BufferedWriter(new OutputStreamWriter(
					new FileOutputStream(fileName), encoding));
			prettyPrintXml(elem, writer);
		} catch (Exception e) {
			throw new Error(e);
		}
	}

	public static String deAccent(String str) {
		String nfdNormalizedString = Normalizer.normalize(str,
				Normalizer.Form.NFD);
		Pattern pattern = Pattern.compile("\\p{InCombiningDiacriticalMarks}+");
		return pattern.matcher(nfdNormalizedString).replaceAll("");
	}

	private static void prettyPrintXml(Node elem, Writer writer) {
		try {
			DOMSource domSource = new DOMSource(elem);
			StreamResult result = new StreamResult(writer);
			TransformerFactory tf = TransformerFactory.newInstance();
			Transformer transformer = tf.newTransformer();
			transformer.setOutputProperty(OutputKeys.CDATA_SECTION_ELEMENTS,
					MESSAGE);
			transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION,
					"yes");
			transformer
					.setOutputProperty(OutputKeys.ENCODING, CommonsUtil.DEFAULT_ENCODING);
			transformer.setOutputProperty(OutputKeys.INDENT, "yes");
			transformer.setOutputProperty(
					"{http://xml.apache.org/xslt}indent-amount", "3");
			transformer.transform(domSource, result);
			writer.flush();
		} catch (Exception e) {
			throw new Error(e);
		}
	}

	public static Element parseXMLString(String string) {
		Document doc = null;
		try {
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory
					.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			doc = dBuilder.parse(new ByteArrayInputStream(string
					.getBytes(CommonsUtil.DEFAULT_ENCODING)));
		} catch (IllegalStateException e) {
			// Http connection is impossible
			String errMess = "impossible to connect using http during parseXML()\n"
					+ e.getMessage() + "\nCause = " + e.getCause() == null ? "no cause"
					: e.getCause().getMessage();
			return errorElement(errMess);
		} catch (IOException e) {
			// I/O failure such as socket timeout or an socket reset
			String errMess = "I/O failure during http request during parseXML()\n"
					+ e.getMessage() + "\nCause = " + e.getCause() == null ? "no cause"
					: e.getCause().getMessage();
			return errorElement(errMess);
		} catch (Exception e) {
			String errMess = "parse XML error";
			return errorElement(errMess);
		}
		return doc.getDocumentElement();
	}

	public static Element errorElement(String mess) {
		Document doc = createDocument();
		Element root = doc.createElement("error");
		root.setAttribute(MESSAGE, mess);
		doc.appendChild(root);
		return root;
	}

	public static Document createDocument() {
		DocumentBuilderFactory docFactory = DocumentBuilderFactory
				.newInstance();
		DocumentBuilder docBuilder;
		try {
			docBuilder = docFactory.newDocumentBuilder();
		} catch (ParserConfigurationException e) {
			throw new RuntimeException(
					"impossible de créer l'élément avec DOM", e);
		}

		// root elements
		Document doc = docBuilder.newDocument();
		return doc;
	}

	public static Document readFile(String xqyFileName) throws IOException,
			SAXException, ParserConfigurationException {
		ClassLoader classloader = Thread.currentThread()
				.getContextClassLoader();

		InputStream inputStream = classloader.getResourceAsStream(xqyFileName);
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		DocumentBuilder db = dbf.newDocumentBuilder();
		return db.parse(inputStream);
	}

	static public Document readFile(InputStream file) throws SAXException,
			IOException, ParserConfigurationException {
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		DocumentBuilder db = dbf.newDocumentBuilder();
		return db.parse(file);
	}

	public static Document xsltTransform(Document doc, String xsltFileName)
			throws TransformerFactoryConfigurationError,
			TransformerConfigurationException, TransformerException,
			ParserConfigurationException {
		Source source = new DOMSource(doc);
		Document newDoc = xsltTransform(source, xsltFileName);
		return newDoc;
	}

	public static Document xsltTransform(Source source)
			throws TransformerConfigurationException,
			TransformerFactoryConfigurationError, TransformerException {
		return xsltTransform(source, null);
	}

	public static Document xsltTransform(Source source, String xsltFileName)
			throws TransformerFactoryConfigurationError,
			TransformerConfigurationException, TransformerException {

		TransformerFactory factory = TransformerFactory.newInstance();

		Transformer transformer = null;
		if (xsltFileName == null) {
			transformer = factory.newTransformer();
		} else {
			ClassLoader classloader = Thread.currentThread()
					.getContextClassLoader();
			InputStream xsltFile = classloader.getResourceAsStream(xsltFileName);
			Source xslt = new StreamSource(xsltFile);
			transformer = factory.newTransformer(xslt);
		}

		DOMResult domResult = new DOMResult();
		transformer.transform(source, domResult);
		Node rootNode = domResult.getNode();
		Document newDoc = rootNode.getOwnerDocument();
		return newDoc;
	}

}

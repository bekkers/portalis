package fr.irisa.lis.portalis.commons;

/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.3.1</a>, using an XML
 * Schema.
 * $Id$
 */

/**
 * Class PersonnesType.
 * 
 * @version $Revision$ $Date$
 */
@SuppressWarnings("serial")
public class PersonnesType implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _personneList.
     */
    private java.util.Vector<Personne> _personneList;


      //----------------/
     //- Constructors -/
    //----------------/

    public PersonnesType() {
        super();
        this._personneList = new java.util.Vector<Personne>();
    }


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * 
     * 
     * @param vPersonne
     * @throws java.lang.IndexOutOfBoundsException if the index
     * given is outside the bounds of the collection
     */
    public void addPersonne(
            final Personne vPersonne)
    throws java.lang.IndexOutOfBoundsException {
        this._personneList.addElement(vPersonne);
    }

    /**
     * 
     * 
     * @param index
     * @param vPersonne
     * @throws java.lang.IndexOutOfBoundsException if the index
     * given is outside the bounds of the collection
     */
    public void addPersonne(
            final int index,
            final Personne vPersonne)
    throws java.lang.IndexOutOfBoundsException {
        this._personneList.add(index, vPersonne);
    }

    /**
     * Method enumeratePersonne.
     * 
     * @return an Enumeration over all Personne elements
     */
    public java.util.Enumeration<? extends Personne> enumeratePersonne(
    ) {
        return this._personneList.elements();
    }

    /**
     * Method getPersonne.
     * 
     * @param index
     * @throws java.lang.IndexOutOfBoundsException if the index
     * given is outside the bounds of the collection
     * @return the value of the Personne at the given index
     */
    public Personne getPersonne(
            final int index)
    throws java.lang.IndexOutOfBoundsException {
        // check bounds for index
        if (index < 0 || index >= this._personneList.size()) {
            throw new IndexOutOfBoundsException("getPersonne: Index value '" + index + "' not in range [0.." + (this._personneList.size() - 1) + "]");
        }

        return (Personne) _personneList.get(index);
    }

    /**
     * Method getPersonne.Returns the contents of the collection in
     * an Array.  <p>Note:  Just in case the collection contents
     * are changing in another thread, we pass a 0-length Array of
     * the correct type into the API call.  This way we <i>know</i>
     * that the Array returned is of exactly the correct length.
     * 
     * @return this collection as an Array
     */
    public Personne[] getPersonne(
    ) {
        Personne[] array = new Personne[0];
        return (Personne[]) this._personneList.toArray(array);
    }

    /**
     * Method getPersonneCount.
     * 
     * @return the size of this collection
     */
    public int getPersonneCount(
    ) {
        return this._personneList.size();
    }

    /**
     */
    public void removeAllPersonne(
    ) {
        this._personneList.clear();
    }

    /**
     * Method removePersonne.
     * 
     * @param vPersonne
     * @return true if the object was removed from the collection.
     */
    public boolean removePersonne(
            final Personne vPersonne) {
        boolean removed = _personneList.remove(vPersonne);
        return removed;
    }

    /**
     * Method removePersonneAt.
     * 
     * @param index
     * @return the element removed from the collection
     */
    public Personne removePersonneAt(
            final int index) {
        java.lang.Object obj = this._personneList.remove(index);
        return (Personne) obj;
    }

    /**
     * 
     * 
     * @param index
     * @param vPersonne
     * @throws java.lang.IndexOutOfBoundsException if the index
     * given is outside the bounds of the collection
     */
    public void setPersonne(
            final int index,
            final Personne vPersonne)
    throws java.lang.IndexOutOfBoundsException {
        // check bounds for index
        if (index < 0 || index >= this._personneList.size()) {
            throw new IndexOutOfBoundsException("setPersonne: Index value '" + index + "' not in range [0.." + (this._personneList.size() - 1) + "]");
        }

        this._personneList.set(index, vPersonne);
    }

    /**
     * 
     * 
     * @param vPersonneArray
     */
    public void setPersonne(
            final Personne[] vPersonneArray) {
        //-- copy array
        _personneList.clear();

        for (int i = 0; i < vPersonneArray.length; i++) {
                this._personneList.add(vPersonneArray[i]);
        }
    }

}

package fr.irisa.lis.portalis.commons;

/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.3.1</a>, using an XML
 * Schema.
 * $Id$
 */

/**
 * Class Personne.
 * 
 * @version $Revision$ $Date$
 */
@SuppressWarnings("serial")
public class Personne extends PersonneType 
implements java.io.Serializable
{


      //----------------/
     //- Constructors -/
    //----------------/

    public Personne() {
        super();
    }

}

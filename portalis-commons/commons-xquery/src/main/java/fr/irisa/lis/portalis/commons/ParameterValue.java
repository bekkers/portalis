package fr.irisa.lis.portalis.commons;

import net.sf.saxon.s9api.QName;
import net.sf.saxon.s9api.XdmValue;

public class ParameterValue {
	
	private QName name;
	private XdmValue value;
	
	public ParameterValue(){}

	public ParameterValue(QName name, XdmValue value){
		this.name = name;
		this.value = value;
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
	}

	public QName getName() {
		return name;
	}

	public void setName(QName name) {
		this.name = name;
	}

	public XdmValue getValue() {
		return value;
	}

	public void setValue(XdmValue value) {
		this.value = value;
	}

}

package fr.irisa.lis.portalis.commons;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.TransformerFactoryConfigurationError;
import javax.xml.transform.dom.DOMResult;
import javax.xml.transform.dom.DOMSource;

import net.sf.saxon.s9api.DocumentBuilder;
import net.sf.saxon.s9api.Processor;
import net.sf.saxon.s9api.QName;
import net.sf.saxon.s9api.SaxonApiException;
import net.sf.saxon.s9api.Serializer;
import net.sf.saxon.s9api.WhitespaceStrippingPolicy;
import net.sf.saxon.s9api.XQueryCompiler;
import net.sf.saxon.s9api.XQueryEvaluator;
import net.sf.saxon.s9api.XQueryExecutable;
import net.sf.saxon.s9api.XdmDestination;
import net.sf.saxon.s9api.XdmItem;
import net.sf.saxon.s9api.XdmNode;
import net.sf.saxon.s9api.XdmValue;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Node;

import fr.irisa.lis.portalis.commons.core.CommonsUtil;
import fr.irisa.lis.portalis.commons.pipe.StringOutputStream;

public class XqueryUtil {

	private static final Logger LOGGER = LoggerFactory.getLogger(XqueryUtil.class.getName());

	private static final Map<String, XQueryExecutable> queries = new HashMap<String, XQueryExecutable>();
	private Processor proc = new Processor(true);

	public XdmNode xqueryTransform(Node doc, String xqyFileName)
			throws Exception {
		return xqueryTransform(doc, xqyFileName,
				new ArrayList<ParameterValue>());
	}

	public Document toDomEval(Document root, String xqyFile, String function)
			throws Exception {
		XdmNode xdmNode = eval(root, xqyFile, function);
		if (xdmNode == null)
			return null;
		return XdmNodeToDomDocument(xdmNode);
	}

	public String toStringEval(Document root, String xqyFile, String function)
			throws Exception {

		XdmNode xdmNode = eval(root, xqyFile, function);
		if (xdmNode == null)
			return null;

		StringOutputStream outputStream = new StringOutputStream();
		serialize(xdmNode, outputStream);
		return outputStream.toString();
	}

	public Document toDomEval(Document root, String xqyFile, String function, XdmItem[] params)
			throws Exception {
		XdmNode xdmNode = eval(root, xqyFile, function, params);
		if (xdmNode == null)
			return null;
		return XdmNodeToDomDocument(xdmNode);
	}

	public String toStringEval(Document root, String xqyFile, String function, XdmItem[] params)
			throws Exception {

		XdmNode xdmNode = eval(root, xqyFile, function, params);
		if (xdmNode == null)
			return null;

		StringOutputStream outputStream = new StringOutputStream();
		serialize(xdmNode, outputStream);
		return outputStream.toString();
	}

	private XdmNode eval(Document root, String xqyFile, String function)
			throws Exception {
		LOGGER.debug(xqyFile+" : "+function);
		List<ParameterValue> externalParams = new ArrayList<ParameterValue>();
		externalParams.add(new ParameterValue(new QName("root"),
				fromDOMToXdmNode(root)));

		QName functionName = new QName("http://www.irisa.fr/bekkers", function);
		XdmValue result = callFunction(xqyFile, functionName, externalParams);
		if (result == null || result.itemAt(0) == null
				|| result.itemAt(0).isAtomicValue()) {
			return null;
		}
		XdmNode xdmNode = (XdmNode) result.itemAt(0);
		return xdmNode;
	}

	private XdmNode eval(Document root, String xqyFile, String function, XdmItem[] params)
			throws Exception {
		LOGGER.debug(xqyFile+" : "+function);
		System.out.println("eval "+xqyFile+" : "+function);


		List<ParameterValue> externalParams = new ArrayList<ParameterValue>();
		externalParams.add(new ParameterValue(new QName("root"),
				fromDOMToXdmNode(root)));

		QName functionName = new QName("http://www.irisa.fr/bekkers", function);
		XdmValue result = callFunction(xqyFile, functionName, params, externalParams);
		if (result == null || result.itemAt(0) == null
				|| result.itemAt(0).isAtomicValue()) {
			return null;
		}
		XdmNode xdmNode = (XdmNode) result.itemAt(0);
		System.out.println("eval "+xdmNode.getStringValue());
		return xdmNode;
	}

	public XdmNode xqueryTransform(Node doc, String xqyFileName,
			List<ParameterValue> externalVariables) throws Exception {

		XQueryEvaluator evaluator = getXQueryEvaluator(xqyFileName);

		for (ParameterValue param : externalVariables) {
			evaluator.setExternalVariable(param.getName(), param.getValue());
		}

		Source source = new DOMSource(doc);
		evaluator.setSource(source);
		XdmDestination dest = new XdmDestination();
		evaluator.run(dest);
		return dest.getXdmNode();
	}

	private XQueryEvaluator getXQueryEvaluator(String xqyFileName)
			throws Exception {
		synchronized (queries) {
			ClassLoader classloader = Thread.currentThread()
					.getContextClassLoader();
			URL url = classloader.getResource(xqyFileName);
			XQueryExecutable compiledXqy = queries.get(url.toString());
			if (compiledXqy == null) {

				InputStreamReader inputStream = new InputStreamReader(
						url.openStream());
				;

				XQueryCompiler compiler = proc.newXQueryCompiler();
				try {
					compiledXqy = compiler.compile(inputStream);
				} catch (net.sf.saxon.s9api.SaxonApiException e) {
					String mess = e.getMessage() + "\n" + CommonsUtil.stack2string(e);
					throw new Exception(mess);
				}
				queries.put(url.toString(), compiledXqy);
			}
			return compiledXqy.load();
		}
	}

	public XdmValue callFunction(String xqyFileName, QName qName,
			List<ParameterValue> externalParams) throws Exception {
		return callFunction(xqyFileName, qName, new XdmItem[0], externalParams);
	}

	public XdmValue callFunction(String xqyFileName, QName qName)
			throws Exception {
		return callFunction(xqyFileName, qName, new XdmItem[0],
				new ArrayList<ParameterValue>());
	}

	public XdmValue callFunction(String xqyFileName, QName qName,
			XdmItem[] params) throws Exception {
		return callFunction(xqyFileName, qName, params,
				new ArrayList<ParameterValue>());
	}

	public XdmValue callFunction(String xqyFileName, QName qName,
			XdmItem[] params, List<ParameterValue> externalParams)
			throws Exception {

		XQueryEvaluator evaluator = getXQueryEvaluator(xqyFileName);

		for (ParameterValue param : externalParams) {
			evaluator.setExternalVariable(param.getName(), param.getValue());
		}

		XdmValue result = evaluator.callFunction(qName, params);
		return result;
	}

	public XdmValue callFunction(String xqyFileName, QName qName,
			XdmItem xdmItem) throws Exception {
		XdmItem[] params = { xdmItem };
		return callFunction(xqyFileName, qName, params);
	}

	public XdmValue callFunction(String xqyFileName, QName qName,
			XdmItem xdmItem, List<ParameterValue> externalParams)
			throws Exception {
		XdmItem[] params = { xdmItem };
		return callFunction(xqyFileName, qName, params, externalParams);
	}

	public XdmNode fromDOMToXdmNode(Node doc) throws SaxonApiException {
		// on cree un document (noeud XDM)
		DocumentBuilder builder = proc.newDocumentBuilder();
		builder.setLineNumbering(true);
		builder.setWhitespaceStrippingPolicy(WhitespaceStrippingPolicy.ALL);
		XdmNode newDoc = builder.build(new DOMSource(doc));
		return newDoc;
	}

	public static void main(String[] args) throws SaxonApiException,
			IOException {
	}

	public static Document XdmNodeToDomDocument(XdmNode xmlNode)
			throws TransformerFactoryConfigurationError,
			TransformerConfigurationException, ParserConfigurationException,
			TransformerException {
		Source source = xmlNode.asSource();
		TransformerFactory tf = TransformerFactory.newInstance();
		Transformer transformer = tf.newTransformer();
		transformer.setOutputProperty(OutputKeys.ENCODING,
				CommonsUtil.DEFAULT_ENCODING);
		transformer.setOutputProperty(OutputKeys.INDENT, "yes");
		transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
		DOMResult domResult = new DOMResult();
		transformer.transform(source, domResult);
		Document doc = (Document) domResult.getNode();
		return doc;
	}

	public void serialize(XdmNode root, OutputStream stream)
			throws SaxonApiException, FileNotFoundException {
		Serializer out = new Serializer();
		out.setOutputProperty(Serializer.Property.METHOD, "xml");
		out.setOutputProperty(Serializer.Property.ENCODING,
				CommonsUtil.DEFAULT_ENCODING);
		out.setOutputProperty(Serializer.Property.INDENT, "yes");
		out.setOutputProperty(Serializer.Property.OMIT_XML_DECLARATION, "yes");
		out.setOutputStream(stream);
		proc.writeXdmValue(root, out);
	}

}

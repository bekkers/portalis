<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>


<%@ page import="java.util.Set"%>

<%@ page import="fr.irisa.lis.portalis.shared.admin.PortalisException"%>
<%@ page import="fr.irisa.lis.portalis.shared.admin.Util"%>
<%@ page import="fr.irisa.lis.portalis.shared.admin.XmlIdentifier"%>
<%@ page import="fr.irisa.lis.portalis.shared.admin.Constants"%>
<%@ page import="fr.irisa.lis.portalis.shared.admin.data.Session"%>
<%@ page import="fr.irisa.lis.portalis.shared.admin.data.ServiceCore"%>
<%@ page import="fr.irisa.lis.portalis.shared.admin.data.Application"%>

<%@ page import="fr.irisa.lis.portalis.shared.http.camelis.CamelisHttp"%>
<%@ page import="fr.irisa.lis.portalis.shared.http.admin.AdminHttp"%>

<%@ page
	import="fr.irisa.lis.portalis.shared.camelis.reponse.StartReponse"%>
<%@ page import="fr.irisa.lis.portalis.shared.admin.data.CamelisService"%>
<%@ page import="fr.irisa.lis.portalis.shared.admin.data.PortalisService"%>
<%@ page
	import="fr.irisa.lis.portalis.shared.camelis.reponse.PingReponse"%>

<%@ page import="fr.irisa.lis.portalis.shared.admin.reponse.SiteReponse"%>

<%@ page import="fr.irisa.lis.portalis.clientLisJsp.ClientLisJsp"%>


<%@ page import="java.util.logging.Logger"%>

<%!private static final String jspName = "testCamelis.jsp";
	static final private LoggerProvider log = LoggerProvider
			.getDefault("fr.irisa.lis.portalis.admin." + jspName);%>
<%
	response.setContentType("text/html; charset=UTF-8");
	response.setHeader("Expires", "0");
%>
<html>
<head>
<meta charset="UTF-8" />
<title>XML LIS server -- test</title>
</head>

<body>
	<jsp:include page="header.html" />
	<%
		LOGGER.fine("\n================================================= "
				+ jspName
				+ " =================================================");

	Session camelisSession = (Session) session
			.getAttribute(XmlIdentifier.CAMELIS_SESSION_ID);
	
	CamelisService camelisService = null;
	
	int port = 0;
	String portNum = null;
	String host = null;
	
	String portalisPort = PortalisService.getPORTALIS_PORT() + "";
	
	String key = null;
	
	
	try {
		if (camelisSession == null) {
			String message = String.format(
					"Erreur %s : pas de session camelis en cours",
					jspName);
			throw new PortalisException(message);
		}
		LOGGER.fine("camelisSession = " + camelisSession);
		
		camelisService = (CamelisService) camelisSession.getActiveService();
		if (camelisService == null) {
			String message = String.format(
					"Erreur %s : pas de serviceCamelis dans la session \n%s",
					jspName, camelisSession.toString());
			throw new PortalisException(message);
		}

		host = camelisService.getHost();
		port = camelisService.getPort();
		portNum = host + "";
		
		key = camelisSession.getActiveUser().getPortalisSessionId();

		} catch (Exception e) {
			String mess = "Erreur " + jspName + e.getMessage();
			LOGGER.severe(mess+Util.stack2string( e));
	%>
	<h1>Erreur dans le chargement de <%=jspName%></h1>
	<pre><%=mess%></pre>
</body>
</html>
	<%
		}
	%>
	<h1>Test Camelis server on <%=host%>:<%=port%></h1>
	<table border="1">
		<tr>
			<td>Your user key</td>
			<td><%=key%></td>
		</tr>
		<tr>
			<td>Creator</td>
			<td><%=camelisService.getCreator()%></td>
		</tr>
		<tr>
			<td>Service</td>
			<td><%=camelisService.getId()%></td>
		</tr>
		<tr>
			<td>Nb Objects</td>
			<td><%=camelisService.getNbObject()%></td>
		</tr>
		<tr>
			<td>Linux process id</td>
			<td><%=camelisService.getPid()%></td>
		</tr>
		<tr>
			<td>Date of creation</td>
			<td><%=Util.writeDate(camelisService.getActivationDate())%></td>
		</tr>
		<tr>
			<td>LastUpdate</td>
			<td><%=Util.writeDate(camelisService.getLastUpdate())%></td>
		</tr>
		<tr>
			<td colspan="2" align="center">
				<form
					action="http://<%=host%>:<%=portalisPort%>/clientlis/testCamelis.jsp">
					<input type="hidden" name="<%=XmlIdentifier.SERVICE_NAME()%>"
						value="<%=camelisService.getId()%>" /> <input type="submit"
						value="Refresh" />
				</form>
			</td>
		</tr>
	</table>
	<h2>Portalis Admin commands</h2>
	<table>
		<tr>
			<td>
				<form action="http://<%=host%>:<%=portalisPort%>/portalis/ping.jsp">
					<input type="submit" value="Ping All Camelis" />
				</form>
			</td>
			<td>
				<form
					action="http://<%=host%>:<%=portalisPort%>/clientlis/sortieStopCamelis.jsp">
					<input type="hidden" value="<%=portNum%>"
						name="<%=XmlIdentifier.PORT()%>" /> <input type="submit"
						value="Stop this Camelis" />
				</form>
			</td>
			<td>
				<form
					action="http://<%=host%>:<%=portalisPort%>/clientlis/sortieStopCamelis.jsp">
					<input type="submit" value="Stop all Camelis" />
				</form>
			</td>
			<td>
				<form
					action="http://<%=host%>:<%=portalisPort%>/clientlis/sortieTestPortalis.jsp">
					<input type="hidden" value="<%=camelisService.getCreator()%>"
						name="email" /> <input type="submit" value="Logout" />
				</form>
			</td>
			<td>
				<form
					action="http://<%=host%>:<%=portalisPort%>/clientlis/testPortalis.jsp">
					<input type="hidden" value="<%=camelisService.getCreator()%>"
						name="email" /> <input type="submit"
						value="retour à Test Portalis Admin" />
				</form>
			</td>
		</tr>
	</table>

	<h2>File Upload</h2>

	<form action="uploadFile.jsp" method="post"
		enctype="multipart/form-data">
		<table border="1">
			<tr>
				<td>Select a file to upload</td>
				<td><input type="file" name="file" size="50" /></td>
			</tr>
			<tr>
				<td colspan="2">
					<div align="center">
						<input type="submit" value="Upload File" />
					</div>
				</td>
			</tr>
		</table>
	</form>

	<h2>Misc Camelis commands</h2>
	<ul>
		<li><a href='http://<%=host%>:<%=portNum%>/ping'>http://<%=host%>:<%=portNum%>/ping
		</a></li>
		<li><a
			href='http://<%=host%>:<%=portNum%>/pingUsers?userKey=<%=key%>'>http://<%=host%>:<%=portNum%>/pingUsers?userKey=<%=key%></a></li>
	</ul>
	<h2>Camelis Admin commands</h2>
	<ul>
		<li><a
			href='http://<%=host%>:<%=portNum%>/resetCamelis?userKey=<%=key%>'>resetCamelis</a></li>
		<li><a
			href='http://<%=host%>:<%=portNum%>/importLis?userKey=<%=key%>&amp;lisfile=plop.lis'>importLis?lisfile=plop.lis</a></li>
		<li><a
			href='http://<%=host%>:<%=portNum%>/registerKey?adminKey=<%=key%>&amp;userKey=abc&amp;role=reader'>registerKey?userKey=abc&amp;role=reader</a></li>
		<li><a
			href='http://<%=host%>:<%=portNum%>/setRole?adminKey=<%=key%>&amp;userKey=abc123ef45&amp;role=collaborator'>setRole?userKey=abc123ef45&amp;role=collaborator</a></li>
		<li><a
			href='http://<%=host%>:<%=portNum%>/delKey?adminKey=<%=key%>&amp;userKey=987fed65cb'>delKey?userKey=987fed65cb</a></li>
		<li><a
			href='http://<%=host%>:<%=portNum%>/resetCreator?email=toto%40dtc.com&amp;newKey=1234'>resetCreator?email=toto%40dtc.com&amp;newKey=1234</a></li>
		<li><a
			href='http://<%=host%>:<%=portNum%>/importFile?userKey=<%=key%>&amp;file=ferre.bib&amp;recursive=true&amp;parts=true'>importFile</a></li>
	</ul>
	<h2>Collaborator commands</h2>
	<ul>
		<li><a
			href='http://<%=host%>:<%=portNum%>/exportLis?userKey=<%=key%>'>exportLis</a></li>
		<li><a
			href='http://<%=host%>:<%=portNum%>/exportCtx?userKey=<%=key%>'>exportCtx</a></li>
		<li><a
			href='http://<%=host%>:<%=portNum%>/delObjects?userKey=<%=key%>&amp;oid=6&amp;oid=1'>delObjects:
				remove pluto and earth</a></li>
		<li><a
			href='http://<%=host%>:<%=portNum%>/delFeature?userKey=<%=key%>&amp;query=all&amp;feature=Distance&amp;feature=Size'>delFeature:
				remove Distance and Size</a></li>
		<li><a
			href='http://<%=host%>:<%=portNum%>/delFeatureFromReq?userKey=<%=key%>&amp;query=all&amp;reqForOids=near&amp;feature=satellite&amp;feature=small'>delFeatureFromReq:
				near --&gt; remove satellite and small</a></li>
		<li><a
			href='http://<%=host%>:<%=portNum%>/delFeatureFromOid?userKey=<%=key%>&amp;query=all&amp;oid=1&amp;feature=satellite&amp;feature=small'>delFeatureFromOid:
				earth --&gt; remove satellite and small</a></li>
	</ul>
	<h2>Editor commands</h2>
	<ul>
		<li><a
			href='http://<%=host%>:<%=portNum%>/createObject?userKey=<%=key%>&amp;name=moon&amp;feature=near'>createObject:
				moon (near)</a></li>
		<li><a
			href='http://<%=host%>:<%=portNum%>/addAxiom?userKey=<%=key%>&amp;premise=satellite&amp;conclusion=Misc'>addAxiom:
				satellite --&gt; Misc</a></li>
		<li><a
			href='http://<%=host%>:<%=portNum%>/addAxiom?userKey=<%=key%>&amp;premise=satellite&amp;conclusion=Misc&amp;extent=true'>addAxiomX:
				satellite --&gt; Misc</a></li>
		<li><a
			href='http://<%=host%>:<%=portNum%>/setFeaturesFromOids?userKey=<%=key%>&amp;query=all&amp;oid=10&amp;feature=big&amp;feature=small'>setFeaturesFromOids:
				moon --&gt; big and small</a></li>
		<li><a
			href='http://<%=host%>:<%=portNum%>/setFeaturesFromReq?userKey=<%=key%>&amp;query=all&amp;reqForOids=big&amp;feature=plop'>setFeaturesFromReq:
				big--&gt; plop</a></li>
	</ul>
	<h2>Reader commands</h2>
	<ul>
		<li><a
			href='http://<%=host%>:<%=portNum%>/intent?userKey=<%=key%>&amp;oid=1&amp;oid=2'>intent:
				1, 2</a></li>
		<li><a
			href='http://<%=host%>:<%=portNum%>/intent?userKey=<%=key%>&amp;oid=1&amp;oid=2&amp;extent=true'>intentX:
				1, 2</a></li>
		<li><a
			href='http://<%=host%>:<%=portNum%>/getTreeFromReq?wq=all&amp;feature=all&amp;userKey=<%=key%>'>getTreeFromReq</a></li>
		<li><a
			href='http://<%=host%>:<%=portNum%>/getTreeFromReq?wq=all&amp;feature=all&amp;userKey=<%=key%>&amp;extent=true'>getTreeFromReqX</a></li>
		<li><a
			href='http://<%=host%>:<%=portNum%>/extent?userKey=<%=key%>&amp;query=satellite%20and%20near'>extent</a></li>
	</ul>
	<h1>TODO</h1>
	<h2>Editor commands</h2>
	<ul>
		<li><a
			href='http://<%=host%>:<%=portNum%>/addFeatures?userKey=<%=key%>'>addFeatures</a></li>
	</ul>
	<h2>Reader commands</h2>
	<ul>
		<li><a
			href='http://<%=host%>:<%=portNum%>/getTreeFromZoom?userKey=<%=key%>'>getTreeFromZoom</a></li>
		<li><a
			href='http://<%=host%>:<%=portNum%>/getFeaturesFromReq?userKey=<%=key%>'>getTreeFromReq</a></li>
		<li><a
			href='http://<%=host%>:<%=portNum%>/getFeaturesFromOids?userKey=<%=key%>'>getFeaturesFromOids</a></li>
	</ul>
</body>
</html>


package fr.irisa.lis.cargo;

import java.io.IOException;
import java.io.StringWriter;
import java.net.URL;

import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.io.OutputFormat;
import org.dom4j.io.SAXReader;
import org.dom4j.io.XMLWriter;


public class XML {

	public static void checkRootName(Element root, String name)
			throws CargoLauncherException {
		if (!root.getName().equals(name)) {
			throw new CargoLauncherException("Erreur dans le source XML : élément attendu = "
					+ name + " et élément lu = " + root.getName());
		}
	}
	
	public static String prettyXmlString(Element element) throws IOException {

		StringWriter stringWritter = new StringWriter();

		OutputFormat format = OutputFormat.createPrettyPrint();
		XMLWriter writer = new XMLWriter(stringWritter, format);
		writer.write(element);

		String result = stringWritter.toString();
		writer.close();
		return result;
	}

	public static Document parse(URL url) throws DocumentException, IOException {
		SAXReader reader = new SAXReader();
		Document document = reader.read(url);
		return document;
	}

}

package fr.irisa.lis.cargo;

import java.io.IOException;
import java.util.Properties;


/**
 * Classe permettant la lecture des proprietés de l'application en test
 */
@SuppressWarnings("serial")
public class CargoProprietes extends Properties {
	private String myFile;


	public static final CargoProprietes cargoProp = new CargoProprietes(
			"cargo.properties");

	private CargoProprietes(String fileName) {
		myFile = fileName;
		ClassLoader cl = Thread.currentThread().getContextClassLoader();
		java.io.InputStream is = cl.getResourceAsStream(fileName);
		if (is != null) {
			try {
				this.load(is);
			} catch (IOException e) {
				System.out.println(e.getMessage());
			}
		}
	}

	private CargoProprietes() {
		super();
	}

	public String getProperty(String key) {
		String prop = super.getProperty(key);
		return prop;
	}

	public String getProperty(String key, boolean force) throws Exception {
		if (force == false) {
			return getProperty(key);
		} else {
			String prop = super.getProperty(key);
			if (prop==null) {
				System.out.println("Value of property "+myFile+":"+key+" = null");
				return null;
			} else
			    return prop;
		}
	}

}
package fr.irisa.lis.client.example;

import fr.irisa.lis.portalis.shared.admin.data.PortalisService;
import fr.irisa.lis.portalis.shared.admin.reponse.VoidReponse;

public class AbstractClient {

	protected static final String sessionId = "ABCED1234F";
	
	protected final static String ADMIN_EMAIL = "ADMIN@irisa.fr";
	protected final static String ADMIN_PASSWORD = "ADMIN";
	protected final static String BEKKERS_EMAIL = "bekkers@irisa.fr";
	protected final static String BEKKERS_PASSWORD = "yves";
	protected final static String SERVICE_PLANET_PLANET = "planets:planets";

	protected final static String PLANETS_PLANETS_FULL_NAME = "planets:planets";
	protected final static String PATRIMOINE_BRETAGNE_FULL_NAME = "patrimoine:patrimoineBretagne";
	protected final static String FORMAL_CONCEPT_NUMBER_FULL_NAME = "formalConcepts:number";
	protected final static String THABOR_ARBRES_THABOR_FULL_NAME = "svt:arbres_thabor";

	protected static void checkResult(VoidReponse loginReponse) {
		System.out.println(loginReponse);
		if (!loginReponse.isOk())
			stop(loginReponse.getMessagesAsString());
	}

	private static void stop(String messagesAsString) {
		System.out.println("Arrêt en erreur : "+messagesAsString);
		System.exit(0);
	}

	protected static void initPortalisWithArgs(String[] args) {
		System.out.println(args.length);
		String host = "localhost";
		int portNum = 8080;
		String portalisAppli = "portalis";
		if (args.length == 3) {
			host = args[0];
			try {
				portNum = Integer.parseInt(args[1]);
			} catch (NumberFormatException e) {
				stopError();
			}
			portalisAppli = args[2];
		} else if (args.length != 0) {
			stopError();
		}
		PortalisService.getInstance().init(host, portNum, portalisAppli);
	}

	private static void stopError() {
		System.out.println("usage is 'androlis [<hostName> <port number> <portalis application name>]'\n"
				+"   default <hostName> = 'localhost'\n"
				+"   default <port number> = 8080\n"
				+"   default <portalis application name> = portalis");
		System.exit(-1);
	}

	public AbstractClient() {
		super();
	}

}
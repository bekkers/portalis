package fr.irisa.lis.client.example;

import static org.junit.Assert.assertTrue;
import fr.irisa.lis.portalis.shared.admin.PortalisException;
import fr.irisa.lis.portalis.shared.admin.XmlIdentifier;
import fr.irisa.lis.portalis.shared.admin.data.ActiveLisService;
import fr.irisa.lis.portalis.shared.admin.data.ActiveUser;
import fr.irisa.lis.portalis.shared.admin.http.AdminHttp;
import fr.irisa.lis.portalis.shared.admin.reponse.LoginReponse;
import fr.irisa.lis.portalis.shared.admin.reponse.VoidReponse;
import fr.irisa.lis.portalis.shared.camelis.http.CamelisHttp;
import fr.irisa.lis.portalis.shared.camelis.reponse.ExtentReponse;
import fr.irisa.lis.portalis.shared.camelis.reponse.ImportCtxReponse;
import fr.irisa.lis.portalis.shared.camelis.reponse.PingReponse;
import fr.irisa.lis.portalis.shared.camelis.reponse.StartReponse;
import fr.irisa.lis.portalis.shared.camelis.reponse.ZoomReponse;

public class CamelisClientExemple extends AbstractClient 
{
	


	@SuppressWarnings("unused")
	public static void main(String[] args) {

		initPortalisWithArgs(args);

			System.out.println("\n====================================== AdminHttp.login ====================================");
			LoginReponse bekkersLoginReponse = AdminHttp.login(BEKKERS_EMAIL,
					BEKKERS_PASSWORD);
			checkReponse(bekkersLoginReponse);
			ActiveUser bekkersActiveUser = bekkersLoginReponse.getActiveUser();
			String bekkersPortalisSessionId = bekkersActiveUser.getPortalisSessionId();


			System.out.println("\n====================================== AdminHttp.startCamelis ====================================");
			StartReponse startPlanetPlanetReponse = AdminHttp.startCamelis(
					bekkersActiveUser, SERVICE_PLANET_PLANET);
			checkReponse(startPlanetPlanetReponse);
			ActiveLisService activeLisService = startPlanetPlanetReponse
					.getActiveLisService();
			assertTrue("ActiveLisService ne doit pas être null",
					activeLisService != null);

			System.out.println("\n====================================== AdminHttp.login ====================================");
			LoginReponse pietLoginReponse = AdminHttp.login(ADMIN_EMAIL,
					ADMIN_PASSWORD);
			checkReponse(pietLoginReponse);
			ActiveUser pietActiveUser = pietLoginReponse.getActiveUser();
			CamelisHttp camelisHttp = null;
			try {
				camelisHttp = new CamelisHttp(pietActiveUser, activeLisService);
			} catch (PortalisException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}


			System.out.println("\n====================================== importCtx ====================================");
			ImportCtxReponse importCtxReponse = camelisHttp
					.importCtx();
			checkReponse(importCtxReponse);

			System.out.println("\n====================================== 20 fois \"CamelisHttp.ping\" suivi de \"Http.zoom\" ====================================");
			System.out.print("Iterating 100 X 3 commands : ");
			System.out.flush();
			for (int i = 0; i < 20; i++) {
				if (i%5==0) {
					System.out.print(".");
					System.out.flush();
				}
				PingReponse pingReponse = CamelisHttp.ping(activeLisService);

				ExtentReponse extentReponse = camelisHttp.extent();

				ZoomReponse zoomreponse = camelisHttp.zoom("all", "satelite");

			}

			System.out.println("\n\n====================================== AdminHttp.stopCamelis ====================================");
			PingReponse pingReponse = AdminHttp.stopCamelis(
					bekkersActiveUser, activeLisService);
			checkReponse(pingReponse);

			VoidReponse bekkersLogoutReponse = AdminHttp
					.logout(bekkersActiveUser);
			checkReponse(bekkersLogoutReponse);

			System.out.println("\n====================================== AdminHttp.logout 2 ====================================");
			VoidReponse pietLogoutReponse = AdminHttp.logout(pietActiveUser);
			checkReponse(pietLogoutReponse);

			System.out.println("Succes");

	}

	private static void checkReponse(VoidReponse reponse) {
		System.out.println("\n"+reponse);
		if (!reponse.getStatus().equals(XmlIdentifier.OK)) {
			String mess = reponse.getMessagesAsString();
			System.out.println(mess);
			System.exit(1);
		}
	}

}

package fr.irisa.lis.client.example;

import fr.irisa.lis.portalis.shared.admin.ClientConstants;
import fr.irisa.lis.portalis.shared.admin.data.ActiveLisService;
import fr.irisa.lis.portalis.shared.admin.data.ActiveUser;
import fr.irisa.lis.portalis.shared.admin.http.AdminHttp;
import fr.irisa.lis.portalis.shared.admin.reponse.LoginReponse;
import fr.irisa.lis.portalis.shared.admin.reponse.SiteReponse;
import fr.irisa.lis.portalis.shared.admin.reponse.VoidReponse;
import fr.irisa.lis.portalis.shared.camelis.http.CamelisHttp;
import fr.irisa.lis.portalis.shared.camelis.reponse.ExtentReponse;
import fr.irisa.lis.portalis.shared.camelis.reponse.LoadContextReponse;
import fr.irisa.lis.portalis.shared.camelis.reponse.PingReponse;
import fr.irisa.lis.portalis.shared.camelis.reponse.StartReponse;

public class PlanetExample extends AbstractClient {
	
	public static void main(String[] args) {

	initPortalisWithArgs(args);

	try {
		ClientConstants.setPrintHttpRequest(true);

		System.out.println("\n====================================== login ====================================");
		LoginReponse loginReponse = AdminHttp.login(BEKKERS_EMAIL, BEKKERS_PASSWORD);
		checkResult(loginReponse);
		ActiveUser activeUser = loginReponse.getActiveUser();

		System.out.println("\n====================================== getSite ====================================");
		SiteReponse siteReponse = AdminHttp.getSite(activeUser);
		checkResult(siteReponse);		

		
		System.out.println("\n====================================== StartCamelis planets/planets ====================================");
		StartReponse startReponse = AdminHttp.startCamelis(activeUser,
				PLANETS_PLANETS_FULL_NAME);
		checkResult(startReponse);
		ActiveLisService activeLisService = startReponse
				.getActiveLisService();
		CamelisHttp camelisHttp = new CamelisHttp(activeUser, activeLisService);

		System.out.println("\n====================================== loadContext planets/planets ====================================");
		LoadContextReponse loadContextReponse = camelisHttp.loadContext();
		checkResult(loadContextReponse);
		
		System.out.println("\n====================================== ping ====================================");
		PingReponse pingReponse0 = CamelisHttp.ping(activeLisService);
		checkResult(pingReponse0);
		
		System.out.println("\n====================================== extent(\"Medium\") ====================================");
		String lisQuery = "Medium";
		ExtentReponse extentReponse = camelisHttp.extent(lisQuery);
		checkResult(extentReponse);

		System.out.println("\n====================================== stopCamelis ====================================");
		PingReponse pingReponse1 = AdminHttp.stopCamelis(activeUser, activeLisService);
		checkResult(pingReponse1);

		System.out.println("\n====================================== logout ====================================");
		VoidReponse logoutReponse = AdminHttp.logout(activeUser);
		checkResult(logoutReponse);

	} catch (Exception e) {
		e.printStackTrace();
	}
}


}

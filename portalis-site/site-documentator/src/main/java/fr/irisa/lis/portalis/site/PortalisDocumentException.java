package fr.irisa.lis.portalis.site;

@SuppressWarnings("serial")
public class PortalisDocumentException extends Exception implements
		java.io.Serializable {

	public PortalisDocumentException(String mess) {
		super(mess);
	}

	public PortalisDocumentException() {
	}

	public PortalisDocumentException(String mess, Throwable e) {
		super(mess, e);
	}
}

.. -*- mode: rst -*-

=======================
Portail Portalis - Bugs
=======================

.. sectionauthor:: Yves Bekkers <bekkers@irisa.fr>

.. only:: html

   :download:`[version PDF] <_build/pdf/bug.pdf>`
   
Bugs au 7/2/2013
----------------

La (les) bugs concernent ``AdminCoreCmd.startCamelis``. Elles apparaissent
uniquement par une utilisation à distance de Portalis. En local, ça marche ...
Ce qui est intéressant c'est que, cette (ces ?) bugs de ``AdminCoreCmd.startCamelis``
apparaîssent quelle que soit la manière dont on utilise Portalis

- A travers un navigateur par navigation http, par des clics
- Par une sequence de code Java qui lance des requètes http

Navigation http à travers un navigateur
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Séquence de début - comportement normal (en local comme à distance)
```````````````````````````````````````````````````````````````````

On lance **Tomcat** avec **Portalis** comme application WEB
(ici en local, mais à distance c'est le même résultat).
Dans un navivateur, on tape l'URL :

``http://localhost:8080/portalis``

Par défaut, **Portalis** ouvre la page d'acceuil HTML ``index.html``. Il s'agit du
formulaire de login suivant : 

.. image:: images/portalis-login.png

On entre ses informations personnelles de login. On click sur le bouton **Enter**,
Portalis vous redirige vers la page JSP ``test.jsp``. Avec les informations de l'utilisateur
en paramètre, l'url devient :

``http://localhost:8080/portalis/test.jsp?email=bekkers%40irisa.fr&password=yves`` 

Après un login avec succès, ``test.jsp`` affiche la page de test des commandes de Portalis
qui est la suivante : 

.. image:: images/portalis-tester-commandes.png
   :width: 16 cm
   
On click sur **Test Camelis for service 'planets:planets'**, ce qui vous redirige vers la page
JSP ``testCamelis.jsp``

Ici le comportement diverge :

1° En local
```````````

L'url est :

``http://localhost:8080/portalis/testCamelis.jsp?email=bekkers%40irisa.fr&serviceName=planets%3Aplanets``

Le résultat est :

.. image:: images/portalis-tester-camelis.png


2° A distance
`````````````

L'url est :

``http://lisfs2008.irisa.fr:8080/portalis/testCamelis.jsp?email=bekkers%40irisa.fr&serviceName=planets%3Aplanets``

Ce qui lance la page ``testCamelis.jsp`` qui se plante sur l'instruction ligne 49 de ``testCamelis.jsp`` qui est

    ``reponse = AdminCoreCmd.startCamelis(camelisSession, serviceName, null, null);``
      
Le résultat est :

.. code-block:: text

  Erreur

  Erreur de lancement de camelis sur le port = null

  [Erreur interne : java.lang.NullPointerException]

  Stack : ------ java.lang.NullPointerException at org.apache.jsp.testCamelis_jsp._jspService(testCamelis_jsp.java:120) 
   at org.apache.jasper.runtime.HttpJspBase.service(HttpJspBase.java:70)
   at javax.servlet.http.HttpServlet.service(HttpServlet.java:728)
   at org.apache.jasper.servlet.JspServletWrapper.service(JspServletWrapper.java:432)
   at org.apache.jasper.servlet.JspServlet.serviceJspFile(JspServlet.java:390) 
   at org.apache.jasper.servlet.JspServlet.service(JspServlet.java:334) 
   at javax.servlet.http.HttpServlet.service(HttpServlet.java:728) 
   at org.apache.catalina.core.ApplicationFilterChain.internalDoFilter(ApplicationFilterChain.java:305) 
   at org.apache.catalina.core.ApplicationFilterChain.doFilter(ApplicationFilterChain.java:210) 
   at org.apache.catalina.core.StandardWrapperValve.invoke(StandardWrapperValve.java:222) 
   at org.apache.catalina.core.StandardContextValve.invoke(StandardContextValve.java:123) 
   at org.apache.catalina.authenticator.AuthenticatorBase.invoke(AuthenticatorBase.java:472) 
   at org.apache.catalina.core.StandardHostValve.invoke(StandardHostValve.java:171) 
   at org.apache.catalina.valves.ErrorReportValve.invoke(ErrorReportValve.java:99) 
   at org.apache.catalina.valves.AccessLogValve.invoke(AccessLogValve.java:936) 
   at org.apache.catalina.core.StandardEngineValve.invoke(StandardEngineValve.java:118) 
   at org.apache.catalina.connector.CoyoteAdapter.service(CoyoteAdapter.java:407) 
   at org.apache.coyote.http11.AbstractHttp11Processor.process(AbstractHttp11Processor.java:1004) 
   at org.apache.coyote.AbstractProtocol$AbstractConnectionHandler.process(AbstractProtocol.java:589) 
   at org.apache.tomcat.util.net.JIoEndpoint$SocketProcessor.run(JIoEndpoint.java:312) 
   at java.util.concurrent.ThreadPoolExecutor.runWorker(ThreadPoolExecutor.java:1145) 
   at java.util.concurrent.ThreadPoolExecutor$Worker.run(ThreadPoolExecutor.java:615) 
   at java.lang.Thread.run(Thread.java:722) ------
   

Navigation par des requètes http lancèes depuis un programme Java
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Le test se fait dans le module ``AndrolisExemple``. Procéder comme suit 

1 Dans le fichier ``portalis.properties`` on positionne la variable ``admin.portalis.server.name`` comme suit :

  ``admin.portalis.server.name=localhost``
  
  ou
  
  ``admin.portalis.server.name=lisfs2008.irisa.fr`` 

2 Vérifier que **Tomcat** est lancé avec l'application **Portalis** dans son répertoire ``webapps``
  
3 Lancer l'exécution de la classe java ``fr.irisa.lis.portalis.Androlis`` du module ``AndrolisExemple``.

Résultat d'une exécution en local
`````````````````````````````````

La trace normale suivante ...

.. code-block:: text

  ====================================== defaultSession ====================================

  <session port="8080" host="localhost">
    <actifUser userKey="c79816e2b1c7afb53e3b505cea6d21dabbcdd0d72029d704ff4cd7da103c9c72">
      <userData email="anonymous@noplace.com"/>
    </actifUser>
  </session>
  ====================================== defaultSession ====================================

  ********************* login *********************
  http://localhost:8080/portalis/login.jsp?sessionId=ABCED1234F&email=bekkers@irisa.fr&password=yves

  <loginResponse status="ok">
    <session port="8080" host="localhost">
      <actifUser userKey="55362c26b69e5460559fc8505e06d4d417dbb398727ff7954a6fe177efd9604a">
        <userData email="bekkers@irisa.fr" pseudo="bekkers"/>
      </actifUser>
    </session>
  </loginResponse>

  ====================================== adminSession ====================================

  <session port="8080" host="localhost">
    <actifUser userKey="55362c26b69e5460559fc8505e06d4d417dbb398727ff7954a6fe177efd9604a">
      <userData email="bekkers@irisa.fr" pseudo="bekkers"/>
    </actifUser>
  </session>
  ====================================== adminSession ====================================

  ******************* startCamelis 1 ****************
  http://localhost:8080/portalis/startCamelis.jsp?serviceName=planets:planets12

  <startResponse status="ok">
    <camelis creator="bekkers@irisa.fr" pid="4074" port="8061" nbObject="0" contextLoaded="false" activationDate="2013-02-07T04:17:19.438+01:00" lastUpdate="none" host="localhost">
      <serviceCore serviceId="planets12" applicationId="planets" src="planets12.ctx"/>
    </camelis>
  </startResponse>

  ====================================== camelisSession ====================================

  <session port="8061" host="localhost">
    <actifUser userKey="55362c26b69e5460559fc8505e06d4d417dbb398727ff7954a6fe177efd9604a">
      <userData email="bekkers@irisa.fr" pseudo="bekkers"/>
    </actifUser>
  </session>
  ====================================== camelisSession ====================================

  *********************** ping ********************
  http://localhost:8061/ping

  <pingResponse status="ok" creator="bekkers@irisa.fr" pid="4074" host="localhost" port="8061" nbObject="0" serviceId="localhost:8061::planets:planets12" contextLoaded="false" activationDate="2013-02-07T04:17:19.438+01:00" lastUpdate="none"/>

  ******************** registerKey *****************
  http://localhost:8061/registerKey?key=1234567890ABCDEF&role=admin

  <registerKeyResponse status="ok">
    <user key="1234567890ABCDEF" role="admin" expires="2013-02-07T07:17:20.000+01:00"/>
  </registerKeyResponse>

  ******************* startCamelis 2 ****************
  http://localhost:8080/portalis/startCamelis.jsp?serviceName=planets:planets999

  <startResponse status="ok">
    <camelis creator="bekkers@irisa.fr" pid="4080" port="8062" nbObject="0" contextLoaded="false" activationDate="2013-02-07T04:17:20.403+01:00" lastUpdate="none" host="localhost">
      <serviceCore serviceId="planets999" applicationId="planets" src="planets999.ctx"/>
    </camelis>
  </startResponse>

  *********************** stopCamelis 1 ********************
  http://localhost:8080/portalis/stopCamelis.jsp?port=8061

  <pingResponse status="ok" creator="bekkers@irisa.fr" pid="4074" host="localhost" port="8061" nbObject="0" serviceId="localhost:8061::planets:planets12" contextLoaded="false" activationDate="2013-02-07T04:17:19.438+01:00" lastUpdate="none"/>

  *********************** stopCamelis 2 ********************
  http://localhost:8080/portalis/stopCamelis.jsp?port=8062

  <pingResponse status="ok" creator="bekkers@irisa.fr" pid="4080" host="localhost" port="8062" nbObject="0" serviceId="localhost:8062::planets:planets999" contextLoaded="false" activationDate="2013-02-07T04:17:20.403+01:00" lastUpdate="none"/>

Résultat d'une exécution à distance
```````````````````````````````````

La trace est alors le suivante ...

.. code-block:: text

  ====================================== defaultSession ====================================

  <session port="8080" host="lisfs2008.irisa.fr">
    <actifUser userKey="628b9b8bd841baf7dc967fb71f9d234aeefaa229cd1945d1662b49ff464b63b6">
      <userData email="anonymous@noplace.com"/>
    </actifUser>
  </session>
  ====================================== defaultSession ====================================

  ********************* login *********************
  http://lisfs2008.irisa.fr:8080/portalis/login.jsp?sessionId=ABCED1234F&email=bekkers@irisa.fr&password=yves

  <loginResponse status="ok">
    <session port="8080" host="lisfs2008.irisa.fr">
      <actifUser userKey="c0832c84b9010d1855a17664221acd67a1bafff406b77057a2d4c36873be4948">
        <userData email="bekkers@irisa.fr" pseudo="bekkers"/>
      </actifUser>
    </session>
  </loginResponse>

  ====================================== adminSession ====================================

  <session port="8080" host="lisfs2008.irisa.fr">
    <actifUser userKey="c0832c84b9010d1855a17664221acd67a1bafff406b77057a2d4c36873be4948">
      <userData email="bekkers@irisa.fr" pseudo="bekkers"/>
    </actifUser>
  </session>
  ====================================== adminSession ====================================

  ******************* startCamelis 1 ****************
  java.lang.NullPointerException
     at fr.irisa.lis.portalis.Androlis.main(Androlis.java:80)
     
Dans les deux cas de plantage, il s'agit d'un ``NullPointerException`` au sein de
``AdminCoreCmd.startCamelis`` !

Par un navigateur on nous dit qu'il s'agit du port qui est null !

Difficulté
----------

La bug se passerait en local, je te la coincerait avec le fichier de log. Mais, ce qui complique la tâche
en ce moment, c'est que cette bug se combine avec une autre bug : sur lisfs2008, on ne retrouve pas le log ...

Conclusion : d'abord retourver les fichiers de log sur lisfs2008, puis faire les tests et regarder les traces ...


.. Portalis documentation master file

Portalis documentation
======================

:Authors: Yves Bekkers, Benjamin Sigonneau
:Compiled: |today|

.. only:: html

   :download:`[version PDF] <_build/pdf/index.pdf>`


Spécifications et protocoles
----------------------------

.. toctree::
   :maxdepth: 1
   :numbered: 3

   generales-spec
   androlis-spec
   androlis-reprise-de-code
   autodoc/protocole-serveur-portalis
   protocole-serveur-camelis-xml
   autodoc/protocole-serveur-camelis

Autres documents
----------------

.. toctree::
   :maxdepth: 1
   :numbered: 3

   toolsAndDeveloppementPrinciples
   maven
   install-camelis-lisfs2008
   git-seb
   quickReferenceGuide
   todo
   bug
   

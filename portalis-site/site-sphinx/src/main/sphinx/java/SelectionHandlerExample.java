package fr.irisa.lis.client;

import com.google.gwt.event.logical.shared.SelectionEvent;
import com.google.gwt.event.logical.shared.SelectionHandler;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.user.client.History;
import com.google.gwt.user.client.ui.TabPanel;

public class SelectionHandlerExample {
	private TabPanel tabPanel;

	public void test() {
		tabPanel.addSelectionHandler(new SelectionHandler<Integer>() {
			public void onSelection(SelectionEvent<Integer> event) {
				History.newItem("page" + event.getSelectedItem());
			}
		});

		History.addValueChangeHandler(new ValueChangeHandler<String>() {
			public void onValueChange(ValueChangeEvent<String> event) {
				String historyToken = event.getValue();

				// Parse the history token
				try {
					if (historyToken.substring(0, 4).equals("page")) {
						String tabIndexToken = historyToken.substring(4, 5);
						int tabIndex = Integer.parseInt(tabIndexToken);
						// Select the specified tab panel
						tabPanel.selectTab(tabIndex);
					} else {
						tabPanel.selectTab(0);
					}

				} catch (IndexOutOfBoundsException e) {
					tabPanel.selectTab(0); // tack whenever you receive a
											// `SelectionEvent` from your
											// `TabPanel`
				}
			}
		});
	}
}

Conventions de nommage du projet Portalis
-----------------------------------------

Les conventions de nommage du projet Portalis suivent les conventions de nommage préconisées par *Maven* et par
la communauté des développeurs *Java*. On utilise les 3 niveaux utilisés dans Maven :

- **groupId** : ``fr.irisa.lis.portalis``, ce nom suit la régle de nom de *paquet* (*librairie Java*) utilisée par la communauté des programmeurs Java.
  Cette régle consiste à utiliser comme nom de paquet un nom de domaine que l'on controle. Ce qui permet d'identifer tout paquet de manière unique sur le WEB.
  Tous les paquets *Java* (librairies *Java*) du projet *Portalis* possédent ``fr.irisa.lis.portalis`` comme préfixe de nom de paquet.
- **artifactId** : Dans le jargon de Maven un *artifact* est un module. Dans le projet Portalis, les identifants de module utilisent une notation pointée.
  On trouve par exemple les identifiants suivants : ``admin``, ``admin.core``, ``admin.core.xmlDataBase``, ``admin.server``, ``root``, ``server`` ...
- **version** : Pour la distribution nous avons choisi une identification de version, classique, avec des numéros et des points (1,0, 1,1, 1.0.1, ...).
  On n'utilise pas les dates car elles sont associées par Maven aux ``SNAPSHOT`` construits (tous les soirs en général).

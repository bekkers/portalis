package fr.irisa.lis.portalis.service;

import java.io.FileInputStream;
import java.io.IOException;
import java.net.URL;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import fr.irisa.lis.portalis.commons.DOMUtil;


public class ImportPatrimoineBreton {

	private static final String BASE_URL = "http://patrimoine.region-bretagne.fr/sdx/sdx/oai/sribzh/db";
	private static final String COMMON = "verb=ListRecords";
	private static final String FIRST = "metadataPrefix=ci";
	private static final String RESUMPTION_TOKEN = "resumptionToken";

	// private static final String OAI_PMH = "OAI-PMH";

	public static Document doConnection(String params) throws SAXException, IOException, ParserConfigurationException {
		URL url = null;
		try {
			url = new URL(String.format("%s?%s", BASE_URL, params));
			System.out.println(url);
		} catch (Exception e) {
			String errMess = String.format(
					"build URL failure during http request\nCause = %s",
					e.getMessage());
			System.out.println(errMess);
		}
		return readFile(url.toString());
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		try {
			Document doc = doConnection(String.format("%s&%s", COMMON, FIRST));
			// System.out.println(HttpUrlConnectionReader.asXML(first));
			Element first = doc.getDocumentElement();
			String rootName = first.getNodeName();
			if (rootName.equals("error")) {
				System.out.println(first.getAttribute("message"));
			} else {
				int i = 0;
				for (NodeList resumptionTokens = first
						.getElementsByTagName(RESUMPTION_TOKEN); resumptionTokens
						.getLength() > 0; resumptionTokens = first
						.getElementsByTagName(RESUMPTION_TOKEN)) {
					String resumptionToken = resumptionTokens.item(0)
							.getTextContent();
					doc = doConnection(String.format("%s&%s=%s", COMMON,
							RESUMPTION_TOKEN, resumptionToken));
					first = doc.getDocumentElement();
					DOMUtil.writeFile(first.getOwnerDocument(), resumptionToken
							+ "_" + i++ + ".xml");
				}
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	static public Document readFile(FileInputStream file) throws SAXException,
			IOException, ParserConfigurationException {
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		DocumentBuilder db = dbf.newDocumentBuilder();
		return db.parse(file);
	}

	static public Document readFile(String fileUri) throws SAXException,
			IOException, ParserConfigurationException {
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		DocumentBuilder db = dbf.newDocumentBuilder();
		return db.parse(fileUri);
	}

}

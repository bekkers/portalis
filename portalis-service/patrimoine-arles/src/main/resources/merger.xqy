declare namespace local = "http://www.irisa.fr/bekkers";
declare variable $inseeVilles as xs:string+ external;
declare variable $cogVilles as xs:string+ external;
declare variable $irisVilles as xs:string+ external;
declare variable $villes as xs:string+ external;
declare variable $surrounding as xs:boolean+ external;
declare variable $cles as xs:boolean+ external;

declare function local:iris($list_iris as element()*) as element()* {
      	for $v in $list_iris
      	return
    let $type := if ($v/TYP_IRIS='H') 
      then 'habitat' else if ($v/TYP_IRIS='A') then 'activité' else 'divers'
      	return
      	<iris nom="{$v/LIB_IRIS}" code="{$v/CODE_IRIS}" pop2010="{$v/Pop_2010}" type="{$type}" />
};

declare function local:content($inseeVille as element()*, $ville as element()*) as element()* {
      for $cogVille in doc($cogVilles)/communes/commune[CODGEO=$inseeVille/@codeInsee][1]
      let $irisVille := doc($irisVilles)/communes/commune[DEPCOM=$inseeVille/@codeInsee]
      return
            	<ville localId="{$ville/idville}" nom="{$ville/nomville}" site="{$ville/site}" 
      	codeInsee="{$inseeVille/@codeInsee}" codesPostaux="{$inseeVille/@codesPostaux}"
      	longitude="{$inseeVille/@longitude}" latitude="{$inseeVille/@latitude}" 
        pop1999="{$cogVille/POP1999}" pop2010="{$cogVille/POP2010}"
      	circonscription="{$inseeVille/ancestor::departement/@nom}-{$inseeVille/ancestor::circonscription/@numero}"
      	>
      	{if ($irisVille[1][not(TYP_IRIS='Z')]) then
         	if (not($irisVille/TRIRIS='ZZZZZZ')) then
         	for $triris in distinct-values($irisVille/TRIRIS)
         	let $nom := ($irisVille[TRIRIS=$triris][1]/LIB_IRIS, for $i in (2 to count($irisVille[TRIRIS=$triris])) return ('--',$irisVille[TRIRIS=$triris][$i]/LIB_IRIS)) 
         	let $pop2010 := sum($irisVille[TRIRIS=$triris]/Pop_2010)
         	return
        	<triris code="{$triris}" nom="{$nom}" pop2010="{$pop2010}">
        	{
        	   local:iris($irisVille[TRIRIS=$triris])
        	}
        	</triris>
        	else
         	   local:iris($irisVille)
        	
        else
      	()
      	}
      	{if ($surrounding) then (
      	<circonscriptionEuropeenne nom="{$inseeVille/ancestor::circonscriptionEuropeenne/@name}" />
      	, <region chefLieu="{$inseeVille/ancestor::region/@chefLieu}" code="{$inseeVille/ancestor::region/@code}" nom="{$inseeVille/ancestor::region/@nom}" />
      	, <departement nom="{$inseeVille/ancestor::departement/@nom}" numero="{$inseeVille/ancestor::departement/@numero}" prefecture="{$inseeVille/ancestor::departement/@prefecture}" />
		) else
		()}
		{
		for $patrimoine in $ville/patrimoine
		return
		<patrimoine nom="{$patrimoine/titre}" id="{$patrimoine/identifiant}"
		latitude="{$patrimoine/latitude}" longitude="{$patrimoine/longitude}" >
		{(
		$patrimoine/description
		, 
		if ($patrimoine/images/image) then
		   <images>
		   {
		   for $image in $patrimoine/images/image
		   return
		   		<image src="{$image}" />
		   }
		   </images>
		   else ()
		 ,
		 if ($cles) then
		 	$patrimoine/MOTS
		 else
		 ()
		)}
		</patrimoine>
		}
      	</ville>
};

declare function local:Ville()
   as element()* {
   <villes status="ok">{
   for $v in
      for $inseeVille in doc($inseeVilles)/(circonscriptionsEuropeennes/circonscriptionEuropeenne[@name='Sud-Est']/region[@code='93']/departement[@numero='13']/circonscription/commune)
      for $ville in doc($villes)/villes/ville[$inseeVille/@nom=nomville][1]
	  return
		  local:content($inseeVille, $ville)
   	order by $v/@nom
   	return $v
   
	  
   }</villes>	
};

declare function local:Ville($nom as xs:string)
   as element() {
      for $inseeVille in doc($inseeVilles)/circonscriptionsEuropeennes/circonscriptionEuropeenne[@name='Sud-Est']/region[@code='93']/departement[@numero='13']/circonscription/commune[$nom=@nom][1]
      for $ville in doc($villes)/villes/ville[$inseeVille/@nom=nomville][1]
      return
      local:content($inseeVille, $ville)
};

()

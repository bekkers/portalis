declare namespace local = "http://www.irisa.fr/bekkers";
declare variable $root as node() external;

declare function local:getFicheVille($id as xs:string) as  element()* {
	for $ville in $root/villes/ville[@localId=$id]
	return
		<section>
			{local:generateOneVille($ville)}
		</section>
};

declare function local:getVilles() as  element()* {
		<section>
		 {
			for $ville in $root/villes/ville
			order by $ville/@nom
			return
				local:generateOneVille($ville)
		}
		</section>
};

declare function local:generateOneVille($ville as element()) as  element()* {
     <article>
		<h1 class="header">{concat('Ville de ',$ville/@nom)}</h1>
		<h3 class="name">Détails</h3>
		<ul class="list">
		<li><em>Nom</em>{concat(' : ', $ville/@nom)}</li>
		<li><em>Département</em>{concat(' : ', $ville/departement/@nom)}</li>
		<li><em>Circonscription</em>{concat(' : ', $ville/@circonscription)}</li>
		<li><em>Circonscription Europeenne</em>{concat(' : ', $ville/circonscriptionEuropeenne/@nom)}</li>
		<li><em>Code Insee</em>{concat(' : ', $ville/@codeInsee)}</li>
		<li><em>Codes postaux</em>{concat(' : ', $ville/@codesPostaux)}</li>
		<li><em>Population en 1999</em>{concat(' : ', $ville/@pop1999)}</li>
		<li><em>Population en 2010</em>{concat(' : ', $ville/@pop2010)}</li>
		</ul>
		<h3 class="name">Patrimoine</h3>
		<ul class="list">
		{let $nb := count($ville/patrimoine)
		for $i in 1 to $nb
		return
		<li>{concat($i, ' - ',$ville/patrimoine[$i]/@nom)}</li>
		}
		</ul>
	</article>
};

()
/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.3.1</a>, using an XML
 * Schema.
 * $Id$
 */

package fr.irisa.lis.portalis.service.patrimoine.arles;

/**
 * Class Images.
 * 
 * @version $Revision$ $Date$
 */
@SuppressWarnings("serial")
public class Images implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _imageList.
     */
    private java.util.Vector<java.lang.String> _imageList;


      //----------------/
     //- Constructors -/
    //----------------/

    public Images() {
        super();
        this._imageList = new java.util.Vector<java.lang.String>();
    }


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * 
     * 
     * @param vImage
     * @throws java.lang.IndexOutOfBoundsException if the index
     * given is outside the bounds of the collection
     */
    public void addImage(
            final java.lang.String vImage)
    throws java.lang.IndexOutOfBoundsException {
        this._imageList.addElement(vImage);
    }

    /**
     * 
     * 
     * @param index
     * @param vImage
     * @throws java.lang.IndexOutOfBoundsException if the index
     * given is outside the bounds of the collection
     */
    public void addImage(
            final int index,
            final java.lang.String vImage)
    throws java.lang.IndexOutOfBoundsException {
        this._imageList.add(index, vImage);
    }

    /**
     * Method enumerateImage.
     * 
     * @return an Enumeration over all java.lang.String elements
     */
    public java.util.Enumeration<? extends java.lang.String> enumerateImage(
    ) {
        return this._imageList.elements();
    }

    /**
     * Method getImage.
     * 
     * @param index
     * @throws java.lang.IndexOutOfBoundsException if the index
     * given is outside the bounds of the collection
     * @return the value of the java.lang.String at the given index
     */
    public java.lang.String getImage(
            final int index)
    throws java.lang.IndexOutOfBoundsException {
        // check bounds for index
        if (index < 0 || index >= this._imageList.size()) {
            throw new IndexOutOfBoundsException("getImage: Index value '" + index + "' not in range [0.." + (this._imageList.size() - 1) + "]");
        }

        return (java.lang.String) _imageList.get(index);
    }

    /**
     * Method getImage.Returns the contents of the collection in an
     * Array.  <p>Note:  Just in case the collection contents are
     * changing in another thread, we pass a 0-length Array of the
     * correct type into the API call.  This way we <i>know</i>
     * that the Array returned is of exactly the correct length.
     * 
     * @return this collection as an Array
     */
    public java.lang.String[] getImage(
    ) {
        java.lang.String[] array = new java.lang.String[0];
        return (java.lang.String[]) this._imageList.toArray(array);
    }

    /**
     * Method getImageCount.
     * 
     * @return the size of this collection
     */
    public int getImageCount(
    ) {
        return this._imageList.size();
    }

    /**
     */
    public void removeAllImage(
    ) {
        this._imageList.clear();
    }

    /**
     * Method removeImage.
     * 
     * @param vImage
     * @return true if the object was removed from the collection.
     */
    public boolean removeImage(
            final java.lang.String vImage) {
        boolean removed = _imageList.remove(vImage);
        return removed;
    }

    /**
     * Method removeImageAt.
     * 
     * @param index
     * @return the element removed from the collection
     */
    public java.lang.String removeImageAt(
            final int index) {
        java.lang.Object obj = this._imageList.remove(index);
        return (java.lang.String) obj;
    }

    /**
     * 
     * 
     * @param index
     * @param vImage
     * @throws java.lang.IndexOutOfBoundsException if the index
     * given is outside the bounds of the collection
     */
    public void setImage(
            final int index,
            final java.lang.String vImage)
    throws java.lang.IndexOutOfBoundsException {
        // check bounds for index
        if (index < 0 || index >= this._imageList.size()) {
            throw new IndexOutOfBoundsException("setImage: Index value '" + index + "' not in range [0.." + (this._imageList.size() - 1) + "]");
        }

        this._imageList.set(index, vImage);
    }

    /**
     * 
     * 
     * @param vImageArray
     */
    public void setImage(
            final java.lang.String[] vImageArray) {
        //-- copy array
        _imageList.clear();

        for (int i = 0; i < vImageArray.length; i++) {
                this._imageList.add(vImageArray[i]);
        }
    }

}

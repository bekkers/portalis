package fr.irisa.lis.portalis.service.patrimoine.arles.db;

import java.io.InputStream;
import java.io.OutputStream;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import net.sf.saxon.s9api.ItemType;
import net.sf.saxon.s9api.QName;
import net.sf.saxon.s9api.XdmAtomicValue;
import net.sf.saxon.s9api.XdmItem;
import net.sf.saxon.s9api.XdmNode;
import net.sf.saxon.s9api.XdmValue;

import org.w3c.dom.Document;

import fr.irisa.lis.portalis.commons.DOMUtil;
import fr.irisa.lis.portalis.commons.ParameterValue;
import fr.irisa.lis.portalis.commons.XqueryUtil;
import fr.irisa.lis.portalis.commons.pipe.StringOutputStream;

public class PatrimoineArlesDataBase {

	private URI arles = new URI("http://www.pays-arles.org/e-patrimoine/spip.php?page=opendata");
	private static final String MERGED_PATRIMOINE_ARLES = "patrimoine.xml";
	private Villes villes;
	private Document root;

	public PatrimoineArlesDataBase() throws Exception {
		// on stocke de deux manières le fichier du patrimoine
		villes = unmarshal(MERGED_PATRIMOINE_ARLES);
		root = DOMUtil.readFile(MERGED_PATRIMOINE_ARLES);

	}

	public Ville getVille(byte localId) {
		Ville result = null;
		for (Ville ville : villes.getVille()) {
			if (ville.getLocalId() == localId) {
				result = ville;
				break;
			}
		}
		return result;
	}

	public Patrimoine getPatrimoine(short id) {
		Patrimoine result = null;
		for (Ville ville : villes.getVille()) {
			for (Patrimoine patrimoine : ville.getPatrimoine()) {
				if (patrimoine.getId() == id) {
					result = patrimoine;
					break;
				}
			}
		}
		return result;
	}

	public String[] getImageUrls(short id) {
		String[] result = null;
		for (Ville ville : villes.getVille()) {
			for (Patrimoine patrimoine : ville.getPatrimoine()) {
				if (patrimoine.getId() == id) {
					Image[] images = patrimoine.getImages().getImage();
					result = new String[images.length];
					for (int i = 0; i < images.length; i++) {
						result[i] = images[i].getSrc();
					}
					break;
				}
			}
		}
		return result;
	}
	
	public String[] getFirstImageUrl(short[] ids) {
		String[] result = new String[ids.length];
		for (int i = 0; i < ids.length; i++) {
			result[i] = getFirstImageUrl(ids[i]);
		}
		return result;
	}

	public String getFirstImageUrl(short id) {
		String result = null;
		for (Ville ville : villes.getVille()) {
			for (Patrimoine patrimoine : ville.getPatrimoine()) {
				if (patrimoine.getId() == id) {
					Image[] images = patrimoine.getImages().getImage();
					result = images[0].getSrc();
					break;
				}
			}
		}
		return result;
	}

	public String getPatrimoineDescription(short id) {
		String result = null;
		for (Ville ville : villes.getVille()) {
			for (Patrimoine patrimoine : ville.getPatrimoine()) {
				if (patrimoine.getId() == id) {
					result = patrimoine.getDescription();
					break;
				}
			}
		}
		return result;
	}

	public String getVilleDescrition(byte localId) {
		String result = null;
		for (Ville ville : villes.getVille()) {
			if (ville.getLocalId() == localId) {
				for (Patrimoine patrimoine : ville.getPatrimoine()) {
					if (patrimoine.getNom().contains("Le patrimoine de ")) {
						result = patrimoine.getDescription();
						break;
					}
				}
			}
		}
		return result;
	}

	public Gps[] getGps(short[] ids) {
		Gps[] result = new Gps[ids.length];
		for (int i = 0; i < ids.length; i++) {
			result[i] = getGps(ids[i]);
		}
		return result;
	}

	public Gps getGps(short id) {
		Gps result = null;
		for (Ville ville : villes.getVille()) {
			for (Patrimoine patrimoine : ville.getPatrimoine()) {
				if (patrimoine.getId() == id) {
					String longitude = patrimoine.getLongitude();
					String latitude = patrimoine.getLatitude();
					if (longitude.length() == 0 || latitude.length() == 0) {
						result = new Gps(ville.getLongitude(),
								ville.getLatitude(), true);
					} else {
						result = new Gps(longitude, latitude);
					}
					break;
				}
			}
		}
		return result;
	}

	public String getHtmlFicheVille(short id) throws Exception {

		XqueryUtil xqueryUtil = new XqueryUtil();

		List<ParameterValue> externalParams = new ArrayList<ParameterValue>();
		externalParams.add(new ParameterValue(new QName("root"), xqueryUtil
				.fromDOMToXdmNode(root)));

		QName functionName = new QName("http://www.irisa.fr/bekkers",
				"getFicheVille");
		XdmItem xdmItem = new XdmAtomicValue(id + "", ItemType.STRING);
		XdmValue result = xqueryUtil.callFunction("ficheVilleToHtml.xqy",
				functionName, xdmItem, externalParams);
		if (result == null || result.itemAt(0) == null
				|| result.itemAt(0).isAtomicValue()) {
			return null;
		}
		XdmNode xdmNode = (XdmNode) result.itemAt(0);

//		OutputStream outputStream = new FileOutputStream("ville.html");
		StringOutputStream outputStream = new StringOutputStream();
		xqueryUtil.serialize(xdmNode, outputStream);

		return outputStream.toString();

	}

	public String getHtmlFicheVille() throws Exception {

		XqueryUtil xqueryUtil = new XqueryUtil();

		List<ParameterValue> externalParams = new ArrayList<ParameterValue>();
		externalParams.add(new ParameterValue(new QName("root"), xqueryUtil
				.fromDOMToXdmNode(root)));

		QName functionName = new QName("http://www.irisa.fr/bekkers",
				"getVilles");
		XdmValue result = xqueryUtil.callFunction("ficheVilleToHtml.xqy",
				functionName, externalParams);
		if (result == null || result.itemAt(0) == null
				|| result.itemAt(0).isAtomicValue()) {
			return null;
		}
		XdmNode xdmNode = (XdmNode) result.itemAt(0);

//		OutputStream outputStream = new FileOutputStream("ville.html");
		OutputStream outputStream = new StringOutputStream();
		xqueryUtil.serialize(xdmNode, outputStream);

		return outputStream.toString();

	}

	public static void main(String[] args) throws Exception {
		PatrimoineArlesDataBase patrimoineArles = new PatrimoineArlesDataBase();
		System.out.println(patrimoineArles.getHtmlFicheVille((byte) 3));
		System.out.println(patrimoineArles.getGps((short) 208));
		System.out.println(patrimoineArles.getVilleDescrition((byte) 3));
		System.out.println(patrimoineArles
				.getPatrimoineDescription((short) 208));
	}

	// public static void main(String[] args) throws JAXBException {
	// PatrimoineArlesComplet patrimoineArles = new PatrimoineArlesComplet();
	//
	// Ville arles = patrimoineArles.getVille((byte) 14);
	// System.out.println(arles.getNom());
	// File file = new File("ville.xml");
	// JAXBContext jaxbContext = JAXBContext.newInstance(Ville.class);
	// Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
	//
	// // output pretty printed
	// jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
	//
	// jaxbMarshaller.marshal(arles, file);
	// jaxbMarshaller.marshal(arles, System.out);
	// }

	private Villes unmarshal(String xmlSource) throws JAXBException {
		InputStream inputStream = getClass().getResourceAsStream(
				"/"+xmlSource);
		JAXBContext jaxbContext = JAXBContext.newInstance(Villes.class);

		Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
		Villes villes = (Villes) jaxbUnmarshaller.unmarshal(inputStream);
		return villes;
	}

	public Villes getVilles() {
		return villes;
	}

}

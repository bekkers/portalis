/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.3.1</a>, using an XML
 * Schema.
 * $Id$
 */

package fr.irisa.lis.portalis.service.patrimoine.arles.db;

import javax.xml.bind.annotation.XmlAttribute;

/**
 * Class PatrimoineType.
 * 
 * @version $Revision$ $Date$
 */
@SuppressWarnings("serial")
public class PatrimoineType implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _id.
     */
    private short _id;

    /**
     * keeps track of state for field: _id
     */
    private boolean _has_id;

    /**
     * Field _latitude.
     */
    private java.lang.String _latitude;

    /**
     * Field _longitude.
     */
    private java.lang.String _longitude;

    /**
     * Field _nom.
     */
    private java.lang.String _nom;

    /**
     * Field _description.
     */
    private java.lang.String _description;

    /**
     * Field _images.
     */
    private fr.irisa.lis.portalis.service.patrimoine.arles.db.Images _images;

    /**
     * Field _MOTS.
     */
    private fr.irisa.lis.portalis.service.patrimoine.arles.db.MOTS _MOTS;


      //----------------/
     //- Constructors -/
    //----------------/

    public PatrimoineType() {
        super();
    }


      //-----------/
     //- Methods -/
    //-----------/

    /**
     */
    public void deleteId(
    ) {
        this._has_id= false;
    }

    /**
     * Returns the value of field 'description'.
     * 
     * @return the value of field 'Description'.
     */
    public java.lang.String getDescription(
    ) {
        return this._description;
    }

    /**
     * Returns the value of field 'id'.
     * 
     * @return the value of field 'Id'.
     */
    @XmlAttribute
    public short getId(
    ) {
        return this._id;
    }

    /**
     * Returns the value of field 'images'.
     * 
     * @return the value of field 'Images'.
     */
    public fr.irisa.lis.portalis.service.patrimoine.arles.db.Images getImages(
    ) {
        return this._images;
    }

    /**
     * Returns the value of field 'latitude'.
     * 
     * @return the value of field 'Latitude'.
     */
    @XmlAttribute
    public java.lang.String getLatitude(
    ) {
        return this._latitude;
    }

    /**
     * Returns the value of field 'longitude'.
     * 
     * @return the value of field 'Longitude'.
     */
    @XmlAttribute
    public java.lang.String getLongitude(
    ) {
        return this._longitude;
    }

    /**
     * Returns the value of field 'MOTS'.
     * 
     * @return the value of field 'MOTS'.
     */
    public fr.irisa.lis.portalis.service.patrimoine.arles.db.MOTS getMOTS(
    ) {
        return this._MOTS;
    }

    /**
     * Returns the value of field 'nom'.
     * 
     * @return the value of field 'Nom'.
     */
    @XmlAttribute
    public java.lang.String getNom(
    ) {
        return this._nom;
    }

    /**
     * Method hasId.
     * 
     * @return true if at least one Id has been added
     */
    public boolean hasId(
    ) {
        return this._has_id;
    }

    /**
     * Sets the value of field 'description'.
     * 
     * @param description the value of field 'description'.
     */
    public void setDescription(
            final java.lang.String description) {
        this._description = description;
    }

    /**
     * Sets the value of field 'id'.
     * 
     * @param id the value of field 'id'.
     */
    public void setId(
            final short id) {
        this._id = id;
        this._has_id = true;
    }

    /**
     * Sets the value of field 'images'.
     * 
     * @param images the value of field 'images'.
     */
    public void setImages(
            final fr.irisa.lis.portalis.service.patrimoine.arles.db.Images images) {
        this._images = images;
    }

    /**
     * Sets the value of field 'latitude'.
     * 
     * @param latitude the value of field 'latitude'.
     */
    public void setLatitude(
            final java.lang.String latitude) {
        this._latitude = latitude;
    }

    /**
     * Sets the value of field 'longitude'.
     * 
     * @param longitude the value of field 'longitude'.
     */
    public void setLongitude(
            final java.lang.String longitude) {
        this._longitude = longitude;
    }

    /**
     * Sets the value of field 'MOTS'.
     * 
     * @param MOTS the value of field 'MOTS'.
     */
    public void setMOTS(
            final fr.irisa.lis.portalis.service.patrimoine.arles.db.MOTS MOTS) {
        this._MOTS = MOTS;
    }

    /**
     * Sets the value of field 'nom'.
     * 
     * @param nom the value of field 'nom'.
     */
    public void setNom(
            final java.lang.String nom) {
        this._nom = nom;
    }

}

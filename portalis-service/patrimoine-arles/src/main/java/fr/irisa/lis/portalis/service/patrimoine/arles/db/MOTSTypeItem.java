/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.3.1</a>, using an XML
 * Schema.
 * $Id$
 */

package fr.irisa.lis.portalis.service.patrimoine.arles.db;

/**
 * Class MOTSTypeItem.
 * 
 * @version $Revision$ $Date$
 */
@SuppressWarnings("serial")
public class MOTSTypeItem implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Internal choice value storage
     */
    private java.lang.Object _choiceValue;

    /**
     * Field _themes.
     */
    private java.lang.String _themes;

    /**
     * Field _categoriedelarchitecture.
     */
    private fr.irisa.lis.portalis.service.patrimoine.arles.db.types.CategoriedelarchitectureType _categoriedelarchitecture;

    /**
     * Field _typedarchitecture.
     */
    private java.lang.String _typedarchitecture;

    /**
     * Field _appartenance.
     */
    private fr.irisa.lis.portalis.service.patrimoine.arles.db.types.AppartenanceType _appartenance;

    /**
     * Field _epoque.
     */
    private java.lang.String _epoque;

    /**
     * Field _stylearchitectural.
     */
    private java.lang.String _stylearchitectural;

    /**
     * Field _datation.
     */
    private java.lang.String _datation;

    /**
     * Field _edifice.
     */
    private java.lang.String _edifice;

    /**
     * Field _propriete.
     */
    private fr.irisa.lis.portalis.service.patrimoine.arles.db.types.ProprieteType _propriete;


      //----------------/
     //- Constructors -/
    //----------------/

    public MOTSTypeItem() {
        super();
    }


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Returns the value of field 'appartenance'.
     * 
     * @return the value of field 'Appartenance'.
     */
    public fr.irisa.lis.portalis.service.patrimoine.arles.db.types.AppartenanceType getAppartenance(
    ) {
        return this._appartenance;
    }

    /**
     * Returns the value of field 'categoriedelarchitecture'.
     * 
     * @return the value of field 'Categoriedelarchitecture'.
     */
    public fr.irisa.lis.portalis.service.patrimoine.arles.db.types.CategoriedelarchitectureType getCategoriedelarchitecture(
    ) {
        return this._categoriedelarchitecture;
    }

    /**
     * Returns the value of field 'choiceValue'. The field
     * 'choiceValue' has the following description: Internal choice
     * value storage
     * 
     * @return the value of field 'ChoiceValue'.
     */
    public java.lang.Object getChoiceValue(
    ) {
        return this._choiceValue;
    }

    /**
     * Returns the value of field 'datation'.
     * 
     * @return the value of field 'Datation'.
     */
    public java.lang.String getDatation(
    ) {
        return this._datation;
    }

    /**
     * Returns the value of field 'edifice'.
     * 
     * @return the value of field 'Edifice'.
     */
    public java.lang.String getEdifice(
    ) {
        return this._edifice;
    }

    /**
     * Returns the value of field 'epoque'.
     * 
     * @return the value of field 'Epoque'.
     */
    public java.lang.String getEpoque(
    ) {
        return this._epoque;
    }

    /**
     * Returns the value of field 'propriete'.
     * 
     * @return the value of field 'Propriete'.
     */
    public fr.irisa.lis.portalis.service.patrimoine.arles.db.types.ProprieteType getPropriete(
    ) {
        return this._propriete;
    }

    /**
     * Returns the value of field 'stylearchitectural'.
     * 
     * @return the value of field 'Stylearchitectural'.
     */
    public java.lang.String getStylearchitectural(
    ) {
        return this._stylearchitectural;
    }

    /**
     * Returns the value of field 'themes'.
     * 
     * @return the value of field 'Themes'.
     */
    public java.lang.String getThemes(
    ) {
        return this._themes;
    }

    /**
     * Returns the value of field 'typedarchitecture'.
     * 
     * @return the value of field 'Typedarchitecture'.
     */
    public java.lang.String getTypedarchitecture(
    ) {
        return this._typedarchitecture;
    }

    /**
     * Sets the value of field 'appartenance'.
     * 
     * @param appartenance the value of field 'appartenance'.
     */
    public void setAppartenance(
            final fr.irisa.lis.portalis.service.patrimoine.arles.db.types.AppartenanceType appartenance) {
        this._appartenance = appartenance;
        this._choiceValue = appartenance;
    }

    /**
     * Sets the value of field 'categoriedelarchitecture'.
     * 
     * @param categoriedelarchitecture the value of field
     * 'categoriedelarchitecture'.
     */
    public void setCategoriedelarchitecture(
            final fr.irisa.lis.portalis.service.patrimoine.arles.db.types.CategoriedelarchitectureType categoriedelarchitecture) {
        this._categoriedelarchitecture = categoriedelarchitecture;
        this._choiceValue = categoriedelarchitecture;
    }

    /**
     * Sets the value of field 'datation'.
     * 
     * @param datation the value of field 'datation'.
     */
    public void setDatation(
            final java.lang.String datation) {
        this._datation = datation;
        this._choiceValue = datation;
    }

    /**
     * Sets the value of field 'edifice'.
     * 
     * @param edifice the value of field 'edifice'.
     */
    public void setEdifice(
            final java.lang.String edifice) {
        this._edifice = edifice;
        this._choiceValue = edifice;
    }

    /**
     * Sets the value of field 'epoque'.
     * 
     * @param epoque the value of field 'epoque'.
     */
    public void setEpoque(
            final java.lang.String epoque) {
        this._epoque = epoque;
        this._choiceValue = epoque;
    }

    /**
     * Sets the value of field 'propriete'.
     * 
     * @param propriete the value of field 'propriete'.
     */
    public void setPropriete(
            final fr.irisa.lis.portalis.service.patrimoine.arles.db.types.ProprieteType propriete) {
        this._propriete = propriete;
        this._choiceValue = propriete;
    }

    /**
     * Sets the value of field 'stylearchitectural'.
     * 
     * @param stylearchitectural the value of field
     * 'stylearchitectural'.
     */
    public void setStylearchitectural(
            final java.lang.String stylearchitectural) {
        this._stylearchitectural = stylearchitectural;
        this._choiceValue = stylearchitectural;
    }

    /**
     * Sets the value of field 'themes'.
     * 
     * @param themes the value of field 'themes'.
     */
    public void setThemes(
            final java.lang.String themes) {
        this._themes = themes;
        this._choiceValue = themes;
    }

    /**
     * Sets the value of field 'typedarchitecture'.
     * 
     * @param typedarchitecture the value of field
     * 'typedarchitecture'.
     */
    public void setTypedarchitecture(
            final java.lang.String typedarchitecture) {
        this._typedarchitecture = typedarchitecture;
        this._choiceValue = typedarchitecture;
    }

}

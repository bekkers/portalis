/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.3.1</a>, using an XML
 * Schema.
 * $Id$
 */

package fr.irisa.lis.portalis.service.patrimoine.arles.db;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlElements;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Class MOTSType.
 * 
 * @version $Revision$ $Date$
 */
@SuppressWarnings("serial")
@XmlRootElement
public class MOTSType implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * internal content storage
     */
    private java.lang.String _content = "";

    /**
     * Field _items.
     */
    private java.util.Vector<fr.irisa.lis.portalis.service.patrimoine.arles.db.MOTSTypeItem> _items;


      //----------------/
     //- Constructors -/
    //----------------/

    public MOTSType() {
        super();
        setContent("");
        this._items = new java.util.Vector<fr.irisa.lis.portalis.service.patrimoine.arles.db.MOTSTypeItem>();
    }


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * 
     * 
     * @param vMOTSTypeItem
     * @throws java.lang.IndexOutOfBoundsException if the index
     * given is outside the bounds of the collection
     */
    public void addMOTSTypeItem(
            final fr.irisa.lis.portalis.service.patrimoine.arles.db.MOTSTypeItem vMOTSTypeItem)
    throws java.lang.IndexOutOfBoundsException {
        this._items.addElement(vMOTSTypeItem);
    }

    /**
     * 
     * 
     * @param index
     * @param vMOTSTypeItem
     * @throws java.lang.IndexOutOfBoundsException if the index
     * given is outside the bounds of the collection
     */
    public void addMOTSTypeItem(
            final int index,
            final fr.irisa.lis.portalis.service.patrimoine.arles.db.MOTSTypeItem vMOTSTypeItem)
    throws java.lang.IndexOutOfBoundsException {
        this._items.add(index, vMOTSTypeItem);
    }

    /**
     * Method enumerateMOTSTypeItem.
     * 
     * @return an Enumeration over all
     * fr.irisa.lis.portalis.service.patrimoine.arles.MOTSTypeItem
     * elements
     */
    public java.util.Enumeration<? extends fr.irisa.lis.portalis.service.patrimoine.arles.db.MOTSTypeItem> enumerateMOTSTypeItem(
    ) {
        return this._items.elements();
    }

    /**
     * Returns the value of field 'content'. The field 'content'
     * has the following description: internal content storage
     * 
     * @return the value of field 'Content'.
     */
    public java.lang.String getContent(
    ) {
        return this._content;
    }

    /**
     * Method getMOTSTypeItem.
     * 
     * @param index
     * @throws java.lang.IndexOutOfBoundsException if the index
     * given is outside the bounds of the collection
     * @return the value of the
     * fr.irisa.lis.portalis.service.patrimoine.arles.MOTSTypeItem
     * at the given index
     */
    public fr.irisa.lis.portalis.service.patrimoine.arles.db.MOTSTypeItem getMOTSTypeItem(
            final int index)
    throws java.lang.IndexOutOfBoundsException {
        // check bounds for index
        if (index < 0 || index >= this._items.size()) {
            throw new IndexOutOfBoundsException("getMOTSTypeItem: Index value '" + index + "' not in range [0.." + (this._items.size() - 1) + "]");
        }

        return (fr.irisa.lis.portalis.service.patrimoine.arles.db.MOTSTypeItem) _items.get(index);
    }

    /**
     * Method getMOTSTypeItem.Returns the contents of the
     * collection in an Array.  <p>Note:  Just in case the
     * collection contents are changing in another thread, we pass
     * a 0-length Array of the correct type into the API call. 
     * This way we <i>know</i> that the Array returned is of
     * exactly the correct length.
     * 
     * @return this collection as an Array
     */
    @XmlElementWrapper(name = "MOTS")
    @XmlElements({
        @XmlElement(name="appartenance"),
        @XmlElement(name="categoriedelarchitecture"),
        @XmlElement(name="typedarchitecture"),
        @XmlElement(name="themes"),
        @XmlElement(name="epoque"),
        @XmlElement(name="stylearchitectural"),
        @XmlElement(name="datation"),
        @XmlElement(name="edifice"),
        @XmlElement(name="propriete")
    })
    public fr.irisa.lis.portalis.service.patrimoine.arles.db.MOTSTypeItem[] getMOTSTypeItem(
    ) {
        fr.irisa.lis.portalis.service.patrimoine.arles.db.MOTSTypeItem[] array = new fr.irisa.lis.portalis.service.patrimoine.arles.db.MOTSTypeItem[0];
        return (fr.irisa.lis.portalis.service.patrimoine.arles.db.MOTSTypeItem[]) this._items.toArray(array);
    }

    /**
     * Method getMOTSTypeItemCount.
     * 
     * @return the size of this collection
     */
    public int getMOTSTypeItemCount(
    ) {
        return this._items.size();
    }

    /**
     */
    public void removeAllMOTSTypeItem(
    ) {
        this._items.clear();
    }

    /**
     * Method removeMOTSTypeItem.
     * 
     * @param vMOTSTypeItem
     * @return true if the object was removed from the collection.
     */
    public boolean removeMOTSTypeItem(
            final fr.irisa.lis.portalis.service.patrimoine.arles.db.MOTSTypeItem vMOTSTypeItem) {
        boolean removed = _items.remove(vMOTSTypeItem);
        return removed;
    }

    /**
     * Method removeMOTSTypeItemAt.
     * 
     * @param index
     * @return the element removed from the collection
     */
    public fr.irisa.lis.portalis.service.patrimoine.arles.db.MOTSTypeItem removeMOTSTypeItemAt(
            final int index) {
        java.lang.Object obj = this._items.remove(index);
        return (fr.irisa.lis.portalis.service.patrimoine.arles.db.MOTSTypeItem) obj;
    }

    /**
     * Sets the value of field 'content'. The field 'content' has
     * the following description: internal content storage
     * 
     * @param content the value of field 'content'.
     */
    public void setContent(
            final java.lang.String content) {
        this._content = content;
    }

    /**
     * 
     * 
     * @param index
     * @param vMOTSTypeItem
     * @throws java.lang.IndexOutOfBoundsException if the index
     * given is outside the bounds of the collection
     */
    public void setMOTSTypeItem(
            final int index,
            final fr.irisa.lis.portalis.service.patrimoine.arles.db.MOTSTypeItem vMOTSTypeItem)
    throws java.lang.IndexOutOfBoundsException {
        // check bounds for index
        if (index < 0 || index >= this._items.size()) {
            throw new IndexOutOfBoundsException("setMOTSTypeItem: Index value '" + index + "' not in range [0.." + (this._items.size() - 1) + "]");
        }

        this._items.set(index, vMOTSTypeItem);
    }

    /**
     * 
     * 
     * @param vMOTSTypeItemArray
     */
    public void setMOTSTypeItem(
            final fr.irisa.lis.portalis.service.patrimoine.arles.db.MOTSTypeItem[] vMOTSTypeItemArray) {
        //-- copy array
        _items.clear();

        for (int i = 0; i < vMOTSTypeItemArray.length; i++) {
                this._items.add(vMOTSTypeItemArray[i]);
        }
    }

}

/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.3.1</a>, using an XML
 * Schema.
 * $Id$
 */

package fr.irisa.lis.portalis.service.patrimoine.arles;

/**
 * Class Patrimoine.
 * 
 * @version $Revision$ $Date$
 */
@SuppressWarnings("serial")
public class Patrimoine implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _fiche.
     */
    private java.lang.String _fiche;

    /**
     * Field _identifiant.
     */
    private short _identifiant;

    /**
     * keeps track of state for field: _identifiant
     */
    private boolean _has_identifiant;

    /**
     * Field _titre.
     */
    private java.lang.String _titre;

    /**
     * Field _description.
     */
    private java.lang.String _description;

    /**
     * Field _latitude.
     */
    private java.lang.String _latitude;

    /**
     * Field _longitude.
     */
    private java.lang.String _longitude;

    /**
     * Field _MOTS.
     */
    private fr.irisa.lis.portalis.service.patrimoine.arles.MOTS _MOTS;

    /**
     * Field _images.
     */
    private fr.irisa.lis.portalis.service.patrimoine.arles.Images _images;


      //----------------/
     //- Constructors -/
    //----------------/

    public Patrimoine() {
        super();
    }


      //-----------/
     //- Methods -/
    //-----------/

    /**
     */
    public void deleteIdentifiant(
    ) {
        this._has_identifiant= false;
    }

    /**
     * Returns the value of field 'description'.
     * 
     * @return the value of field 'Description'.
     */
    public java.lang.String getDescription(
    ) {
        return this._description;
    }

    /**
     * Returns the value of field 'fiche'.
     * 
     * @return the value of field 'Fiche'.
     */
    public java.lang.String getFiche(
    ) {
        return this._fiche;
    }

    /**
     * Returns the value of field 'identifiant'.
     * 
     * @return the value of field 'Identifiant'.
     */
    public short getIdentifiant(
    ) {
        return this._identifiant;
    }

    /**
     * Returns the value of field 'images'.
     * 
     * @return the value of field 'Images'.
     */
    public fr.irisa.lis.portalis.service.patrimoine.arles.Images getImages(
    ) {
        return this._images;
    }

    /**
     * Returns the value of field 'latitude'.
     * 
     * @return the value of field 'Latitude'.
     */
    public java.lang.String getLatitude(
    ) {
        return this._latitude;
    }

    /**
     * Returns the value of field 'longitude'.
     * 
     * @return the value of field 'Longitude'.
     */
    public java.lang.String getLongitude(
    ) {
        return this._longitude;
    }

    /**
     * Returns the value of field 'MOTS'.
     * 
     * @return the value of field 'MOTS'.
     */
    public fr.irisa.lis.portalis.service.patrimoine.arles.MOTS getMOTS(
    ) {
        return this._MOTS;
    }

    /**
     * Returns the value of field 'titre'.
     * 
     * @return the value of field 'Titre'.
     */
    public java.lang.String getTitre(
    ) {
        return this._titre;
    }

    /**
     * Method hasIdentifiant.
     * 
     * @return true if at least one Identifiant has been added
     */
    public boolean hasIdentifiant(
    ) {
        return this._has_identifiant;
    }

    /**
     * Sets the value of field 'description'.
     * 
     * @param description the value of field 'description'.
     */
    public void setDescription(
            final java.lang.String description) {
        this._description = description;
    }

    /**
     * Sets the value of field 'fiche'.
     * 
     * @param fiche the value of field 'fiche'.
     */
    public void setFiche(
            final java.lang.String fiche) {
        this._fiche = fiche;
    }

    /**
     * Sets the value of field 'identifiant'.
     * 
     * @param identifiant the value of field 'identifiant'.
     */
    public void setIdentifiant(
            final short identifiant) {
        this._identifiant = identifiant;
        this._has_identifiant = true;
    }

    /**
     * Sets the value of field 'images'.
     * 
     * @param images the value of field 'images'.
     */
    public void setImages(
            final fr.irisa.lis.portalis.service.patrimoine.arles.Images images) {
        this._images = images;
    }

    /**
     * Sets the value of field 'latitude'.
     * 
     * @param latitude the value of field 'latitude'.
     */
    public void setLatitude(
            final java.lang.String latitude) {
        this._latitude = latitude;
    }

    /**
     * Sets the value of field 'longitude'.
     * 
     * @param longitude the value of field 'longitude'.
     */
    public void setLongitude(
            final java.lang.String longitude) {
        this._longitude = longitude;
    }

    /**
     * Sets the value of field 'MOTS'.
     * 
     * @param MOTS the value of field 'MOTS'.
     */
    public void setMOTS(
            final fr.irisa.lis.portalis.service.patrimoine.arles.MOTS MOTS) {
        this._MOTS = MOTS;
    }

    /**
     * Sets the value of field 'titre'.
     * 
     * @param titre the value of field 'titre'.
     */
    public void setTitre(
            final java.lang.String titre) {
        this._titre = titre;
    }

}

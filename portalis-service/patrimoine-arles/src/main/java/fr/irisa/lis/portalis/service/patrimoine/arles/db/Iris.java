/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.3.1</a>, using an XML
 * Schema.
 * $Id$
 */

package fr.irisa.lis.portalis.service.patrimoine.arles.db;

/**
 * Class Iris.
 * 
 * @version $Revision$ $Date$
 */
@SuppressWarnings("serial")
public class Iris extends IrisType 
implements java.io.Serializable
{


      //----------------/
     //- Constructors -/
    //----------------/

    public Iris() {
        super();
    }

    public Iris(final java.lang.String defaultValue) {
        super(defaultValue);
    }

}

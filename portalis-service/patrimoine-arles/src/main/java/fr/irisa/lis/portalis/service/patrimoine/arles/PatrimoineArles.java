package fr.irisa.lis.portalis.service.patrimoine.arles;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintStream;
import java.io.Writer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactoryConfigurationError;

import net.sf.saxon.s9api.ItemType;
import net.sf.saxon.s9api.QName;
import net.sf.saxon.s9api.XdmAtomicValue;
import net.sf.saxon.s9api.XdmItem;
import net.sf.saxon.s9api.XdmNode;
import net.sf.saxon.s9api.XdmValue;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.SAXException;

import fr.irisa.lis.portalis.commons.DOMUtil;
import fr.irisa.lis.portalis.commons.ParameterValue;
import fr.irisa.lis.portalis.commons.XqueryUtil;
import fr.irisa.lis.portalis.commons.core.CommonsUtil;
import fr.irisa.lis.portalis.system.linux.ImageResize;
import fr.irisa.lis.portalis.taxonomy.insee.communes.CommunesInfo;

public class PatrimoineArles {
	

	public static class ConfigXQuery {
		private boolean surrounding = false;
		private boolean cles = false;

		public ConfigXQuery() {
		}

		public ConfigXQuery(boolean surrounding, boolean cles) {
			this.surrounding = surrounding;
			this.cles = cles;
		}

		public boolean isSurrounding() {
			return surrounding;
		}

		public boolean isCles() {
			return cles;
		}

	}

	private static final Map<String, Integer> motsTitre = new HashMap<String, Integer>();
	private static final Map<String, Integer> motsDescription = new HashMap<String, Integer>();

	private static final Set<String> datationSet = new HashSet<String>();
	private static final Set<String> edificeSet = new HashSet<String>();
	private static final Set<String> epoqueSet = new HashSet<String>();
	private static final Set<String> styleArchitectureSet = new HashSet<String>();
	private static final Set<String> appartenanceSet = new HashSet<String>();
	private static final Set<String> typeArchitectureSet = new HashSet<String>();
	private static final Set<String> categorieArchitectureSet = new HashSet<String>();
	private static final Set<String> proprieteSet = new HashSet<String>();
	private static final Set<String> themeSet = new HashSet<String>();

	private static final Map<String, Set<String>> mots = new HashMap<String, Set<String>>();

	public static final Set<String> allDateAvant = new HashSet<String>();
	public static final Set<String> allDateApres = new HashSet<String>();
	public static final Set<String> allMoisApres = new HashSet<String>();

	private static final String WEBAPPS_DIR = "/srv/webapps";
	private static final String ARLES_DIR = WEBAPPS_DIR
			+ "/patrimoine/patrimoineArles";
	private static final String ICONS_DIR = ARLES_DIR+"/icons";
	public static final String MOTS_XML = ARLES_DIR + "/mots.xml";
	public static final String ARLES_XML = ARLES_DIR + "/patrimoineArles.xml";
	public static final String ARLES_CTX = ARLES_DIR + "/patrimoineArles.ctx";

	private Villes villes;
	private Document doc;

	public PatrimoineArles() throws Exception {
		mots.put("datation", datationSet);
		mots.put("edifice", edificeSet);
		mots.put("epoque", epoqueSet);
		mots.put("styleArchitectural", styleArchitectureSet);
		mots.put("appartenance", appartenanceSet);
		mots.put("typeDArchitecture", typeArchitectureSet);
		mots.put("categorieDeLArchitecture", categorieArchitectureSet);
		mots.put("propriete", proprieteSet);
		mots.put("theme", themeSet);
	}

	// Export
	public static void marshal(Villes villes, File selectedFile)
			throws IOException, JAXBException {
		JAXBContext context;
		BufferedWriter writer = null;
		writer = new BufferedWriter(new FileWriter(selectedFile));
		context = JAXBContext.newInstance(Villes.class);
		Marshaller m = context.createMarshaller();
		m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
		m.marshal(villes, writer);
		writer.close();
	}

	public void generateCtx() {
		try {
			
			File iconsDir = new File(ICONS_DIR);
			if (iconsDir.isDirectory()) {
				for (File file : iconsDir.listFiles()) {
					if (!file.delete()) {
						throw new Exception("Impossible de nettoyer le répertoire "+ICONS_DIR);
					}
				}
			} else if (!iconsDir.exists()) {
				if (!iconsDir.mkdir()) {
					throw new Exception("Impossible de créer le répertoire "+ICONS_DIR);
				}
			} else {
				if (!iconsDir.isDirectory()) {
					throw new Exception("Le fichier "+ICONS_DIR+" n'est pas un répertoire");
				}
			}

			OutputStream outputStream = new FileOutputStream(ARLES_CTX);
			getVilles();

			Writer out = new BufferedWriter(new OutputStreamWriter(
					outputStream, "UTF-8"));
			generateCtx(out);
			String axioms = generateAxioms();
			out.append(axioms);
			out.close();

			collectedMotsToXml(MOTS_XML);

			// showVilles();
			// showCollectedMots();
			// showMotsTitre();
			// showMotsDescription();
			// showDates();

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void showMotsDescription() {
		for (Entry<String, Integer> cle : motsDescription.entrySet()) {
			System.out.println(cle.getKey() + "\t" + cle.getValue());
		}
	}

	private void showMotsTitre() {
		for (Entry<String, Integer> cle : motsTitre.entrySet()) {
			System.out.println(cle.getKey() + "\t" + cle.getValue());
		}
	}

	private void showVilles() {
		System.out.println("nb villes = " + villes.getVille().length);
		for (int i = 0; i < villes.getVille().length; i++) {
			System.out.print("\t" + villes.getVille(i).getNomville());
			if ((i + 1) % 5 == 0)
				System.out.println();
		}
		System.out.println();
	}

	private void showCollectedMots() {
		for (Entry<String, Set<String>> cle : mots.entrySet()) {
			System.out.println("\nMot : " + cle.getKey());
			printSet(cle.getValue(), System.out);
			System.out.println();
		}
	}

	private static void showDates() {
		for (String s : allDateAvant) {
			System.out.println(s);
		}

		for (String s : allDateApres) {
			System.out.println(s);
		}

		for (String s : allMoisApres) {
			System.out.println(s);
		}
	}

	private void collectedMotsToXml(String motsXml) {
		Document doc = DOMUtil.createDocument();
		Element root = doc.createElement("Mots");
		doc.appendChild(root);

		for (Entry<String, Set<String>> cle : mots.entrySet()) {
			Element cleElement = doc.createElement("Mot");
			cleElement.setAttribute("nom", cle.getKey());
			root.appendChild(cleElement);
			Set<String> values = cle.getValue();
			String[] valTab = new String[values.size()];
			for (String val : values.toArray(valTab)) {
				Element valElement = doc.createElement("Mot");
				valElement.setAttribute("nom", val);
				cleElement.appendChild(valElement);
			}
		}

		DOMUtil.prettyPrintXml(doc, motsXml);
	}

	private void printSet(Set<String> value, PrintStream out) {
		Iterator<String> iterator = value.iterator();
		for (int i = 0; iterator.hasNext(); i++) {
			if (i % 5 == 0)
				out.print("\n" + iterator.next());
			else
				out.print("\t" + iterator.next());
		}

	}

	private String generateAxioms() throws Exception,
			TransformerFactoryConfigurationError, TransformerException,
			SAXException, ParserConfigurationException {
		XqueryUtil xqueryUtil = new XqueryUtil();
		Document root = DOMUtil.readFile("mots.xml");
		System.out.println("==================================="
				+ root.getNodeName());

		List<ParameterValue> externalParams = new ArrayList<ParameterValue>();
		externalParams.add(new ParameterValue(new QName("root"), xqueryUtil
				.fromDOMToXdmNode(root)));

		XdmValue result = xqueryUtil.xqueryTransform(root, "extractAxioms.xqy",
				externalParams);
		if (result == null || result.itemAt(0) == null
				|| result.itemAt(0).isAtomicValue()) {
			return null;
		}
		XdmNode xdmNode = (XdmNode) result.itemAt(0);
		return xdmNode.getStringValue();
	}

	public Document mergePatrimoine() throws Exception {
		return mergePatrimoine(null, new ConfigXQuery());
	}

	public Document mergePatrimoine(ConfigXQuery configXQuery)
			throws Exception {
		return mergePatrimoine(null, configXQuery);
	}

	public Document mergePatrimoine(String name) throws Exception {
		return mergePatrimoine(name, new ConfigXQuery());
	}

	public Document mergePatrimoine(String name, ConfigXQuery configXQuery)
			throws Exception {

		List<ParameterValue> externalParams = new ArrayList<ParameterValue>();
		externalParams.add(new ParameterValue(new QName("villes"),
				new XdmAtomicValue(ARLES_XML, ItemType.STRING)));
		String sourceCircons = CommunesInfo.circonscriptionGpsXmlFileName;
		externalParams.add(new ParameterValue(new QName("inseeVilles"),
				new XdmAtomicValue(sourceCircons, ItemType.STRING)));
		String sourceCOG = CommunesInfo.cogXmlFileName;
		externalParams.add(new ParameterValue(new QName("cogVilles"),
				new XdmAtomicValue(sourceCOG, ItemType.STRING)));
		String sourceIRIS = CommunesInfo.irisXmlFileName;
		externalParams.add(new ParameterValue(new QName("irisVilles"),
				new XdmAtomicValue(sourceIRIS, ItemType.STRING)));
		externalParams.add(new ParameterValue(new QName("surrounding"),
				new XdmAtomicValue(Boolean.toString(configXQuery
						.isSurrounding()), ItemType.BOOLEAN)));
		externalParams.add(new ParameterValue(new QName("cles"),
				new XdmAtomicValue(Boolean.toString(configXQuery.isCles()),
						ItemType.BOOLEAN)));

		QName functionName = new QName("http://www.irisa.fr/bekkers", "Ville");
		XdmValue result = null;
		if (name != null) {
			XdmItem xdmItem = new XdmAtomicValue(name, ItemType.STRING);
			result = new XqueryUtil().callFunction("merger.xqy",
					functionName, xdmItem, externalParams);
		} else {
			result = new XqueryUtil().callFunction("merger.xqy",
					functionName, externalParams);
		}

		if (result == null || result.itemAt(0) == null
				|| result.itemAt(0).isAtomicValue()) {
			return null;
		}
		XdmNode xdmNode = (XdmNode) result.itemAt(0);

		return XqueryUtil.XdmNodeToDomDocument(xdmNode);
	}

	public Villes getVilles() throws FileNotFoundException, JAXBException {
		if (villes == null) {
			JAXBContext context = JAXBContext.newInstance(Villes.class);
			Unmarshaller um = context.createUnmarshaller();
			InputStream inputStream = new FileInputStream(ARLES_XML);
			villes = (Villes) um.unmarshal(inputStream);
		}
		return villes;
	}

	public Document getDocument() throws SAXException, IOException,
			ParserConfigurationException {
		if (doc == null) {
			InputStream inputStream = new FileInputStream(ARLES_XML);
			doc = DOMUtil.readFile(inputStream);
		}
		return doc;
	}

	public void reset() {
		villes = null;
		doc = null;
	}

	public static void main(String[] args) {
		try {
			PatrimoineArles patrimoineArles = new PatrimoineArles();
			Document doc = patrimoineArles
					.mergePatrimoine(new PatrimoineArles.ConfigXQuery(true,
							true));
			System.out.println(DOMUtil.prettyXmlString(doc));
			DOMUtil.prettyPrintXml(doc, new File("out.xml"));

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

//	public static void main(String[] args) {
//		try {
//			PatrimoineArles patrimoineArles = new PatrimoineArles();
//			patrimoineArles.generateCtx();
//			// Document doc = patrimoineArles.getDocument();
//			// NodeList list = doc.getElementsByTagName("description");
//
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//	}

	private static final String[] themesEau = { "Fontaine", "Thermes",
			"Lavoir", "Puit", "Aqueduc", "Moulin", "Canaux", "Canal", "Noria",
			"Baptistère", "Pluie", "Phare", "Bassin" };

	private static final String[] themesDefensif = { "Tour", "Château",
			"Porte", "Mur", "Castella", "Castrum", "Grand Portail", "Oppidum" };

	private static final String[] themesReligion = { "Calviniste", "Martyre",
			"Croix", "Oratoire", "Quatre-Évangélistes", "Niche", "Cénotaphe",
			"Calvaire", "Tombeau", "Tombe", "Mausolée", "Assomption",
			"Bénédictin", "Autel", "Baptistère", "Notre Dame", "Saint",
			"Vierge", "Dolmen", "Menhir", "Confrérie", "Marie-Madeleine",
			"Statue", "Autel", "Stèle", "Jésus", "Christ", "Nécropole", "Buste" };

	private static final String[] batiReligion = { "Abbaye", "Chapelle",
			"Couvent", "clocher", "[Eeé]glise", "Achevêché", "Prieuré",
			"Cloître", "Ermitage", "Monastère", "Collégiale", "Temple" };

	private static final String[] batiHabitation = { "Maison",
			"Hôtel particulier", "Ferme", "Villas", "Château", "Mas", "Palais",
			"Troglodytes" };

	private static final String[] batiPublic = { "écoles", "Hôpital", "Mairie",
			"Hôtel des Postes", ",Arènes", "Musée", "Théâtre", "Caserne",
			"Cirque" };

	private static final String[] batiVoiesDeCommunication = { "Passage",
			"Voie", "Pont", "Ligne et bâtiment ferroviaire" };

	private static final String[] distributionEau = { "Fontaine", "Puit",
			"Noria", "Aqueduc", "Cana(ux|l)", "hydraulique", "Citerne" };

	private static final String[] insdustrieArtisanat = { "Ferme", "Meunerie",
			"Bergerie", "Miel", "Carrière", "Four", "Site industriel" };

	private static final String[][] cles = { themesEau, themesDefensif,
			themesReligion, batiReligion, batiPublic, batiHabitation,
			distributionEau, insdustrieArtisanat, batiVoiesDeCommunication };

	private void generateCtx(Writer out) throws IOException {
		out.append("include \""+WEBAPPS_DIR+"/patrimoine/epoque.ctx\"\n");

		// for (String themeTransportEau : themesTransportEau) {
		// out.append("axiom theme '" + themeTransportEau
		// + "', theme 'Alimentation en eau'\n");
		// }
		//
		// for (String themeReligion : themesReligion) {
		// out.append("axiom theme '" + themeReligion
		// + "', theme 'Religion'\n");
		// }
		//
		// for (String themeDefensif : themesDefensif) {
		// out.append("axiom theme '" + themeDefensif
		// + "', theme 'Bâtiment défensif'\n");
		// }
		//
		// for (String batimentReligieux : batimentsReligieux) {
		// out.append("axiom edifice '" + batimentReligieux
		// + "', edifice 'Bâtiment religieux'\n");
		// }
		//
		// for (String themeHabitation : themesHabitation) {
		// out.append("axiom edifice '" + themeHabitation
		// + "', edifice 'Bâtiment d'habitation'\n");
		// }
		//
		// for (String themeEau : themesEau) {
		// out.append("axiom theme '" + themeEau + "', theme 'Eau'\n");
		// }

		for (Ville ville : villes.getVille()) {
			Patrimoine[] patrimoines = ville.getPatrimoine();
			for (Patrimoine patrimoine : patrimoines) {
				String titre = patrimoine.getTitre();
				if (titre.startsWith("Le patrimoine de la commune")) {
					out.append("mk \"")
							.append(ville.getIdville() + "")
							.append("-")
							.append(CommonsUtil.normalize(CommonsUtil
									.normalize(ville.getNomville())))
							.append("\"");
					out.append(" type Ville");
				} else {
					String imageName = generateImage(patrimoine);
					if (imageName==null) {
					out.append("mk \"")
							.append(patrimoine.getIdentifiant() + "-")
							.append(CommonsUtil.normalize(titre))
							.append("\"");
					} else {
						out.append("mk Picture(\"")
						.append(imageName).append("\", \"")
						.append(patrimoine.getIdentifiant() + "-")
						.append(CommonsUtil.normalize(titre))
						.append("\")");
					}
					out.append(" type Patrimoine, ville '")
							.append(ville.getIdville() + "").append("-")
							.append(CommonsUtil.normalize(ville.getNomville()))
							.append("'");
				}

				storeMotsTitre(patrimoine.getTitre(), motsTitre);
				storeMotsTitre(patrimoine.getDescription(), motsDescription);

				Set<String> datesAvant = getDatesAvant(patrimoine
						.getDescription());
				if (datesAvant.size() > 0) {
					for (String date : datesAvant) {
						out.append(", dateReconnue date = ").append(date);
					}
				}

				Set<String> datesApres = getDatesApres(patrimoine
						.getDescription());
				if (datesApres.size() > 0) {
					for (String date : datesApres) {
						out.append(", dateReconnue date = ").append(date);
					}
				}

				Set<String> moisApres = getMoisApres(patrimoine
						.getDescription());
				if (moisApres.size() > 0) {
					for (String date : moisApres) {
						out.append(", dateReconnue date = ").append(date);
					}
				}

				MOTS mots = patrimoine.getMOTS();

				String[] datations = mots.getDatation();
				if (datations != null && datations.length > 0) {
					for (String datation : datations) {
						String normalisedDatation = CommonsUtil
								.normalize(datation);
						out.append(", datation '").append(normalisedDatation)
								.append("'");
						datationSet.add(normalisedDatation);
					}
				}

				String[] edifices = mots.getEdifice();
				if (edifices != null && edifices.length > 0) {
					for (String edifice : edifices) {
						String normalisedEdifice = CommonsUtil
								.normalize(edifice);
						out.append(", edifice '").append(normalisedEdifice)
								.append("'");
						edificeSet.add(normalisedEdifice);
					}
				}

				String[] epoques = mots.getEpoque();
				if (epoques != null && epoques.length > 0) {
					for (String epoque : epoques) {
						if (epoque.equals("Moyen-Age"))
							epoque = "Moyen âge";
						String normalisedEpoque = CommonsUtil.normalize(epoque);
						out.append(", epoque '").append(normalisedEpoque)
								.append("'");
						epoqueSet.add(normalisedEpoque);
					}
				}

				String[] stylesArchitecturaux = mots.getStylearchitectural();
				if (stylesArchitecturaux != null
						&& stylesArchitecturaux.length > 0) {
					for (String stylearchitectural : stylesArchitecturaux) {
						String normalisedStylearchitectural = CommonsUtil
								.normalize(stylearchitectural);
						out.append(", styleArchitectural '")
								.append(normalisedStylearchitectural)
								.append("'");
						styleArchitectureSet.add(normalisedStylearchitectural);
					}
				}

				String[] appartenances = mots.getAppartenance();
				if (appartenances != null && appartenances.length > 0) {
					for (String appartenance : appartenances) {
						String normalisedAppartenance = CommonsUtil
								.normalize(appartenance);
						out.append(", appartenance '")
								.append(normalisedAppartenance).append("'");
						appartenanceSet.add(normalisedAppartenance);
					}
				}

				String[] typedarchitecture = mots.getTypedarchitecture();
				if (typedarchitecture != null && typedarchitecture.length > 0) {
					for (String typearchitecture : typedarchitecture) {
						String normalisedTypearchitecture = CommonsUtil
								.normalize(typearchitecture);
						out.append(", typeDArchitecture '")
								.append(normalisedTypearchitecture).append("'");
						typeArchitectureSet.add(normalisedTypearchitecture);
					}
				}

				String[] categoriesdelarchitecture = mots
						.getCategoriedelarchitecture();
				if (categoriesdelarchitecture != null
						&& categoriesdelarchitecture.length > 0) {
					for (String categoriedelarchitecture : categoriesdelarchitecture) {
						String normalisedCategoriedelarchitecture = CommonsUtil
								.normalize(categoriedelarchitecture);
						out.append(", categorieDeLArchitecture '")
								.append(normalisedCategoriedelarchitecture)
								.append("'");
						categorieArchitectureSet
								.add(normalisedCategoriedelarchitecture);
					}
				}

				String[] proprietes = mots.getPropriete();
				if (proprietes != null && proprietes.length > 0) {
					for (String propriete : proprietes) {
						String normalisedPropriete = CommonsUtil
								.normalize(propriete);
						out.append(", propriete '").append(normalisedPropriete)
								.append("'");
						proprieteSet.add(normalisedPropriete);
					}
				}

				String[] themes = mots.getThemes();
				if (themes != null && themes.length > 0) {
					for (String theme : themes) {
						String normalisedTheme = CommonsUtil.normalize(theme);
						out.append(", themes '").append(normalisedTheme)
								.append("'");
						themeSet.add(normalisedTheme);
					}
				}

				// // ajout des propriètés selon les mots clès présents dans legenerateImage
				// // titre
				// for (String batimentReligieux : batimentsReligieux) {
				// String titre = patrimoine.getTitre();
				// if (titre.contains(batimentReligieux)
				// || titre.contains(batimentReligieux.toLowerCase())) {
				// out.append(", edifice '")
				// .append(CommonsUtil
				// .normalize(batimentReligieux))
				// .append("'");
				// }
				//
				// }
				// for (String themeEau : themesEau) {
				// String titre = patrimoine.getTitre();
				// if (titre.contains(themeEau)
				// || titre.contains(themeEau.toLowerCase())) {
				// out.append(", theme '")
				// .append(CommonsUtil.normalize(themeEau))
				// .append("'");
				// }
				//
				// }
				// for (String themeHabitation : themesHabitation) {
				// String titre = patrimoine.getTitre();
				// if (titre.contains(themeHabitation)
				// || titre.contains(themeHabitation.toLowerCase())) {
				// out.append(", theme '")
				// .append(CommonsUtil.normalize(themeHabitation))
				// .append("'");
				// }
				//
				// }
				// for (String themeDefensif : themesDefensif) {
				// String titre = patrimoine.getTitre();
				// if (titre.contains(themeDefensif)
				// || titre.contains(themeDefensif.toLowerCase())) {
				// out.append(", theme '")
				// .append(CommonsUtil.normalize(themeDefensif))
				// .append("'");
				// }
				//
				// }
				// for (String themeReligion : themesReligion) {
				// String titre = patrimoine.getTitre();
				// if (titre.contains(themeReligion)
				// || titre.contains(themeReligion.toLowerCase())) {
				// out.append(", theme '")
				// .append(CommonsUtil.normalize(themeReligion))
				// .append("'");generateImage
				// }
				// }
				// for (String batimentReligieux : batimentsReligieux) {
				// String titre = patrimoine.getTitre();
				// if (titre.contains(batimentReligieux)
				// || titre.contains(batimentReligieux.toLowerCase())) {
				// out.append(", theme '")
				// .append(CommonsUtil
				// .normalize(batimentReligieux))
				// .append("'");
				// }
				// }
				out.append("\n");
			}
		}

	}

	private String generateImage(Patrimoine patrimoine) {
		if (patrimoine.getImages()==null || patrimoine.getImages().getImageCount()==0)
			return null;
		else {
			String imageName = CommonsUtil.normalize(patrimoine.getImages().getImage(0));
			String output = ICONS_DIR+"/img-"+patrimoine.getIdentifiant()+".jpg";
			ImageResize.imageResize(imageName, output);
			return output;
		}

	}

	private void storeMotsTitre(String titre, Map<String, Integer> lesMots) {
		String[] motsDuTitre = titre.split(" ");
		for (String token : motsDuTitre) {
			Integer nb = lesMots.get(token);
			if (nb == null) {
				lesMots.put(token, new Integer(1));
			} else {
				lesMots.put(token, new Integer(nb + 1));
			}
		}
	}

	private static final String PREFIXE_MOIS = "([j|J]anvier|[f|F]évrier|[m|M]ars|[a|A]vril|[m|M]ai|[j|J]uin|[j|J]uillet|[a|A]out|[s|S]eptembre|"
			+ "[o|O]ctobre|[n|N]ovembre|[d|D]écembre)\\s+";
	private static final String PREFIXE_ANNEE = "(\\s[dD]epuis\\s|\\s[eE]n\\s|\\s[fF]in\\s|\\s[aA]nnée\\s|\\s[dD]e\\s|\\s[aA]nnées\\s|\\s[vV]ers\\s)";
	private static final String ANNEE_AVANT = "\\s(\\d{2,4})\\s+(avant|av|av\\.)\\s+J";
	private static final String ANNEE_PARENTHESES = "\\((\\d{3,4})\\)";
	private static final String ANNEE_APRES = PREFIXE_ANNEE
			+ "(\\d{2,4})(\\.\\s|,\\s|\\s+(?!avant\\s+J|av\\s+J|av\\.J|œuvres\\b|\\d+|langues\\b|mois\\b|à\\b|mètres\\b|m\\b|milles\\b))";
	private static final String MOIS_APRES = PREFIXE_MOIS
			+ "(\\d{2,4})(\\.\\s|,\\s|\\s+(?!avant\\s+J|av\\s+J|av\\.\\s+J|œuvres\\b|\\d+|langues\\b|mois\\b|à\\b|mètres\\b|m\\b|milles\\b))";

	private Set<String> getDatesAvant(String description) {

		Set<String> allMatches = new HashSet<String>();
		Matcher m = Pattern.compile(ANNEE_AVANT).matcher(description);
		while (m.find()) {
			String s = m.group();
			allMatches.add(s.replaceAll(ANNEE_AVANT, "-$1"));

		}

		allDateAvant.addAll(allMatches);

		return allMatches;
	}

	private Set<String> getDatesApres(String description) {

		Set<String> allMatches = new HashSet<String>();
		Matcher m = Pattern.compile(ANNEE_APRES).matcher(description);
		while (m.find()) {
			String s = m.group();
			allMatches.add(s.replaceAll(ANNEE_APRES, "$2"));

		}
		m = Pattern.compile(ANNEE_PARENTHESES).matcher(description);
		while (m.find()) {
			String s = m.group();
			allMatches.add(s.replaceAll(ANNEE_PARENTHESES, "$1"));

		}

		allDateApres.addAll(allMatches);

		return allMatches;
	}

	private Set<String> getMoisApres(String description) {

		Set<String> allMatches = new HashSet<String>();
		Matcher m = Pattern.compile(MOIS_APRES).matcher(description);
		while (m.find()) {
			String s = m.group();
			allMatches.add(s.replaceAll(MOIS_APRES, "$1 $2").toLowerCase()
					.replace("janvier", "jan").replace("février", "feb")
					.replace("mars", "mar").replace("avril", "apr")
					.replace("mai", "may").replace("juin", "jun")
					.replace("juillet", "jul").replace("aout", "aug")
					.replace("septembre", "sep").replace("octobre", "oct")
					.replace("novembre", "nov").replace("décembre", "dec"));

		}

		allMoisApres.addAll(allMatches);

		return allMatches;
	}

}

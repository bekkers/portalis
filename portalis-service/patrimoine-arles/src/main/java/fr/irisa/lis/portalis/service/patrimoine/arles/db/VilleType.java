/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.3.1</a>, using an XML
 * Schema.
 * $Id$
 */

package fr.irisa.lis.portalis.service.patrimoine.arles.db;

import javax.xml.bind.annotation.XmlAttribute;

/**
 * Class VilleType.
 * 
 * @version $Revision$ $Date$
 */
@SuppressWarnings("serial")
public class VilleType implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _circonscription.
     */
    private java.lang.String _circonscription;

    /**
     * Field _codeInsee.
     */
    private short _codeInsee;

    /**
     * keeps track of state for field: _codeInsee
     */
    private boolean _has_codeInsee;

    /**
     * Field _codesPostaux.
     */
    private short _codesPostaux;

    /**
     * keeps track of state for field: _codesPostaux
     */
    private boolean _has_codesPostaux;

    /**
     * Field _latitude.
     */
    private java.lang.String _latitude;

    /**
     * Field _localId.
     */
    private byte _localId;

    /**
     * keeps track of state for field: _localId
     */
    private boolean _has_localId;

    /**
     * Field _longitude.
     */
    private java.lang.String _longitude;

    /**
     * Field _nom.
     */
    private java.lang.String _nom;

    /**
     * Field _pop1999.
     */
    private int _pop1999;

    /**
     * keeps track of state for field: _pop1999
     */
    private boolean _has_pop1999;

    /**
     * Field _pop2010.
     */
    private int _pop2010;

    /**
     * keeps track of state for field: _pop2010
     */
    private boolean _has_pop2010;

    /**
     * Field _site.
     */
    private java.lang.String _site;

    /**
     * Field _tririsList.
     */
    private java.util.Vector<fr.irisa.lis.portalis.service.patrimoine.arles.db.Triris> _tririsList;

    /**
     * Field _irisList.
     */
    private java.util.Vector<fr.irisa.lis.portalis.service.patrimoine.arles.db.Iris> _irisList;

    /**
     * Field _circonscriptionEuropeenne.
     */
    private fr.irisa.lis.portalis.service.patrimoine.arles.db.CirconscriptionEuropeenne _circonscriptionEuropeenne;

    /**
     * Field _region.
     */
    private fr.irisa.lis.portalis.service.patrimoine.arles.db.Region _region;

    /**
     * Field _departement.
     */
    private fr.irisa.lis.portalis.service.patrimoine.arles.db.Departement _departement;

    /**
     * Field _patrimoineList.
     */
    private java.util.Vector<fr.irisa.lis.portalis.service.patrimoine.arles.db.Patrimoine> _patrimoineList;


      //----------------/
     //- Constructors -/
    //----------------/

    public VilleType() {
        super();
        this._tririsList = new java.util.Vector<fr.irisa.lis.portalis.service.patrimoine.arles.db.Triris>();
        this._irisList = new java.util.Vector<fr.irisa.lis.portalis.service.patrimoine.arles.db.Iris>();
        this._patrimoineList = new java.util.Vector<fr.irisa.lis.portalis.service.patrimoine.arles.db.Patrimoine>();
    }


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * 
     * 
     * @param vIris
     * @throws java.lang.IndexOutOfBoundsException if the index
     * given is outside the bounds of the collection
     */
    public void addIris(
            final fr.irisa.lis.portalis.service.patrimoine.arles.db.Iris vIris)
    throws java.lang.IndexOutOfBoundsException {
        this._irisList.addElement(vIris);
    }

    /**
     * 
     * 
     * @param index
     * @param vIris
     * @throws java.lang.IndexOutOfBoundsException if the index
     * given is outside the bounds of the collection
     */
    public void addIris(
            final int index,
            final fr.irisa.lis.portalis.service.patrimoine.arles.db.Iris vIris)
    throws java.lang.IndexOutOfBoundsException {
        this._irisList.add(index, vIris);
    }

    /**
     * 
     * 
     * @param vPatrimoine
     * @throws java.lang.IndexOutOfBoundsException if the index
     * given is outside the bounds of the collection
     */
    public void addPatrimoine(
            final fr.irisa.lis.portalis.service.patrimoine.arles.db.Patrimoine vPatrimoine)
    throws java.lang.IndexOutOfBoundsException {
        this._patrimoineList.addElement(vPatrimoine);
    }

    /**
     * 
     * 
     * @param index
     * @param vPatrimoine
     * @throws java.lang.IndexOutOfBoundsException if the index
     * given is outside the bounds of the collection
     */
    public void addPatrimoine(
            final int index,
            final fr.irisa.lis.portalis.service.patrimoine.arles.db.Patrimoine vPatrimoine)
    throws java.lang.IndexOutOfBoundsException {
        this._patrimoineList.add(index, vPatrimoine);
    }

    /**
     * 
     * 
     * @param vTriris
     * @throws java.lang.IndexOutOfBoundsException if the index
     * given is outside the bounds of the collection
     */
    public void addTriris(
            final fr.irisa.lis.portalis.service.patrimoine.arles.db.Triris vTriris)
    throws java.lang.IndexOutOfBoundsException {
        this._tririsList.addElement(vTriris);
    }

    /**
     * 
     * 
     * @param index
     * @param vTriris
     * @throws java.lang.IndexOutOfBoundsException if the index
     * given is outside the bounds of the collection
     */
    public void addTriris(
            final int index,
            final fr.irisa.lis.portalis.service.patrimoine.arles.db.Triris vTriris)
    throws java.lang.IndexOutOfBoundsException {
        this._tririsList.add(index, vTriris);
    }

    /**
     */
    public void deleteCodeInsee(
    ) {
        this._has_codeInsee= false;
    }

    /**
     */
    public void deleteCodesPostaux(
    ) {
        this._has_codesPostaux= false;
    }

    /**
     */
    public void deleteLocalId(
    ) {
        this._has_localId= false;
    }

    /**
     */
    public void deletePop1999(
    ) {
        this._has_pop1999= false;
    }

    /**
     */
    public void deletePop2010(
    ) {
        this._has_pop2010= false;
    }

    /**
     * Method enumerateIris.
     * 
     * @return an Enumeration over all
     * fr.irisa.lis.portalis.service.patrimoine.arles.Iris elements
     */
    public java.util.Enumeration<? extends fr.irisa.lis.portalis.service.patrimoine.arles.db.Iris> enumerateIris(
    ) {
        return this._irisList.elements();
    }

    /**
     * Method enumeratePatrimoine.
     * 
     * @return an Enumeration over all
     * fr.irisa.lis.portalis.service.patrimoine.arles.Patrimoine
     * elements
     */
    public java.util.Enumeration<? extends fr.irisa.lis.portalis.service.patrimoine.arles.db.Patrimoine> enumeratePatrimoine(
    ) {
        return this._patrimoineList.elements();
    }

    /**
     * Method enumerateTriris.
     * 
     * @return an Enumeration over all
     * fr.irisa.lis.portalis.service.patrimoine.arles.Triris element
     */
    public java.util.Enumeration<? extends fr.irisa.lis.portalis.service.patrimoine.arles.db.Triris> enumerateTriris(
    ) {
        return this._tririsList.elements();
    }

    /**
     * Returns the value of field 'circonscription'.
     * 
     * @return the value of field 'Circonscription'.
     */
    public java.lang.String getCirconscription(
    ) {
        return this._circonscription;
    }

    /**
     * Returns the value of field 'circonscriptionEuropeenne'.
     * 
     * @return the value of field 'CirconscriptionEuropeenne'.
     */
    public fr.irisa.lis.portalis.service.patrimoine.arles.db.CirconscriptionEuropeenne getCirconscriptionEuropeenne(
    ) {
        return this._circonscriptionEuropeenne;
    }

    /**
     * Returns the value of field 'codeInsee'.
     * 
     * @return the value of field 'CodeInsee'.
     */
    @XmlAttribute
    public short getCodeInsee(
    ) {
        return this._codeInsee;
    }

    /**
     * Returns the value of field 'codesPostaux'.
     * 
     * @return the value of field 'CodesPostaux'.
     */
    @XmlAttribute
    public short getCodesPostaux(
    ) {
        return this._codesPostaux;
    }

    /**
     * Returns the value of field 'departement'.
     * 
     * @return the value of field 'Departement'.
     */
    public fr.irisa.lis.portalis.service.patrimoine.arles.db.Departement getDepartement(
    ) {
        return this._departement;
    }

    /**
     * Method getIris.
     * 
     * @param index
     * @throws java.lang.IndexOutOfBoundsException if the index
     * given is outside the bounds of the collection
     * @return the value of the
     * fr.irisa.lis.portalis.service.patrimoine.arles.Iris at the
     * given index
     */
    public fr.irisa.lis.portalis.service.patrimoine.arles.db.Iris getIris(
            final int index)
    throws java.lang.IndexOutOfBoundsException {
        // check bounds for index
        if (index < 0 || index >= this._irisList.size()) {
            throw new IndexOutOfBoundsException("getIris: Index value '" + index + "' not in range [0.." + (this._irisList.size() - 1) + "]");
        }

        return (fr.irisa.lis.portalis.service.patrimoine.arles.db.Iris) _irisList.get(index);
    }

    /**
     * Method getIris.Returns the contents of the collection in an
     * Array.  <p>Note:  Just in case the collection contents are
     * changing in another thread, we pass a 0-length Array of the
     * correct type into the API call.  This way we <i>know</i>
     * that the Array returned is of exactly the correct length.
     * 
     * @return this collection as an Array
     */
    public fr.irisa.lis.portalis.service.patrimoine.arles.db.Iris[] getIris(
    ) {
        fr.irisa.lis.portalis.service.patrimoine.arles.db.Iris[] array = new fr.irisa.lis.portalis.service.patrimoine.arles.db.Iris[0];
        return (fr.irisa.lis.portalis.service.patrimoine.arles.db.Iris[]) this._irisList.toArray(array);
    }

    /**
     * Method getIrisCount.
     * 
     * @return the size of this collection
     */
    public int getIrisCount(
    ) {
        return this._irisList.size();
    }

    /**
     * Returns the value of field 'latitude'.
     * 
     * @return the value of field 'Latitude'.
     */
    @XmlAttribute
    public java.lang.String getLatitude(
    ) {
        return this._latitude;
    }

    /**
     * Returns the value of field 'localId'.
     * 
     * @return the value of field 'LocalId'.
     */
    @XmlAttribute
    public byte getLocalId(
    ) {
        return this._localId;
    }

    /**
     * Returns the value of field 'longitude'.
     * 
     * @return the value of field 'Longitude'.
     */
    @XmlAttribute
    public java.lang.String getLongitude(
    ) {
        return this._longitude;
    }

    /**
     * Returns the value of field 'nom'.
     * 
     * @return the value of field 'Nom'.
     */
    @XmlAttribute
    public java.lang.String getNom(
    ) {
        return this._nom;
    }

    /**
     * Method getPatrimoine.
     * 
     * @param index
     * @throws java.lang.IndexOutOfBoundsException if the index
     * given is outside the bounds of the collection
     * @return the value of the
     * fr.irisa.lis.portalis.service.patrimoine.arles.Patrimoine at
     * the given index
     */
    public fr.irisa.lis.portalis.service.patrimoine.arles.db.Patrimoine getPatrimoine(
            final int index)
    throws java.lang.IndexOutOfBoundsException {
        // check bounds for index
        if (index < 0 || index >= this._patrimoineList.size()) {
            throw new IndexOutOfBoundsException("getPatrimoine: Index value '" + index + "' not in range [0.." + (this._patrimoineList.size() - 1) + "]");
        }

        return (fr.irisa.lis.portalis.service.patrimoine.arles.db.Patrimoine) _patrimoineList.get(index);
    }

    /**
     * Method getPatrimoine.Returns the contents of the collection
     * in an Array.  <p>Note:  Just in case the collection contents
     * are changing in another thread, we pass a 0-length Array of
     * the correct type into the API call.  This way we <i>know</i>
     * that the Array returned is of exactly the correct length.
     * 
     * @return this collection as an Array
     */
    public fr.irisa.lis.portalis.service.patrimoine.arles.db.Patrimoine[] getPatrimoine(
    ) {
        fr.irisa.lis.portalis.service.patrimoine.arles.db.Patrimoine[] array = new fr.irisa.lis.portalis.service.patrimoine.arles.db.Patrimoine[0];
        return (fr.irisa.lis.portalis.service.patrimoine.arles.db.Patrimoine[]) this._patrimoineList.toArray(array);
    }

    /**
     * Method getPatrimoineCount.
     * 
     * @return the size of this collection
     */
    public int getPatrimoineCount(
    ) {
        return this._patrimoineList.size();
    }

    /**
     * Returns the value of field 'pop1999'.
     * 
     * @return the value of field 'Pop1999'.
     */
    @XmlAttribute
    public int getPop1999(
    ) {
        return this._pop1999;
    }

    /**
     * Returns the value of field 'pop2010'.
     * 
     * @return the value of field 'Pop2010'.
     */
    @XmlAttribute
    public int getPop2010(
    ) {
        return this._pop2010;
    }

    /**
     * Returns the value of field 'region'.
     * 
     * @return the value of field 'Region'.
     */
    public fr.irisa.lis.portalis.service.patrimoine.arles.db.Region getRegion(
    ) {
        return this._region;
    }

    /**
     * Returns the value of field 'site'.
     * 
     * @return the value of field 'Site'.
     */
    @XmlAttribute
    public java.lang.String getSite(
    ) {
        return this._site;
    }

    /**
     * Method getTriris.
     * 
     * @param index
     * @throws java.lang.IndexOutOfBoundsException if the index
     * given is outside the bounds of the collection
     * @return the value of the
     * fr.irisa.lis.portalis.service.patrimoine.arles.Triris at the
     * given index
     */
    public fr.irisa.lis.portalis.service.patrimoine.arles.db.Triris getTriris(
            final int index)
    throws java.lang.IndexOutOfBoundsException {
        // check bounds for index
        if (index < 0 || index >= this._tririsList.size()) {
            throw new IndexOutOfBoundsException("getTriris: Index value '" + index + "' not in range [0.." + (this._tririsList.size() - 1) + "]");
        }

        return (fr.irisa.lis.portalis.service.patrimoine.arles.db.Triris) _tririsList.get(index);
    }

    /**
     * Method getTriris.Returns the contents of the collection in
     * an Array.  <p>Note:  Just in case the collection contents
     * are changing in another thread, we pass a 0-length Array of
     * the correct type into the API call.  This way we <i>know</i>
     * that the Array returned is of exactly the correct length.
     * 
     * @return this collection as an Array
     */
    public fr.irisa.lis.portalis.service.patrimoine.arles.db.Triris[] getTriris(
    ) {
        fr.irisa.lis.portalis.service.patrimoine.arles.db.Triris[] array = new fr.irisa.lis.portalis.service.patrimoine.arles.db.Triris[0];
        return (fr.irisa.lis.portalis.service.patrimoine.arles.db.Triris[]) this._tririsList.toArray(array);
    }

    /**
     * Method getTririsCount.
     * 
     * @return the size of this collection
     */
    public int getTririsCount(
    ) {
        return this._tririsList.size();
    }

    /**
     * Method hasCodeInsee.
     * 
     * @return true if at least one CodeInsee has been added
     */
    public boolean hasCodeInsee(
    ) {
        return this._has_codeInsee;
    }

    /**
     * Method hasCodesPostaux.
     * 
     * @return true if at least one CodesPostaux has been added
     */
    public boolean hasCodesPostaux(
    ) {
        return this._has_codesPostaux;
    }

    /**
     * Method hasLocalId.
     * 
     * @return true if at least one LocalId has been added
     */
    public boolean hasLocalId(
    ) {
        return this._has_localId;
    }

    /**
     * Method hasPop1999.
     * 
     * @return true if at least one Pop1999 has been added
     */
    public boolean hasPop1999(
    ) {
        return this._has_pop1999;
    }

    /**
     * Method hasPop2010.
     * 
     * @return true if at least one Pop2010 has been added
     */
    public boolean hasPop2010(
    ) {
        return this._has_pop2010;
    }

    /**
     */
    public void removeAllIris(
    ) {
        this._irisList.clear();
    }

    /**
     */
    public void removeAllPatrimoine(
    ) {
        this._patrimoineList.clear();
    }

    /**
     */
    public void removeAllTriris(
    ) {
        this._tririsList.clear();
    }

    /**
     * Method removeIris.
     * 
     * @param vIris
     * @return true if the object was removed from the collection.
     */
    public boolean removeIris(
            final fr.irisa.lis.portalis.service.patrimoine.arles.db.Iris vIris) {
        boolean removed = _irisList.remove(vIris);
        return removed;
    }

    /**
     * Method removeIrisAt.
     * 
     * @param index
     * @return the element removed from the collection
     */
    public fr.irisa.lis.portalis.service.patrimoine.arles.db.Iris removeIrisAt(
            final int index) {
        java.lang.Object obj = this._irisList.remove(index);
        return (fr.irisa.lis.portalis.service.patrimoine.arles.db.Iris) obj;
    }

    /**
     * Method removePatrimoine.
     * 
     * @param vPatrimoine
     * @return true if the object was removed from the collection.
     */
    public boolean removePatrimoine(
            final fr.irisa.lis.portalis.service.patrimoine.arles.db.Patrimoine vPatrimoine) {
        boolean removed = _patrimoineList.remove(vPatrimoine);
        return removed;
    }

    /**
     * Method removePatrimoineAt.
     * 
     * @param index
     * @return the element removed from the collection
     */
    public fr.irisa.lis.portalis.service.patrimoine.arles.db.Patrimoine removePatrimoineAt(
            final int index) {
        java.lang.Object obj = this._patrimoineList.remove(index);
        return (fr.irisa.lis.portalis.service.patrimoine.arles.db.Patrimoine) obj;
    }

    /**
     * Method removeTriris.
     * 
     * @param vTriris
     * @return true if the object was removed from the collection.
     */
    public boolean removeTriris(
            final fr.irisa.lis.portalis.service.patrimoine.arles.db.Triris vTriris) {
        boolean removed = _tririsList.remove(vTriris);
        return removed;
    }

    /**
     * Method removeTririsAt.
     * 
     * @param index
     * @return the element removed from the collection
     */
    public fr.irisa.lis.portalis.service.patrimoine.arles.db.Triris removeTririsAt(
            final int index) {
        java.lang.Object obj = this._tririsList.remove(index);
        return (fr.irisa.lis.portalis.service.patrimoine.arles.db.Triris) obj;
    }

    /**
     * Sets the value of field 'circonscription'.
     * 
     * @param circonscription the value of field 'circonscription'.
     */
    public void setCirconscription(
            final java.lang.String circonscription) {
        this._circonscription = circonscription;
    }

    /**
     * Sets the value of field 'circonscriptionEuropeenne'.
     * 
     * @param circonscriptionEuropeenne the value of field
     * 'circonscriptionEuropeenne'.
     */
    public void setCirconscriptionEuropeenne(
            final fr.irisa.lis.portalis.service.patrimoine.arles.db.CirconscriptionEuropeenne circonscriptionEuropeenne) {
        this._circonscriptionEuropeenne = circonscriptionEuropeenne;
    }

    /**
     * Sets the value of field 'codeInsee'.
     * 
     * @param codeInsee the value of field 'codeInsee'.
     */
    public void setCodeInsee(
            final short codeInsee) {
        this._codeInsee = codeInsee;
        this._has_codeInsee = true;
    }

    /**
     * Sets the value of field 'codesPostaux'.
     * 
     * @param codesPostaux the value of field 'codesPostaux'.
     */
    public void setCodesPostaux(
            final short codesPostaux) {
        this._codesPostaux = codesPostaux;
        this._has_codesPostaux = true;
    }

    /**
     * Sets the value of field 'departement'.
     * 
     * @param departement the value of field 'departement'.
     */
    public void setDepartement(
            final fr.irisa.lis.portalis.service.patrimoine.arles.db.Departement departement) {
        this._departement = departement;
    }

    /**
     * 
     * 
     * @param index
     * @param vIris
     * @throws java.lang.IndexOutOfBoundsException if the index
     * given is outside the bounds of the collection
     */
    public void setIris(
            final int index,
            final fr.irisa.lis.portalis.service.patrimoine.arles.db.Iris vIris)
    throws java.lang.IndexOutOfBoundsException {
        // check bounds for index
        if (index < 0 || index >= this._irisList.size()) {
            throw new IndexOutOfBoundsException("setIris: Index value '" + index + "' not in range [0.." + (this._irisList.size() - 1) + "]");
        }

        this._irisList.set(index, vIris);
    }

    /**
     * 
     * 
     * @param vIrisArray
     */
    public void setIris(
            final fr.irisa.lis.portalis.service.patrimoine.arles.db.Iris[] vIrisArray) {
        //-- copy array
        _irisList.clear();

        for (int i = 0; i < vIrisArray.length; i++) {
                this._irisList.add(vIrisArray[i]);
        }
    }

    /**
     * Sets the value of field 'latitude'.
     * 
     * @param latitude the value of field 'latitude'.
     */
    public void setLatitude(
            final java.lang.String latitude) {
        this._latitude = latitude;
    }

    /**
     * Sets the value of field 'localId'.
     * 
     * @param localId the value of field 'localId'.
     */
    public void setLocalId(
            final byte localId) {
        this._localId = localId;
        this._has_localId = true;
    }

    /**
     * Sets the value of field 'longitude'.
     * 
     * @param longitude the value of field 'longitude'.
     */
    public void setLongitude(
            final java.lang.String longitude) {
        this._longitude = longitude;
    }

    /**
     * Sets the value of field 'nom'.
     * 
     * @param nom the value of field 'nom'.
     */
    public void setNom(
            final java.lang.String nom) {
        this._nom = nom;
    }

    /**
     * 
     * 
     * @param index
     * @param vPatrimoine
     * @throws java.lang.IndexOutOfBoundsException if the index
     * given is outside the bounds of the collection
     */
    public void setPatrimoine(
            final int index,
            final fr.irisa.lis.portalis.service.patrimoine.arles.db.Patrimoine vPatrimoine)
    throws java.lang.IndexOutOfBoundsException {
        // check bounds for index
        if (index < 0 || index >= this._patrimoineList.size()) {
            throw new IndexOutOfBoundsException("setPatrimoine: Index value '" + index + "' not in range [0.." + (this._patrimoineList.size() - 1) + "]");
        }

        this._patrimoineList.set(index, vPatrimoine);
    }

    /**
     * 
     * 
     * @param vPatrimoineArray
     */
    public void setPatrimoine(
            final fr.irisa.lis.portalis.service.patrimoine.arles.db.Patrimoine[] vPatrimoineArray) {
        //-- copy array
        _patrimoineList.clear();

        for (int i = 0; i < vPatrimoineArray.length; i++) {
                this._patrimoineList.add(vPatrimoineArray[i]);
        }
    }

    /**
     * Sets the value of field 'pop1999'.
     * 
     * @param pop1999 the value of field 'pop1999'.
     */
    public void setPop1999(
            final int pop1999) {
        this._pop1999 = pop1999;
        this._has_pop1999 = true;
    }

    /**
     * Sets the value of field 'pop2010'.
     * 
     * @param pop2010 the value of field 'pop2010'.
     */
    public void setPop2010(
            final int pop2010) {
        this._pop2010 = pop2010;
        this._has_pop2010 = true;
    }

    /**
     * Sets the value of field 'region'.
     * 
     * @param region the value of field 'region'.
     */
    public void setRegion(
            final fr.irisa.lis.portalis.service.patrimoine.arles.db.Region region) {
        this._region = region;
    }

    /**
     * Sets the value of field 'site'.
     * 
     * @param site the value of field 'site'.
     */
    public void setSite(
            final java.lang.String site) {
        this._site = site;
    }

    /**
     * 
     * 
     * @param index
     * @param vTriris
     * @throws java.lang.IndexOutOfBoundsException if the index
     * given is outside the bounds of the collection
     */
    public void setTriris(
            final int index,
            final fr.irisa.lis.portalis.service.patrimoine.arles.db.Triris vTriris)
    throws java.lang.IndexOutOfBoundsException {
        // check bounds for index
        if (index < 0 || index >= this._tririsList.size()) {
            throw new IndexOutOfBoundsException("setTriris: Index value '" + index + "' not in range [0.." + (this._tririsList.size() - 1) + "]");
        }

        this._tririsList.set(index, vTriris);
    }

    /**
     * 
     * 
     * @param vTririsArray
     */
    public void setTriris(
            final fr.irisa.lis.portalis.service.patrimoine.arles.db.Triris[] vTririsArray) {
        //-- copy array
        _tririsList.clear();

        for (int i = 0; i < vTririsArray.length; i++) {
                this._tririsList.add(vTririsArray[i]);
        }
    }

}

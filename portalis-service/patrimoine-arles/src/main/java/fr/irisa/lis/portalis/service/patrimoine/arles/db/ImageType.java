/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.3.1</a>, using an XML
 * Schema.
 * $Id$
 */

package fr.irisa.lis.portalis.service.patrimoine.arles.db;

import javax.xml.bind.annotation.XmlAttribute;

/**
 * Class ImageType.
 * 
 * @version $Revision$ $Date$
 */
@SuppressWarnings("serial")
public class ImageType implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * internal content storage
     */
    private java.lang.String _content = "";

    /**
     * Field _src.
     */
    private java.lang.String _src;


      //----------------/
     //- Constructors -/
    //----------------/

    public ImageType() {
        super();
        setContent("");
    }

    public ImageType(final java.lang.String defaultValue) {
        try {
            setContent( new java.lang.String(defaultValue));
         } catch(Exception e) {
            throw new RuntimeException("Unable to cast default value for simple content!");
         } 
    }


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Returns the value of field 'content'. The field 'content'
     * has the following description: internal content storage
     * 
     * @return the value of field 'Content'.
     */
    public java.lang.String getContent(
    ) {
        return this._content;
    }

    /**
     * Returns the value of field 'src'.
     * 
     * @return the value of field 'Src'.
     */
    @XmlAttribute
    public java.lang.String getSrc(
    ) {
        return this._src;
    }

    /**
     * Sets the value of field 'content'. The field 'content' has
     * the following description: internal content storage
     * 
     * @param content the value of field 'content'.
     */
    public void setContent(
            final java.lang.String content) {
        this._content = content;
    }

    /**
     * Sets the value of field 'src'.
     * 
     * @param src the value of field 'src'.
     */
    public void setSrc(
            final java.lang.String src) {
        this._src = src;
    }

}

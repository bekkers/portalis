/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.3.1</a>, using an XML
 * Schema.
 * $Id$
 */

package fr.irisa.lis.portalis.service.patrimoine.arles;

/**
 * Class Ville.
 * 
 * @version $Revision$ $Date$
 */
@SuppressWarnings("serial")
public class Ville extends VilleLight implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _patrimoineList.
     */
    private java.util.Vector<fr.irisa.lis.portalis.service.patrimoine.arles.Patrimoine> _patrimoineList;


      //----------------/
     //- Constructors -/
    //----------------/

    public Ville() {
        super();
        this._patrimoineList = new java.util.Vector<fr.irisa.lis.portalis.service.patrimoine.arles.Patrimoine>();
    }


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * 
     * 
     * @param vPatrimoine
     * @throws java.lang.IndexOutOfBoundsException if the index
     * given is outside the bounds of the collection
     */
    public void addPatrimoine(
            final fr.irisa.lis.portalis.service.patrimoine.arles.Patrimoine vPatrimoine)
    throws java.lang.IndexOutOfBoundsException {
        this._patrimoineList.addElement(vPatrimoine);
    }

    /**
     * 
     * 
     * @param index
     * @param vPatrimoine
     * @throws java.lang.IndexOutOfBoundsException if the index
     * given is outside the bounds of the collection
     */
    public void addPatrimoine(
            final int index,
            final fr.irisa.lis.portalis.service.patrimoine.arles.Patrimoine vPatrimoine)
    throws java.lang.IndexOutOfBoundsException {
        this._patrimoineList.add(index, vPatrimoine);
    }

    /**
     * Method enumeratePatrimoine.
     * 
     * @return an Enumeration over all
     * fr.irisa.lis.portalis.service.patrimoine.arles.Patrimoine
     * elements
     */
    public java.util.Enumeration<? extends fr.irisa.lis.portalis.service.patrimoine.arles.Patrimoine> enumeratePatrimoine(
    ) {
        return this._patrimoineList.elements();
    }

    /**
     * Method getPatrimoine.
     * 
     * @param index
     * @throws java.lang.IndexOutOfBoundsException if the index
     * given is outside the bounds of the collection
     * @return the value of the
     * fr.irisa.lis.portalis.service.patrimoine.arles.Patrimoine at
     * the given index
     */
    public fr.irisa.lis.portalis.service.patrimoine.arles.Patrimoine getPatrimoine(
            final int index)
    throws java.lang.IndexOutOfBoundsException {
        // check bounds for index
        if (index < 0 || index >= this._patrimoineList.size()) {
            throw new IndexOutOfBoundsException("getPatrimoine: Index value '" + index + "' not in range [0.." + (this._patrimoineList.size() - 1) + "]");
        }

        return (fr.irisa.lis.portalis.service.patrimoine.arles.Patrimoine) _patrimoineList.get(index);
    }

    /**
     * Method getPatrimoine.Returns the contents of the collection
     * in an Array.  <p>Note:  Just in case the collection contents
     * are changing in another thread, we pass a 0-length Array of
     * the correct type into the API call.  This way we <i>know</i>
     * that the Array returned is of exactly the correct length.
     * 
     * @return this collection as an Array
     */
    public fr.irisa.lis.portalis.service.patrimoine.arles.Patrimoine[] getPatrimoine(
    ) {
        fr.irisa.lis.portalis.service.patrimoine.arles.Patrimoine[] array = new fr.irisa.lis.portalis.service.patrimoine.arles.Patrimoine[0];
        return (fr.irisa.lis.portalis.service.patrimoine.arles.Patrimoine[]) this._patrimoineList.toArray(array);
    }

    /**
     * Method getPatrimoineCount.
     * 
     * @return the size of this collection
     */
    public int getPatrimoineCount(
    ) {
        return this._patrimoineList.size();
    }

    /**
     */
    public void removeAllPatrimoine(
    ) {
        this._patrimoineList.clear();
    }

    /**
     * Method removePatrimoine.
     * 
     * @param vPatrimoine
     * @return true if the object was removed from the collection.
     */
    public boolean removePatrimoine(
            final fr.irisa.lis.portalis.service.patrimoine.arles.Patrimoine vPatrimoine) {
        boolean removed = _patrimoineList.remove(vPatrimoine);
        return removed;
    }

    /**
     * Method removePatrimoineAt.
     * 
     * @param index
     * @return the element removed from the collection
     */
    public fr.irisa.lis.portalis.service.patrimoine.arles.Patrimoine removePatrimoineAt(
            final int index) {
        java.lang.Object obj = this._patrimoineList.remove(index);
        return (fr.irisa.lis.portalis.service.patrimoine.arles.Patrimoine) obj;
    }

    /**
     * 
     * 
     * @param index
     * @param vPatrimoine
     * @throws java.lang.IndexOutOfBoundsException if the index
     * given is outside the bounds of the collection
     */
    public void setPatrimoine(
            final int index,
            final fr.irisa.lis.portalis.service.patrimoine.arles.Patrimoine vPatrimoine)
    throws java.lang.IndexOutOfBoundsException {
        // check bounds for index
        if (index < 0 || index >= this._patrimoineList.size()) {
            throw new IndexOutOfBoundsException("setPatrimoine: Index value '" + index + "' not in range [0.." + (this._patrimoineList.size() - 1) + "]");
        }

        this._patrimoineList.set(index, vPatrimoine);
    }

    /**
     * 
     * 
     * @param vPatrimoineArray
     */
    public void setPatrimoine(
            final fr.irisa.lis.portalis.service.patrimoine.arles.Patrimoine[] vPatrimoineArray) {
        //-- copy array
        _patrimoineList.clear();

        for (int i = 0; i < vPatrimoineArray.length; i++) {
                this._patrimoineList.add(vPatrimoineArray[i]);
        }
    }

}

/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.3.1</a>, using an XML
 * Schema.
 * $Id$
 */

package fr.irisa.lis.portalis.service.patrimoine.arles.db;

import javax.xml.bind.annotation.XmlAttribute;

/**
 * Class IrisType.
 * 
 * @version $Revision$ $Date$
 */
@SuppressWarnings("serial")
public class IrisType implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * internal content storage
     */
    private java.lang.String _content = "";

    /**
     * Field _code.
     */
    private int _code;

    /**
     * keeps track of state for field: _code
     */
    private boolean _has_code;

    /**
     * Field _nom.
     */
    private java.lang.String _nom;

    /**
     * Field _pop2010.
     */
    private short _pop2010;

    /**
     * keeps track of state for field: _pop2010
     */
    private boolean _has_pop2010;

    /**
     * Field _type.
     */
    private java.lang.String _type;


      //----------------/
     //- Constructors -/
    //----------------/

    public IrisType() {
        super();
        setContent("");
    }

    public IrisType(final java.lang.String defaultValue) {
        try {
            setContent( new java.lang.String(defaultValue));
         } catch(Exception e) {
            throw new RuntimeException("Unable to cast default value for simple content!");
         } 
    }


      //-----------/
     //- Methods -/
    //-----------/

    /**
     */
    public void deleteCode(
    ) {
        this._has_code= false;
    }

    /**
     */
    public void deletePop2010(
    ) {
        this._has_pop2010= false;
    }

    /**
     * Returns the value of field 'code'.
     * 
     * @return the value of field 'Code'.
     */
    @XmlAttribute
    public int getCode(
    ) {
        return this._code;
    }

    /**
     * Returns the value of field 'content'. The field 'content'
     * has the following description: internal content storage
     * 
     * @return the value of field 'Content'.
     */
    public java.lang.String getContent(
    ) {
        return this._content;
    }

    /**
     * Returns the value of field 'nom'.
     * 
     * @return the value of field 'Nom'.
     */
    @XmlAttribute
    public java.lang.String getNom(
    ) {
        return this._nom;
    }

    /**
     * Returns the value of field 'pop2010'.
     * 
     * @return the value of field 'Pop2010'.
     */
    @XmlAttribute
    public short getPop2010(
    ) {
        return this._pop2010;
    }

    /**
     * Returns the value of field 'type'.
     * 
     * @return the value of field 'Type'.
     */
    @XmlAttribute
    public java.lang.String getType(
    ) {
        return this._type;
    }

    /**
     * Method hasCode.
     * 
     * @return true if at least one Code has been added
     */
    public boolean hasCode(
    ) {
        return this._has_code;
    }

    /**
     * Method hasPop2010.
     * 
     * @return true if at least one Pop2010 has been added
     */
    public boolean hasPop2010(
    ) {
        return this._has_pop2010;
    }

    /**
     * Sets the value of field 'code'.
     * 
     * @param code the value of field 'code'.
     */
    public void setCode(
            final int code) {
        this._code = code;
        this._has_code = true;
    }

    /**
     * Sets the value of field 'content'. The field 'content' has
     * the following description: internal content storage
     * 
     * @param content the value of field 'content'.
     */
    public void setContent(
            final java.lang.String content) {
        this._content = content;
    }

    /**
     * Sets the value of field 'nom'.
     * 
     * @param nom the value of field 'nom'.
     */
    public void setNom(
            final java.lang.String nom) {
        this._nom = nom;
    }

    /**
     * Sets the value of field 'pop2010'.
     * 
     * @param pop2010 the value of field 'pop2010'.
     */
    public void setPop2010(
            final short pop2010) {
        this._pop2010 = pop2010;
        this._has_pop2010 = true;
    }

    /**
     * Sets the value of field 'type'.
     * 
     * @param type the value of field 'type'.
     */
    public void setType(
            final java.lang.String type) {
        this._type = type;
    }

}

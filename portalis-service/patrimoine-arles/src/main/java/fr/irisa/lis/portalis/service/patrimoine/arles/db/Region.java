/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.3.1</a>, using an XML
 * Schema.
 * $Id$
 */

package fr.irisa.lis.portalis.service.patrimoine.arles.db;

/**
 * Class Region.
 * 
 * @version $Revision$ $Date$
 */
@SuppressWarnings("serial")
public class Region extends RegionType 
implements java.io.Serializable
{


      //----------------/
     //- Constructors -/
    //----------------/

    public Region() {
        super();
    }

    public Region(final java.lang.String defaultValue) {
        super(defaultValue);
    }

}

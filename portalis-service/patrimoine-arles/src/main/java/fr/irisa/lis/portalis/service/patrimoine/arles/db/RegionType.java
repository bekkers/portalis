/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.3.1</a>, using an XML
 * Schema.
 * $Id$
 */

package fr.irisa.lis.portalis.service.patrimoine.arles.db;

import javax.xml.bind.annotation.XmlAttribute;

/**
 * Class RegionType.
 * 
 * @version $Revision$ $Date$
 */
@SuppressWarnings("serial")
public class RegionType implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * internal content storage
     */
    private java.lang.String _content = "";

    /**
     * Field _chefLieu.
     */
    private java.lang.String _chefLieu;

    /**
     * Field _code.
     */
    private byte _code;

    /**
     * keeps track of state for field: _code
     */
    private boolean _has_code;

    /**
     * Field _nom.
     */
    private java.lang.String _nom;


      //----------------/
     //- Constructors -/
    //----------------/

    public RegionType() {
        super();
        setContent("");
    }

    public RegionType(final java.lang.String defaultValue) {
        try {
            setContent( new java.lang.String(defaultValue));
         } catch(Exception e) {
            throw new RuntimeException("Unable to cast default value for simple content!");
         } 
    }


      //-----------/
     //- Methods -/
    //-----------/

    /**
     */
    public void deleteCode(
    ) {
        this._has_code= false;
    }

    /**
     * Returns the value of field 'chefLieu'.
     * 
     * @return the value of field 'ChefLieu'.
     */
    @XmlAttribute
    public java.lang.String getChefLieu(
    ) {
        return this._chefLieu;
    }

    /**
     * Returns the value of field 'code'.
     * 
     * @return the value of field 'Code'.
     */
    @XmlAttribute
    public byte getCode(
    ) {
        return this._code;
    }

    /**
     * Returns the value of field 'content'. The field 'content'
     * has the following description: internal content storage
     * 
     * @return the value of field 'Content'.
     */
    public java.lang.String getContent(
    ) {
        return this._content;
    }

    /**
     * Returns the value of field 'nom'.
     * 
     * @return the value of field 'Nom'.
     */
    @XmlAttribute
    public java.lang.String getNom(
    ) {
        return this._nom;
    }

    /**
     * Method hasCode.
     * 
     * @return true if at least one Code has been added
     */
    public boolean hasCode(
    ) {
        return this._has_code;
    }

    /**
     * Sets the value of field 'chefLieu'.
     * 
     * @param chefLieu the value of field 'chefLieu'.
     */
    public void setChefLieu(
            final java.lang.String chefLieu) {
        this._chefLieu = chefLieu;
    }

    /**
     * Sets the value of field 'code'.
     * 
     * @param code the value of field 'code'.
     */
    public void setCode(
            final byte code) {
        this._code = code;
        this._has_code = true;
    }

    /**
     * Sets the value of field 'content'. The field 'content' has
     * the following description: internal content storage
     * 
     * @param content the value of field 'content'.
     */
    public void setContent(
            final java.lang.String content) {
        this._content = content;
    }

    /**
     * Sets the value of field 'nom'.
     * 
     * @param nom the value of field 'nom'.
     */
    public void setNom(
            final java.lang.String nom) {
        this._nom = nom;
    }

}

/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.3.1</a>, using an XML
 * Schema.
 * $Id$
 */

package fr.irisa.lis.portalis.service.patrimoine.arles.db;

import javax.xml.bind.annotation.XmlAttribute;

/**
 * Class TririsType.
 * 
 * @version $Revision$ $Date$
 */
@SuppressWarnings("serial")
public class TririsType implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _code.
     */
    private int _code;

    /**
     * keeps track of state for field: _code
     */
    private boolean _has_code;

    /**
     * Field _nom.
     */
    private java.lang.String _nom;

    /**
     * Field _pop2010.
     */
    private short _pop2010;

    /**
     * keeps track of state for field: _pop2010
     */
    private boolean _has_pop2010;

    /**
     * Field _irisList.
     */
    private java.util.Vector<fr.irisa.lis.portalis.service.patrimoine.arles.db.Iris> _irisList;


      //----------------/
     //- Constructors -/
    //----------------/

    public TririsType() {
        super();
        this._irisList = new java.util.Vector<fr.irisa.lis.portalis.service.patrimoine.arles.db.Iris>();
    }


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * 
     * 
     * @param vIris
     * @throws java.lang.IndexOutOfBoundsException if the index
     * given is outside the bounds of the collection
     */
    public void addIris(
            final fr.irisa.lis.portalis.service.patrimoine.arles.db.Iris vIris)
    throws java.lang.IndexOutOfBoundsException {
        this._irisList.addElement(vIris);
    }

    /**
     * 
     * 
     * @param index
     * @param vIris
     * @throws java.lang.IndexOutOfBoundsException if the index
     * given is outside the bounds of the collection
     */
    public void addIris(
            final int index,
            final fr.irisa.lis.portalis.service.patrimoine.arles.db.Iris vIris)
    throws java.lang.IndexOutOfBoundsException {
        this._irisList.add(index, vIris);
    }

    /**
     */
    public void deleteCode(
    ) {
        this._has_code= false;
    }

    /**
     */
    public void deletePop2010(
    ) {
        this._has_pop2010= false;
    }

    /**
     * Method enumerateIris.
     * 
     * @return an Enumeration over all
     * fr.irisa.lis.portalis.service.patrimoine.arles.Iris elements
     */
    public java.util.Enumeration<? extends fr.irisa.lis.portalis.service.patrimoine.arles.db.Iris> enumerateIris(
    ) {
        return this._irisList.elements();
    }

    /**
     * Returns the value of field 'code'.
     * 
     * @return the value of field 'Code'.
     */
    @XmlAttribute
    public int getCode(
    ) {
        return this._code;
    }

    /**
     * Method getIris.
     * 
     * @param index
     * @throws java.lang.IndexOutOfBoundsException if the index
     * given is outside the bounds of the collection
     * @return the value of the
     * fr.irisa.lis.portalis.service.patrimoine.arles.Iris at the
     * given index
     */
    public fr.irisa.lis.portalis.service.patrimoine.arles.db.Iris getIris(
            final int index)
    throws java.lang.IndexOutOfBoundsException {
        // check bounds for index
        if (index < 0 || index >= this._irisList.size()) {
            throw new IndexOutOfBoundsException("getIris: Index value '" + index + "' not in range [0.." + (this._irisList.size() - 1) + "]");
        }

        return (fr.irisa.lis.portalis.service.patrimoine.arles.db.Iris) _irisList.get(index);
    }

    /**
     * Method getIris.Returns the contents of the collection in an
     * Array.  <p>Note:  Just in case the collection contents are
     * changing in another thread, we pass a 0-length Array of the
     * correct type into the API call.  This way we <i>know</i>
     * that the Array returned is of exactly the correct length.
     * 
     * @return this collection as an Array
     */
    public fr.irisa.lis.portalis.service.patrimoine.arles.db.Iris[] getIris(
    ) {
        fr.irisa.lis.portalis.service.patrimoine.arles.db.Iris[] array = new fr.irisa.lis.portalis.service.patrimoine.arles.db.Iris[0];
        return (fr.irisa.lis.portalis.service.patrimoine.arles.db.Iris[]) this._irisList.toArray(array);
    }

    /**
     * Method getIrisCount.
     * 
     * @return the size of this collection
     */
    public int getIrisCount(
    ) {
        return this._irisList.size();
    }

    /**
     * Returns the value of field 'nom'.
     * 
     * @return the value of field 'Nom'.
     */
    @XmlAttribute
    public java.lang.String getNom(
    ) {
        return this._nom;
    }

    /**
     * Returns the value of field 'pop2010'.
     * 
     * @return the value of field 'Pop2010'.
     */
    @XmlAttribute
    public short getPop2010(
    ) {
        return this._pop2010;
    }

    /**
     * Method hasCode.
     * 
     * @return true if at least one Code has been added
     */
    public boolean hasCode(
    ) {
        return this._has_code;
    }

    /**
     * Method hasPop2010.
     * 
     * @return true if at least one Pop2010 has been added
     */
    public boolean hasPop2010(
    ) {
        return this._has_pop2010;
    }

    /**
     */
    public void removeAllIris(
    ) {
        this._irisList.clear();
    }

    /**
     * Method removeIris.
     * 
     * @param vIris
     * @return true if the object was removed from the collection.
     */
    public boolean removeIris(
            final fr.irisa.lis.portalis.service.patrimoine.arles.db.Iris vIris) {
        boolean removed = _irisList.remove(vIris);
        return removed;
    }

    /**
     * Method removeIrisAt.
     * 
     * @param index
     * @return the element removed from the collection
     */
    public fr.irisa.lis.portalis.service.patrimoine.arles.db.Iris removeIrisAt(
            final int index) {
        java.lang.Object obj = this._irisList.remove(index);
        return (fr.irisa.lis.portalis.service.patrimoine.arles.db.Iris) obj;
    }

    /**
     * Sets the value of field 'code'.
     * 
     * @param code the value of field 'code'.
     */
    public void setCode(
            final int code) {
        this._code = code;
        this._has_code = true;
    }

    /**
     * 
     * 
     * @param index
     * @param vIris
     * @throws java.lang.IndexOutOfBoundsException if the index
     * given is outside the bounds of the collection
     */
    public void setIris(
            final int index,
            final fr.irisa.lis.portalis.service.patrimoine.arles.db.Iris vIris)
    throws java.lang.IndexOutOfBoundsException {
        // check bounds for index
        if (index < 0 || index >= this._irisList.size()) {
            throw new IndexOutOfBoundsException("setIris: Index value '" + index + "' not in range [0.." + (this._irisList.size() - 1) + "]");
        }

        this._irisList.set(index, vIris);
    }

    /**
     * 
     * 
     * @param vIrisArray
     */
    public void setIris(
            final fr.irisa.lis.portalis.service.patrimoine.arles.db.Iris[] vIrisArray) {
        //-- copy array
        _irisList.clear();

        for (int i = 0; i < vIrisArray.length; i++) {
                this._irisList.add(vIrisArray[i]);
        }
    }

    /**
     * Sets the value of field 'nom'.
     * 
     * @param nom the value of field 'nom'.
     */
    public void setNom(
            final java.lang.String nom) {
        this._nom = nom;
    }

    /**
     * Sets the value of field 'pop2010'.
     * 
     * @param pop2010 the value of field 'pop2010'.
     */
    public void setPop2010(
            final short pop2010) {
        this._pop2010 = pop2010;
        this._has_pop2010 = true;
    }

}

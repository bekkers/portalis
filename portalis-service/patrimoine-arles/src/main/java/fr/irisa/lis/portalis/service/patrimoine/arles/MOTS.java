/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.3.1</a>, using an XML
 * Schema.
 * $Id$
 */

package fr.irisa.lis.portalis.service.patrimoine.arles;

/**
 * Class MOTS.
 * 
 * @version $Revision$ $Date$
 */
@SuppressWarnings("serial")
public class MOTS implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _themesList.
     */
    private java.util.Vector<java.lang.String> _themesList;

    /**
     * Field _categoriedelarchitectureList.
     */
    private java.util.Vector<java.lang.String> _categoriedelarchitectureList;

    /**
     * Field _typedarchitectureList.
     */
    private java.util.Vector<java.lang.String> _typedarchitectureList;

    /**
     * Field _appartenanceList.
     */
    private java.util.Vector<java.lang.String> _appartenanceList;

    /**
     * Field _epoqueList.
     */
    private java.util.Vector<java.lang.String> _epoqueList;

    /**
     * Field _stylearchitecturalList.
     */
    private java.util.Vector<java.lang.String> _stylearchitecturalList;

    /**
     * Field _datationList.
     */
    private java.util.Vector<java.lang.String> _datationList;

    /**
     * Field _edificeList.
     */
    private java.util.Vector<java.lang.String> _edificeList;

    /**
     * Field _proprieteList.
     */
    private java.util.Vector<java.lang.String> _proprieteList;


      //----------------/
     //- Constructors -/
    //----------------/

    public MOTS() {
        super();
        this._themesList = new java.util.Vector<java.lang.String>();
        this._categoriedelarchitectureList = new java.util.Vector<java.lang.String>();
        this._typedarchitectureList = new java.util.Vector<java.lang.String>();
        this._appartenanceList = new java.util.Vector<java.lang.String>();
        this._epoqueList = new java.util.Vector<java.lang.String>();
        this._stylearchitecturalList = new java.util.Vector<java.lang.String>();
        this._datationList = new java.util.Vector<java.lang.String>();
        this._edificeList = new java.util.Vector<java.lang.String>();
        this._proprieteList = new java.util.Vector<java.lang.String>();
    }


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * 
     * 
     * @param vAppartenance
     * @throws java.lang.IndexOutOfBoundsException if the index
     * given is outside the bounds of the collection
     */
    public void addAppartenance(
            final java.lang.String vAppartenance)
    throws java.lang.IndexOutOfBoundsException {
        this._appartenanceList.addElement(vAppartenance);
    }

    /**
     * 
     * 
     * @param index
     * @param vAppartenance
     * @throws java.lang.IndexOutOfBoundsException if the index
     * given is outside the bounds of the collection
     */
    public void addAppartenance(
            final int index,
            final java.lang.String vAppartenance)
    throws java.lang.IndexOutOfBoundsException {
        this._appartenanceList.add(index, vAppartenance);
    }

    /**
     * 
     * 
     * @param vCategoriedelarchitecture
     * @throws java.lang.IndexOutOfBoundsException if the index
     * given is outside the bounds of the collection
     */
    public void addCategoriedelarchitecture(
            final java.lang.String vCategoriedelarchitecture)
    throws java.lang.IndexOutOfBoundsException {
        this._categoriedelarchitectureList.addElement(vCategoriedelarchitecture);
    }

    /**
     * 
     * 
     * @param index
     * @param vCategoriedelarchitecture
     * @throws java.lang.IndexOutOfBoundsException if the index
     * given is outside the bounds of the collection
     */
    public void addCategoriedelarchitecture(
            final int index,
            final java.lang.String vCategoriedelarchitecture)
    throws java.lang.IndexOutOfBoundsException {
        this._categoriedelarchitectureList.add(index, vCategoriedelarchitecture);
    }

    /**
     * 
     * 
     * @param vDatation
     * @throws java.lang.IndexOutOfBoundsException if the index
     * given is outside the bounds of the collection
     */
    public void addDatation(
            final java.lang.String vDatation)
    throws java.lang.IndexOutOfBoundsException {
        this._datationList.addElement(vDatation);
    }

    /**
     * 
     * 
     * @param index
     * @param vDatation
     * @throws java.lang.IndexOutOfBoundsException if the index
     * given is outside the bounds of the collection
     */
    public void addDatation(
            final int index,
            final java.lang.String vDatation)
    throws java.lang.IndexOutOfBoundsException {
        this._datationList.add(index, vDatation);
    }

    /**
     * 
     * 
     * @param vEdifice
     * @throws java.lang.IndexOutOfBoundsException if the index
     * given is outside the bounds of the collection
     */
    public void addEdifice(
            final java.lang.String vEdifice)
    throws java.lang.IndexOutOfBoundsException {
        this._edificeList.addElement(vEdifice);
    }

    /**
     * 
     * 
     * @param index
     * @param vEdifice
     * @throws java.lang.IndexOutOfBoundsException if the index
     * given is outside the bounds of the collection
     */
    public void addEdifice(
            final int index,
            final java.lang.String vEdifice)
    throws java.lang.IndexOutOfBoundsException {
        this._edificeList.add(index, vEdifice);
    }

    /**
     * 
     * 
     * @param vEpoque
     * @throws java.lang.IndexOutOfBoundsException if the index
     * given is outside the bounds of the collection
     */
    public void addEpoque(
            final java.lang.String vEpoque)
    throws java.lang.IndexOutOfBoundsException {
        this._epoqueList.addElement(vEpoque);
    }

    /**
     * 
     * 
     * @param index
     * @param vEpoque
     * @throws java.lang.IndexOutOfBoundsException if the index
     * given is outside the bounds of the collection
     */
    public void addEpoque(
            final int index,
            final java.lang.String vEpoque)
    throws java.lang.IndexOutOfBoundsException {
        this._epoqueList.add(index, vEpoque);
    }

    /**
     * 
     * 
     * @param vPropriete
     * @throws java.lang.IndexOutOfBoundsException if the index
     * given is outside the bounds of the collection
     */
    public void addPropriete(
            final java.lang.String vPropriete)
    throws java.lang.IndexOutOfBoundsException {
        this._proprieteList.addElement(vPropriete);
    }

    /**
     * 
     * 
     * @param index
     * @param vPropriete
     * @throws java.lang.IndexOutOfBoundsException if the index
     * given is outside the bounds of the collection
     */
    public void addPropriete(
            final int index,
            final java.lang.String vPropriete)
    throws java.lang.IndexOutOfBoundsException {
        this._proprieteList.add(index, vPropriete);
    }

    /**
     * 
     * 
     * @param vStylearchitectural
     * @throws java.lang.IndexOutOfBoundsException if the index
     * given is outside the bounds of the collection
     */
    public void addStylearchitectural(
            final java.lang.String vStylearchitectural)
    throws java.lang.IndexOutOfBoundsException {
        this._stylearchitecturalList.addElement(vStylearchitectural);
    }

    /**
     * 
     * 
     * @param index
     * @param vStylearchitectural
     * @throws java.lang.IndexOutOfBoundsException if the index
     * given is outside the bounds of the collection
     */
    public void addStylearchitectural(
            final int index,
            final java.lang.String vStylearchitectural)
    throws java.lang.IndexOutOfBoundsException {
        this._stylearchitecturalList.add(index, vStylearchitectural);
    }

    /**
     * 
     * 
     * @param vThemes
     * @throws java.lang.IndexOutOfBoundsException if the index
     * given is outside the bounds of the collection
     */
    public void addThemes(
            final java.lang.String vThemes)
    throws java.lang.IndexOutOfBoundsException {
        this._themesList.addElement(vThemes);
    }

    /**
     * 
     * 
     * @param index
     * @param vThemes
     * @throws java.lang.IndexOutOfBoundsException if the index
     * given is outside the bounds of the collection
     */
    public void addThemes(
            final int index,
            final java.lang.String vThemes)
    throws java.lang.IndexOutOfBoundsException {
        this._themesList.add(index, vThemes);
    }

    /**
     * 
     * 
     * @param vTypedarchitecture
     * @throws java.lang.IndexOutOfBoundsException if the index
     * given is outside the bounds of the collection
     */
    public void addTypedarchitecture(
            final java.lang.String vTypedarchitecture)
    throws java.lang.IndexOutOfBoundsException {
        this._typedarchitectureList.addElement(vTypedarchitecture);
    }

    /**
     * 
     * 
     * @param index
     * @param vTypedarchitecture
     * @throws java.lang.IndexOutOfBoundsException if the index
     * given is outside the bounds of the collection
     */
    public void addTypedarchitecture(
            final int index,
            final java.lang.String vTypedarchitecture)
    throws java.lang.IndexOutOfBoundsException {
        this._typedarchitectureList.add(index, vTypedarchitecture);
    }

    /**
     * Method enumerateAppartenance.
     * 
     * @return an Enumeration over all java.lang.String elements
     */
    public java.util.Enumeration<? extends java.lang.String> enumerateAppartenance(
    ) {
        return this._appartenanceList.elements();
    }

    /**
     * Method enumerateCategoriedelarchitecture.
     * 
     * @return an Enumeration over all java.lang.String elements
     */
    public java.util.Enumeration<? extends java.lang.String> enumerateCategoriedelarchitecture(
    ) {
        return this._categoriedelarchitectureList.elements();
    }

    /**
     * Method enumerateDatation.
     * 
     * @return an Enumeration over all java.lang.String elements
     */
    public java.util.Enumeration<? extends java.lang.String> enumerateDatation(
    ) {
        return this._datationList.elements();
    }

    /**
     * Method enumerateEdifice.
     * 
     * @return an Enumeration over all java.lang.String elements
     */
    public java.util.Enumeration<? extends java.lang.String> enumerateEdifice(
    ) {
        return this._edificeList.elements();
    }

    /**
     * Method enumerateEpoque.
     * 
     * @return an Enumeration over all java.lang.String elements
     */
    public java.util.Enumeration<? extends java.lang.String> enumerateEpoque(
    ) {
        return this._epoqueList.elements();
    }

    /**
     * Method enumeratePropriete.
     * 
     * @return an Enumeration over all java.lang.String elements
     */
    public java.util.Enumeration<? extends java.lang.String> enumeratePropriete(
    ) {
        return this._proprieteList.elements();
    }

    /**
     * Method enumerateStylearchitectural.
     * 
     * @return an Enumeration over all java.lang.String elements
     */
    public java.util.Enumeration<? extends java.lang.String> enumerateStylearchitectural(
    ) {
        return this._stylearchitecturalList.elements();
    }

    /**
     * Method enumerateThemes.
     * 
     * @return an Enumeration over all java.lang.String elements
     */
    public java.util.Enumeration<? extends java.lang.String> enumerateThemes(
    ) {
        return this._themesList.elements();
    }

    /**
     * Method enumerateTypedarchitecture.
     * 
     * @return an Enumeration over all java.lang.String elements
     */
    public java.util.Enumeration<? extends java.lang.String> enumerateTypedarchitecture(
    ) {
        return this._typedarchitectureList.elements();
    }

    /**
     * Method getAppartenance.
     * 
     * @param index
     * @throws java.lang.IndexOutOfBoundsException if the index
     * given is outside the bounds of the collection
     * @return the value of the java.lang.String at the given index
     */
    public java.lang.String getAppartenance(
            final int index)
    throws java.lang.IndexOutOfBoundsException {
        // check bounds for index
        if (index < 0 || index >= this._appartenanceList.size()) {
            throw new IndexOutOfBoundsException("getAppartenance: Index value '" + index + "' not in range [0.." + (this._appartenanceList.size() - 1) + "]");
        }

        return (java.lang.String) _appartenanceList.get(index);
    }

    /**
     * Method getAppartenance.Returns the contents of the
     * collection in an Array.  <p>Note:  Just in case the
     * collection contents are changing in another thread, we pass
     * a 0-length Array of the correct type into the API call. 
     * This way we <i>know</i> that the Array returned is of
     * exactly the correct length.
     * 
     * @return this collection as an Array
     */
    public java.lang.String[] getAppartenance(
    ) {
        java.lang.String[] array = new java.lang.String[0];
        return (java.lang.String[]) this._appartenanceList.toArray(array);
    }

    /**
     * Method getAppartenanceCount.
     * 
     * @return the size of this collection
     */
    public int getAppartenanceCount(
    ) {
        return this._appartenanceList.size();
    }

    /**
     * Method getCategoriedelarchitecture.
     * 
     * @param index
     * @throws java.lang.IndexOutOfBoundsException if the index
     * given is outside the bounds of the collection
     * @return the value of the java.lang.String at the given index
     */
    public java.lang.String getCategoriedelarchitecture(
            final int index)
    throws java.lang.IndexOutOfBoundsException {
        // check bounds for index
        if (index < 0 || index >= this._categoriedelarchitectureList.size()) {
            throw new IndexOutOfBoundsException("getCategoriedelarchitecture: Index value '" + index + "' not in range [0.." + (this._categoriedelarchitectureList.size() - 1) + "]");
        }

        return (java.lang.String) _categoriedelarchitectureList.get(index);
    }

    /**
     * Method getCategoriedelarchitecture.Returns the contents of
     * the collection in an Array.  <p>Note:  Just in case the
     * collection contents are changing in another thread, we pass
     * a 0-length Array of the correct type into the API call. 
     * This way we <i>know</i> that the Array returned is of
     * exactly the correct length.
     * 
     * @return this collection as an Array
     */
    public java.lang.String[] getCategoriedelarchitecture(
    ) {
        java.lang.String[] array = new java.lang.String[0];
        return (java.lang.String[]) this._categoriedelarchitectureList.toArray(array);
    }

    /**
     * Method getCategoriedelarchitectureCount.
     * 
     * @return the size of this collection
     */
    public int getCategoriedelarchitectureCount(
    ) {
        return this._categoriedelarchitectureList.size();
    }

    /**
     * Method getDatation.
     * 
     * @param index
     * @throws java.lang.IndexOutOfBoundsException if the index
     * given is outside the bounds of the collection
     * @return the value of the java.lang.String at the given index
     */
    public java.lang.String getDatation(
            final int index)
    throws java.lang.IndexOutOfBoundsException {
        // check bounds for index
        if (index < 0 || index >= this._datationList.size()) {
            throw new IndexOutOfBoundsException("getDatation: Index value '" + index + "' not in range [0.." + (this._datationList.size() - 1) + "]");
        }

        return (java.lang.String) _datationList.get(index);
    }

    /**
     * Method getDatation.Returns the contents of the collection in
     * an Array.  <p>Note:  Just in case the collection contents
     * are changing in another thread, we pass a 0-length Array of
     * the correct type into the API call.  This way we <i>know</i>
     * that the Array returned is of exactly the correct length.
     * 
     * @return this collection as an Array
     */
    public java.lang.String[] getDatation(
    ) {
        java.lang.String[] array = new java.lang.String[0];
        return (java.lang.String[]) this._datationList.toArray(array);
    }

    /**
     * Method getDatationCount.
     * 
     * @return the size of this collection
     */
    public int getDatationCount(
    ) {
        return this._datationList.size();
    }

    /**
     * Method getEdifice.
     * 
     * @param index
     * @throws java.lang.IndexOutOfBoundsException if the index
     * given is outside the bounds of the collection
     * @return the value of the java.lang.String at the given index
     */
    public java.lang.String getEdifice(
            final int index)
    throws java.lang.IndexOutOfBoundsException {
        // check bounds for index
        if (index < 0 || index >= this._edificeList.size()) {
            throw new IndexOutOfBoundsException("getEdifice: Index value '" + index + "' not in range [0.." + (this._edificeList.size() - 1) + "]");
        }

        return (java.lang.String) _edificeList.get(index);
    }

    /**
     * Method getEdifice.Returns the contents of the collection in
     * an Array.  <p>Note:  Just in case the collection contents
     * are changing in another thread, we pass a 0-length Array of
     * the correct type into the API call.  This way we <i>know</i>
     * that the Array returned is of exactly the correct length.
     * 
     * @return this collection as an Array
     */
    public java.lang.String[] getEdifice(
    ) {
        java.lang.String[] array = new java.lang.String[0];
        return (java.lang.String[]) this._edificeList.toArray(array);
    }

    /**
     * Method getEdificeCount.
     * 
     * @return the size of this collection
     */
    public int getEdificeCount(
    ) {
        return this._edificeList.size();
    }

    /**
     * Method getEpoque.
     * 
     * @param index
     * @throws java.lang.IndexOutOfBoundsException if the index
     * given is outside the bounds of the collection
     * @return the value of the java.lang.String at the given index
     */
    public java.lang.String getEpoque(
            final int index)
    throws java.lang.IndexOutOfBoundsException {
        // check bounds for index
        if (index < 0 || index >= this._epoqueList.size()) {
            throw new IndexOutOfBoundsException("getEpoque: Index value '" + index + "' not in range [0.." + (this._epoqueList.size() - 1) + "]");
        }

        return (java.lang.String) _epoqueList.get(index);
    }

    /**
     * Method getEpoque.Returns the contents of the collection in
     * an Array.  <p>Note:  Just in case the collection contents
     * are changing in another thread, we pass a 0-length Array of
     * the correct type into the API call.  This way we <i>know</i>
     * that the Array returned is of exactly the correct length.
     * 
     * @return this collection as an Array
     */
    public java.lang.String[] getEpoque(
    ) {
        java.lang.String[] array = new java.lang.String[0];
        return (java.lang.String[]) this._epoqueList.toArray(array);
    }

    /**
     * Method getEpoqueCount.
     * 
     * @return the size of this collection
     */
    public int getEpoqueCount(
    ) {
        return this._epoqueList.size();
    }

    /**
     * Method getPropriete.
     * 
     * @param index
     * @throws java.lang.IndexOutOfBoundsException if the index
     * given is outside the bounds of the collection
     * @return the value of the java.lang.String at the given index
     */
    public java.lang.String getPropriete(
            final int index)
    throws java.lang.IndexOutOfBoundsException {
        // check bounds for index
        if (index < 0 || index >= this._proprieteList.size()) {
            throw new IndexOutOfBoundsException("getPropriete: Index value '" + index + "' not in range [0.." + (this._proprieteList.size() - 1) + "]");
        }

        return (java.lang.String) _proprieteList.get(index);
    }

    /**
     * Method getPropriete.Returns the contents of the collection
     * in an Array.  <p>Note:  Just in case the collection contents
     * are changing in another thread, we pass a 0-length Array of
     * the correct type into the API call.  This way we <i>know</i>
     * that the Array returned is of exactly the correct length.
     * 
     * @return this collection as an Array
     */
    public java.lang.String[] getPropriete(
    ) {
        java.lang.String[] array = new java.lang.String[0];
        return (java.lang.String[]) this._proprieteList.toArray(array);
    }

    /**
     * Method getProprieteCount.
     * 
     * @return the size of this collection
     */
    public int getProprieteCount(
    ) {
        return this._proprieteList.size();
    }

    /**
     * Method getStylearchitectural.
     * 
     * @param index
     * @throws java.lang.IndexOutOfBoundsException if the index
     * given is outside the bounds of the collection
     * @return the value of the java.lang.String at the given index
     */
    public java.lang.String getStylearchitectural(
            final int index)
    throws java.lang.IndexOutOfBoundsException {
        // check bounds for index
        if (index < 0 || index >= this._stylearchitecturalList.size()) {
            throw new IndexOutOfBoundsException("getStylearchitectural: Index value '" + index + "' not in range [0.." + (this._stylearchitecturalList.size() - 1) + "]");
        }

        return (java.lang.String) _stylearchitecturalList.get(index);
    }

    /**
     * Method getStylearchitectural.Returns the contents of the
     * collection in an Array.  <p>Note:  Just in case the
     * collection contents are changing in another thread, we pass
     * a 0-length Array of the correct type into the API call. 
     * This way we <i>know</i> that the Array returned is of
     * exactly the correct length.
     * 
     * @return this collection as an Array
     */
    public java.lang.String[] getStylearchitectural(
    ) {
        java.lang.String[] array = new java.lang.String[0];
        return (java.lang.String[]) this._stylearchitecturalList.toArray(array);
    }

    /**
     * Method getStylearchitecturalCount.
     * 
     * @return the size of this collection
     */
    public int getStylearchitecturalCount(
    ) {
        return this._stylearchitecturalList.size();
    }

    /**
     * Method getThemes.
     * 
     * @param index
     * @throws java.lang.IndexOutOfBoundsException if the index
     * given is outside the bounds of the collection
     * @return the value of the java.lang.String at the given index
     */
    public java.lang.String getThemes(
            final int index)
    throws java.lang.IndexOutOfBoundsException {
        // check bounds for index
        if (index < 0 || index >= this._themesList.size()) {
            throw new IndexOutOfBoundsException("getThemes: Index value '" + index + "' not in range [0.." + (this._themesList.size() - 1) + "]");
        }

        return (java.lang.String) _themesList.get(index);
    }

    /**
     * Method getThemes.Returns the contents of the collection in
     * an Array.  <p>Note:  Just in case the collection contents
     * are changing in another thread, we pass a 0-length Array of
     * the correct type into the API call.  This way we <i>know</i>
     * that the Array returned is of exactly the correct length.
     * 
     * @return this collection as an Array
     */
    public java.lang.String[] getThemes(
    ) {
        java.lang.String[] array = new java.lang.String[0];
        return (java.lang.String[]) this._themesList.toArray(array);
    }

    /**
     * Method getThemesCount.
     * 
     * @return the size of this collection
     */
    public int getThemesCount(
    ) {
        return this._themesList.size();
    }

    /**
     * Method getTypedarchitecture.
     * 
     * @param index
     * @throws java.lang.IndexOutOfBoundsException if the index
     * given is outside the bounds of the collection
     * @return the value of the java.lang.String at the given index
     */
    public java.lang.String getTypedarchitecture(
            final int index)
    throws java.lang.IndexOutOfBoundsException {
        // check bounds for index
        if (index < 0 || index >= this._typedarchitectureList.size()) {
            throw new IndexOutOfBoundsException("getTypedarchitecture: Index value '" + index + "' not in range [0.." + (this._typedarchitectureList.size() - 1) + "]");
        }

        return (java.lang.String) _typedarchitectureList.get(index);
    }

    /**
     * Method getTypedarchitecture.Returns the contents of the
     * collection in an Array.  <p>Note:  Just in case the
     * collection contents are changing in another thread, we pass
     * a 0-length Array of the correct type into the API call. 
     * This way we <i>know</i> that the Array returned is of
     * exactly the correct length.
     * 
     * @return this collection as an Array
     */
    public java.lang.String[] getTypedarchitecture(
    ) {
        java.lang.String[] array = new java.lang.String[0];
        return (java.lang.String[]) this._typedarchitectureList.toArray(array);
    }

    /**
     * Method getTypedarchitectureCount.
     * 
     * @return the size of this collection
     */
    public int getTypedarchitectureCount(
    ) {
        return this._typedarchitectureList.size();
    }

    /**
     */
    public void removeAllAppartenance(
    ) {
        this._appartenanceList.clear();
    }

    /**
     */
    public void removeAllCategoriedelarchitecture(
    ) {
        this._categoriedelarchitectureList.clear();
    }

    /**
     */
    public void removeAllDatation(
    ) {
        this._datationList.clear();
    }

    /**
     */
    public void removeAllEdifice(
    ) {
        this._edificeList.clear();
    }

    /**
     */
    public void removeAllEpoque(
    ) {
        this._epoqueList.clear();
    }

    /**
     */
    public void removeAllPropriete(
    ) {
        this._proprieteList.clear();
    }

    /**
     */
    public void removeAllStylearchitectural(
    ) {
        this._stylearchitecturalList.clear();
    }

    /**
     */
    public void removeAllThemes(
    ) {
        this._themesList.clear();
    }

    /**
     */
    public void removeAllTypedarchitecture(
    ) {
        this._typedarchitectureList.clear();
    }

    /**
     * Method removeAppartenance.
     * 
     * @param vAppartenance
     * @return true if the object was removed from the collection.
     */
    public boolean removeAppartenance(
            final java.lang.String vAppartenance) {
        boolean removed = _appartenanceList.remove(vAppartenance);
        return removed;
    }

    /**
     * Method removeAppartenanceAt.
     * 
     * @param index
     * @return the element removed from the collection
     */
    public java.lang.String removeAppartenanceAt(
            final int index) {
        java.lang.Object obj = this._appartenanceList.remove(index);
        return (java.lang.String) obj;
    }

    /**
     * Method removeCategoriedelarchitecture.
     * 
     * @param vCategoriedelarchitecture
     * @return true if the object was removed from the collection.
     */
    public boolean removeCategoriedelarchitecture(
            final java.lang.String vCategoriedelarchitecture) {
        boolean removed = _categoriedelarchitectureList.remove(vCategoriedelarchitecture);
        return removed;
    }

    /**
     * Method removeCategoriedelarchitectureAt.
     * 
     * @param index
     * @return the element removed from the collection
     */
    public java.lang.String removeCategoriedelarchitectureAt(
            final int index) {
        java.lang.Object obj = this._categoriedelarchitectureList.remove(index);
        return (java.lang.String) obj;
    }

    /**
     * Method removeDatation.
     * 
     * @param vDatation
     * @return true if the object was removed from the collection.
     */
    public boolean removeDatation(
            final java.lang.String vDatation) {
        boolean removed = _datationList.remove(vDatation);
        return removed;
    }

    /**
     * Method removeDatationAt.
     * 
     * @param index
     * @return the element removed from the collection
     */
    public java.lang.String removeDatationAt(
            final int index) {
        java.lang.Object obj = this._datationList.remove(index);
        return (java.lang.String) obj;
    }

    /**
     * Method removeEdifice.
     * 
     * @param vEdifice
     * @return true if the object was removed from the collection.
     */
    public boolean removeEdifice(
            final java.lang.String vEdifice) {
        boolean removed = _edificeList.remove(vEdifice);
        return removed;
    }

    /**
     * Method removeEdificeAt.
     * 
     * @param index
     * @return the element removed from the collection
     */
    public java.lang.String removeEdificeAt(
            final int index) {
        java.lang.Object obj = this._edificeList.remove(index);
        return (java.lang.String) obj;
    }

    /**
     * Method removeEpoque.
     * 
     * @param vEpoque
     * @return true if the object was removed from the collection.
     */
    public boolean removeEpoque(
            final java.lang.String vEpoque) {
        boolean removed = _epoqueList.remove(vEpoque);
        return removed;
    }

    /**
     * Method removeEpoqueAt.
     * 
     * @param index
     * @return the element removed from the collection
     */
    public java.lang.String removeEpoqueAt(
            final int index) {
        java.lang.Object obj = this._epoqueList.remove(index);
        return (java.lang.String) obj;
    }

    /**
     * Method removePropriete.
     * 
     * @param vPropriete
     * @return true if the object was removed from the collection.
     */
    public boolean removePropriete(
            final java.lang.String vPropriete) {
        boolean removed = _proprieteList.remove(vPropriete);
        return removed;
    }

    /**
     * Method removeProprieteAt.
     * 
     * @param index
     * @return the element removed from the collection
     */
    public java.lang.String removeProprieteAt(
            final int index) {
        java.lang.Object obj = this._proprieteList.remove(index);
        return (java.lang.String) obj;
    }

    /**
     * Method removeStylearchitectural.
     * 
     * @param vStylearchitectural
     * @return true if the object was removed from the collection.
     */
    public boolean removeStylearchitectural(
            final java.lang.String vStylearchitectural) {
        boolean removed = _stylearchitecturalList.remove(vStylearchitectural);
        return removed;
    }

    /**
     * Method removeStylearchitecturalAt.
     * 
     * @param index
     * @return the element removed from the collection
     */
    public java.lang.String removeStylearchitecturalAt(
            final int index) {
        java.lang.Object obj = this._stylearchitecturalList.remove(index);
        return (java.lang.String) obj;
    }

    /**
     * Method removeThemes.
     * 
     * @param vThemes
     * @return true if the object was removed from the collection.
     */
    public boolean removeThemes(
            final java.lang.String vThemes) {
        boolean removed = _themesList.remove(vThemes);
        return removed;
    }

    /**
     * Method removeThemesAt.
     * 
     * @param index
     * @return the element removed from the collection
     */
    public java.lang.String removeThemesAt(
            final int index) {
        java.lang.Object obj = this._themesList.remove(index);
        return (java.lang.String) obj;
    }

    /**
     * Method removeTypedarchitecture.
     * 
     * @param vTypedarchitecture
     * @return true if the object was removed from the collection.
     */
    public boolean removeTypedarchitecture(
            final java.lang.String vTypedarchitecture) {
        boolean removed = _typedarchitectureList.remove(vTypedarchitecture);
        return removed;
    }

    /**
     * Method removeTypedarchitectureAt.
     * 
     * @param index
     * @return the element removed from the collection
     */
    public java.lang.String removeTypedarchitectureAt(
            final int index) {
        java.lang.Object obj = this._typedarchitectureList.remove(index);
        return (java.lang.String) obj;
    }

    /**
     * 
     * 
     * @param index
     * @param vAppartenance
     * @throws java.lang.IndexOutOfBoundsException if the index
     * given is outside the bounds of the collection
     */
    public void setAppartenance(
            final int index,
            final java.lang.String vAppartenance)
    throws java.lang.IndexOutOfBoundsException {
        // check bounds for index
        if (index < 0 || index >= this._appartenanceList.size()) {
            throw new IndexOutOfBoundsException("setAppartenance: Index value '" + index + "' not in range [0.." + (this._appartenanceList.size() - 1) + "]");
        }

        this._appartenanceList.set(index, vAppartenance);
    }

    /**
     * 
     * 
     * @param vAppartenanceArray
     */
    public void setAppartenance(
            final java.lang.String[] vAppartenanceArray) {
        //-- copy array
        _appartenanceList.clear();

        for (int i = 0; i < vAppartenanceArray.length; i++) {
                this._appartenanceList.add(vAppartenanceArray[i]);
        }
    }

    /**
     * 
     * 
     * @param index
     * @param vCategoriedelarchitecture
     * @throws java.lang.IndexOutOfBoundsException if the index
     * given is outside the bounds of the collection
     */
    public void setCategoriedelarchitecture(
            final int index,
            final java.lang.String vCategoriedelarchitecture)
    throws java.lang.IndexOutOfBoundsException {
        // check bounds for index
        if (index < 0 || index >= this._categoriedelarchitectureList.size()) {
            throw new IndexOutOfBoundsException("setCategoriedelarchitecture: Index value '" + index + "' not in range [0.." + (this._categoriedelarchitectureList.size() - 1) + "]");
        }

        this._categoriedelarchitectureList.set(index, vCategoriedelarchitecture);
    }

    /**
     * 
     * 
     * @param vCategoriedelarchitectureArray
     */
    public void setCategoriedelarchitecture(
            final java.lang.String[] vCategoriedelarchitectureArray) {
        //-- copy array
        _categoriedelarchitectureList.clear();

        for (int i = 0; i < vCategoriedelarchitectureArray.length; i++) {
                this._categoriedelarchitectureList.add(vCategoriedelarchitectureArray[i]);
        }
    }

    /**
     * 
     * 
     * @param index
     * @param vDatation
     * @throws java.lang.IndexOutOfBoundsException if the index
     * given is outside the bounds of the collection
     */
    public void setDatation(
            final int index,
            final java.lang.String vDatation)
    throws java.lang.IndexOutOfBoundsException {
        // check bounds for index
        if (index < 0 || index >= this._datationList.size()) {
            throw new IndexOutOfBoundsException("setDatation: Index value '" + index + "' not in range [0.." + (this._datationList.size() - 1) + "]");
        }

        this._datationList.set(index, vDatation);
    }

    /**
     * 
     * 
     * @param vDatationArray
     */
    public void setDatation(
            final java.lang.String[] vDatationArray) {
        //-- copy array
        _datationList.clear();

        for (int i = 0; i < vDatationArray.length; i++) {
                this._datationList.add(vDatationArray[i]);
        }
    }

    /**
     * 
     * 
     * @param index
     * @param vEdifice
     * @throws java.lang.IndexOutOfBoundsException if the index
     * given is outside the bounds of the collection
     */
    public void setEdifice(
            final int index,
            final java.lang.String vEdifice)
    throws java.lang.IndexOutOfBoundsException {
        // check bounds for index
        if (index < 0 || index >= this._edificeList.size()) {
            throw new IndexOutOfBoundsException("setEdifice: Index value '" + index + "' not in range [0.." + (this._edificeList.size() - 1) + "]");
        }

        this._edificeList.set(index, vEdifice);
    }

    /**
     * 
     * 
     * @param vEdificeArray
     */
    public void setEdifice(
            final java.lang.String[] vEdificeArray) {
        //-- copy array
        _edificeList.clear();

        for (int i = 0; i < vEdificeArray.length; i++) {
                this._edificeList.add(vEdificeArray[i]);
        }
    }

    /**
     * 
     * 
     * @param index
     * @param vEpoque
     * @throws java.lang.IndexOutOfBoundsException if the index
     * given is outside the bounds of the collection
     */
    public void setEpoque(
            final int index,
            final java.lang.String vEpoque)
    throws java.lang.IndexOutOfBoundsException {
        // check bounds for index
        if (index < 0 || index >= this._epoqueList.size()) {
            throw new IndexOutOfBoundsException("setEpoque: Index value '" + index + "' not in range [0.." + (this._epoqueList.size() - 1) + "]");
        }

        this._epoqueList.set(index, vEpoque);
    }

    /**
     * 
     * 
     * @param vEpoqueArray
     */
    public void setEpoque(
            final java.lang.String[] vEpoqueArray) {
        //-- copy array
        _epoqueList.clear();

        for (int i = 0; i < vEpoqueArray.length; i++) {
                this._epoqueList.add(vEpoqueArray[i]);
        }
    }

    /**
     * 
     * 
     * @param index
     * @param vPropriete
     * @throws java.lang.IndexOutOfBoundsException if the index
     * given is outside the bounds of the collection
     */
    public void setPropriete(
            final int index,
            final java.lang.String vPropriete)
    throws java.lang.IndexOutOfBoundsException {
        // check bounds for index
        if (index < 0 || index >= this._proprieteList.size()) {
            throw new IndexOutOfBoundsException("setPropriete: Index value '" + index + "' not in range [0.." + (this._proprieteList.size() - 1) + "]");
        }

        this._proprieteList.set(index, vPropriete);
    }

    /**
     * 
     * 
     * @param vProprieteArray
     */
    public void setPropriete(
            final java.lang.String[] vProprieteArray) {
        //-- copy array
        _proprieteList.clear();

        for (int i = 0; i < vProprieteArray.length; i++) {
                this._proprieteList.add(vProprieteArray[i]);
        }
    }

    /**
     * 
     * 
     * @param index
     * @param vStylearchitectural
     * @throws java.lang.IndexOutOfBoundsException if the index
     * given is outside the bounds of the collection
     */
    public void setStylearchitectural(
            final int index,
            final java.lang.String vStylearchitectural)
    throws java.lang.IndexOutOfBoundsException {
        // check bounds for index
        if (index < 0 || index >= this._stylearchitecturalList.size()) {
            throw new IndexOutOfBoundsException("setStylearchitectural: Index value '" + index + "' not in range [0.." + (this._stylearchitecturalList.size() - 1) + "]");
        }

        this._stylearchitecturalList.set(index, vStylearchitectural);
    }

    /**
     * 
     * 
     * @param vStylearchitecturalArray
     */
    public void setStylearchitectural(
            final java.lang.String[] vStylearchitecturalArray) {
        //-- copy array
        _stylearchitecturalList.clear();

        for (int i = 0; i < vStylearchitecturalArray.length; i++) {
                this._stylearchitecturalList.add(vStylearchitecturalArray[i]);
        }
    }

    /**
     * 
     * 
     * @param index
     * @param vThemes
     * @throws java.lang.IndexOutOfBoundsException if the index
     * given is outside the bounds of the collection
     */
    public void setThemes(
            final int index,
            final java.lang.String vThemes)
    throws java.lang.IndexOutOfBoundsException {
        // check bounds for index
        if (index < 0 || index >= this._themesList.size()) {
            throw new IndexOutOfBoundsException("setThemes: Index value '" + index + "' not in range [0.." + (this._themesList.size() - 1) + "]");
        }

        this._themesList.set(index, vThemes);
    }

    /**
     * 
     * 
     * @param vThemesArray
     */
    public void setThemes(
            final java.lang.String[] vThemesArray) {
        //-- copy array
        _themesList.clear();

        for (int i = 0; i < vThemesArray.length; i++) {
                this._themesList.add(vThemesArray[i]);
        }
    }

    /**
     * 
     * 
     * @param index
     * @param vTypedarchitecture
     * @throws java.lang.IndexOutOfBoundsException if the index
     * given is outside the bounds of the collection
     */
    public void setTypedarchitecture(
            final int index,
            final java.lang.String vTypedarchitecture)
    throws java.lang.IndexOutOfBoundsException {
        // check bounds for index
        if (index < 0 || index >= this._typedarchitectureList.size()) {
            throw new IndexOutOfBoundsException("setTypedarchitecture: Index value '" + index + "' not in range [0.." + (this._typedarchitectureList.size() - 1) + "]");
        }

        this._typedarchitectureList.set(index, vTypedarchitecture);
    }

    /**
     * 
     * 
     * @param vTypedarchitectureArray
     */
    public void setTypedarchitecture(
            final java.lang.String[] vTypedarchitectureArray) {
        //-- copy array
        _typedarchitectureList.clear();

        for (int i = 0; i < vTypedarchitectureArray.length; i++) {
                this._typedarchitectureList.add(vTypedarchitectureArray[i]);
        }
    }

}

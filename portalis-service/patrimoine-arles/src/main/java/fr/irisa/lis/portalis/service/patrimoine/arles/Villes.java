/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.3.1</a>, using an XML
 * Schema.
 * $Id$
 */

package fr.irisa.lis.portalis.service.patrimoine.arles;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * Class Villes.
 * 
 * @version $Revision$ $Date$
 */
@SuppressWarnings("serial")
@XmlRootElement
public class Villes implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _villeList.
     */
    private java.util.Vector<fr.irisa.lis.portalis.service.patrimoine.arles.Ville> _villeList;


      //----------------/
     //- Constructors -/
    //----------------/

    public Villes() {
        super();
        this._villeList = new java.util.Vector<fr.irisa.lis.portalis.service.patrimoine.arles.Ville>();
    }


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * 
     * 
     * @param vVille
     * @throws java.lang.IndexOutOfBoundsException if the index
     * given is outside the bounds of the collection
     */
    public void addVille(
            final fr.irisa.lis.portalis.service.patrimoine.arles.Ville vVille)
    throws java.lang.IndexOutOfBoundsException {
        this._villeList.addElement(vVille);
    }

    /**
     * 
     * 
     * @param index
     * @param vVille
     * @throws java.lang.IndexOutOfBoundsException if the index
     * given is outside the bounds of the collection
     */
    public void addVille(
            final int index,
            final fr.irisa.lis.portalis.service.patrimoine.arles.Ville vVille)
    throws java.lang.IndexOutOfBoundsException {
        this._villeList.add(index, vVille);
    }

    /**
     * Method enumerateVille.
     * 
     * @return an Enumeration over all
     * fr.irisa.lis.portalis.service.patrimoine.arles.Ville elements
     */
    public java.util.Enumeration<? extends fr.irisa.lis.portalis.service.patrimoine.arles.Ville> enumerateVille(
    ) {
        return this._villeList.elements();
    }

    /**
     * Method getVille.
     * 
     * @param index
     * @throws java.lang.IndexOutOfBoundsException if the index
     * given is outside the bounds of the collection
     * @return the value of the
     * fr.irisa.lis.portalis.service.patrimoine.arles.Ville at the
     * given index
     */
    public VilleLight getVille(
            final int index)
    throws java.lang.IndexOutOfBoundsException {
        // check bounds for index
        if (index < 0 || index >= this._villeList.size()) {
            throw new IndexOutOfBoundsException("getVille: Index value '" + index + "' not in range [0.." + (this._villeList.size() - 1) + "]");
        }

        return (VilleLight) _villeList.get(index);
    }

    /**
     * Method getVille.Returns the contents of the collection in an
     * Array.  <p>Note:  Just in case the collection contents are
     * changing in another thread, we pass a 0-length Array of the
     * correct type into the API call.  This way we <i>know</i>
     * that the Array returned is of exactly the correct length.
     * 
     * @return this collection as an Array
     */
    public fr.irisa.lis.portalis.service.patrimoine.arles.Ville[] getVille(
    ) {
        fr.irisa.lis.portalis.service.patrimoine.arles.Ville[] array = new fr.irisa.lis.portalis.service.patrimoine.arles.Ville[0];
        return (fr.irisa.lis.portalis.service.patrimoine.arles.Ville[]) this._villeList.toArray(array);
    }

    /**
     * Method getVilleCount.
     * 
     * @return the size of this collection
     */
    public int getVilleCount(
    ) {
        return this._villeList.size();
    }

    /**
     */
    public void removeAllVille(
    ) {
        this._villeList.clear();
    }

    /**
     * Method removeVille.
     * 
     * @param vVille
     * @return true if the object was removed from the collection.
     */
    public boolean removeVille(
            final VilleLight vVille) {
        boolean removed = _villeList.remove(vVille);
        return removed;
    }

    /**
     * Method removeVilleAt.
     * 
     * @param index
     * @return the element removed from the collection
     */
    public VilleLight removeVilleAt(
            final int index) {
        java.lang.Object obj = this._villeList.remove(index);
        return (VilleLight) obj;
    }

    /**
     * 
     * 
     * @param index
     * @param vVille
     * @throws java.lang.IndexOutOfBoundsException if the index
     * given is outside the bounds of the collection
     */
    public void setVille(
            final int index,
            final fr.irisa.lis.portalis.service.patrimoine.arles.Ville vVille)
    throws java.lang.IndexOutOfBoundsException {
        // check bounds for index
        if (index < 0 || index >= this._villeList.size()) {
            throw new IndexOutOfBoundsException("setVille: Index value '" + index + "' not in range [0.." + (this._villeList.size() - 1) + "]");
        }

        this._villeList.set(index, vVille);
    }

    /**
     * 
     * 
     * @param vVilleArray
     */
    public void setVille(
            final fr.irisa.lis.portalis.service.patrimoine.arles.Ville[] vVilleArray) {
        //-- copy array
        _villeList.clear();

        for (int i = 0; i < vVilleArray.length; i++) {
                this._villeList.add(vVilleArray[i]);
        }
    }

}

/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.3.1</a>, using an XML
 * Schema.
 * $Id$
 */

package fr.irisa.lis.portalis.service.patrimoine.arles.db;

/**
 * Class Image.
 * 
 * @version $Revision$ $Date$
 */
@SuppressWarnings("serial")
public class Image extends ImageType 
implements java.io.Serializable
{


      //----------------/
     //- Constructors -/
    //----------------/

    public Image() {
        super();
    }

    public Image(final java.lang.String defaultValue) {
        super(defaultValue);
    }

}

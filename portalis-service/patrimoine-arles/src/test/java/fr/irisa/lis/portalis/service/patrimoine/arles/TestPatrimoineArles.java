package fr.irisa.lis.portalis.service.patrimoine.arles;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.io.File;

import org.junit.Test;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import fr.irisa.lis.portalis.commons.DOMUtil;

public class TestPatrimoineArles {

	
	@Test
	public void testGenerateCtx() {
		try {
			File ctxFile = new File(PatrimoineArles.ARLES_CTX);
			ctxFile.delete();
			PatrimoineArles patrimoineArles = new PatrimoineArles();
			patrimoineArles.generateCtx();
			assertTrue(new File(PatrimoineArles.ARLES_CTX).exists());
		} catch (Exception e) {
			fail("Exception rencontrée lors du test : " + e.getMessage());
			e.printStackTrace();
		}
	}
	@Test
	public void testGetVilleFromXmlAll() {
		try {
			PatrimoineArles patrimoineArles = new PatrimoineArles();
			Document doc = patrimoineArles.mergePatrimoine();
			assertNotNull(doc);
			DOMUtil.prettyPrintXml(doc, new File("villesLight.xml"));
			Element root = doc.getDocumentElement();
			assertEquals("villes", root.getLocalName());
			NodeList listVilles = root.getElementsByTagName("ville");
			
			for (int i=0; i<listVilles.getLength(); i++) {
				String nom = ((Element)listVilles.item(i)).getAttribute("nom");
				System.out.print("\t"+nom);
				if ((i+1)%5 == 0)
					System.out.println();
			}
			System.out.println();

			
			System.out.println("nbVille = "+listVilles.getLength());
			assertEquals(patrimoineArles.getVilles().getVilleCount(), listVilles.getLength());
			assertEquals(9, root.getElementsByTagName("triris").getLength());
			
		} catch (Exception e) {
			fail("Exception rencontrée lors du test : " + e.getMessage());
			e.printStackTrace();
		}
	}

	@Test
	public void testGetVilleFromXmlSingle() {
		try {
			PatrimoineArles patrimoineArles = new PatrimoineArles();
			Document doc = patrimoineArles.mergePatrimoine("Arles");
			assertNotNull(doc);
			DOMUtil.prettyPrintXml(doc, new File("villeLight.xml"));
			assertEquals("ville", doc.getDocumentElement().getLocalName());
			Element root = doc.getDocumentElement();
			assertTrue(root.getElementsByTagName("triris").getLength()>0);
			
		} catch (Exception e) {
			fail("Exception rencontrée lors du test : " + e.getMessage());
			e.printStackTrace();
		}
	}

}

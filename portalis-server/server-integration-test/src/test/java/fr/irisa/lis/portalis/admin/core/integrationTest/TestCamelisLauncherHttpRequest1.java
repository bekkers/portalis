package fr.irisa.lis.portalis.admin.core.integrationTest;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.net.InetAddress;
import java.net.URL;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.irisa.lis.cargo.CargoLauncher;
import fr.irisa.lis.portalis.camelis.http.UtilTest;
import fr.irisa.lis.portalis.commons.DOMUtil;
import fr.irisa.lis.portalis.commons.core.CommonsUtil;
import fr.irisa.lis.portalis.shared.admin.PortalisException;
import fr.irisa.lis.portalis.shared.admin.Util;
import fr.irisa.lis.portalis.shared.admin.data.ActiveLisService;
import fr.irisa.lis.portalis.shared.admin.data.ActiveUser;
import fr.irisa.lis.portalis.shared.admin.data.PortalisService;
import fr.irisa.lis.portalis.shared.admin.http.AdminHttp;
import fr.irisa.lis.portalis.shared.admin.http.HttpPortalisUrlConnection;
import fr.irisa.lis.portalis.shared.admin.reponse.BooleanReponse;
import fr.irisa.lis.portalis.shared.admin.reponse.LoginReponse;
import fr.irisa.lis.portalis.shared.admin.reponse.PortsActifsReponse;
import fr.irisa.lis.portalis.shared.admin.reponse.VoidReponse;
import fr.irisa.lis.portalis.shared.camelis.reponse.PingReponse;
import fr.irisa.lis.portalis.shared.camelis.reponse.StartReponse;
import fr.irisa.lis.portalis.test.CoreTestConstants;

public class TestCamelisLauncherHttpRequest1 {
	private static final Logger LOGGER = LoggerFactory
			.getLogger(TestCamelisLauncherHttpRequest1.class.getName());

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		final String host = InetAddress.getLocalHost().getCanonicalHostName();
		final int portNum = 8080;
		final String portalisAppli = "portalis";
		PortalisService.getInstance().init(host, portNum, portalisAppli);
		Util.clientInitLogPropertiesFile();
		CargoLauncher.start();
		LOGGER.info(""
				+ "\n            ----------------------------------------------------------------"
				+ "\n            |         @BeforeClass : TestCamelisLauncherHttpRequest1       |"
				+ "\n            ----------------------------------------------------------------\n");
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		CargoLauncher.stop();
		LOGGER.info(""
				+ "\n            ---------------------------------------------------------------"
				+ "\n            |         @AfterClass : TestCamelisLauncherHttpRequest1        |"
				+ "\n            ----------------------------------------------------------------\n");
	}

	@Before
	public void setUp() throws Exception {
		UtilTest.killAllCamelisProcessAndClearUsers();
		LOGGER.info("--------------> @Before : killAllCamelisProcessAndClearUsers has been executed, debut de test\n");
	}

	@After
	public void tearDown() throws Exception {
		LOGGER.info("--------------> @After : fin de test\n");
		UtilTest.killAllCamelisProcessAndClearUsers();
		LOGGER.info("--------------> @After : killAllCamelisProcessAndClearUsers has been executed\n");
	}

	@Test
	public void testHttpErr404() {
		LOGGER.info("--------------> @Test : testHttpErr404\n");
		try {
			LOGGER.debug("\n\n              ========================================> login <=========================");
			LoginReponse loginReponse = AdminHttp.login(
					CoreTestConstants.MAIL_YVES,
					CoreTestConstants.PASSWORD_YVES);
			assertNotNull(loginReponse);
			assertTrue("loginReponse ne doit doit pas être en erreur :"
					+ loginReponse, loginReponse.isOk());
			ActiveUser activeUser = loginReponse.getActiveUser();
			
			LOGGER.debug("\n\n              ========================================> connect 'noPage' <=========================");
			URL url = HttpPortalisUrlConnection.buildUrl(PortalisService.getInstance().getHost(),
					PortalisService.getInstance().getPort(), "noPage.jsp", new String[0][2]);

			try {
				HttpPortalisUrlConnection.connect(activeUser, url, CommonsUtil.DEFAULT_ENCODING);
			    fail( "ttpPortalisUrlConnection.connect didn't throw when I expected it to" );
			} catch (PortalisException expectedException) {
				
			}
			
			
		} catch (Exception e) {
			fail(e.getClass().getCanonicalName()
					+ " rencontrée lors du test : " + e.getMessage()
					+ "\nStack :\n" + CommonsUtil.stack2string(e));
		}
	}
	@Test
	public void testKillAll() {
		LOGGER.info("--------------> @Test : testKillAll\n");
		try {
			LOGGER.debug("\n\n              ========================================> login <=========================");
			LoginReponse loginReponse = AdminHttp.login(
					CoreTestConstants.MAIL_YVES,
					CoreTestConstants.PASSWORD_YVES);
			assertNotNull(loginReponse);
			assertTrue("loginReponse ne doit doit pas être en erreur :"
					+ loginReponse, loginReponse.isOk());
			ActiveUser activeUser = loginReponse.getActiveUser();

			LOGGER.debug("\n\n              ========================================> startCamelis <=========================");
			StartReponse startReponse = AdminHttp.startCamelis(activeUser,
					CoreTestConstants.SERVICE_PLANETS_DISTANCE);
			assertTrue("La reponse ne doit pas être en erreur, startReponse=" + startReponse,
					startReponse.isOk());
			assertTrue("Le service Camelis ne doit pas être nul",
					startReponse.getActiveLisService() != null);

			PortsActifsReponse actifs = AdminHttp.getActifsPorts(activeUser);
			assertTrue("actifs ne doit pas être null", actifs != null);
			assertTrue("actifs ne doit pas être en erreur, il vaut "+actifs, actifs != null);

			LOGGER.debug("\n\n              ========================================> getActifsPorts <=========================");
			PortsActifsReponse reponse = AdminHttp.getActifsPorts(activeUser);
			assertTrue("La réponse ne doit pas avoir d'erreur, elle vaut :"
					+ reponse, reponse.isOk());

			LOGGER.debug("\n\n              ========================================> killAllCamelisProcess <=========================");
			PingReponse pingReponse = AdminHttp.killAllCamelisProcess(activeUser);
			assertNotNull(pingReponse);
			assertTrue("pingReponse ne doit doit pas être en erreur :"
					+ pingReponse, pingReponse.isOk());

			LOGGER.debug("\n\n              ========================================> getActifsPorts <=========================");
			VoidReponse voidReponse = AdminHttp.clearActifUsers(activeUser);
			assertNotNull(voidReponse);
			assertTrue("voidReponse ne doit doit pas être en erreur :"
					+ voidReponse, voidReponse.isOk());

			LOGGER.debug("\n\n              ========================================> getActifsPorts <=========================");
			PortsActifsReponse actifPort1 = AdminHttp.getActifsPorts(activeUser);
			assertTrue("La réponse ne doit pas avoir d'erreur, elle vaut :"
					+ reponse, reponse.isOk());
			assertTrue("Il ne doit plus y avoir de ports actif : "+actifPort1, actifPort1.getListPortsActifs().size()==0);

		} catch (Exception e) {
			fail(e.getClass().getCanonicalName()
					+ " rencontrée lors du test : " + e.getMessage()
					+ "\nStack :\n" + CommonsUtil.stack2string(e));
		}
	}

	@Test
	public void testBootTomcat() {
		LOGGER.info("--------------> @Test : testBootTomcat\n");
		try {
			LoginReponse loginReponse = AdminHttp.login(
					CoreTestConstants.MAIL_YVES,
					CoreTestConstants.PASSWORD_YVES);
			assertTrue("La réponse doit être sans erreur, loginReponse ="
					+ loginReponse, loginReponse.isOk());
			ActiveUser activeUser = loginReponse.getActiveUser();
			LOGGER.debug("camelisSession = " + activeUser.getPortalisSessionId());
			BooleanReponse booleanReponse = AdminHttp.portIsBusy(activeUser, PortalisService
					.getInstance().getPort());
			assertTrue("La réponse doit être sans erreur, booleanReponse ="
					+ booleanReponse, booleanReponse.isOk());
			assertTrue("Il doit y avoir un processus sur le port "
					+ PortalisService.getInstance().getPort(),
					booleanReponse.isAnswer());

		} catch (Exception e) {
			fail(e.getClass().getCanonicalName()
					+ " rencontrée lors du test : " + e.getMessage()
					+ "\nStack :\n" + CommonsUtil.stack2string(e));
		}
	}

	@Test
	public void testStartCamelis() {
		LOGGER.info("--------------> @Test : testStartCamelis\n");
		try {
			String host = PortalisService.getInstance().getHost();
			String email = CoreTestConstants.SUPER_USER.getEmail();
			String password = CoreTestConstants.SUPER_USER.getPassword();

			// ********************* login *********************
			LOGGER.debug("\n\n              ===========================================> login <============================");
			LoginReponse loginReponse = UtilTest.loginHttpAndCheck(email,
					password);
			assertTrue("La réponse doit être sans erreur, loginReponse ="
					+ loginReponse, loginReponse.isOk());
			ActiveUser activeUser = loginReponse.getActiveUser();
			assertTrue("Il doit y avoir une session", activeUser != null);

			// ******************* startCamelis ****************
			LOGGER.debug("\n\n              ========================================> startCamelis <=========================");
			StartReponse startReponse = AdminHttp.startCamelis(activeUser,
					CoreTestConstants.SERVICE_PLANETS_DISTANCE);
			assertTrue("La réponse doit être sans erreur, startReponse ="
					+ startReponse, startReponse.isOk());
			ActiveLisService camelisService = startReponse
					.getActiveLisService();
			assertTrue("Il doit y avoir un camelisService",
					camelisService != null);
			assertEquals(host, camelisService.getHost());

		} catch (Exception e) {
			fail(e.getClass().getCanonicalName()
					+ " rencontrée lors du test : " + e.getMessage()
					+ "\nStack :\n" + CommonsUtil.stack2string(e));
		}
	}

	@Test
	public void testLoginHttp() {
		LOGGER.info("--------------> @Test : testLoginHttp sur le port "
				+ CoreTestConstants.PORT1 + "\n");
		try {
			String email = CoreTestConstants.SUPER_USER.getEmail();
			String password = CoreTestConstants.SUPER_USER.getPassword();
			LoginReponse reponse = AdminHttp.login(email, password);
			assertTrue("La reponse ne doit pas être en erreur : " + reponse,
					reponse.isOk());

		} catch (Exception e) {
			fail(e.getClass().getCanonicalName()
					+ " rencontrée lors du test : " + e.getMessage()
					+ "\nStack :\n" + CommonsUtil.stack2string(e));
		}
	}

	@Test
	public void testAnonymousLoginHttp() {
		LOGGER.info("--------------> @Test : testAnonymousLoginHttp sur le port "
				+ CoreTestConstants.PORT1 + "\n");
		try {
			LoginReponse reponse = AdminHttp.anonymousLogin();
			assertTrue("La reponse ne doit pas être en erreur : " + reponse,
					reponse.isOk());

		} catch (Exception e) {
			fail(e.getClass().getCanonicalName()
					+ " rencontrée lors du test : " + e.getMessage()
					+ "\nStack :\n" + CommonsUtil.stack2string(e));
		}
	}

}

package fr.irisa.lis.portalis.admin.core.integrationTest;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.net.InetAddress;
import java.util.Set;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.irisa.lis.cargo.CargoLauncher;
import fr.irisa.lis.portalis.camelis.http.UtilTest;
import fr.irisa.lis.portalis.commons.core.CommonsUtil;
import fr.irisa.lis.portalis.shared.admin.AdminProprietes;
import fr.irisa.lis.portalis.shared.admin.ErrorMessages;
import fr.irisa.lis.portalis.shared.admin.PortalisException;
import fr.irisa.lis.portalis.shared.admin.Util;
import fr.irisa.lis.portalis.shared.admin.XmlIdentifier;
import fr.irisa.lis.portalis.shared.admin.data.ActiveLisService;
import fr.irisa.lis.portalis.shared.admin.data.ActiveUser;
import fr.irisa.lis.portalis.shared.admin.data.PortActif;
import fr.irisa.lis.portalis.shared.admin.data.PortalisService;
import fr.irisa.lis.portalis.shared.admin.http.AdminHttp;
import fr.irisa.lis.portalis.shared.admin.reponse.LoginReponse;
import fr.irisa.lis.portalis.shared.admin.reponse.PidReponse;
import fr.irisa.lis.portalis.shared.admin.reponse.PortsActifsReponse;
import fr.irisa.lis.portalis.shared.admin.reponse.SiteReponse;
import fr.irisa.lis.portalis.shared.admin.reponse.VoidReponse;
import fr.irisa.lis.portalis.shared.camelis.http.CamelisHttp;
import fr.irisa.lis.portalis.shared.camelis.reponse.PingReponse;
import fr.irisa.lis.portalis.shared.camelis.reponse.StartReponse;
import fr.irisa.lis.portalis.test.CoreTestConstants;

public class TestCamelisLauncherHttpRequest2 {
	private static final Logger LOGGER = LoggerFactory
			.getLogger(TestCamelisLauncherHttpRequest2.class.getName());

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		final String host = InetAddress.getLocalHost().getCanonicalHostName();
		final int portNum = 8080;
		final String portalisAppli = "portalis";
		PortalisService.getInstance().init(host, portNum, portalisAppli);
		Util.clientInitLogPropertiesFile();
		LOGGER.info("\nPortalisCtx **************************************************** @BeforeClass TestCamelisLauncherHttpRequest2 ************");
		CargoLauncher.start();
		UtilTest.killAllCamelisProcessAndClearUsers();
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		LOGGER.info("\nPortalisCtx **************************************************** @AfterClass ******************************");
		CargoLauncher.stop();
		LOGGER.info("\nPortalisCtx **************************************************** @fin AfterClass TestCamelisLauncherHttpRequest2 **********");
	}

	@Before
	public void setUp() throws Exception {
		LOGGER.info("\nPortalisCtx **************************************************** @Before **********************************");
	}

	@After
	public void tearDown() throws Exception {
		LOGGER.info("\nPortalisCtx **************************************************** @After ***********************************");
		UtilTest.killAllCamelisProcessAndClearUsers();
	}

	@Test
	public void testGetSite() {
		LOGGER.info("\nPortalisCtx **************************************************** @Test testGetSite ***********************************\n");
		try {
			String email = CoreTestConstants.SUPER_USER.getEmail();
			String password = CoreTestConstants.SUPER_USER.getPassword();

			// ********************* login *********************
			LoginReponse loginReponse = UtilTest.loginHttpAndCheck(email,
					password);
			assertTrue("La réponse doit être sans erreur, loginReponse ="
					+ loginReponse, loginReponse.isOk());
			ActiveUser activeUser = loginReponse.getActiveUser();
			assertTrue("Il doit y avoir un utilisateur actif",
					activeUser != null);

			// ********************* getSite *********************
			SiteReponse siteReponse = AdminHttp.getSite(activeUser);

			assertTrue("La réponse doit être sans erreur, siteReponse ="
					+ siteReponse, siteReponse.isOk());
			LOGGER.info("siteReponse = " + siteReponse.toString());

		} catch (Exception e) {
			fail(e.getMessage() + "\nStack :\n" + CommonsUtil.stack2string(e));
		}
	}

	@Test
	public void testServiceInconnu() {
		LOGGER.info("\nPortalisCtx **************************************************** @Test testServiceInconnu ***********************************\n");
		try {
			String email = CoreTestConstants.SUPER_USER.getEmail();
			String password = CoreTestConstants.SUPER_USER.getPassword();

			// ********************* login *********************
			LoginReponse loginReponse = UtilTest.loginHttpAndCheck(email,
					password);
			assertTrue("La réponse doit être sans erreur, loginReponse ="
					+ loginReponse, loginReponse.isOk());
			ActiveUser activeUser = loginReponse.getActiveUser();
			assertTrue("Il doit y avoir un utilisateur actif",
					activeUser != null);

			// ******************* startCamelis ****************
			StartReponse startReponse = AdminHttp.startCamelis(activeUser,
					CoreTestConstants.SERVICE_PLANETS_DISTANCE_ERR);
			assertTrue("La réponse doit être en erreur, startReponse ="
					+ startReponse, !startReponse.isOk());

			final String TEMOIN = ErrorMessages.TEMOIN_UNKNOWN_SERVICE_NAME_FOR_APPLICATION;
			assertTrue(
					"Les messages devraient contenir le témoin '" + TEMOIN
							+ "' message(s) reçu(s) :\n"
							+ startReponse.getMessagesAsString(),
					startReponse.messagesContains(TEMOIN));
		} catch (Exception e) {
			fail(e.getMessage() + "\nStack :\n" + CommonsUtil.stack2string(e));
		}
	}

	@Test
	public void testgetCamelisProcessIdCmdOk() {
		LOGGER.info("\nPortalisCtx **************************************************** @Test testgetCamelisProcessIdCmdOk ***********************************\n");
		try {
			String host = PortalisService.getInstance().getHost();
			String email = CoreTestConstants.SUPER_USER.getEmail();
			String password = CoreTestConstants.SUPER_USER.getPassword();

			// ********************* login *********************
			LoginReponse loginReponse = UtilTest.loginHttpAndCheck(email,
					password);
			assertTrue("La réponse doit être sans erreur, loginReponse ="
					+ loginReponse, loginReponse.isOk());
			ActiveUser activeUser = loginReponse.getActiveUser();
			assertTrue("Il doit y avoir un utilisateur actif",
					activeUser != null);

			// ******************* startCamelis ****************
			StartReponse startReponse = AdminHttp.startCamelis(activeUser,
					CoreTestConstants.SERVICE_PLANETS_DISTANCE);
			assertTrue("La réponse doit être sans erreur, startReponse ="
					+ startReponse, startReponse.isOk());
			ActiveLisService ActiveLisService = startReponse
					.getActiveLisService();
			assertTrue("Il doit y avoir un ActiveLisService",
					ActiveLisService != null);
			assertEquals(host, ActiveLisService.getHost());

			// ********************* getCamelisProcessId *******************
			PidReponse pidReponse = AdminHttp.getCamelisProcessId(activeUser, startReponse
					.getActiveLisService().getPort());
			Integer[] processNum = new Integer[pidReponse.getPids().size()];
			int actifPid = pidReponse.getPids().toArray(processNum)[0];
			assertTrue("La réponse doit être sans erreur, PidReponse ="
					+ pidReponse, pidReponse.isOk());

			// ********************* getActifsPorts *******************
			PortsActifsReponse portsActifsReponse = AdminHttp
					.getActifsPorts(activeUser);
			assertTrue("La réponse doit être sans erreur, portsActifsReponse ="
					+ portsActifsReponse, portsActifsReponse.isOk());

			Set<PortActif> actifs = portsActifsReponse.getListPortsActifs();

			boolean trouve = false;
			for (PortActif pid : actifs.toArray(new PortActif[actifs.size()])) {
				if (actifPid == pid.getPid()) {
					trouve = true;
					break;
				}
			}
			assertTrue(
					String.format(
							"portsActifsReponse = %s, Le processus %d n'est pas dans %s",
							portsActifsReponse, actifPid, pidReponse), trouve);
		} catch (Exception e) {
			fail(e.getMessage() + "\nStack :\n" + CommonsUtil.stack2string(e));
		}
	}

	@Test
	public void testgetCamelisProcessIdHttpOk() {
		LOGGER.info("\nPortalisCtx **************************************************** @Test testgetCamelisProcessIdHttpOk ***********************************\n");
		String email = CoreTestConstants.SUPER_USER.getEmail();
		String password = CoreTestConstants.SUPER_USER.getPassword();
		try {
			String host = PortalisService.getInstance().getHost();

			// ********************* login *********************
			LoginReponse loginReponse = UtilTest.loginHttpAndCheck(email,
					password);
			assertTrue("La réponse doit être sans erreur, loginReponse ="
					+ loginReponse, loginReponse.isOk());
			ActiveUser activeUser = loginReponse.getActiveUser();
			assertTrue("Il doit y avoir un utilisateur actif",
					activeUser != null);

			// ******************* startCamelis ****************
			StartReponse startReponse = AdminHttp.startCamelis(activeUser,
					CoreTestConstants.SERVICE_PLANETS_DISTANCE);
			assertTrue("La réponse doit être sans erreur, startReponse ="
					+ startReponse, startReponse.isOk());
			ActiveLisService ActiveLisService = startReponse
					.getActiveLisService();
			assertTrue("Il doit y avoir un ActiveLisService",
					ActiveLisService != null);
			assertEquals(host, ActiveLisService.getHost());

			// ********************* getCamelisProcessId *******************
			PidReponse pidReponse = AdminHttp.getCamelisProcessId(activeUser, startReponse
					.getActiveLisService().getPort());
			assertTrue("La réponse doit être sans erreur, PidReponse ="
					+ pidReponse, pidReponse.isOk());
			Set<Integer> listInt = pidReponse.getPids();
			Integer activPid = listInt.toArray(new Integer[listInt.size()])[0];

			// ********************* getActifsPorts *******************
			PortsActifsReponse portsActifsReponse = AdminHttp
					.getActifsPorts(activeUser);
			assertTrue("La réponse doit être sans erreur, portsActifsReponse ="
					+ portsActifsReponse, portsActifsReponse.isOk());

			Set<PortActif> actifs = portsActifsReponse.getListPortsActifs();

			boolean trouve = false;
			for (PortActif actif : actifs) {
				if (actif.getPid().intValue() == activPid) {
					trouve = true;
					break;
				}
			}
			assertTrue(String.format("Le processus %d n'est pas dans %s",
					activPid, portsActifsReponse), trouve);
		} catch (Exception e) {
			fail(e.getMessage() + "\nStack :\n" + CommonsUtil.stack2string(e));
		}
	}

	@Test
	public void testgetCamelisProcessIdNoSession() {
		LOGGER.info("\nPortalisCtx **************************************************** @Test testgetCamelisProcessIdNoSession ***********************************\n");
		int anyPort = 8070;
		String email = CoreTestConstants.SUPER_USER.getEmail();
		String password = CoreTestConstants.SUPER_USER.getPassword();
		try {
			LOGGER.info("\nPortalisCtx ========================================> login "+email+" <============================");
			LoginReponse loginReponse = UtilTest.loginHttpAndCheck(email,
					password);
			assertTrue("La réponse doit être sans erreur, loginReponse ="
					+ loginReponse, loginReponse.isOk());
			ActiveUser activeUser = loginReponse.getActiveUser();
			assertTrue("Il doit y avoir un utilisateur actif",
					activeUser != null);

		LOGGER.info("\nPortalisCtx ========================================> getCamelisProcessId <============================");
		PidReponse in = AdminHttp.getCamelisProcessId(activeUser, anyPort);

		assertTrue("La réponse ne doit pas être en erreur, elle vaut :" + in,
				in.isOk());
		assertEquals(0, in.getPids().size());
		} catch (Exception e) {
			fail(e.getMessage() + "\nStack :\n" + CommonsUtil.stack2string(e));
		}
	}

	@Test
	public void testPingPortCamelisListening() {
		LOGGER.info("\nPortalisCtx **************************************************** @Test testPingPortCamelisListening ***********************************\n");
		try {
			String host = PortalisService.getInstance().getHost();
			String email = CoreTestConstants.SUPER_USER.getEmail();
			String password = CoreTestConstants.SUPER_USER.getPassword();

			// ********************* login *********************
			LoginReponse loginReponse = UtilTest.loginHttpAndCheck(email,
					password);
			assertTrue("La réponse doit être sans erreur, loginReponse ="
					+ loginReponse, loginReponse.isOk());
			ActiveUser activeUser = loginReponse.getActiveUser();
			assertTrue("Il doit y avoir un utilisateur actif",
					activeUser != null);

			// ******************* startCamelis ****************
			StartReponse startReponse = AdminHttp.startCamelis(activeUser,
					CoreTestConstants.SERVICE_PLANETS_DISTANCE);
			assertTrue("La réponse doit être sans erreur, startReponse ="
					+ startReponse, startReponse.isOk());
			ActiveLisService activeLisService = startReponse
					.getActiveLisService();
			assertTrue("Il doit y avoir un ActiveLisService",
					activeLisService != null);
			assertEquals(host, activeLisService.getHost());

			// *********************** ping ********************
			PingReponse pingReponse = CamelisHttp.ping(activeLisService);
			assertTrue("La réponse doit être sans erreur, pingReponse ="
					+ pingReponse, pingReponse.isOk());
			assertEquals(activeUser.getUserCore().getEmail(),
					activeLisService.getCreator());
			assertEquals(CoreTestConstants.SERVICE_PLANETS_DISTANCE,
					activeLisService.getFullName());
		} catch (Exception e) {
			fail(e.getClass().getCanonicalName()
					+ " rencontrée lors du test : " + e.getMessage()
					+ "\nStack :\n" + CommonsUtil.stack2string(e));
		}
	}

	@Test
	public void testStartCalmelisDefaultPort() {
		LOGGER.info("\nPortalisCtx **************************************************** @Test testStartCalmelisDefaultPort ***********************************\n");
		try {
			String host = PortalisService.getInstance().getHost();
			String email = CoreTestConstants.SUPER_USER.getEmail();
			String password = CoreTestConstants.SUPER_USER.getPassword();

			// ********************* login *********************
			LoginReponse loginReponse = UtilTest.loginHttpAndCheck(email,
					password);
			assertTrue("La réponse doit être sans erreur, loginReponse ="
					+ loginReponse, loginReponse.isOk());
			ActiveUser activeUser = loginReponse.getActiveUser();
			assertTrue("Il doit y avoir un utilisateur actif",
					activeUser != null);

			// ******************* startCamelis ****************
			String serviceName = CoreTestConstants.SERVICE_PLANETS_DISTANCE;
			StartReponse startReponse = AdminHttp.startCamelis(activeUser,
					serviceName);
			assertTrue("La réponse doit être sans erreur, startReponse ="
					+ startReponse, startReponse.isOk());
			LOGGER.info("StartReponse = " + startReponse.toString());

			// ******************* getActifsPorts ****************
			PortsActifsReponse portActifReponse = AdminHttp
					.getActifsPorts(activeUser);
			assertTrue("La réponse doit être sans erreur, portActifReponse ="
					+ portActifReponse, portActifReponse.isOk());
			Set<PortActif> actifs = portActifReponse.getListPortsActifs();
			assertTrue("Liste d'actifs null", actifs != null);
			assertEquals(1, actifs.size());
			ActiveLisService ActiveLisService = startReponse
					.getActiveLisService();
			PortActif portActif = new PortActif(host, startReponse
					.getActiveLisService().getPort(), ActiveLisService.getPid());
			assertTrue("le port actif " + portActif
					+ " doit être dans la liste des actfs",
					actifs.contains(portActif));
			assertEquals(serviceName, ActiveLisService.getFullName());
			assertEquals(CoreTestConstants.SUPER_USER.getEmail(),
					ActiveLisService.getCreator());
		} catch (Exception e) {
			fail(e.getClass().getCanonicalName()
					+ " rencontrée lors du test : " + e.getMessage()
					+ "\nStack :\n" + CommonsUtil.stack2string(e));
		}
	}

	@Test
	public void testStartCamelisSpecifiedPort() {
		LOGGER.info("\nPortalisCtx **************************************************** @Test testStartCamelisSpecifiedPort ***********************************\n");

		try {
			String host = PortalisService.getInstance().getHost();
			String email = CoreTestConstants.SUPER_USER.getEmail();
			String password = CoreTestConstants.SUPER_USER.getPassword();

			// ********************* login *********************
			LoginReponse loginReponse = UtilTest.loginHttpAndCheck(email,
					password);
			assertTrue("La réponse doit être sans erreur, loginReponse ="
					+ loginReponse, loginReponse.isOk());
			ActiveUser activeUser = loginReponse.getActiveUser();
			assertTrue("Il doit y avoir un utilisateur actif",
					activeUser != null);

			// ******************* startCamelis ****************
			String serviceFullName = CoreTestConstants.SERVICE_PLANETS_DISTANCE;
			StartReponse startReponse = AdminHttp.startCamelis(activeUser,
					serviceFullName);
			assertTrue("La réponse doit être sans erreur, startReponse ="
					+ startReponse, startReponse.isOk());
			ActiveLisService ActiveLisService = startReponse
					.getActiveLisService();
			assertTrue("Il doit y avoir un ActiveLisService",
					ActiveLisService != null);
			assertEquals(host, ActiveLisService.getHost());

			int port1 = ActiveLisService.getPort();

			// ******************* getActifsPorts ****************
			PortsActifsReponse portActifReponse = AdminHttp
					.getActifsPorts(activeUser);
			assertTrue("La réponse doit être sans erreur, portActifReponse ="
					+ portActifReponse, portActifReponse.isOk());
			Set<PortActif> actifs = portActifReponse.getListPortsActifs();
			assertTrue("La liste d'actifs actifs ne doit pas être nulle",
					actifs != null);
			assertEquals(1, actifs.size());
			PortActif actif = new PortActif(host, port1,
					ActiveLisService.getPid());
			assertTrue(actif + " devrait être dans la liste des actifs",
					actifs.contains(actif));

			assertEquals(port1, ActiveLisService.getPort());
			assertEquals(serviceFullName, ActiveLisService.getFullName());
			assertEquals(CoreTestConstants.SUPER_USER.getEmail(),
					ActiveLisService.getCreator());
		} catch (PortalisException e) {
			fail(e.getClass().getCanonicalName()
					+ " rencontrée lors du test : " + e.getMessage()
					+ "\nStack :\n" + CommonsUtil.stack2string(e));
		}
	}

	@Test
	public void testStartCalmelisArgumentsNeeded() {
		LOGGER.info("\nPortalisCtx **************************************************** @Test testStartCalmelisArgumentsNeeded ***********************************\n");
		try {
			String serviceName = CoreTestConstants.SERVICE_PLANETS_DISTANCE;
			StartReponse reponse = AdminHttp.startCamelis(
					CoreTestConstants.SuperActiveUser, serviceName);

			LOGGER.info("StartReponse = " + reponse);

			assertTrue("La réponse doit être en erreur, elle vaut :" + reponse,
					!reponse.isOk());

		} catch (Exception e) {
			fail(e.getClass().getCanonicalName()
					+ " rencontrée lors du test : " + e.getMessage()
					+ "\nAttention pushDocument() non exécuté");
		}
	}

	@Test
	public void testStartCamelisSpecifiedPortAllreadyInUse() {
		LOGGER.info("\nPortalisCtx **************************************************** @Test testStartCamelisSpecifiedPortAllreadyInUse ***********************************\n");

		try {
			String email = CoreTestConstants.SUPER_USER.getEmail();
			String password = CoreTestConstants.SUPER_USER.getPassword();

			// *************************** login ***********************
			LOGGER.debug("\n\n              ===========================================> login <============================");
			LoginReponse loginReponse = UtilTest.loginHttpAndCheck(email,
					password);
			ActiveUser activeUser = loginReponse.getActiveUser();

			// ************************ startCamelis ********************
			LOGGER.debug("\n\n              ========================================> startCamelis <=========================");
			StartReponse startReponse1 = AdminHttp.startCamelis(activeUser,
					CoreTestConstants.SERVICE_PLANETS_DISTANCE);
			LOGGER.info("StartReponse1 = " + startReponse1);
			int port1 = startReponse1.getActiveLisService().getPort();

			// ************************ startCamelis ********************
			LOGGER.debug("\n\n              ========================================> startCamelis <=========================");
			StartReponse startReponse2 = AdminHttp.startCamelis(activeUser,
					CoreTestConstants.SERVICE_PLANETS_DISTANCE, port1);
			LOGGER.info("StartReponse2 = " + startReponse2);

			assertTrue("La réponse doit être en erreur, elle vaut :"
					+ startReponse2, !startReponse2.isOk());

		} catch (PortalisException e) {
			fail(e.getClass().getCanonicalName()
					+ " rencontrée lors du test : " + e.getMessage()
					+ "\nStack :\n" + CommonsUtil.stack2string(e));
		}
	}

	@Test
	public void testStartCamelisIncorrectServiceName() {
		LOGGER.info("\nPortalisCtx **************************************************** @Test testStartCamelisIncorrectServiceName ***********************************\n");

		LOGGER.debug("\n\n              ===========================================> login <============================");
		try {
			LoginReponse loginReponse = UtilTest.loginHttpAndCheck(
					CoreTestConstants.SUPER_USER.getEmail(),
					CoreTestConstants.SUPER_USER.getPassword());
			assertTrue("La réponse doit être sans erreur, loginReponse ="
					+ loginReponse, loginReponse.isOk());
			ActiveUser activeUser = loginReponse.getActiveUser();

			LOGGER.debug("\n\n              ========================================> startCamelis <=========================");
			String serviceName = "service-1";
			StartReponse startReponse = AdminHttp.startCamelis(activeUser,
					serviceName, CoreTestConstants.PORT);
			assertTrue("startReponse doit être en erreur, il vaut "
					+ startReponse, !startReponse.isOk());
			final String TEMOIN = ErrorMessages.TEMOIN_INCORRECT_SERVICE_NAME;
			assertTrue("Les messages devraient contenir le témoin '" + TEMOIN
					+ "' La réponse est :\n" + startReponse,
					startReponse.messagesContains(TEMOIN));
		} catch (Exception e) {
			fail(e.getClass().getCanonicalName()
					+ " rencontrée lors du test : " + e.getMessage()
					+ "\nStack :\n" + CommonsUtil.stack2string(e));
		}
	}

	@Test
	public void testGetCamelisProcessIdByActiveService() {
		LOGGER.info("\nPortalisCtx **************************************************** @Test testGetCamelisProcessIdDefaultPort ***********************************\n");

		LOGGER.debug("\n\n              ===========================================> login <============================");
		try {
			LoginReponse loginReponse = UtilTest.loginHttpAndCheck(
					CoreTestConstants.SUPER_USER.getEmail(),
					CoreTestConstants.SUPER_USER.getPassword());
			assertTrue("La réponse doit être sans erreur, loginReponse ="
					+ loginReponse, loginReponse.isOk());
			ActiveUser activeUser = loginReponse.getActiveUser();

			LOGGER.debug("\n\n              ========================================> startCamelis <=========================");
			String serviceName = CoreTestConstants.SERVICE_PLANETS_DISTANCE;
			StartReponse startReponse = AdminHttp.startCamelis(activeUser,
					serviceName, CoreTestConstants.PORT1);
			assertTrue("startReponse ne doit pas être en erreur, il vaut "
					+ startReponse, startReponse.isOk());
			assertEquals(CoreTestConstants.PORT1, startReponse
					.getActiveLisService().getPort());
			ActiveLisService service = startReponse.getActiveLisService();

			LOGGER.debug("\n\n              ========================================> GetCamelisProcessId <=========================");
			PidReponse pidReponse = AdminHttp.getCamelisProcessId(activeUser,
					service);
			assertTrue("pidReponse ne doit pas être en erreur, il vaut "
					+ pidReponse, pidReponse.isOk());
			LOGGER.info("PidReponse=" + pidReponse.toString());

		} catch (PortalisException e) {
			fail(e.getClass().getCanonicalName()
					+ " rencontrée lors du test : " + e.getMessage()
					+ "\nStack :\n" + CommonsUtil.stack2string(e));
		}
	}

	@Test
	public void testGetCamelisProcessIdByServiceFullName() {
		LOGGER.info("\nPortalisCtx **************************************************** @Test testGetCamelisProcessIdByServiceFullName ***********************************\n");

		LOGGER.debug("\n\n              ===========================================> login <============================");
		try {
			LoginReponse loginReponse = UtilTest.loginHttpAndCheck(
					CoreTestConstants.SUPER_USER.getEmail(),
					CoreTestConstants.SUPER_USER.getPassword());
			assertTrue("La réponse doit être sans erreur, loginReponse ="
					+ loginReponse, loginReponse.isOk());
			ActiveUser activeUser = loginReponse.getActiveUser();

			LOGGER.debug("\n\n              ========================================> startCamelis 1 <=========================");
			String serviceName = CoreTestConstants.SERVICE_PLANETS_DISTANCE;
			StartReponse startReponse = AdminHttp.startCamelis(activeUser,
					serviceName, CoreTestConstants.PORT1);
			assertTrue("startReponse ne doit pas être en erreur, il vaut "
					+ startReponse, startReponse.isOk());
			assertEquals(CoreTestConstants.PORT1, startReponse
					.getActiveLisService().getPort());

			LOGGER.debug("\n\n              ========================================> startCamelis 2 <=========================");
			StartReponse startReponse2 = AdminHttp.startCamelis(activeUser,
					serviceName);
			assertTrue("startReponse ne doit pas être en erreur, il vaut "
					+ startReponse2, startReponse2.isOk());

			LOGGER.debug("\n\n              ========================================> GetCamelisProcessId <=========================");
			PidReponse pidReponse = AdminHttp.getCamelisProcessId(activeUser,
					serviceName);
			assertTrue("pidReponse ne doit pas être en erreur, il vaut "
					+ pidReponse, pidReponse.isOk());
			LOGGER.info("PidReponse=" + pidReponse.toString());

		} catch (PortalisException e) {
			fail(e.getClass().getCanonicalName()
					+ " rencontrée lors du test : " + e.getMessage()
					+ "\nStack :\n" + CommonsUtil.stack2string(e));
		}
	}

	@Test
	public void testGetCamelisProcessIdByPort() {
		LOGGER.info("\nPortalisCtx **************************************************** @Test testGetCamelisProcessIdByPort ***********************************\n");

		try {
			String host = PortalisService.getInstance().getHost();
			String email = CoreTestConstants.SUPER_USER.getEmail();
			String password = CoreTestConstants.SUPER_USER.getPassword();

			// ********************* login *********************
			LoginReponse loginReponse = UtilTest.loginHttpAndCheck(email,
					password);
			assertTrue("La réponse doit être sans erreur, loginReponse ="
					+ loginReponse, loginReponse.isOk());
			ActiveUser activeUser = loginReponse.getActiveUser();
			assertTrue("Il doit y avoir un utilisateur actif",
					activeUser != null);

			// ******************* startCamelis ****************
			StartReponse startReponse = AdminHttp.startCamelis(activeUser,
					CoreTestConstants.SERVICE_PLANETS_DISTANCE);
			assertTrue("La réponse doit être sans erreur, startReponse ="
					+ startReponse, startReponse.isOk());
			ActiveLisService activeLisService = startReponse
					.getActiveLisService();
			assertTrue("Il doit y avoir un ActiveLisService",
					activeLisService != null);
			int port1 = activeLisService.getPort();
			assertEquals(host, activeLisService.getHost());

			// ***************** getCamelisProcessId ****************
			PidReponse pidReponse = AdminHttp.getCamelisProcessId(activeUser,
					port1);
			assertTrue("La réponse doit être sans erreur, pidReponse ="
					+ pidReponse, pidReponse.isOk());
			LOGGER.info("PidReponse=" + pidReponse.toString());

			// ********************** ping ********************
			PingReponse pingReponse = CamelisHttp.ping(activeLisService);
			assertTrue(pingReponse != null);
			assertTrue("La réponse doit être sans erreur, pingReponse ="
					+ pingReponse, pingReponse.isOk());
			ActiveLisService ActiveLisService1 = pingReponse.getFirstService();

			assertEquals(activeLisService, ActiveLisService1);
			assertEquals(activeLisService.getPid(), ActiveLisService1.getPid());

			UtilTest.isPresent(activeLisService.getPid(), pidReponse);
		} catch (Exception e) {
			fail(e.getClass().getCanonicalName()
					+ " rencontrée lors du test : " + e.getMessage()
					+ "\nStack :\n" + CommonsUtil.stack2string(e));
		}
	}

	@Test
	public void testGetCamelisProcessIdOnBadPort() {
		LOGGER.info("\nPortalisCtx **************************************************** @Test testGetCamelisProcessIdOnBadPort ***********************************\n");
		try {
			LOGGER.debug("\n\n              ========================================> login <=========================");
			String email = CoreTestConstants.SUPER_USER.getEmail();
			String password = CoreTestConstants.SUPER_USER.getPassword();
			LoginReponse loginReponse = UtilTest.loginHttpAndCheck(email,
					password);
			assertEquals(CoreTestConstants.SUPER_USER, loginReponse
					.getActiveUser().getUserCore());
			ActiveUser activeUser = loginReponse.getActiveUser();
			assertTrue("Il doit y avoir un utilisateur actif",
					activeUser != null);

			// ******************* startCamelis ****************
			LOGGER.debug("\n\n              ========================================> startCamelis <=========================");
			StartReponse startReponse = AdminHttp.startCamelis(activeUser,
					CoreTestConstants.SERVICE_PLANETS_DISTANCE);
			assertTrue("La réponse doit être sans erreur, startReponse ="
					+ startReponse, startReponse.isOk());
			ActiveLisService ActiveLisService = startReponse
					.getActiveLisService();
			assertTrue("Il doit y avoir un ActiveLisService",
					ActiveLisService != null);

			// ******************* stopCamelis ****************
			LOGGER.debug("\n\n              ========================================> stopCamelis <=========================");
			PingReponse pingReponse = AdminHttp.stopCamelis(activeUser,
					ActiveLisService);
			assertTrue("La réponse doit être sans erreur, pingReponse ="
					+ pingReponse, pingReponse.isOk());

			LOGGER.debug("\n\n              ========================================> GetCamelisProcessId <=========================");
			PidReponse pidReponse = AdminHttp.getCamelisProcessId(activeUser,
					8070);
			assertTrue("La réponse ne pas doit être en erreur, pidReponse ="
					+ pidReponse, pidReponse.isOk());
			assertTrue(
					"Il ne doit pas y avoir de processus, la réponse est :\n"
							+ pidReponse, pidReponse.getPids().size() == 0);

		} catch (Exception e) {
			fail(e.getClass().getCanonicalName()
					+ " rencontrée lors du test : " + e.getMessage()
					+ "\nStack :\n" + CommonsUtil.stack2string(e));
		}
	}

	@Test
	public void testGetCamelisProcessIdBadService() {
		LOGGER.info("\nPortalisCtx **************************************************** @Test testGetCamelisProcessIdBadService ***********************************\n");

		try {
			LOGGER.debug("\n\n              ========================================> login <=========================");
			String email = CoreTestConstants.SUPER_USER.getEmail();
			String password = CoreTestConstants.SUPER_USER.getPassword();
			LoginReponse loginReponse = UtilTest.loginHttpAndCheck(email,
					password);
			assertEquals(CoreTestConstants.SUPER_USER, loginReponse
					.getActiveUser().getUserCore());
			ActiveUser activeUser = loginReponse.getActiveUser();
			assertTrue("Il doit y avoir un utilisateur actif",
					activeUser != null);

			LOGGER.debug("\n\n              ========================================> GetCamelisProcessId <=========================");
			PidReponse pidReponse = AdminHttp.getCamelisProcessId(activeUser,
					"");
			assertTrue("La réponse doit être en erreur, pidReponse ="
					+ pidReponse, !pidReponse.isOk());

		} catch (Exception e) {
			fail(e.getClass().getCanonicalName()
					+ " rencontrée lors du test : " + e.getMessage()
					+ "\nStack :\n" + CommonsUtil.stack2string(e));
		}
	}

	@Test
	public void testGetActifsPorts() {
		LOGGER.info("\nPortalisCtx **************************************************** @Test testGetActifsPorts ***********************************\n");
		try {

			// ************************** login ************************
			LOGGER.debug("\n\n              ===========================================> login <============================");
			LoginReponse loginReponse = UtilTest.loginHttpAndCheck(
					CoreTestConstants.SUPER_USER.getEmail(),
					CoreTestConstants.SUPER_USER.getPassword());
			assertTrue("La réponse doit être sans erreur, loginReponse ="
					+ loginReponse, loginReponse.isOk());
			ActiveUser activeUser = loginReponse.getActiveUser();
			assertTrue("Il doit y avoir un utilisateur actif",
					activeUser != null);

			// ************************ startCamelis ********************
			LOGGER.debug("\n\n              ========================================> startCamelis <=========================");
			StartReponse startReponse1 = UtilTest.startCamelis(activeUser);
			assertTrue("La réponse doit être sans erreur, startReponse1 ="
					+ startReponse1, startReponse1.isOk());
			PortActif actif1 = new PortActif(PortalisService.getInstance()
					.getHost(), startReponse1.getActiveLisService().getPort(),
					startReponse1.getActiveLisService().getPid());
			LOGGER.debug("\n\n              ========================================> startCamelis <=========================");
			StartReponse startReponse2 = UtilTest.startCamelis(activeUser,
					CoreTestConstants.PORT);
			assertTrue("La réponse doit être sans erreur, startReponse2 ="
					+ startReponse2, startReponse2.isOk());
			PortActif actif2 = new PortActif(PortalisService.getInstance()
					.getHost(), CoreTestConstants.PORT, startReponse2
					.getActiveLisService().getPid());
			LOGGER.debug("\n\n              ========================================> startCamelis <=========================");
			StartReponse startReponse3 = UtilTest.startCamelis(activeUser);
			assertTrue("La réponse doit être sans erreur, startReponse3 ="
					+ startReponse3, startReponse3.isOk());
			PortActif actif3 = new PortActif(PortalisService.getInstance()
					.getHost(), startReponse3.getActiveLisService().getPort(),
					startReponse3.getActiveLisService().getPid());

			// ************************ getActifsPorts ********************
			LOGGER.debug("\n\n              ========================================> getActifsPorts <=========================");
			PortsActifsReponse reponse = AdminHttp.getActifsPorts(activeUser);
			assertTrue("La réponse ne doit pas avoir d'erreur, elle vaut :"
					+ reponse, reponse.isOk());

			Set<PortActif> actifs = reponse.getListPortsActifs();
			assertEquals(3, actifs.size());

			assertTrue(actif1 + " devrait être dans la liste des actifs",
					actifs.contains(actif1));
			assertTrue(actif2 + " devrait être dans la liste des actifs",
					actifs.contains(actif2));
			assertTrue(actif3 + " devrait être dans la liste des actifs",
					actifs.contains(actif3));

		} catch (Exception e) {
			fail(e.getClass().getCanonicalName() + " rencontrée : "
					+ e.getMessage() + "\nStack :\n" + CommonsUtil.stack2string(e));
		}
	}

	@Test
	public void testGetActifsPortsNoActif() {
		LOGGER.info("\nPortalisCtx **************************************************** @Test testGetActifsPortsNoActif ***********************************\n");
		try {

			// ************************** login ************************
			LOGGER.debug("\n\n              ===========================================> login <============================");
			LoginReponse loginReponse = UtilTest.loginHttpAndCheck(
					CoreTestConstants.SUPER_USER.getEmail(),
					CoreTestConstants.SUPER_USER.getPassword());
			assertTrue("La réponse doit être sans erreur, loginReponse ="
					+ loginReponse, loginReponse.isOk());
			ActiveUser activeUser = loginReponse.getActiveUser();
			assertTrue("Il doit y avoir un utilisateur actif",
					activeUser != null);

			// ************************ getActifsPorts ********************
			LOGGER.debug("\n\n              ========================================> getActifsPorts <=========================");
			PortsActifsReponse reponse = AdminHttp.getActifsPorts(activeUser);
			assertTrue("La réponse ne doit pas avoir d'erreur, elle vaut :"
					+ reponse, reponse.isOk());

			Set<PortActif> actifs = reponse.getListPortsActifs();
			assertEquals(0, actifs.size());

		} catch (Exception e) {
			fail(e.getClass().getCanonicalName() + " rencontrée : "
					+ e.getMessage() + "\nStack :\n" + CommonsUtil.stack2string(e));
		}
	}

	@Test
	public void testLoginSucess() {
		LOGGER.info("\nPortalisCtx **************************************************** @Test testLoginSucess ***********************************\n");
		try {
			String email = CoreTestConstants.SUPER_USER.getEmail();
			String password = CoreTestConstants.SUPER_USER.getPassword();
			LoginReponse loginReponse = UtilTest.loginHttpAndCheck(email,
					password);
			assertTrue("la réponse ne doit pas être en erreur " + loginReponse,
					loginReponse.getStatus().equals(XmlIdentifier.OK));
			assertEquals(CoreTestConstants.SUPER_USER, loginReponse
					.getActiveUser().getUserCore());

		} catch (Exception e) {
			fail(e.getClass().getCanonicalName()
					+ " rencontrée lors du test : " + e.getMessage()
					+ "\nStack :\n" + CommonsUtil.stack2string(e));
		}
	}

	@Test
	public void testLoginMaxUserError() {
		LOGGER.info("\nPortalisCtx **************************************************** @Test testLoginMaxUserError ***********************************\n");
		try {
			String email = CoreTestConstants.STANDARD_USER.getEmail();
			String password = CoreTestConstants.STANDARD_USER.getPassword();

			int maxUsers = Integer.parseInt(AdminProprietes.portalis
					.getProperty("portalis.one.user.max"));

			for (int i = 0; i < maxUsers; i++) {
				LoginReponse loginReponse = UtilTest.loginHttpAndCheck(email,
						password);
				assertTrue("la réponse ne doit pas être en erreur "
						+ loginReponse, loginReponse.isOk());
				assertEquals(CoreTestConstants.STANDARD_USER, loginReponse
						.getActiveUser().getUserCore());
			}
			LoginReponse loginReponse1 = AdminHttp.login(email, password);
			assertTrue("la réponse doit être en erreur " + loginReponse1,
					loginReponse1.getStatus().equals(XmlIdentifier.ERROR));

		} catch (Exception e) {
			fail(e.getClass().getCanonicalName()
					+ " rencontrée lors du test : " + e.getMessage()
					+ "\nStack :\n" + CommonsUtil.stack2string(e));
		}
	}

	@Test
	public void testGetPidFromSystem() {
		LOGGER.info("\nPortalisCtx **************************************************** @Test testGetPidFromSystem ***********************************\n");
		try {
			String host = PortalisService.getInstance().getHost();
			String email = CoreTestConstants.SUPER_USER.getEmail();
			String password = CoreTestConstants.SUPER_USER.getPassword();

			// ********************* login *********************
			LOGGER.debug("\n\n              ===========================================> login <============================");
			LoginReponse loginReponse = UtilTest.loginHttpAndCheck(email,
					password);
			assertTrue("La réponse doit être sans erreur, loginReponse ="
					+ loginReponse, loginReponse.isOk());
			ActiveUser activeUser = loginReponse.getActiveUser();
			assertTrue("Il doit y avoir un utilisateur actif",
					activeUser != null);

			// ******************* startCamelis ****************
			LOGGER.debug("\n\n              ========================================> startCamelis <=========================");
			StartReponse startReponse = AdminHttp.startCamelis(activeUser,
					CoreTestConstants.SERVICE_PLANETS_DISTANCE);
			assertTrue("La réponse doit être sans erreur, startReponse ="
					+ startReponse, startReponse.isOk());
			ActiveLisService activeLisService = startReponse
					.getActiveLisService();
			assertTrue("Il doit y avoir un ActiveLisService",
					activeLisService != null);
			int port = activeLisService.getPort();
			assertEquals(host, activeLisService.getHost());

			// ********************** ping *******************
			LOGGER.debug("\n\n              ========================================> ping <=========================");
			PingReponse pingReponse = CamelisHttp.ping(activeLisService);
			assertTrue("La réponse doit être sans erreur, pingReponse ="
					+ pingReponse, pingReponse.isOk());

			// ********************** getCamelisProcessId *******************
			LOGGER.debug("\n\n              ========================================> getCamelisProcessId <=========================");
			PidReponse pidReponse = AdminHttp.getCamelisProcessId(activeUser,
					port);
			assertEquals(port, pingReponse.getFirstService().getPort());
			assertTrue("La réponse doit être sans erreur, pidReponse ="
					+ pidReponse, pidReponse.isOk());
			ActiveLisService ActiveLisService1 = pingReponse.getFirstService();
			assertTrue("ActiveLisService1 ne doit pas être null",
					ActiveLisService1 != null);
			Set<Integer> pids = pidReponse.getPids();
			Integer[] tabPids = pids.toArray(new Integer[pids.size()]);
			assertEquals(tabPids[0].intValue(), ActiveLisService1.getPid());

		} catch (Exception e) {
			fail(e.getClass().getCanonicalName()
					+ " rencontrée lors du test : " + e.getMessage()
					+ "\nStack :\n" + CommonsUtil.stack2string(e));
		}
	}

	@Test
	public void testFindPortAndStartCamelis() {
		LOGGER.info("\nPortalisCtx **************************************************** @Test testFindPortAndStartCamelis ***********************************\n");
		try {
			String email = CoreTestConstants.SUPER_USER.getEmail();
			String password = CoreTestConstants.SUPER_USER.getPassword();

			// *************************** login ***********************
			LOGGER.debug("\n\n              ===========================================> login <============================");
			LoginReponse loginReponse = UtilTest.loginHttpAndCheck(email,
					password);
			ActiveUser activeUser = loginReponse.getActiveUser();

			// ************************ startCamelis ********************
			LOGGER.debug("\n\n              ========================================> startCamelis <=========================");

			StartReponse startReponse = AdminHttp.startCamelis(activeUser,
					CoreTestConstants.SERVICE_PLANETS_DISTANCE);

			assertTrue("startReponse doit être sans erreur, il vaut "
					+ startReponse, startReponse.isOk());
			LOGGER.info("StartReponse = " + startReponse);

		} catch (Exception e) {
			fail(e.getClass().getCanonicalName()
					+ " rencontrée lors du test : " + e.getMessage()
					+ "\nStack :\n" + CommonsUtil.stack2string(e));
		}
	}

	@Test
	public void testStopCalmelis() {
		LOGGER.info("\nPortalisCtx **************************************************** @Test testStopCalmelis ***********************************\n");

		String serviceName = CoreTestConstants.SERVICE_PLANETS_DISTANCE;
		try {
			String email = CoreTestConstants.SUPER_USER.getEmail();
			String password = CoreTestConstants.SUPER_USER.getPassword();

			// ********************* login *********************
			LoginReponse loginReponse = UtilTest.loginHttpAndCheck(email,
					password);
			assertTrue("La réponse doit être sans erreur, loginReponse ="
					+ loginReponse, loginReponse.isOk());
			ActiveUser activeUser = loginReponse.getActiveUser();
			assertTrue("Il doit y avoir un utilisateur actif",
					activeUser != null);

			// ******************* startCamelis ****************
			StartReponse startReponse = AdminHttp.startCamelis(activeUser,
					serviceName);
			assertTrue("La réponse doit être sans erreur, startReponse ="
					+ startReponse, startReponse.isOk());
			LOGGER.info("StartReponse = " + startReponse.toString());
			ActiveLisService activeLisService = startReponse
					.getActiveLisService();

			// *********************** ping ********************
			PingReponse pingReponse = CamelisHttp.ping(activeLisService);
			assertTrue("La réponse ne doit pas être en erreur, pingReponse ="
					+ pingReponse, pingReponse.isOk());
			assertEquals(activeLisService, pingReponse.getFirstService());

			// ******************* stopCamelis ****************
			PingReponse pingReponse1 = AdminHttp.stopCamelis(activeUser,
					activeLisService);
			assertTrue("La réponse doit être sans erreur, pingReponse1 ="
					+ pingReponse1, pingReponse1.isOk());
			LOGGER.info("pingReponse1 = " + pingReponse1.toString());

			// *********************** ping ********************
			PingReponse pingReponse2 = CamelisHttp.ping(activeLisService);
			assertTrue("La réponse doit être en erreur, pingReponse ="
					+ pingReponse2, !pingReponse2.isOk());
			final String TEMOIN = ErrorMessages.TEMOIN_ERROR_DURING_HTTP_REQUEST;
			assertTrue("Les messages devraient contenir le témoin '" + TEMOIN
					+ "' La réponse est :\n" + pingReponse2,
					pingReponse2.messagesContains(TEMOIN));

		} catch (Exception e) {
			fail(e.getClass().getCanonicalName()
					+ " rencontrée lors du test : " + e.getMessage()
					+ "\nStack :\n" + CommonsUtil.stack2string(e));
		}
	}

	@Test
	public void testUsingAnExpiredSession() {
		LOGGER.info("\nPortalisCtx **************************************************** @Test testUsingAnExpiredSession ***********************************\n");

		try {
			String email = CoreTestConstants.SUPER_USER.getEmail();
			String password = CoreTestConstants.SUPER_USER.getPassword();

			LOGGER.info("\nPortalisCtx ========================================> login <============================");
			LoginReponse loginReponse = UtilTest.loginHttpAndCheck(email,
					password);
			assertTrue("La réponse doit être sans erreur, loginReponse ="
					+ loginReponse, loginReponse.isOk());
			ActiveUser activeUser = loginReponse.getActiveUser();
			assertTrue("Il doit y avoir un utilisateur actif",
					activeUser != null);

			LOGGER.info("\nPortalisCtx ========================================> logout <============================");
			VoidReponse voidReponse = AdminHttp.logout(activeUser);
			assertTrue("La réponse doit être sans erreur, voidReponse ="
					+ voidReponse, voidReponse.isOk());

			LOGGER.info("\nPortalisCtx ========================================> startCamelis <============================");
			StartReponse startReponse = AdminHttp.startCamelis(activeUser,
					CoreTestConstants.SERVICE_PLANETS_DISTANCE);
			assertTrue("La réponse doit être en erreur, startReponse ="
					+ startReponse, !startReponse.isOk());
			final String TEMOIN = ErrorMessages.TEMOIN_NO_USER_LOGGED_IN;
			assertTrue("Les messages devraient contenir le témoin '" + TEMOIN
					+ "' La réponse est :\n" + startReponse,
					startReponse.messagesContains(TEMOIN));

		} catch (Exception e) {
			fail(e.getClass().getCanonicalName()
					+ " rencontrée lors du test : " + e.getMessage()
					+ "\nStack :\n" + CommonsUtil.stack2string(e));
		}
	}

}

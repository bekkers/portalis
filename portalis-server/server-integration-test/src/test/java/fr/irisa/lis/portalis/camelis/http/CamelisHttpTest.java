package fr.irisa.lis.portalis.camelis.http;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.net.InetAddress;
import java.util.Date;
import java.util.List;
import java.util.Set;

import org.joda.time.DateTime;
import org.joda.time.Duration;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.irisa.lis.cargo.CargoLauncher;
import fr.irisa.lis.cargo.CargoProprietes;
import fr.irisa.lis.portalis.commons.core.CommonsUtil;
import fr.irisa.lis.portalis.shared.admin.ErrorMessages;
import fr.irisa.lis.portalis.shared.admin.PortalisException;
import fr.irisa.lis.portalis.shared.admin.RightValue;
import fr.irisa.lis.portalis.shared.admin.Util;
import fr.irisa.lis.portalis.shared.admin.XmlIdentifier;
import fr.irisa.lis.portalis.shared.admin.data.ActiveLisService;
import fr.irisa.lis.portalis.shared.admin.data.ActiveService;
import fr.irisa.lis.portalis.shared.admin.data.ActiveUser;
import fr.irisa.lis.portalis.shared.admin.data.CamelisServiceCore;
import fr.irisa.lis.portalis.shared.admin.data.PortActif;
import fr.irisa.lis.portalis.shared.admin.data.PortalisService;
import fr.irisa.lis.portalis.shared.admin.data.LisServiceCore;
import fr.irisa.lis.portalis.shared.admin.data.UserData;
import fr.irisa.lis.portalis.shared.admin.http.AdminHttp;
import fr.irisa.lis.portalis.shared.admin.reponse.BooleanReponse;
import fr.irisa.lis.portalis.shared.admin.reponse.LoginReponse;
import fr.irisa.lis.portalis.shared.admin.reponse.PidReponse;
import fr.irisa.lis.portalis.shared.admin.reponse.PortsActifsReponse;
import fr.irisa.lis.portalis.shared.admin.reponse.VoidReponse;
import fr.irisa.lis.portalis.shared.camelis.CamelisXmlName;
import fr.irisa.lis.portalis.shared.camelis.data.LisExtent;
import fr.irisa.lis.portalis.shared.camelis.data.LisObject;
import fr.irisa.lis.portalis.shared.camelis.http.CamelisHttp;
import fr.irisa.lis.portalis.shared.camelis.reponse.ExtentReponse;
import fr.irisa.lis.portalis.shared.camelis.reponse.ImportCtxReponse;
import fr.irisa.lis.portalis.shared.camelis.reponse.ImportLisReponse;
import fr.irisa.lis.portalis.shared.camelis.reponse.LoadContextReponse;
import fr.irisa.lis.portalis.shared.camelis.reponse.PingReponse;
import fr.irisa.lis.portalis.shared.camelis.reponse.RegisterKeyReponse;
import fr.irisa.lis.portalis.shared.camelis.reponse.SetRoleReponse;
import fr.irisa.lis.portalis.shared.camelis.reponse.StartReponse;
import fr.irisa.lis.portalis.test.CoreTestConstants;

public class CamelisHttpTest {

	private static final Logger LOGGER = LoggerFactory
			.getLogger(CamelisHttpTest.class.getName());

	private static final String tomcatContainerPath = CargoProprietes.cargoProp
			.getProperty("tomcatPath");
	private static final String tomcatContainerName = CargoProprietes.cargoProp
			.getProperty("tomcatName");
	private static final String tomcatContainer = tomcatContainerPath
			+ tomcatContainerName;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		final String host = InetAddress.getLocalHost().getCanonicalHostName();
		final int portNum = 8080;
		final String portalisAppli = "portalis";
		PortalisService.getInstance().init(host, portNum, portalisAppli);
		Util.clientInitLogPropertiesFile();
		if (CargoLauncher.tomcatIsActif()) {
			CargoLauncher.stop();
		}
		LOGGER.info("\nPortalisCtx **************************************************** @BeforeClass CamelisHttpTest ************");
		CargoLauncher.start();
		UtilTest.installResourcesForFreshPlanetsCtx();
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		LOGGER.info("\nPortalisCtx **************************************************** @AfterClass ******************************");
		UtilTest.killAllCamelisProcess();
		CargoLauncher.stop();
		LOGGER.info("\nPortalisCtx **************************************************** @fin AfterClass CamelisHttpTest **********");
	}

	@Before
	public void setUp() throws Exception {
		LOGGER.info("\nPortalisCtx **************************************************** @Before **********************************");
		UtilTest.killAllCamelisProcessAndClearUsers();
	}

	@After
	public void tearDown() throws Exception {
		LOGGER.info("\nPortalisCtx **************************************************** @After ***********************************");
	}

	@Test
	public void test8080IsBusy() {
		LOGGER.info("\nPortalisCtx **************************************************** @Test test8080IsBusy ***********************************\n");
		try {
			LoginReponse loginReponse = AdminHttp.login(
					CoreTestConstants.MAIL_YVES,
					CoreTestConstants.PASSWORD_YVES);
			assertTrue("La réponse doit être sans erreur, loginReponse ="
					+ loginReponse, loginReponse.isOk());
			ActiveUser activeUser = loginReponse.getActiveUser();

			int port = PortalisService.getInstance().getPort();
			BooleanReponse booleanReponse = AdminHttp.portIsBusy(activeUser,
					port);
			assertTrue("La réponse doit être sans erreur, pidReponse ="
					+ booleanReponse, booleanReponse.isOk());
			assertTrue("Il doit y avoir un processus sur le port " + port,
					booleanReponse.isAnswer());
			VoidReponse logoutReponse = AdminHttp.logout(activeUser);
			assertTrue("La réponse doit être sans erreur, logoutReponse ="
					+ logoutReponse, logoutReponse.isOk());

		} catch (Exception e) {
			fail(e.getClass().getCanonicalName()
					+ " rencontrée lors du test : " + e.getMessage()
					+ "\nStack :\n" + CommonsUtil.stack2string(e));
		}
	}

	@Test
	public void testTomcatIsActif() {
		LOGGER.info("\nPortalisCtx **************************************************** @Test testTomcatIsActif ***********************************\n");
		try {
			assertTrue(tomcatContainer + " doit être actif",
					CargoLauncher.tomcatIsActif());

		} catch (Exception e) {
			fail(e.getClass().getCanonicalName()
					+ " rencontrée lors du test : " + e.getMessage()
					+ "\nStack :\n" + CommonsUtil.stack2string(e));
		}
	}

	@Test
	public void testImportCtx() {
		LOGGER.info("\nPortalisCtx **************************************************** @Test testImportCtx ***********************************\n");
		try {
			String email = CoreTestConstants.SUPER_USER.getEmail();
			String password = CoreTestConstants.SUPER_USER.getPassword();

			LOGGER.info("\nPortalisCtx ========================================> login <============================");
			LoginReponse loginReponse = UtilTest.loginHttpAndCheck(email,
					password);
			assertTrue("La réponse doit être sans erreur, loginReponse ="
					+ loginReponse, loginReponse.isOk());
			ActiveUser activeUser = loginReponse.getActiveUser();

			LOGGER.info("\nPortalisCtx ========================================> startCamelis <============================");
			int camelisPort = CoreTestConstants.PORT1;

			StartReponse startReponse = AdminHttp.startCamelis(activeUser,
					CoreTestConstants.SERVICE_PLANETS_PLANETS, camelisPort);
			assertTrue("La réponse doit être sans erreur, startReponse ="
					+ startReponse, startReponse.isOk());
			ActiveLisService activeLisService = startReponse
					.getActiveLisService();
			assertTrue("ActiveLisService ne doit pas être null",
					activeLisService != null);

			LOGGER.info("\nPortalisCtx ========================================> importCtx <============================");
			String ctxFile = "planets.ctx";
			ImportCtxReponse importReponse = new CamelisHttp(activeUser,
					activeLisService).importCtx(ctxFile);
			assertTrue("La réponse doit être sans erreur, importReponse ="
					+ importReponse, importReponse.isOk());

		} catch (Exception e) {
			fail(e.getClass().getCanonicalName()
					+ " rencontrée lors du test : " + e.getMessage()
					+ "\nStack :\n" + CommonsUtil.stack2string(e));
		}
	}

	@Test
	public void testLSIALA() {
		LOGGER.info("\nPortalisCtx **************************************************** @Test testLSIALA ***********************************\n");
		/* test le broadcast vers les camelis lors d'un nouveau login */
		try {
			String email1 = CoreTestConstants.SUPER_USER.getEmail();
			String password1 = CoreTestConstants.SUPER_USER.getPassword();

			LOGGER.info("\nPortalisCtx ========================================> login <============================");
			LoginReponse loginReponse1 = UtilTest.loginHttpAndCheck(email1,
					password1);
			assertTrue("La réponse doit être sans erreur, loginReponse ="
					+ loginReponse1, loginReponse1.isOk());
			ActiveUser activeUser1 = loginReponse1.getActiveUser();

			LOGGER.info("\nPortalisCtx ========================================> startCamelis <============================");
			int camelisPort = CoreTestConstants.PORT1;

			StartReponse startReponse = AdminHttp.startCamelis(activeUser1,
					CoreTestConstants.SERVICE_PLANETS_PLANETS, camelisPort);
			assertTrue("La réponse doit être sans erreur, startReponse ="
					+ startReponse, startReponse.isOk());
			ActiveLisService activeLisService = startReponse
					.getActiveLisService();
			assertTrue("ActiveLisService ne doit pas être null",
					activeLisService != null);

			CamelisHttp camelisHttp1 = new CamelisHttp(activeUser1,
					activeLisService);
			LOGGER.info("\nPortalisCtx ========================================> importCtx <============================");
			String ctxFile = "planets.ctx";
			ImportCtxReponse importReponse = camelisHttp1.importCtx(ctxFile);
			assertTrue("La réponse doit être sans erreur, importReponse ="
					+ importReponse, importReponse.isOk());

			LOGGER.info("\nPortalisCtx ========================================> extent Medium (user1) <============================");
			String lisQuery = "Medium";
			ExtentReponse reponse = camelisHttp1.extent(lisQuery);
			assertNotNull(reponse);
			assertTrue("l'extension ne doit doit pas être en erreur :"
					+ reponse, reponse.isOk());
			LisExtent receivedExtent = reponse.getExtent();
			assertTrue("Il doit y avoir deux éléments dans l'extension :"
					+ reponse, receivedExtent != null
					&& receivedExtent.card() == 2);
			// extent(medium) should contain two objects: neptune and uranus
			String ctxDir = CoreTestConstants.WEBAPPS_DIR + "/planets/planets/";
			LisObject[] expectedObjs = {
					new LisObject(5, "neptune", ctxDir + "images/neptune.jpg",
							""),
					new LisObject(8, "uranus", ctxDir + "images/uranus.jpg", "") };
			LisExtent expectedExtent = new LisExtent(expectedObjs);
			assertTrue("Received extent\n" + receivedExtent
					+ "does not match expected extent\n" + expectedExtent,
					expectedExtent.equals(receivedExtent));

			LOGGER.info("\nPortalisCtx ========================================> logout 1 <============================");
			VoidReponse voidReponse1 = AdminHttp.logout(activeUser1);
			assertTrue("La réponse doit être sans erreur, voidReponse ="
					+ voidReponse1, voidReponse1.isOk());

			LOGGER.info("\nPortalisCtx ========================================> second login <============================");

			String email2 = CoreTestConstants.STANDARD_USER.getEmail();
			String password2 = CoreTestConstants.STANDARD_USER.getPassword();
			LoginReponse reponse2 = AdminHttp.login(email2, password2);
			assertTrue("la réponse doit être sans erreur, elle vaut :"
					+ reponse2, reponse2.isOk());

			ActiveUser activeUser2 = reponse2.getActiveUser();
			LOGGER.info("\nPortalisCtx ========================================> extent Medium (user2) <============================");
			ExtentReponse extentReponse2 = new CamelisHttp(activeUser2,
					activeLisService).extent(lisQuery);
			assertNotNull(extentReponse2);
			assertTrue("l'extension ne doit doit pas être en erreur :"
					+ extentReponse2, extentReponse2.isOk());
			LisExtent receivedExtent2 = extentReponse2.getExtent();
			assertTrue("Il doit y avoir deux éléments dans l'extension :"
					+ extentReponse2, receivedExtent2 != null
					&& receivedExtent2.card() == 2);
			// extent(medium) should contain two objects: neptune and uranus
			assertTrue("Received extent\n" + receivedExtent
					+ "does not match expected extent\n" + expectedExtent,
					expectedExtent.equals(receivedExtent2));

			LOGGER.info("\nPortalisCtx ========================================> logout 2 <============================");
			VoidReponse voidReponse2 = AdminHttp.logout(activeUser2);
			assertTrue("La réponse doit être sans erreur, voidReponse ="
					+ voidReponse2, voidReponse2.isOk());

		} catch (Exception e) {
			fail(e.getClass().getCanonicalName()
					+ " rencontrée lors du test : " + e.getMessage()
					+ "\nStack :\n" + CommonsUtil.stack2string(e));
		}
	}

	@Test
	public void testLSIALABis() {
		LOGGER.info("\nPortalisCtx **************************************************** @Test testLSIALA ***********************************\n");
		/* test le broadcast vers les camelis lors d'un nouveau login */
		try {
			String email1 = CoreTestConstants.SUPER_USER.getEmail();
			String password1 = CoreTestConstants.SUPER_USER.getPassword();

			LOGGER.info("\nPortalisCtx ========================================> login <============================");
			LoginReponse loginReponse1 = UtilTest.loginHttpAndCheck(email1,
					password1);
			assertTrue("La réponse doit être sans erreur, loginReponse ="
					+ loginReponse1, loginReponse1.isOk());

			ActiveUser activeUser1 = loginReponse1.getActiveUser();

			LOGGER.info("\nPortalisCtx ========================================> startCamelis <============================");
			int camelisPort = CoreTestConstants.PORT1;

			StartReponse startReponse = AdminHttp.startCamelis(activeUser1,
					CoreTestConstants.SERVICE_PLANETS_PLANETS, camelisPort);
			assertTrue("La réponse doit être sans erreur, startReponse ="
					+ startReponse, startReponse.isOk());
			ActiveLisService activeLisService = startReponse
					.getActiveLisService();
			assertTrue("ActiveLisService ne doit pas être null",
					activeLisService != null);

			CamelisHttp camelisHttp1 = new CamelisHttp(activeUser1,
					activeLisService);
			LOGGER.info("\nPortalisCtx ========================================> importCtx <============================");
			String ctxFile = "planets.ctx";
			ImportCtxReponse importReponse = camelisHttp1.importCtx(ctxFile);
			assertTrue("La réponse doit être sans erreur, importReponse ="
					+ importReponse, importReponse.isOk());

			LOGGER.info("\nPortalisCtx ========================================> extent Medium (user1) <============================");
			String lisQuery = "Medium";
			ExtentReponse reponse = camelisHttp1.extent(lisQuery);
			assertNotNull(reponse);
			assertTrue("l'extension ne doit doit pas être en erreur :"
					+ reponse, reponse.isOk());
			LisExtent receivedExtent = reponse.getExtent();
			assertTrue("Il doit y avoir deux éléments dans l'extension :"
					+ reponse, receivedExtent != null
					&& receivedExtent.card() == 2);
			// extent(medium) should contain two objects: neptune and uranus
			String ctxDir = CoreTestConstants.WEBAPPS_DIR + "/planets/planets/";
			LisObject[] expectedObjs = {
					new LisObject(5, "neptune", ctxDir + "images/neptune.jpg",
							""),
					new LisObject(8, "uranus", ctxDir + "images/uranus.jpg", "") };
			LisExtent expectedExtent = new LisExtent(expectedObjs);
			assertTrue("Received extent\n" + receivedExtent
					+ "does not match expected extent\n" + expectedExtent,
					expectedExtent.equals(receivedExtent));

			LOGGER.info("\nPortalisCtx ========================================> logout 1 <============================");
			VoidReponse voidReponse1 = AdminHttp.logout(activeUser1);
			assertTrue("La réponse doit être sans erreur, voidReponse ="
					+ voidReponse1, voidReponse1.isOk());

			LOGGER.info("\nPortalisCtx ========================================> second login <============================");

			LoginReponse reponse2 = AdminHttp.login(email1, password1);
			assertTrue("la réponse doit être sans erreur, elle vaut :"
					+ reponse2, reponse2.isOk());

			ActiveUser activeUser2 = reponse2.getActiveUser();

			LOGGER.info("\nPortalisCtx ========================================> extent Medium (user2) <============================");
			ExtentReponse extentReponse2 = new CamelisHttp(activeUser2,
					activeLisService).extent(lisQuery);
			assertNotNull(extentReponse2);
			assertTrue("l'extension ne doit doit pas être en erreur :"
					+ extentReponse2, extentReponse2.isOk());
			LisExtent receivedExtent2 = extentReponse2.getExtent();
			assertTrue("Il doit y avoir deux éléments dans l'extension :"
					+ extentReponse2, receivedExtent2 != null
					&& receivedExtent2.card() == 2);
			// extent(medium) should contain two objects: neptune and uranus
			assertTrue("Received extent\n" + receivedExtent
					+ "does not match expected extent\n" + expectedExtent,
					expectedExtent.equals(receivedExtent2));

			LOGGER.info("\nPortalisCtx ========================================> logout <============================");
			VoidReponse voidReponse2 = AdminHttp.logout(activeUser2);
			assertTrue("La réponse doit être sans erreur, voidReponse ="
					+ voidReponse2, voidReponse2.isOk());

		} catch (Exception e) {
			fail(e.getClass().getCanonicalName()
					+ " rencontrée lors du test : " + e.getMessage()
					+ "\nStack :\n" + CommonsUtil.stack2string(e));
		}
	}

	@Test
	public void testLLSIAA() {
		LOGGER.info("\nPortalisCtx **************************************************** @Test testLLSIAA ***********************************\n");
		/* test le brocast des utilisateurs actifs lors d'un startCamelis */
		try {
			String email1 = CoreTestConstants.SUPER_USER.getEmail();
			String password1 = CoreTestConstants.SUPER_USER.getPassword();

			LOGGER.info("\nPortalisCtx ========================================> login <============================");
			LoginReponse loginReponse = UtilTest.loginHttpAndCheck(email1,
					password1);
			assertTrue("La réponse doit être sans erreur, loginReponse ="
					+ loginReponse, loginReponse.isOk());

			ActiveUser activeUser1 = loginReponse.getActiveUser();

			LOGGER.info("\nPortalisCtx ========================================> second login <============================");
			String email2 = CoreTestConstants.STANDARD_USER.getEmail();
			String password2 = CoreTestConstants.STANDARD_USER.getPassword();
			LoginReponse reponse2 = AdminHttp.login(email2, password2);
			assertTrue("la réponse doit être sans erreur, elle vaut :"
					+ reponse2, reponse2.isOk());

			ActiveUser activeUser2 = reponse2.getActiveUser();

			assertTrue(activeUser2 != null);
			UserData UserCore2 = activeUser2.getUserCore();
			assertTrue(UserCore2 != null);
			assertEquals(email2, UserCore2.getEmail());

			LOGGER.info("\nPortalisCtx ========================================> startCamelis <============================");
			int camelisPort = CoreTestConstants.PORT1;
			StartReponse startReponse = AdminHttp.startCamelis(activeUser1,
					CoreTestConstants.SERVICE_PLANETS_PLANETS, camelisPort);
			assertTrue("La réponse doit être sans erreur, startReponse ="
					+ startReponse, startReponse.isOk());
			ActiveLisService activeLisService = startReponse
					.getActiveLisService();
			assertTrue("ActiveLisService ne doit pas être null",
					activeLisService != null);

			LOGGER.info("\nPortalisCtx ========================================> importCtx <============================");
			String ctxFile = "planets.ctx";
			ImportCtxReponse importReponse = new CamelisHttp(activeUser1,
					activeLisService).importCtx(ctxFile);
			assertTrue("La réponse doit être sans erreur, importReponse ="
					+ importReponse, importReponse.isOk());

			LOGGER.info("\nPortalisCtx ========================================> extent Medium (user1) <============================");
			String lisQuery = "Medium";
			ExtentReponse reponse = new CamelisHttp(activeUser1,
					activeLisService).extent(lisQuery);
			assertNotNull(reponse);
			assertTrue("l'extension ne doit doit pas être en erreur :"
					+ reponse, reponse.isOk());
			LisExtent receivedExtent = reponse.getExtent();
			assertTrue("Il doit y avoir deux éléments dans l'extension :"
					+ reponse, receivedExtent != null
					&& receivedExtent.card() == 2);
			// extent(medium) should contain two objects: neptune and uranus
			String ctxDir = CoreTestConstants.WEBAPPS_DIR + "/planets/planets/";
			LisObject[] expectedObjs = {
					new LisObject(5, "neptune", ctxDir + "images/neptune.jpg",
							""),
					new LisObject(8, "uranus", ctxDir + "images/uranus.jpg", "") };
			LisExtent expectedExtent = new LisExtent(expectedObjs);
			assertTrue("Received extent\n" + receivedExtent
					+ "does not match expected extent\n" + expectedExtent,
					expectedExtent.equals(receivedExtent));

			LOGGER.info("\nPortalisCtx ========================================> extent Medium (user2) <============================");

			ExtentReponse extentReponse2 = new CamelisHttp(activeUser2,
					activeLisService).extent(lisQuery);
			assertNotNull(extentReponse2);
			assertTrue("l'extension ne doit doit pas être en erreur :"
					+ extentReponse2, extentReponse2.isOk());
			LisExtent receivedExtent2 = extentReponse2.getExtent();
			assertTrue("Il doit y avoir deux éléments dans l'extension :"
					+ extentReponse2, receivedExtent2 != null
					&& receivedExtent2.card() == 2);
			// extent(medium) should contain two objects: neptune and uranus
			assertTrue("Received extent\n" + receivedExtent
					+ "does not match expected extent\n" + expectedExtent,
					expectedExtent.equals(receivedExtent2));

			LOGGER.info("\nPortalisCtx ========================================> logout <============================");
			VoidReponse voidReponse1 = AdminHttp.logout(activeUser1);
			assertTrue("La réponse doit être sans erreur, voidReponse ="
					+ voidReponse1, voidReponse1.isOk());

			LOGGER.info("\nPortalisCtx ========================================> logout <============================");
			VoidReponse voidReponse2 = AdminHttp.logout(activeUser2);
			assertTrue("La réponse doit être sans erreur, voidReponse ="
					+ voidReponse2, voidReponse2.isOk());

		} catch (Exception e) {
			fail(e.getClass().getCanonicalName()
					+ " rencontrée lors du test : " + e.getMessage()
					+ "\nStack :\n" + CommonsUtil.stack2string(e));
		}
	}

	@Test
	public void testRegisterKeyAlreadyRegistered() {
		LOGGER.info("\nPortalisCtx **************************************************** @Test testRegisterKeyAlreadyRegistered ***********************************\n");
		try {
			String email1 = CoreTestConstants.SUPER_USER.getEmail();
			String password1 = CoreTestConstants.SUPER_USER.getPassword();

			LOGGER.info("\nPortalisCtx ========================================> login <============================");
			LoginReponse loginReponse1 = UtilTest.loginHttpAndCheck(email1,
					password1);
			assertTrue("La réponse doit être sans erreur, loginReponse ="
					+ loginReponse1, loginReponse1.isOk());
			ActiveUser activeUser1 = loginReponse1.getActiveUser();

			LOGGER.info("\nPortalisCtx ========================================> second login <============================");
			String email2 = CoreTestConstants.STANDARD_USER.getEmail();
			String password2 = CoreTestConstants.STANDARD_USER.getPassword();
			LoginReponse reponse2 = AdminHttp.login(email2, password2);
			assertTrue("la réponse doit être sans erreur, elle vaut :"
					+ reponse2, reponse2.isOk());

			ActiveUser activeUser2 = reponse2.getActiveUser();
			assertTrue(activeUser2 != null);
			String portalisSessionId2 = activeUser2.getPortalisSessionId();

			UserData UserCore2 = activeUser2.getUserCore();
			assertTrue(UserCore2 != null);
			assertEquals(activeUser2.getUserCore(), UserCore2);

			LOGGER.info("\nPortalisCtx ========================================> startCamelis <============================");
			int camelisPort = CoreTestConstants.PORT1;
			StartReponse startReponse = AdminHttp.startCamelis(activeUser1,
					CoreTestConstants.SERVICE_PLANETS_PLANETS, camelisPort);
			assertTrue("La réponse doit être sans erreur, startReponse ="
					+ startReponse, startReponse.isOk());
			ActiveLisService activeLisService = startReponse
					.getActiveLisService();
			assertTrue("ActiveLisService ne doit pas être null",
					activeLisService != null);

			LOGGER.info("\nPortalisCtx ========================================> importCtx <============================");
			String ctxFile = "planets.ctx";
			ImportCtxReponse importReponse = new CamelisHttp(activeUser1,
					activeLisService).importCtx(ctxFile);
			assertTrue("La réponse doit être sans erreur, importReponse ="
					+ importReponse, importReponse.isOk());

			LOGGER.info("\nPortalisCtx ========================================> registerKey <============================");

			RegisterKeyReponse camelisUserResponse = new CamelisHttp(
					activeUser1, activeLisService).registerKey(
					portalisSessionId2, RightValue.READER);
			LOGGER.info("CamelisUserResponse = " + camelisUserResponse);
			assertTrue("Le retour doit être en erreur\nreponse = "
					+ camelisUserResponse, !camelisUserResponse.isOk());

			final String TEMOIN = ErrorMessages.TEMOIN_IS_ALREADY_REGISTERED;
			assertTrue("Les messages devraient contenir le témoin '" + TEMOIN
					+ "' La réponse est :\n" + camelisUserResponse,
					camelisUserResponse.messagesContains(TEMOIN));

		} catch (Exception e) {
			fail(e.getClass().getCanonicalName()
					+ " rencontrée lors du test : " + e.getMessage()
					+ "\nStack :\n" + CommonsUtil.stack2string(e));
		}
	}

	@Test
	public void testImportCtxNoFileName() {
		LOGGER.info("\nPortalisCtx **************************************************** @Test testImportCtxNoFileName ***********************************\n");
		try {
			String email = CoreTestConstants.SUPER_USER.getEmail();
			String password = CoreTestConstants.SUPER_USER.getPassword();

			LOGGER.info("\nPortalisCtx ========================================> login <============================");
			LoginReponse loginReponse = UtilTest.loginHttpAndCheck(email,
					password);
			assertTrue("La réponse doit être sans erreur, loginReponse ="
					+ loginReponse, loginReponse.isOk());

			ActiveUser activeUser = loginReponse.getActiveUser();

			LOGGER.info("\nPortalisCtx ========================================> startCamelis <============================");
			int camelisPort = CoreTestConstants.PORT1;

			StartReponse startReponse = AdminHttp.startCamelis(activeUser,
					CoreTestConstants.SERVICE_PLANETS_PLANETS, camelisPort);
			assertTrue("La réponse doit être sans erreur, startReponse ="
					+ startReponse, startReponse.isOk());
			ActiveLisService activeLisService = startReponse
					.getActiveLisService();
			assertTrue("ActiveLisService ne doit pas être null",
					activeLisService != null);

			LOGGER.info("\nPortalisCtx ========================================> importCtx <============================");
			ImportCtxReponse importReponse = new CamelisHttp(activeUser,
					activeLisService).importCtx();
			assertTrue("La réponse doit être sans erreur, importReponse ="
					+ importReponse, importReponse.isOk());

		} catch (Exception e) {
			fail(e.getClass().getCanonicalName()
					+ " rencontrée lors du test : " + e.getMessage()
					+ "\nStack :\n" + CommonsUtil.stack2string(e));
		}
	}

	@Test
	public void testImportCtxWithRelativeFileName() {
		LOGGER.info("\nPortalisCtx **************************************************** @Test testImportCtxWithRelativeFileName ***********************************\n");
		try {
			String email = CoreTestConstants.SUPER_USER.getEmail();
			String password = CoreTestConstants.SUPER_USER.getPassword();

			LOGGER.info("\nPortalisCtx ========================================> login <============================");
			LoginReponse loginReponse = UtilTest.loginHttpAndCheck(email,
					password);
			assertTrue("La réponse doit être sans erreur, loginReponse ="
					+ loginReponse, loginReponse.isOk());

			ActiveUser activeUser = loginReponse.getActiveUser();

			LOGGER.info("\nPortalisCtx ========================================> startCamelis <============================");
			int camelisPort = CoreTestConstants.PORT1;

			StartReponse startReponse = AdminHttp.startCamelis(activeUser,
					CoreTestConstants.SERVICE_PLANETS_PLANETS, camelisPort);
			assertTrue("La réponse doit être sans erreur, startReponse ="
					+ startReponse, startReponse.isOk());
			ActiveLisService activeLisService = startReponse
					.getActiveLisService();
			assertTrue("ActiveLisService ne doit pas être null",
					activeLisService != null);

			LOGGER.info("\nPortalisCtx ========================================> importCtx <============================");
			ImportCtxReponse importReponse = new CamelisHttp(activeUser,
					activeLisService).importCtx("planets.ctx");
			assertTrue("La réponse doit être sans erreur, importReponse ="
					+ importReponse, importReponse.isOk());

		} catch (Exception e) {
			fail(e.getClass().getCanonicalName()
					+ " rencontrée lors du test : " + e.getMessage()
					+ "\nStack :\n" + CommonsUtil.stack2string(e));
		}
	}

	@Ignore
	@Test
	public void testImportCtxWithAbsoluteFileName() {
		LOGGER.info("\nPortalisCtx **************************************************** @Test testImportCtxWithAbsoluteFileName ***********************************\n");
		try {
			String email = CoreTestConstants.SUPER_USER.getEmail();
			String password = CoreTestConstants.SUPER_USER.getPassword();

			LOGGER.info("\nPortalisCtx ========================================> login <============================");
			LoginReponse loginReponse = UtilTest.loginHttpAndCheck(email,
					password);
			assertTrue("La réponse doit être sans erreur, loginReponse ="
					+ loginReponse, loginReponse.isOk());

			ActiveUser activeUser = loginReponse.getActiveUser();

			LOGGER.info("\nPortalisCtx ========================================> startCamelis <============================");
			int camelisPort = CoreTestConstants.PORT1;

			StartReponse startReponse = AdminHttp.startCamelis(activeUser,
					CoreTestConstants.SERVICE_PLANETS_PLANETS, camelisPort);
			assertTrue("La réponse doit être sans erreur, startReponse ="
					+ startReponse, startReponse.isOk());
			ActiveLisService activeLisService = startReponse
					.getActiveLisService();
			assertTrue("ActiveLisService ne doit pas être null",
					activeLisService != null);

			LOGGER.info("\nPortalisCtx ========================================> importCtx <============================");
			ImportCtxReponse importReponse = new CamelisHttp(activeUser,
					activeLisService).importCtx("/planets/planets/planets.ctx");
			assertTrue("La réponse doit être sans erreur, importReponse ="
					+ importReponse, importReponse.isOk());

		} catch (Exception e) {
			fail(e.getClass().getCanonicalName()
					+ " rencontrée lors du test : " + e.getMessage()
					+ "\nStack :\n" + CommonsUtil.stack2string(e));
		}
	}

	@Test
	public void testImportLisRelativeName() {
		LOGGER.info("\nPortalisCtx **************************************************** @Test testImportLisRelativeName ***********************************\n");
		try {

			LOGGER.info("\nPortalisCtx ========================================> login <============================");
			String email = CoreTestConstants.SUPER_USER.getEmail();
			String password = CoreTestConstants.SUPER_USER.getPassword();
			LoginReponse loginReponse = UtilTest.loginHttpAndCheck(email,
					password);
			assertTrue("La réponse doit être sans erreur, loginReponse ="
					+ loginReponse, loginReponse.isOk());

			ActiveUser activeUser = loginReponse.getActiveUser();

			int camelisPort = CoreTestConstants.PORT1;

			LOGGER.info("\nPortalisCtx ========================================> startCamelis <============================");
			StartReponse startReponse = AdminHttp.startCamelis(activeUser,
					CoreTestConstants.SERVICE_PLANETS_PLANETS, camelisPort);
			assertTrue("La réponse doit être sans erreur, startReponse ="
					+ startReponse, startReponse.isOk());
			ActiveLisService activeLisService = startReponse
					.getActiveLisService();
			assertTrue("ActiveLisService ne doit pas être null",
					activeLisService != null);

			LOGGER.info("\nPortalisCtx ========================================> importlis Relative <============================");
			String lisFile = "planets.lis";
			ImportLisReponse importReponse = new CamelisHttp(activeUser,
					activeLisService).importLis(lisFile);
			assertTrue("La réponse doit être sans erreur, importReponse ="
					+ importReponse, importReponse.isOk());
		} catch (Exception e) {
			fail(e.getClass().getCanonicalName()
					+ " rencontrée lors du test : " + e.getMessage()
					+ "\nStack :\n" + CommonsUtil.stack2string(e));
		}
	}

	@Ignore
	@Test
	public void testImportLisAbsoluteName() {
		LOGGER.info("\nPortalisCtx **************************************************** @Test testImportLisAbsoluteName ***********************************\n");
		try {

			LOGGER.info("\nPortalisCtx ========================================> login <============================");
			String email = CoreTestConstants.SUPER_USER.getEmail();
			String password = CoreTestConstants.SUPER_USER.getPassword();
			LoginReponse loginReponse = UtilTest.loginHttpAndCheck(email,
					password);
			assertTrue("La réponse doit être sans erreur, loginReponse ="
					+ loginReponse, loginReponse.isOk());

			ActiveUser activeUser = loginReponse.getActiveUser();

			int camelisPort = CoreTestConstants.PORT1;

			LOGGER.info("\nPortalisCtx ========================================> startCamelis <============================");
			StartReponse startReponse = AdminHttp.startCamelis(activeUser,
					CoreTestConstants.SERVICE_PLANETS_PLANETS, camelisPort);
			assertTrue("La réponse doit être sans erreur, startReponse ="
					+ startReponse, startReponse.isOk());
			ActiveLisService activeLisService = startReponse
					.getActiveLisService();
			assertTrue("ActiveLisService ne doit pas être null",
					activeLisService != null);

			LOGGER.info("\nPortalisCtx ========================================> importLis Absolute <============================");
			String lisFile = "/planets/planets/planets.lis";
			ImportLisReponse importReponse = new CamelisHttp(activeUser,
					activeLisService).importLis(lisFile);
			assertTrue("La réponse doit être sans erreur, importReponse ="
					+ importReponse, importReponse.isOk());
		} catch (Exception e) {
			fail(e.getClass().getCanonicalName()
					+ " rencontrée lors du test : " + e.getMessage()
					+ "\nStack :\n" + CommonsUtil.stack2string(e));
		}
	}

	@Test
	public void testImportLisNoFileName() {
		LOGGER.info("\nPortalisCtx **************************************************** @Test testImportLisNoFileName ***********************************\n");
		try {
			String email = CoreTestConstants.SUPER_USER.getEmail();
			String password = CoreTestConstants.SUPER_USER.getPassword();

			LOGGER.info("\nPortalisCtx ========================================> login <============================");
			LoginReponse loginReponse = UtilTest.loginHttpAndCheck(email,
					password);
			assertTrue("La réponse doit être sans erreur, loginReponse ="
					+ loginReponse, loginReponse.isOk());

			ActiveUser activeUser = loginReponse.getActiveUser();

			int camelisPort = CoreTestConstants.PORT1;

			LOGGER.info("\nPortalisCtx ========================================> startCamelis <============================");
			StartReponse startReponse = AdminHttp.startCamelis(activeUser,
					CoreTestConstants.SERVICE_PLANETS_PLANETS, camelisPort);
			assertTrue("La réponse doit être sans erreur, startReponse ="
					+ startReponse, startReponse.isOk());
			ActiveLisService activeLisService = startReponse
					.getActiveLisService();
			assertTrue("ActiveLisService ne doit pas être null",
					activeLisService != null);

			LOGGER.info("\nPortalisCtx ========================================> importLis <============================");
			ImportLisReponse importReponse = new CamelisHttp(activeUser,
					activeLisService).importLis();
			assertTrue("La réponse doit être sans erreur, importReponse ="
					+ importReponse, importReponse.isOk());

		} catch (Exception e) {
			fail(e.getClass().getCanonicalName()
					+ " rencontrée lors du test : " + e.getMessage()
					+ "\nStack :\n" + CommonsUtil.stack2string(e));
		}
	}

	@Test
	public void testLoadContext() {
		LOGGER.info("\nPortalisCtx **************************************************** @Test testLoadContext ***********************************\n");
		try {

			LOGGER.info("\nPortalisCtx ========================================> login <============================");
			String email = CoreTestConstants.SUPER_USER.getEmail();
			String password = CoreTestConstants.SUPER_USER.getPassword();

			LoginReponse loginReponse = UtilTest.loginHttpAndCheck(email,
					password);
			assertTrue("La réponse doit être sans erreur, loginReponse ="
					+ loginReponse, loginReponse.isOk());

			ActiveUser activeUser = loginReponse.getActiveUser();

			int camelisPort = CoreTestConstants.PORT1;

			LOGGER.info("\nPortalisCtx ========================================> startCamelis <============================");
			StartReponse startReponse = AdminHttp.startCamelis(activeUser,
					CoreTestConstants.SERVICE_PLANETS_PLANETS, camelisPort);
			assertTrue("La réponse doit être sans erreur, startReponse ="
					+ startReponse, startReponse.isOk());
			ActiveLisService camelisService = startReponse
					.getActiveLisService();
			assertTrue("camelisService ne doit pas être null",
					camelisService != null);

			LOGGER.info("\nPortalisCtx ========================================> loadContext <============================");
			LoadContextReponse reponse = new CamelisHttp(activeUser,
					camelisService).loadContext();
			assertTrue("La réponse doit être sans erreur, loadContextReponse ="
					+ reponse, reponse.isOk());
		} catch (Exception e) {
			fail(e.getClass().getCanonicalName()
					+ " rencontrée lors du test : " + e.getMessage()
					+ "\nStack :\n" + CommonsUtil.stack2string(e));
		}
	}

	@Test
	public void testStart1ProcessSpecifiedPort() {
		LOGGER.info("\nPortalisCtx **************************************************** @Test testStart1ProcessSpecifiedPort ***********************************\n");
		try {
			String email = CoreTestConstants.SUPER_USER.getEmail();
			String password = CoreTestConstants.SUPER_USER.getPassword();

			LOGGER.info("\nPortalisCtx ========================================> login <============================");
			LoginReponse loginReponse = UtilTest.loginHttpAndCheck(email,
					password);
			assertTrue("La réponse doit être sans erreur, loginReponse ="
					+ loginReponse, loginReponse.isOk());

			ActiveUser activeUser = loginReponse.getActiveUser();
			@SuppressWarnings("unused")
			String portalisSessionId = activeUser.getPortalisSessionId();

			LOGGER.info("\nPortalisCtx ========================================> startCamelis <============================");
			int camelisPort = CoreTestConstants.PORT1;
			StartReponse startReponse = AdminHttp.startCamelis(activeUser,
					CoreTestConstants.SERVICE_PLANETS_DISTANCE, camelisPort);
			assertTrue("La réponse doit être sans erreur, startReponse ="
					+ startReponse, startReponse.isOk());

			LOGGER.info("\nPortalisCtx ========================================> startReponse.getActiveLisService() <============================");
			ActiveService activeLisService = startReponse.getActiveLisService();
			assertTrue("ActiveLisService ne doit pas être null",
					activeLisService != null);

			String creator = CoreTestConstants.SuperActiveUser.getUserCore()
					.getEmail();
			String idAppli = LisServiceCore
					.extractAppliName(CoreTestConstants.SERVICE_PLANETS_DISTANCE);
			String idService = LisServiceCore
					.extractServiceName(CoreTestConstants.SERVICE_PLANETS_DISTANCE);
			CamelisServiceCore serviceCore = new CamelisServiceCore(idAppli,
					idService, RightValue.READER);

			assertTrue(!activeLisService.equals(new ActiveLisService(creator, PortalisService
							.getInstance().getHost(), camelisPort, new Date(),
					null, 5656, 654, serviceCore.getFullName())));

			LOGGER.info("\nPortalisCtx ========================================> getActifsPorts <============================");
			PortsActifsReponse portsActifsReponse = AdminHttp
					.getActifsPorts(activeUser);
			assertTrue("La réponse doit être sans erreur, portsActifsReponse ="
					+ portsActifsReponse, portsActifsReponse.isOk());
			Set<PortActif> actifs = portsActifsReponse.getListPortsActifs();
			assertTrue("Liste d'actifs null", actifs != null);
			assertEquals(1, actifs.size());

		} catch (Exception e) {
			fail(e.getClass().getCanonicalName()
					+ " rencontrée lors du test : " + e.getMessage()
					+ "\nStack :\n" + CommonsUtil.stack2string(e));
		}
	}

	@Test
	public void testGetCamelisServicesOnPortalis() {
		LOGGER.info("\nPortalisCtx **************************************************** @Test testGetCamelisServicesOnPortalis ***********************************\n");
		try {
			String email = CoreTestConstants.SUPER_USER.getEmail();
			String password = CoreTestConstants.SUPER_USER.getPassword();

			LOGGER.info("\nPortalisCtx ========================================> login <============================");
			LoginReponse loginReponse = UtilTest.loginHttpAndCheck(email,
					password);
			assertTrue("La réponse doit être sans erreur, loginReponse ="
					+ loginReponse, loginReponse.isOk());

			ActiveUser activeUser = loginReponse.getActiveUser();
			@SuppressWarnings("unused")
			String portalisSessionId = activeUser.getPortalisSessionId();

			LOGGER.info("\nPortalisCtx ========================================> startCamelis <============================");
			StartReponse startReponse = AdminHttp.startCamelis(activeUser,
					CoreTestConstants.SERVICE_PLANETS_DISTANCE);
			assertTrue("Le retour ne doit pas être en erreur :" + startReponse,
					startReponse.isOk());

			ActiveLisService ActiveLisService = startReponse
					.getActiveLisService();
			int port = ActiveLisService.getPort();

			LOGGER.info("\nPortalisCtx ========================================> ping <============================");
			PingReponse pingReponse1 = CamelisHttp.ping(ActiveLisService);
			assertTrue("Le retour ne doit pas être en erreur :" + pingReponse1,
					pingReponse1.isOk());
			assertEquals(ActiveLisService, pingReponse1.getFirstService());
			assertEquals(port, pingReponse1.getFirstService().getPort());

			LOGGER.info("\nPortalisCtx ========================================> getCamelisServicesOnPortalis <============");
			PingReponse pingReponse2 = AdminHttp
					.getActiveLisServicesOnPortalis(activeUser);
			assertTrue("La réponse doit être sans erreur, pingReponse ="
					+ pingReponse2, pingReponse2.isOk());
			List<ActiveLisService> lesServices1 = pingReponse2.getLesServices();
			assertEquals(1, lesServices1.size());

		} catch (Exception e) {
			fail(e.getClass().getCanonicalName()
					+ " rencontrée lors du test : " + e.getMessage()
					+ "\nStack :\n" + CommonsUtil.stack2string(e));
		}
	}

	@Test
	public void testStart3Processes() {
		LOGGER.info("\nPortalisCtx **************************************************** @Test testStart3Processes ***********************************\n");
		try {
			String email = CoreTestConstants.SUPER_USER.getEmail();
			String password = CoreTestConstants.SUPER_USER.getPassword();

			LOGGER.info("\nPortalisCtx ========================================> login <============================");
			LoginReponse loginReponse = UtilTest.loginHttpAndCheck(email,
					password);
			assertTrue("La réponse doit être sans erreur, loginReponse ="
					+ loginReponse, loginReponse.isOk());

			ActiveUser activeUser = loginReponse.getActiveUser();
			@SuppressWarnings("unused")
			String portalisSessionId = activeUser.getPortalisSessionId();

			LOGGER.info("\nPortalisCtx ========================================> startCamelis <============================");
			UtilTest.startOneCamelis(activeUser,
					CoreTestConstants.SERVICE_PLANETS_DISTANCE);

			LOGGER.info("\nPortalisCtx ========================================> startCamelis <============================");
			int port2 = CoreTestConstants.PORT2;
			StartReponse startReponse = AdminHttp.startCamelis(activeUser,
					CoreTestConstants.SERVICE_PLANETS_DISTANCE, port2);
			assertTrue("La réponse doit être sans erreur, startReponse ="
					+ startReponse, startReponse.isOk());

			LOGGER.info("\nPortalisCtx ========================================> startCamelis <============================");
			UtilTest.startOneCamelis(activeUser,
					CoreTestConstants.SERVICE_PLANETS_DISTANCE);

			LOGGER.info("\nPortalisCtx ========================================> getCamelisServicesOnPortalis <============");
			PingReponse pingReponse1 = AdminHttp
					.getActiveLisServicesOnPortalis(activeUser);
			assertTrue("La réponse doit être sans erreur, pingReponse ="
					+ pingReponse1, pingReponse1.isOk());
			List<ActiveLisService> lesServices1 = pingReponse1.getLesServices();
			assertEquals(3, lesServices1.size());

			LOGGER.info("\nPortalisCtx ========================================> getActifsPorts <==========================");
			PortsActifsReponse portsActifsReponse = AdminHttp
					.getActifsPorts(activeUser);
			assertTrue("La réponse doit être sans erreur, portsActifsReponse ="
					+ portsActifsReponse, portsActifsReponse.isOk());
			Set<PortActif> actifs = portsActifsReponse.getListPortsActifs();
			assertTrue("Liste d'actifs null", actifs != null);
			assertEquals(3, actifs.size());
		} catch (Exception e) {
			fail(e.getClass().getCanonicalName()
					+ " rencontrée lors du test : " + e.getMessage()
					+ "\nStack :\n" + CommonsUtil.stack2string(e));
		}
	}

	@Test
	public void testKillAllProcesses() {
		LOGGER.info("\nPortalisCtx **************************************************** @Test testKillAllProcesses ***********************************\n");
		try {
			String email = CoreTestConstants.SUPER_USER.getEmail();
			String password = CoreTestConstants.SUPER_USER.getPassword();

			LOGGER.info("\nPortalisCtx ========================================> login <============================");
			LoginReponse loginReponse = UtilTest.loginHttpAndCheck(email,
					password);
			assertTrue("La réponse doit être sans erreur, loginReponse ="
					+ loginReponse, loginReponse.isOk());

			ActiveUser activeUser = loginReponse.getActiveUser();
			@SuppressWarnings("unused")
			String portalisSessionId = activeUser.getPortalisSessionId();

			LOGGER.info("\nPortalisCtx ========================================> startCamelis <============================");
			StartReponse rep1 = AdminHttp.startCamelis(activeUser,
					CoreTestConstants.SERVICE_PLANETS_DISTANCE);
			assertTrue("La réponse doit être sans erreur, rep1 =" + rep1,
					rep1.isOk());

			LOGGER.info("\nPortalisCtx ========================================> startCamelis <============================");
			StartReponse rep2 = AdminHttp.startCamelis(activeUser,
					CoreTestConstants.SERVICE_PLANETS_DISTANCE);
			assertTrue("La réponse doit être sans erreur, rep2 =" + rep2,
					rep2.isOk());

			LOGGER.info("\nPortalisCtx ========================================> startCamelis <============================");
			StartReponse rep3 = AdminHttp.startCamelis(activeUser,
					CoreTestConstants.SERVICE_PLANETS_DISTANCE);
			assertTrue("La réponse doit être sans erreur, rep3 =" + rep3,
					rep3.isOk());

			LOGGER.info("\nPortalisCtx ========================================> getActifsPorts <============================");
			PortsActifsReponse portsActifsReponse = AdminHttp
					.getActifsPorts(activeUser);
			Set<PortActif> actifs = portsActifsReponse.getListPortsActifs();
			assertTrue("Liste d'actifs null", actifs != null);
			assertEquals(3, actifs.size());

			assertTrue("les deux réponses doivent être différentes rep1="
					+ rep1 + "\nrep1=" + rep2, !rep1.equals(rep2));
			assertTrue("les deux réponses doivent être différentes",
					!rep1.equals(rep3));
			assertTrue("les deux réponses doivent être différentes",
					!rep2.equals(rep3));

			LOGGER.info("\nPortalisCtx ========================================> stopCamelis 2 <============================");
			AdminHttp.killAllCamelisProcess(activeUser);

			LOGGER.info("\nPortalisCtx ========================================> getActifsPorts <============================");
			PortsActifsReponse portsActifsReponse2 = AdminHttp
					.getActifsPorts(activeUser);
			Set<PortActif> actifs2 = portsActifsReponse2.getListPortsActifs();
			assertTrue("Liste d'actifs null", actifs2 != null);
			assertEquals(0, actifs2.size());

		} catch (Exception e) {
			fail(e.getClass().getCanonicalName()
					+ " rencontrée lors du test : " + e.getMessage()
					+ "\nStack :\n" + CommonsUtil.stack2string(e));
		}
	}

	@Test
	public void testPingSuccessDefaultPort() {
		LOGGER.info("\nPortalisCtx **************************************************** @Test testPingSuccessDefaultPort ***********************************\n");
		try {
			String email = CoreTestConstants.SUPER_USER.getEmail();
			String password = CoreTestConstants.SUPER_USER.getPassword();

			LOGGER.info("\nPortalisCtx ========================================> login <============================");
			LoginReponse loginReponse = UtilTest.loginHttpAndCheck(email,
					password);
			assertTrue("La réponse doit être sans erreur, loginReponse ="
					+ loginReponse, loginReponse.isOk());

			ActiveUser activeUser = loginReponse.getActiveUser();
			@SuppressWarnings("unused")
			String portalisSessionId = activeUser.getPortalisSessionId();

			LOGGER.info("\nPortalisCtx ========================================> startCamelis <============================");

			StartReponse startReponse = AdminHttp.startCamelis(activeUser,
					CoreTestConstants.SERVICE_PLANETS_DISTANCE);
			assertTrue(
					"Le retour ne doit pas être en erreur : " + startReponse,
					startReponse.isOk());

			ActiveLisService ActiveLisService = startReponse
					.getActiveLisService();
			assertTrue("ActiveLisService ne doit pas être null, startReponse ="
					+ startReponse, ActiveLisService != null);

			LOGGER.info("\nPortalisCtx ========================================> CamelisHttp.ping <============================");
			PingReponse pingReponse = CamelisHttp.ping(ActiveLisService);
			assertTrue("Le retour ne doit pas être en erreur : " + pingReponse,
					pingReponse.isOk());

			assertEquals(ActiveLisService, pingReponse.getFirstService());

			LOGGER.info("\nPortalisCtx ========================================> getCamelisProcessId <============================");
			PidReponse pidReponse = AdminHttp.getCamelisProcessId(activeUser,
					ActiveLisService.getPort());
			assertTrue("Le retour ne doit pas être en erreur : " + pidReponse,
					pidReponse.isOk());

			assertTrue(pidReponse.getPids().contains(
					pingReponse.getFirstService().getPid()));

		} catch (Exception e) {
			fail(e.getClass().getCanonicalName()
					+ " rencontrée lors du test : " + e.getMessage()
					+ "\nStack :\n" + CommonsUtil.stack2string(e));
		}
	}

	@Test
	public void testPingSuccessSpecifiedPort() {
		LOGGER.info("\nPortalisCtx **************************************************** @Test testPingSuccessSpecifiedPort ***********************************\n");
		try {
			String email = CoreTestConstants.SUPER_USER.getEmail();
			String password = CoreTestConstants.SUPER_USER.getPassword();

			LOGGER.info("\nPortalisCtx ========================================> login <============================");
			LoginReponse loginReponse = UtilTest.loginHttpAndCheck(email,
					password);
			assertTrue("loginReponse doit être sans erreur, loginReponse ="
					+ loginReponse, loginReponse.isOk());

			ActiveUser activeUser = loginReponse.getActiveUser();
			@SuppressWarnings("unused")
			String portalisSessionId = activeUser.getPortalisSessionId();

			LOGGER.info("\nPortalisCtx ========================================> startCamelis <============================");
			StartReponse startReponse = AdminHttp.startCamelis(activeUser,
					CoreTestConstants.SERVICE_PLANETS_DISTANCE);
			assertTrue("startReponse ne doit pas être en erreur : "
					+ startReponse, startReponse.isOk());

			ActiveLisService activeLisService = startReponse
					.getActiveLisService();
			assertTrue("ActiveLisService ne doit pas être null, startReponse ="
					+ startReponse, activeLisService != null);

			LOGGER.info("\nPortalisCtx ========================================> CamelisHttp.ping <============================");
			PingReponse pingReponse = CamelisHttp.ping(activeLisService);
			assertTrue("pingReponse ne doit pas être en erreur : "
					+ pingReponse, pingReponse.isOk());

			assertEquals(activeLisService, pingReponse.getFirstService());

			LOGGER.info("\nPortalisCtx ========================================> getCamelisProcessId <============================");
			PidReponse pidReponse = AdminHttp.getCamelisProcessId(activeUser,
					activeLisService.getPort());
			assertTrue("Le retour ne doit pas être en erreur : " + pidReponse,
					pidReponse.isOk());
			assertTrue(pidReponse.getPids().contains(
					pingReponse.getFirstService().getPid()));

		} catch (Exception e) {
			fail(e.getClass().getCanonicalName()
					+ " rencontrée lors du test : " + e.getMessage()
					+ "\nStack :\n" + CommonsUtil.stack2string(e));
		}

	}

	/*
	 * ------------------------------------- testRegisterKey
	 * ------------------------------------------
	 */

	@Test
	public void testRegistetestRegisterKey2rKey2() {
		LOGGER.info("\nPortalisCtx **************************************************** @Test testRegisterKey2 ***********************************\n");

		try {
			String email = CoreTestConstants.SUPER_USER.getEmail();
			String password = CoreTestConstants.SUPER_USER.getPassword();

			LOGGER.info("\nPortalisCtx ========================================> login <============================");
			LoginReponse loginReponse = UtilTest.loginHttpAndCheck(email,
					password);

			ActiveUser activeUser = loginReponse.getActiveUser();

			LOGGER.debug("\n\n              ===========================================> startCamelis <============================");
			ActiveLisService activeLisService = UtilTest
					.startPlanetsPhoto(activeUser);

			LOGGER.info("\nPortalisCtx ========================================> registerKey 1 <============================");
			RightValue rightValue = RightValue.ADMIN;
			RegisterKeyReponse reponse = new CamelisHttp(activeUser,
					activeLisService).registerKey(CoreTestConstants.USER_KEY,
					rightValue);
			LOGGER.info("SetRoleReponse = " + reponse);
			assertTrue("Le retour ne doit pas être en erreur, reponse ="
					+ reponse, reponse.isOk());
			assertTrue("Un utilisateur doit être là. La réponse est :\n"
					+ reponse, reponse.getUser() != null);

			assertEquals(CoreTestConstants.USER_KEY, reponse.getUser().getKey());
			assertEquals(rightValue, reponse.getUser().getRole());

			LOGGER.info("\nPortalisCtx ========================================> registerKey 2 <============================");
			RightValue rightValue1 = RightValue.NONE;
			RegisterKeyReponse reponse1 = new CamelisHttp(activeUser,
					activeLisService).registerKey(CoreTestConstants.USER_KEY,
					rightValue1);
			LOGGER.info("SetRoleReponse = " + reponse1);
			assertTrue("Le retour doit être en erreur. reponse = " + reponse1,
					!reponse1.isOk());
			assertTrue("Un utilisateur ne pas doit être là. La réponse est :\n"
					+ reponse1, reponse1.getUser() == null);

			final String TEMOIN = ErrorMessages.TEMOIN_IS_ALREADY_REGISTERED;
			assertTrue("Les messages devraient contenir le témoin '" + TEMOIN
					+ "' La réponse est :\n" + reponse1,
					reponse1.messagesContains(TEMOIN));
		} catch (Exception e) {
			fail(e.getClass().getCanonicalName()
					+ " rencontrée lors du test : " + e.getMessage()
					+ "\nStack :\n" + CommonsUtil.stack2string(e));
		}
	}

	@Test
	public void testRegisterKeyWithTimeout() {
		LOGGER.info("\nPortalisCtx **************************************************** @Test testRegisterKeyWithTimeout ***********************************\n");
		try {
			String email = CoreTestConstants.SUPER_USER.getEmail();
			String password = CoreTestConstants.SUPER_USER.getPassword();

			LOGGER.info("\nPortalisCtx ========================================> login <============================");
			LoginReponse loginReponse = UtilTest.loginHttpAndCheck(email,
					password);
			assertTrue("Login error : " + loginReponse, loginReponse.isOk());

			ActiveUser activeUser = loginReponse.getActiveUser();

			LOGGER.info("\nPortalisCtx ========================================> startCamelis <============================");
			ActiveLisService activeLisService = UtilTest
					.startPlanetsPhoto(activeUser);

			// registerKey
			RightValue rightValue = RightValue.NONE;
			int timeout = 300; // 5 minutes, arbitrary
			RegisterKeyReponse reponse = new CamelisHttp(activeUser,
					activeLisService).registerKey(CoreTestConstants.USER_KEY,
					rightValue, timeout);
			assertTrue("RegisterKey returned an error: " + reponse,
					reponse.isOk());
			assertTrue("No user in response: " + reponse,
					reponse.getUser() != null);

			assertEquals(CoreTestConstants.USER_KEY, reponse.getUser().getKey());
			assertEquals(rightValue, reponse.getUser().getRole());

			// is the expiration date really 5 minutes from now?
			DateTime now = new DateTime();
			DateTime exp = new DateTime(reponse.getUser().getExpires());
			Duration diff = new Duration(now, exp);
			boolean accurateTimeout = diff.isShorterThan(Duration
					.standardSeconds(timeout + 1))
					&& diff.isLongerThan(Duration.standardSeconds(timeout - 1));
			assertTrue("Timeout was not set correctly", accurateTimeout);

		} catch (Exception e) {
			fail(e.getClass().getCanonicalName()
					+ " rencontrée lors du test : " + e.getMessage()
					+ "\nStack :\n" + CommonsUtil.stack2string(e));
		}
	}

	/*
	 * ------------------------------------- setRole
	 * ------------------------------------------
	 */

	@Test
	public void testSetRoleOk1() {
		LOGGER.info("\nPortalisCtx **************************************************** @Test testSetRoleOk1 ***********************************\n");

		try {
			String email = CoreTestConstants.SUPER_USER.getEmail();
			String password = CoreTestConstants.SUPER_USER.getPassword();

			LOGGER.info("\nPortalisCtx ========================================> login <============================");
			LoginReponse loginReponse = UtilTest.loginHttpAndCheck(email,
					password);
			assertTrue("Login error : " + loginReponse, loginReponse.isOk());

			ActiveUser activeUser = loginReponse.getActiveUser();

			LOGGER.info("\nPortalisCtx ========================================> startCamelis <============================");
			ActiveLisService activeLisService = UtilTest
					.startPlanetsPhoto(activeUser);

			RightValue rightValue0 = RightValue.NONE;
			String userKey = CoreTestConstants.USER_KEY;

			UtilTest.registerUser(activeUser, activeLisService, userKey,
					rightValue0);

			RightValue rightValue1 = RightValue.ADMIN;

			LOGGER.info("\nPortalisCtx ========================================> setRole <============================");
			SetRoleReponse setRoleReponse = new CamelisHttp(activeUser,
					activeLisService).setRole(CoreTestConstants.USER_KEY,
					rightValue1);
			LOGGER.info("SetRoleReponse = " + setRoleReponse);
			assertTrue("Le retour ne doit pas être en erreur",
					setRoleReponse.isOk());
			assertTrue("Un utilisateur doit être là. La réponse est :\n"
					+ setRoleReponse, setRoleReponse.getUser() != null);

			assertEquals(rightValue1, setRoleReponse.getUser().getRole());

			assertEquals(CoreTestConstants.USER_KEY, setRoleReponse.getUser()
					.getKey());
			assertEquals(rightValue1, setRoleReponse.getUser().getRole());

		} catch (Exception e) {
			fail(e.getClass().getCanonicalName()
					+ " rencontrée lors du test : " + e.getMessage()
					+ "\nStack :\n" + CommonsUtil.stack2string(e));
		}
	}

	@Test
	public void testSetRoleNoAdminKey() {
		LOGGER.info("\nPortalisCtx **************************************************** @Test testSetRoleNoAdminKey ***********************************\n");

		try {
			String email = CoreTestConstants.SUPER_USER.getEmail();
			String password = CoreTestConstants.SUPER_USER.getPassword();

			LOGGER.info("\nPortalisCtx ========================================> login <============================");
			LoginReponse loginReponse = UtilTest.loginHttpAndCheck(email,
					password);
			assertTrue("Login error : " + loginReponse, loginReponse.isOk());

			ActiveUser activeUser = loginReponse.getActiveUser();
			@SuppressWarnings("unused")
			String portalisSessionId = activeUser.getPortalisSessionId();

			LOGGER.info("\nPortalisCtx ========================================> startCamelis <============================");
			ActiveLisService activeLisService = UtilTest
					.startPlanetsPhoto(activeUser);

			RightValue rightValue0 = RightValue.NONE;
			String userKey = CoreTestConstants.USER_KEY;

			LOGGER.info("\nPortalisCtx ========================================> registerKey <============================");
			UtilTest.registerUser(activeUser, activeLisService, userKey,
					rightValue0);

			RightValue rightValue = RightValue.READER;

			LOGGER.info("\nPortalisCtx ========================================> setRole <============================");
			try {
				@SuppressWarnings("unused")
				SetRoleReponse reponse = new CamelisHttp(null, activeLisService)
						.setRole(userKey, rightValue);
			} catch (PortalisException e) {
				final String TEMOIN = ErrorMessages.TEMOIN_IS_MISSING;
				assertTrue("Les messages devraient contenir le témoin '"
						+ TEMOIN + "' La réponse est :\n" + e.getMessage(), e
						.getMessage().contains(TEMOIN));
			}

		} catch (Exception e) {
			fail(e.getClass().getCanonicalName()
					+ " rencontrée lors du test : " + e.getMessage()
					+ "\nStack :\n" + CommonsUtil.stack2string(e));
		}
	}

	@Test
	public void testSetRoleNoUserKey() {
		LOGGER.info("\nPortalisCtx **************************************************** @Test testSetRoleNoUserKey ***********************************\n");

		try {
			String email = CoreTestConstants.SUPER_USER.getEmail();
			String password = CoreTestConstants.SUPER_USER.getPassword();

			LOGGER.info("\nPortalisCtx ========================================> login <============================");
			LoginReponse loginReponse = UtilTest.loginHttpAndCheck(email,
					password);
			assertTrue("Login error : " + loginReponse, loginReponse.isOk());

			ActiveUser activeUser = loginReponse.getActiveUser();

			LOGGER.info("\nPortalisCtx ========================================> startCamelis <============================");
			ActiveLisService activeLisService = UtilTest
					.startPlanetsPhoto(activeUser);

			RightValue rightValue0 = RightValue.NONE;
			String userKey = CoreTestConstants.USER_KEY;

			LOGGER.info("\nPortalisCtx ========================================> registerKey <============================");
			UtilTest.registerUser(activeUser, activeLisService, userKey,
					rightValue0);

			LOGGER.info("\nPortalisCtx ========================================> setRole <============================");
			RightValue rightValue = RightValue.EDITOR;
			SetRoleReponse reponse = new CamelisHttp(activeUser,
					activeLisService).setRole(null, rightValue);
			LOGGER.info("SetRoleReponse = " + reponse);
			assertTrue("Le retour doit être en erreur", !reponse.isOk());
			assertTrue("Un utilisateur ne doit pas être là. La réponse est :\n"
					+ reponse, reponse.getUser() == null);

			assertEquals(CamelisXmlName.ERROR, reponse.getStatus());
			final String TEMOIN = ErrorMessages.TEMOIN_IS_MISSING;
			assertTrue("Les messages devraient contenir le témoin '" + TEMOIN
					+ "' La réponse est :\n" + reponse,
					reponse.messagesContains(TEMOIN));

		} catch (Exception e) {
			fail(e.getMessage() + "\nStack :\n" + CommonsUtil.stack2string(e));
		}
	}

	@Test
	public void testSetRoleNoRole() {
		LOGGER.info("\nPortalisCtx **************************************************** @Test testSetRoleNoRole ***********************************\n");

		try {

			String role = null;
			String email = CoreTestConstants.SUPER_USER.getEmail();
			String password = CoreTestConstants.SUPER_USER.getPassword();

			LOGGER.info("\nPortalisCtx ========================================> login <============================");
			LoginReponse loginReponse = UtilTest.loginHttpAndCheck(email,
					password);
			assertTrue("Login error : " + loginReponse, loginReponse.isOk());

			ActiveUser activeUser = loginReponse.getActiveUser();

			LOGGER.info("\nPortalisCtx ========================================> startCamelis <============================");
			ActiveLisService activeLisService = UtilTest
					.startPlanetsPhoto(activeUser);

			LOGGER.info("\nPortalisCtx ========================================> ping <============================");
			PingReponse pingReponse = CamelisHttp.ping(activeLisService);
			assertTrue("Le retour ne doit pas être en erreur",
					pingReponse.isOk());

			assertEquals(activeLisService, pingReponse.getFirstService());

			LOGGER.info("\nPortalisCtx ========================================> setRole <============================");
			SetRoleReponse reponse = new CamelisHttp(activeUser,
					activeLisService).setRole(CoreTestConstants.USER_KEY, role);
			LOGGER.info("SetRoleReponse = " + reponse);
			assertTrue("Le retour doit être en erreur", !reponse.isOk());
			assertTrue("Un utilisateur ne doit pas être là. La réponse est :\n"
					+ reponse, reponse.getUser() == null);

			assertEquals(CamelisXmlName.ERROR, reponse.getStatus());
			final String TEMOIN = ErrorMessages.TEMOIN_IS_INCORRECT;
			assertTrue("Les messages devraient contenir le témoin '" + TEMOIN
					+ "' La réponse est :\n" + reponse,
					reponse.messagesContains(TEMOIN));

		} catch (Exception e) {
			fail(e.getMessage() + "\nStack :\n" + CommonsUtil.stack2string(e));
		}
	}

	@Test
	public void testSetRoleBadRole() {
		LOGGER.info("\nPortalisCtx **************************************************** @Test testSetRoleBadRole ***********************************\n");

		try {
			String email = CoreTestConstants.SUPER_USER.getEmail();
			String password = CoreTestConstants.SUPER_USER.getPassword();

			LOGGER.info("\nPortalisCtx ========================================> login <============================");
			LoginReponse loginReponse = UtilTest.loginHttpAndCheck(email,
					password);
			assertTrue("Login error : " + loginReponse, loginReponse.isOk());

			ActiveUser activeUser = loginReponse.getActiveUser();

			LOGGER.info("\nPortalisCtx ========================================> startCamelis <============================");
			ActiveLisService activeLisService = UtilTest
					.startPlanetsPhoto(activeUser);

			RightValue rightValue0 = RightValue.COLLABORATOR;
			String userKey = CoreTestConstants.USER_KEY;

			UtilTest.registerUser(activeUser, activeLisService, userKey,
					rightValue0);

			String role = "noRole";
			SetRoleReponse reponse = new CamelisHttp(activeUser,
					activeLisService).setRole(CoreTestConstants.USER_KEY, role);
			LOGGER.info("SetRoleReponse = " + reponse);
			assertTrue("Le retour doit être en erreur\nreponse = " + reponse,
					!reponse.isOk());
			assertTrue("Un utilisateur ne doit pas être là. La réponse est :\n"
					+ reponse, reponse.getUser() == null);

			assertEquals(CamelisXmlName.ERROR, reponse.getStatus());
			final String TEMOIN = ErrorMessages.TEMOIN_IS_INCORRECT;
			assertTrue("Les messages devraient contenir le témoin '" + TEMOIN
					+ "' La réponse est :\n" + reponse,
					reponse.messagesContains(TEMOIN));

		} catch (Exception e) {
			fail(e.getMessage() + "\nStack :\n" + CommonsUtil.stack2string(e));
		}
	}

	@Test
	public void testSetRoleNotRegisteredAdminKey() {
		LOGGER.info("\nPortalisCtx **************************************************** @Test testSetRoleNotRegisteredAdminKey ***********************************\n");

		try {
			String email = CoreTestConstants.SUPER_USER.getEmail();
			String password = CoreTestConstants.SUPER_USER.getPassword();

			LOGGER.info("\nPortalisCtx ========================================> login <============================");
			LoginReponse loginReponse = UtilTest.loginHttpAndCheck(email,
					password);
			assertTrue("Login error : " + loginReponse, loginReponse.isOk());

			ActiveUser activeUser = loginReponse.getActiveUser();
			@SuppressWarnings("unused")
			String portalisSessionId = activeUser.getPortalisSessionId();

			LOGGER.info("\nPortalisCtx ========================================> startCamelis <============================");
			ActiveLisService activeLisService = UtilTest
					.startPlanetsPhoto(activeUser);

			LOGGER.info("\nPortalisCtx ========================================> setRole <============================");
			RightValue rightValue = RightValue.READER;
			SetRoleReponse reponse = new CamelisHttp(activeUser,
					activeLisService).setRole(CoreTestConstants.USER_KEY,
					rightValue);
			LOGGER.info("SetRoleReponse = " + reponse);
			assertTrue("Le retour doit être en erreur", !reponse.isOk());
			assertTrue("Un utilisateur ne doit pas être là. La réponse est :\n"
					+ reponse, reponse.getUser() == null);

			assertEquals(CamelisXmlName.ERROR, reponse.getStatus());
			final String TEMOIN = ErrorMessages.TEMOIN_IS_NOT_REGISTERED;
			assertTrue("Les messages devraient contenir le témoin '" + TEMOIN
					+ "' La réponse est :\n" + reponse,
					reponse.messagesContains(TEMOIN));

		} catch (Exception e) {
			fail(e.getClass().getCanonicalName()
					+ " rencontrée lors du test : " + e.getMessage()
					+ "\nStack :\n" + CommonsUtil.stack2string(e));
		}
	}

	@Test
	public void testSetRoleUserUnkown() {
		LOGGER.info("\nPortalisCtx **************************************************** @Test testSetRoleUserUnkown ***********************************\n");

		try {
			String userKey = "123456654321";
			String email = CoreTestConstants.SUPER_USER.getEmail();
			String password = CoreTestConstants.SUPER_USER.getPassword();

			LOGGER.info("\nPortalisCtx ========================================> login <============================");
			LoginReponse loginReponse = UtilTest.loginHttpAndCheck(email,
					password);
			assertTrue("Login error : " + loginReponse, loginReponse.isOk());

			ActiveUser activeUser = loginReponse.getActiveUser();

			LOGGER.info("\nPortalisCtx ========================================> startCamelis <============================");
			ActiveLisService activeLisService = UtilTest
					.startPlanetsPhoto(activeUser);

			RightValue rightValue = RightValue.NONE;

			LOGGER.info("\nPortalisCtx ========================================> setRole <============================");
			SetRoleReponse reponse = new CamelisHttp(activeUser,
					activeLisService).setRole(userKey, rightValue);
			LOGGER.info("SetRoleReponse = " + reponse);
			assertTrue("Le retour doit être en erreur", !reponse.isOk());
			assertTrue("Un utilisateur ne doit pas être là. La réponse est :\n"
					+ reponse, reponse.getUser() == null);

			assertEquals(CamelisXmlName.ERROR, reponse.getStatus());
			final String TEMOIN = ErrorMessages.TEMOIN_IS_NOT_REGISTERED;
			assertTrue("Les messages devraient contenir le témoin '" + TEMOIN
					+ "' La réponse est :\n" + reponse,
					reponse.messagesContains(TEMOIN));

		} catch (Exception e) {
			fail(e.getClass().getCanonicalName()
					+ " rencontrée lors du test : " + e.getMessage()
					+ "\nStack :\n" + CommonsUtil.stack2string(e));
		}
	}

	@Test
	public void testSetRoleNotRegisteredUser() {
		LOGGER.info("\nPortalisCtx **************************************************** @Test testSetRoleOk2 ***********************************\n");

		try {
			String email = CoreTestConstants.SUPER_USER.getEmail();
			String password = CoreTestConstants.SUPER_USER.getPassword();

			LOGGER.info("\nPortalisCtx ========================================> login <============================");
			LoginReponse loginReponse = UtilTest.loginHttpAndCheck(email,
					password);
			assertTrue("Login error : " + loginReponse, loginReponse.isOk());

			ActiveUser activeUser = loginReponse.getActiveUser();

			LOGGER.info("\nPortalisCtx ========================================> startCamelis <============================");
			ActiveLisService activeLisService = UtilTest
					.startPlanetsPhoto(activeUser);

			LOGGER.info("\nPortalisCtx ========================================> setRole <============================");
			RightValue rightValue = RightValue.ADMIN;
			SetRoleReponse reponse = new CamelisHttp(activeUser,
					activeLisService).setRole(CoreTestConstants.USER_KEY,
					rightValue);
			LOGGER.info("SetRoleReponse = " + reponse);
			assertTrue("Le retour doit être en erreur", !reponse.isOk());
			assertTrue("Un utilisateur ne doit pas être là. La réponse est :\n"
					+ reponse, reponse.getUser() == null);

			final String TEMOIN = ErrorMessages.TEMOIN_IS_NOT_REGISTERED;
			assertTrue("Les messages devraient contenir le témoin '" + TEMOIN
					+ "' La réponse est :\n" + reponse,
					reponse.messagesContains(TEMOIN));

		} catch (Exception e) {
			fail(e.getClass().getCanonicalName()
					+ " rencontrée lors du test : " + e.getMessage()
					+ "\nStack :\n" + CommonsUtil.stack2string(e));
		}
	}

	@Test
	public void testLogin() {
		LOGGER.info("\nPortalisCtx **************************************************** @Test testLogin ***********************************\n");

		try {
			String email = CoreTestConstants.SUPER_USER.getEmail();
			String password = CoreTestConstants.SUPER_USER.getPassword();

			LOGGER.info("\nPortalisCtx ========================================> login <============================");
			LoginReponse loginReponse = AdminHttp.login(email, password);
			assertTrue("la réponse doit être sans erreur, elle vaut :"
					+ loginReponse, loginReponse.isOk());

			ActiveUser activeUser = loginReponse.getActiveUser();
			assertTrue(activeUser != null);

			UserData userdata = activeUser.getUserCore();
			assertTrue(userdata != null);
			assertEquals(email, userdata.getEmail());
		} catch (Exception e) {
			fail(e.getClass().getCanonicalName()
					+ " rencontrée lors du test : " + e.getMessage()
					+ "\nStack :\n" + CommonsUtil.stack2string(e));
		}

	}

	@Test
	public void testStartCalmelisDefaultPort() {
		LOGGER.info("\nPortalisCtx **************************************************** @Test testStartCalmelisDefaultPort\n");
		String serviceName = CoreTestConstants.SERVICE_PLANETS_DISTANCE;
		try {
			String host = PortalisService.getInstance().getHost();
			String email = CoreTestConstants.SUPER_USER.getEmail();
			String password = CoreTestConstants.SUPER_USER.getPassword();

			LOGGER.info("\nPortalisCtx ========================================> login <============================");
			LoginReponse loginReponse = AdminHttp.login(email, password);
			assertTrue("la réponse doit être sans erreur, elle vaut :"
					+ loginReponse, loginReponse.isOk());

			ActiveUser activeUser = loginReponse.getActiveUser();
			assertTrue(activeUser != null);

			LOGGER.info("\nPortalisCtx ========================================> startCamelis <============================");
			StartReponse startReponse = AdminHttp.startCamelis(activeUser,
					serviceName);
			assertTrue("La réponse doit être sans erreur, startReponse ="
					+ startReponse, startReponse.isOk());
			ActiveLisService ActiveLisService = startReponse
					.getActiveLisService();

			LOGGER.info("\nPortalisCtx ========================================> getActifsPorts <============================");
			PortsActifsReponse portsActifsReponse2 = AdminHttp
					.getActifsPorts(activeUser);
			assertTrue(
					"La réponse doit être sans erreur, portsActifsReponse2 ="
							+ portsActifsReponse2, portsActifsReponse2.isOk());
			Set<PortActif> actifs = portsActifsReponse2.getListPortsActifs();
			assertTrue("Liste d'actifs null", actifs != null);
			assertEquals(1, actifs.size());

			PortActif portActif = new PortActif(host, startReponse
					.getActiveLisService().getPort(), ActiveLisService.getPid());

			assertTrue("le port actif " + portActif
					+ " doit être dans la liste des actfs",
					actifs.contains(portActif));
			assertEquals(serviceName, ActiveLisService.getFullName());
			assertEquals(CoreTestConstants.SUPER_USER.getEmail(),
					ActiveLisService.getCreator());
		} catch (Exception e) {
			fail(e.getClass().getCanonicalName()
					+ " rencontrée lors du test : " + e.getMessage()
					+ "\nStack :\n" + CommonsUtil.stack2string(e));
		}
	}

	@Test
	public void testStartCamelisSpecifiedPort() {
		LOGGER.info("\nPortalisCtx **************************************************** @Test testStartCamelisSpecifiedPort ***********************************\n");

		String serviceName = CoreTestConstants.SERVICE_PLANETS_DISTANCE;
		String host = PortalisService.getInstance().getHost();
		String email = CoreTestConstants.SUPER_USER.getEmail();
		String password = CoreTestConstants.SUPER_USER.getPassword();

		LOGGER.info("\nPortalisCtx ========================================> login <============================");
		LoginReponse loginReponse = AdminHttp.login(email, password);
		assertTrue("la réponse doit être sans erreur, elle vaut :"
				+ loginReponse, loginReponse.isOk());

		ActiveUser activeUser = loginReponse.getActiveUser();
		assertTrue(activeUser != null);

		LOGGER.info("\nPortalisCtx ========================================> startCamelis <============================");
		StartReponse startReponse = AdminHttp.startCamelis(activeUser,
				serviceName);
		assertTrue("La réponse doit être sans erreur, startReponse ="
				+ startReponse, startReponse.isOk());
		assertTrue("La réponse doit être sans erreur, startReponse ="
				+ startReponse, startReponse.isOk());
		ActiveLisService ActiveLisService = startReponse.getActiveLisService();
		assertTrue("Il doit y avoir un ActiveLisService",
				ActiveLisService != null);
		assertEquals(host, ActiveLisService.getHost());

		assertTrue("La réponse ne doit pas être en erreur, elle vaut :"
				+ startReponse, startReponse.isOk());

		LOGGER.info("\nPortalisCtx ========================================> getActifsPorts <============================");
		PortsActifsReponse portsActifsReponse = AdminHttp
				.getActifsPorts(activeUser);
		assertTrue("La réponse doit être sans erreur, portsActifsReponse ="
				+ portsActifsReponse, portsActifsReponse.isOk());
		Set<PortActif> actifs = portsActifsReponse.getListPortsActifs();
		assertTrue("La liste d'actifs actifs ne doit pas être nulle",
				actifs != null);
		assertEquals(1, actifs.size());
		assertEquals(serviceName, ActiveLisService.getFullName());
		assertEquals(CoreTestConstants.SUPER_USER.getEmail(),
				ActiveLisService.getCreator());
	}

	@Test
	public void testStartCalmelisArgumentsNeeded() {
		LOGGER.info("\nPortalisCtx **************************************************** @Test testStartCalmelisArgumentsNeeded ***********************************\n");
		try {
			String email = CoreTestConstants.SUPER_USER.getEmail();
			String password = CoreTestConstants.SUPER_USER.getPassword();

			LOGGER.info("\nPortalisCtx ========================================> login <============================");
			LoginReponse loginReponse = AdminHttp.login(email, password);
			assertTrue("la réponse doit être sans erreur, elle vaut :"
					+ loginReponse, loginReponse.isOk());

			ActiveUser activeUser = loginReponse.getActiveUser();
			assertTrue(activeUser != null);

			LOGGER.info("\nPortalisCtx ========================================> startCamelis <============================");
			String serviceName = XmlIdentifier.SERVICE_NAME();
			StartReponse reponse = AdminHttp.startCamelis(activeUser,
					serviceName);

			LOGGER.info("StartReponse = " + reponse);

			assertTrue("La réponse doit être en erreur, elle vaut :" + reponse,
					!reponse.isOk());

		} catch (Exception e) {
			fail(e.getClass().getCanonicalName()
					+ " rencontrée lors du test : " + e.getMessage()
					+ "\nAttention pushDocument() non exécuté");
		}
	}

	@Test
	public void testStartCamelisSpecifiedPortAllreadyInUse() {
		LOGGER.info("\nPortalisCtx **************************************************** @Test testStartCamelisSpecifiedPortAllreadyInUse ***********************************\n");

		String email = CoreTestConstants.SUPER_USER.getEmail();
		String password = CoreTestConstants.SUPER_USER.getPassword();

		LOGGER.info("\nPortalisCtx ========================================> login <============================");
		LoginReponse loginReponse = AdminHttp.login(email, password);
		assertTrue("la réponse doit être sans erreur, elle vaut :"
				+ loginReponse, loginReponse.isOk());

		ActiveUser activeUser = loginReponse.getActiveUser();
		assertTrue(activeUser != null);

		LOGGER.info("\nPortalisCtx ========================================> startCamelis <=========================");
		StartReponse startReponse1 = AdminHttp.startCamelis(activeUser,
				CoreTestConstants.SERVICE_PLANETS_DISTANCE);
		assertTrue("la réponse doit être sans erreur, elle vaut :"
				+ startReponse1, startReponse1.isOk());

		int port1 = startReponse1.getActiveLisService().getPort();

		LOGGER.info("\nPortalisCtx ========================================> startCamelis <=========================");
		StartReponse startReponse2 = AdminHttp.startCamelis(activeUser,
				CoreTestConstants.SERVICE_PLANETS_DISTANCE, port1);
		LOGGER.info("StartReponse2 = " + startReponse2);

		assertTrue("La réponse doit être en erreur, elle vaut :"
				+ startReponse2, !startReponse2.isOk());

		LOGGER.info("\nPortalisCtx ========================================> getActifsPorts <============================");
		PortsActifsReponse portsActifsReponse = AdminHttp
				.getActifsPorts(activeUser);
		assertTrue("La réponse doit être sans erreur, portsActifsReponse ="
				+ portsActifsReponse, portsActifsReponse.isOk());
		Set<PortActif> actifs = portsActifsReponse.getListPortsActifs();
		assertEquals(1, actifs.size());
		String host = PortalisService.getInstance().getHost();

		PortActif actif = new PortActif(host, port1, startReponse1
				.getActiveLisService().getPid());
		assertTrue(actif + " devrait être dans la liste des actifs",
				actifs.contains(actif));

	}

	@Test
	public void testStartCamelisIncorrectServiceName() {
		LOGGER.info("\nPortalisCtx **************************************************** @Test testStartCamelisIncorrectServiceName\n");
		try {
			LOGGER.info("\nPortalisCtx ========================================> login <============================");
			LoginReponse loginReponse = UtilTest.loginHttpAndCheck(
					CoreTestConstants.SUPER_USER.getEmail(),
					CoreTestConstants.SUPER_USER.getPassword());
			assertTrue("La réponse doit être sans erreur, loginReponse ="
					+ loginReponse, loginReponse.isOk());

			ActiveUser activeUser = loginReponse.getActiveUser();

			LOGGER.info("\nPortalisCtx ========================================> startCamelis <=========================");
			String serviceName = "service-1";
			StartReponse startReponse = AdminHttp.startCamelis(activeUser,
					serviceName, CoreTestConstants.PORT);
			assertTrue("startReponse doit être en erreur, il vaut "
					+ startReponse, !startReponse.isOk());
			final String TEMOIN = ErrorMessages.TEMOIN_INCORRECT_SERVICE_NAME;
			assertTrue("Les messages devraient contenir le témoin '" + TEMOIN
					+ "' La réponse est :\n" + startReponse,
					startReponse.messagesContains(TEMOIN));
		} catch (Exception e) {
			fail(e.getClass().getCanonicalName()
					+ " rencontrée lors du test : " + e.getMessage()
					+ "\nStack :\n" + CommonsUtil.stack2string(e));
		}
	}

	@Test
	public void testStopCalmelis() {
		LOGGER.info("\nPortalisCtx **************************************************** @Test testStopCalmelis\n");
		String serviceName = CoreTestConstants.SERVICE_PLANETS_DISTANCE;
		try {
			String email = CoreTestConstants.SUPER_USER.getEmail();
			String password = CoreTestConstants.SUPER_USER.getPassword();

			// ********************* login *********************
			LOGGER.info("\nPortalisCtx ========================================> login <=========================");
			LoginReponse loginReponse = UtilTest.loginHttpAndCheck(email,
					password);
			assertTrue("La réponse doit être sans erreur, loginReponse ="
					+ loginReponse, loginReponse.isOk());
			ActiveUser activeUser = loginReponse.getActiveUser();

			// ******************* startCamelis ****************
			LOGGER.info("\nPortalisCtx ========================================> startCamelis <=========================");
			StartReponse startReponse = AdminHttp.startCamelis(
					loginReponse.getActiveUser(), serviceName);
			assertTrue("La réponse doit être sans erreur, startReponse ="
					+ startReponse, startReponse.isOk());
			LOGGER.info("StartReponse = " + startReponse.toString());
			ActiveLisService ActiveLisService = startReponse
					.getActiveLisService();

			// *********************** ping ********************
			LOGGER.info("\nPortalisCtx ========================================> ping <=========================");
			PingReponse pingReponse = CamelisHttp.ping(ActiveLisService);
			assertTrue("La réponse ne doit pas être en erreur, pingReponse ="
					+ pingReponse, pingReponse.isOk());
			assertEquals(ActiveLisService, pingReponse.getFirstService());

			// ******************* stopCamelis ****************
			LOGGER.info("\nPortalisCtx ========================================> stopCamelis <=========================");
			PingReponse pingReponse1 = AdminHttp.stopCamelis(activeUser,
					ActiveLisService);
			assertTrue("La réponse doit être sans erreur, pingReponse1 ="
					+ pingReponse1, pingReponse1.isOk());
			LOGGER.info("pingReponse1 = " + pingReponse1.toString());

			// *********************** ping ********************
			LOGGER.info("\nPortalisCtx ========================================> ping <=========================");
			PingReponse pingReponse2 = CamelisHttp.ping(ActiveLisService);
			assertTrue("La réponse doit être en erreur, pingReponse ="
					+ pingReponse2, !pingReponse2.isOk());
			assertTrue(
					"Les messages devraient contenir le témoin '"
							+ ErrorMessages.TEMOIN_ERROR_DURING_HTTP_REQUEST
							+ "' La réponse est :\n" + pingReponse2,
					pingReponse2
							.messagesContains(ErrorMessages.TEMOIN_ERROR_DURING_HTTP_REQUEST));

		} catch (Exception e) {
			fail(e.getClass().getCanonicalName()
					+ " rencontrée lors du test : " + e.getMessage()
					+ "\nStack :\n" + CommonsUtil.stack2string(e));
		}
	}

}

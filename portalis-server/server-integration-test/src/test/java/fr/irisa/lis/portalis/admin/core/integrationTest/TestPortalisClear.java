package fr.irisa.lis.portalis.admin.core.integrationTest;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.net.InetAddress;
import java.util.Set;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.irisa.lis.cargo.CargoLauncher;
import fr.irisa.lis.cargo.CargoProprietes;
import fr.irisa.lis.portalis.camelis.http.UtilTest;
import fr.irisa.lis.portalis.commons.core.CommonsUtil;
import fr.irisa.lis.portalis.shared.admin.AdminProprietes;
import fr.irisa.lis.portalis.shared.admin.PortalisException;
import fr.irisa.lis.portalis.shared.admin.Util;
import fr.irisa.lis.portalis.shared.admin.data.ActiveUser;
import fr.irisa.lis.portalis.shared.admin.data.PortActif;
import fr.irisa.lis.portalis.shared.admin.data.PortalisService;
import fr.irisa.lis.portalis.shared.admin.http.AdminHttp;
import fr.irisa.lis.portalis.shared.admin.reponse.BooleanReponse;
import fr.irisa.lis.portalis.shared.admin.reponse.LoginReponse;
import fr.irisa.lis.portalis.shared.admin.reponse.PortsActifsReponse;
import fr.irisa.lis.portalis.shared.admin.reponse.VoidReponse;
import fr.irisa.lis.portalis.test.CoreTestConstants;

public class TestPortalisClear {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(TestPortalisClear.class.getName());
	
	private static final String tomcatContainerPath = CargoProprietes.cargoProp
			.getProperty("tomcatPath");
	private static final String tomcatContainerName = CargoProprietes.cargoProp
			.getProperty("tomcatName");
	private static final String tomcatContainer = tomcatContainerPath + tomcatContainerName;


	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		final String host = InetAddress.getLocalHost().getCanonicalHostName();
		final int portNum = 8080;
		final String portalisAppli = "portalis";
		PortalisService.getInstance().init(host, portNum, portalisAppli);		
		Util.clientInitLogPropertiesFile();
		LOGGER.info("\nPortalisCtx **************************************************** @BeforeClass TestPortalisClear ************");
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		LOGGER.info("\nPortalisCtx **************************************************** @AfterClass TestPortalisClear ******************************");
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testProperties() {
		LOGGER.info("\nPortalisCtx **************************************************** @Test testProperties ***********************************\n");
		try {
			CargoLauncher.start();
			UtilTest.killAllCamelisProcess();
			
			String email = CoreTestConstants.SUPER_USER.getEmail();
			String password = CoreTestConstants.SUPER_USER.getPassword();

			LOGGER.info("\nPortalisCtx ========================================> login <============================");
			LoginReponse loginReponse = UtilTest.loginHttpAndCheck(email,
					password);
			assertTrue("La réponse doit être sans erreur, loginReponse ="
					+ loginReponse, loginReponse.isOk());
			assertTrue(tomcatContainer+" doit être actif", CargoLauncher.tomcatIsActif());
			ActiveUser activeUser = loginReponse.getActiveUser();
			
			LOGGER.info("\nPortalisCtx ========================================> propertyFilesCheck <============================");
			VoidReponse voidReponse = AdminHttp.propertyFilesCheck(activeUser);
			assertTrue("La réponse ne doit pas être en erreur : "+voidReponse, voidReponse.isOk());
		} catch (Exception e) {
			fail("Exception rencontrée lors du test : " + e.getMessage()
					+ "\nStack :\n" + CommonsUtil.stack2string(e));
		} finally {
			try {
				UtilTest.killAllCamelisProcess();
			} catch (PortalisException e) {
				fail("On n'arrive pas à tuer les processus");
			}
			CargoLauncher.stop();
		}
	}
	
	@Test
	public void testAdminPropertiesFilesArePresent() {
		LOGGER.info("\nPortalisCtx **************************************************** @Test testAdminPropertiesFilesArePresent ***********************************\n");
		try {
			assertTrue(AdminProprietes.test());
		} catch (Exception e) {
			fail(e.getClass().getCanonicalName()
					+ " rencontrée lors du test : " + e.getMessage()
					+ "\nStack :\n" + CommonsUtil.stack2string(e));
		}
	}


	@Test
	public void testNoActifsPorts() {
		LOGGER.info("\nPortalisCtx **************************************************** @Test testNoActifsPorts ***********************************\n");
		try {
			CargoLauncher.start();
			UtilTest.killAllCamelisProcess();

			String email = CoreTestConstants.SUPER_USER.getEmail();
			String password = CoreTestConstants.SUPER_USER.getPassword();

			LOGGER.info("\nPortalisCtx ========================================> login <============================");
			LoginReponse loginReponse = UtilTest.loginHttpAndCheck(email,
					password);
			assertTrue("La réponse doit être sans erreur, loginReponse ="
					+ loginReponse, loginReponse.isOk());
			ActiveUser activeUser = loginReponse.getActiveUser();

			LOGGER.info("\nPortalisCtx ========================================> getActifsPorts <============================");
			PortsActifsReponse portsActifsReponse = AdminHttp.getActifsPorts(activeUser);
			Set<PortActif> actifs = portsActifsReponse.getListPortsActifs();
			assertTrue("Liste d'actifs null", actifs != null);
			assertEquals(0, actifs.size());

		} catch (Exception e) {
			fail(e.getClass().getCanonicalName()
					+ " rencontrée lors du test : " + e.getMessage()
					+ "\nStack :\n" + CommonsUtil.stack2string(e));
		} finally {
			try {
				UtilTest.killAllCamelisProcess();
			} catch (PortalisException e) {
				fail("On n'arrive pas à tuer les processus");
			}
			CargoLauncher.stop();
		}
	}

	
	@Test
	public void testTomcatIsActif() {
		LOGGER.info("\nPortalisCtx **************************************************** @Test testTomcatIsActif ***********************************\n");
		try {
			CargoLauncher.start();
			UtilTest.killAllCamelisProcess();

			String email = CoreTestConstants.SUPER_USER.getEmail();
			String password = CoreTestConstants.SUPER_USER.getPassword();

			LOGGER.info("\nPortalisCtx ========================================> login <============================");
			LoginReponse loginReponse = UtilTest.loginHttpAndCheck(email,
					password);
			assertTrue("La réponse doit être sans erreur, loginReponse ="
					+ loginReponse, loginReponse.isOk());
			ActiveUser activeUser = loginReponse.getActiveUser();

			assertTrue(tomcatContainer+" doit être actif", CargoLauncher.tomcatIsActif());
			
			LOGGER.info("\nPortalisCtx ========================================> getCamelisProcessId <============================");
			int port = PortalisService.getInstance().getPort();
			BooleanReponse booleanReponse = AdminHttp.portIsBusy(activeUser, port);
			assertTrue("La réponse doit être sans erreur, pidReponse ="
					+ booleanReponse, booleanReponse.isOk());
			assertTrue("Il doit y avoir un processus sur le port "+
					port, booleanReponse.isAnswer());

		} catch (Exception e) {
			fail(e.getClass().getCanonicalName()
					+ " rencontrée lors du test : " + e.getMessage()
					+ "\nStack :\n" + CommonsUtil.stack2string(e));
		} finally {
			try {
				UtilTest.killAllCamelisProcess();
			} catch (PortalisException e) {
				fail("On n'arrive pas à tuer les processus");
			}
			CargoLauncher.stop();
		}
	}

	@Test
	public void testStartTomcat() {
		LOGGER.info("\nPortalisCtx **************************************************** @Test testStartTomcat ***********************************\n");
		try {
			CargoLauncher.start();
			UtilTest.killAllCamelisProcess();
			assertTrue(tomcatContainer+" doit être actif", CargoLauncher.tomcatIsActif());

			String email = CoreTestConstants.SUPER_USER.getEmail();
			String password = CoreTestConstants.SUPER_USER.getPassword();

			LOGGER.info("\nPortalisCtx ========================================> login <============================");
			LoginReponse loginReponse = UtilTest.loginHttpAndCheck(email,
					password);
			assertTrue("La réponse doit être sans erreur, loginReponse ="
					+ loginReponse, loginReponse.isOk());
		} catch (Exception e) {
			fail(e.getClass().getCanonicalName()
					+ " rencontrée lors du test : " + e.getMessage()
					+ "\nStack :\n" + CommonsUtil.stack2string(e));
		}  finally {
			try {
				UtilTest.killAllCamelisProcess();
			} catch (PortalisException e) {
				fail("On n'arrive pas à tuer les processus");
			}
			CargoLauncher.stop();
		}
	}



}

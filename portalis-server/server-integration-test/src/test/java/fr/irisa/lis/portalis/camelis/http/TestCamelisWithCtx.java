package fr.irisa.lis.portalis.camelis.http;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.io.File;
import java.net.InetAddress;
import java.net.URLEncoder;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.irisa.lis.cargo.CargoLauncher;
import fr.irisa.lis.portalis.commons.core.CommonsUtil;
import fr.irisa.lis.portalis.shared.admin.Util;
import fr.irisa.lis.portalis.shared.admin.XmlIdentifier;
import fr.irisa.lis.portalis.shared.admin.data.ActiveLisService;
import fr.irisa.lis.portalis.shared.admin.data.ActiveUser;
import fr.irisa.lis.portalis.shared.admin.data.PortalisService;
import fr.irisa.lis.portalis.shared.admin.data.Property;
import fr.irisa.lis.portalis.shared.admin.http.AdminHttp;
import fr.irisa.lis.portalis.shared.admin.reponse.LoginReponse;
import fr.irisa.lis.portalis.shared.admin.reponse.VoidReponse;
import fr.irisa.lis.portalis.shared.camelis.data.LisExtent;
import fr.irisa.lis.portalis.shared.camelis.data.LisIncrement;
import fr.irisa.lis.portalis.shared.camelis.data.LisIncrementSet;
import fr.irisa.lis.portalis.shared.camelis.data.LisIntent;
import fr.irisa.lis.portalis.shared.camelis.data.LisObject;
import fr.irisa.lis.portalis.shared.camelis.http.CamelisHttp;
import fr.irisa.lis.portalis.shared.camelis.reponse.ExtentReponse;
import fr.irisa.lis.portalis.shared.camelis.reponse.GetValuedFeaturesReponse;
import fr.irisa.lis.portalis.shared.camelis.reponse.ImportCtxReponse;
import fr.irisa.lis.portalis.shared.camelis.reponse.IntentReponse;
import fr.irisa.lis.portalis.shared.camelis.reponse.PingReponse;
import fr.irisa.lis.portalis.shared.camelis.reponse.StartReponse;
import fr.irisa.lis.portalis.shared.camelis.reponse.ZoomReponse;
import fr.irisa.lis.portalis.test.CoreTestConstants;



public class TestCamelisWithCtx {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(TestCamelisWithCtx.class.getName());
	private static ActiveLisService activeLisService;
	private static ActiveUser activeUser;
	private static CamelisHttp camelisHttp;


	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		final String host = InetAddress.getLocalHost().getCanonicalHostName();
		final int portNum = 8080;
		final String portalisAppli = "portalis";
		PortalisService.getInstance().init(host, portNum, portalisAppli);
		Util.clientInitLogPropertiesFile();
		LOGGER.info("\nPortalisCtx **************************************************** @BeforeClass TestCamelisWithCtx ************");

			if (CargoLauncher.tomcatIsActif()) {
				CargoLauncher.stop();
			}
			CargoLauncher.start();

		try {
			String email = CoreTestConstants.SUPER_USER.getEmail();
			String password = CoreTestConstants.SUPER_USER.getPassword();

			LOGGER.info("\nPortalisCtx ========================================> Login <============================");
			LoginReponse loginReponse = UtilTest.loginHttpAndCheck(email, password);
			activeUser = loginReponse.getActiveUser();

			LOGGER.info("\nPortalisCtx ========================================> startCamelis <============================");
			int camelisPort = CoreTestConstants.PORT1;
			StartReponse startReponse = AdminHttp.startCamelis(activeUser, CoreTestConstants.SERVICE_PLANETS_PLANETS, camelisPort);
			assertTrue("Le démarrage de Camelis n'a pas marché", startReponse!=null);
			assertTrue("La réponse doit être sans erreur, startReponse ="
					+ startReponse, startReponse.isOk());
			activeLisService = startReponse.getActiveLisService();
			assertTrue("pas de service en retour du démarage de Camelis"+startReponse, activeLisService!=null);
			camelisHttp = new CamelisHttp(activeUser, activeLisService);

			LOGGER.info("\nPortalisCtx ========================================> installFreshCtx <============================");
			UtilTest.installResourcesForFreshPlanetsCtx();
			String ctxFile = "planets.ctx";

			LOGGER.info("\nPortalisCtx ========================================> importCtx <============================");
			ImportCtxReponse importReponse = camelisHttp.importCtx(ctxFile);
			assertTrue("L'import n'a pas marché", importReponse!=null);
			assertTrue("problème d'importCtx : "+importReponse, importReponse.isOk());
		} catch (Exception e) {
			fail(e.getClass().getCanonicalName()
					+ " rencontrée lors du test : " + e.getMessage()
					+ "\nStack :\n" + CommonsUtil.stack2string(e));
		}
}


	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		LOGGER.info("\nPortalisCtx **************************************************** @AfterClass ******************************");
		UtilTest.killAllCamelisProcessAndClearUsers();
		CargoLauncher.stop();
		LOGGER.info("\nPortalisCtx **************************************************** @fin AfterClass TestCamelisWithCtx **********");
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testExtentReponse() {
		LOGGER.info("\nPortalisCtx **************************************************** @Test testExtentReponse ***********************************\n");
		try {
			String lisQuery = "Medium";
			ExtentReponse reponse = camelisHttp.extent(lisQuery);
			assertNotNull(reponse);
			assertTrue("l'extension ne doit doit pas être en erreur :"+ reponse, reponse.isOk());
			LisExtent receivedExtent = reponse.getExtent();
			assertTrue("Il doit y avoir deux éléments dans l'extension :"+ reponse,
						receivedExtent != null && receivedExtent.card() == 2);
			// extent(medium) should contain two objects: neptune and uranus
			String ctxDir = CoreTestConstants.WEBAPPS_DIR + "/planets/planets/";
			LisObject[] expectedObjs = { new LisObject(5, "neptune", ctxDir + "images/neptune.jpg", ""),
					  					 new LisObject(8, "uranus", ctxDir + "images/uranus.jpg", "") };
			LisExtent expectedExtent = new LisExtent(expectedObjs);
			assertTrue("Received extent\n"+receivedExtent+"does not match expected extent\n"+expectedExtent,
						expectedExtent.equals(receivedExtent));
		} catch (Exception e) {
			fail(e.getClass().getCanonicalName()
					+ " rencontrée lors du test : " + e.getMessage()
					+ "\nStack :\n" + CommonsUtil.stack2string(e));
		}
	}


	@Test @Ignore 
	public void testExtentERR() {
		LOGGER.info("\nPortalisCtx **************************************************** @Test testExtentERR ***********************************\n");
		try {

			LOGGER.info("\nPortalisCtx ========================================> login <============================");
			LoginReponse loginReponse = AdminHttp.login("bekkers@irisa.fr", "yves");
			assertTrue("la réponse doit être OK : "+loginReponse, loginReponse.getStatus().equals(XmlIdentifier.OK));
			ActiveUser activeUser1 = loginReponse.getActiveUser();

			LOGGER.info("\nPortalisCtx ========================================> startCamelis <============================");
			StartReponse startReponse = AdminHttp.startCamelis(activeUser1, CoreTestConstants.SERVICE_THABOR_ARBRES_THABOR);
			assertTrue("Le démarrage de Camelis n'a pas marché", startReponse!=null);
			assertTrue("la réponse doit être OK : "+startReponse, startReponse.getStatus().equals(XmlIdentifier.OK));
			ActiveLisService activeLisService1 = startReponse.getActiveLisService();
			assertTrue("pas de service en retour du démarage de Camelis : "+startReponse, activeLisService1!=null);

			LOGGER.info("\nPortalisCtx ========================================> getActiveLisServicesOnPortalis <============================");
			PingReponse pingReponse = AdminHttp.getActiveLisServicesOnPortalis(activeUser1);
			assertTrue("la réponse doit être OK : "+pingReponse, pingReponse.getStatus().equals(XmlIdentifier.OK));
			assertTrue("La réponse doit contenir le service actif : "+activeLisService1+
					"\nreponse = "+pingReponse, pingReponse.contains(activeLisService1));
			
			LOGGER.info("\nPortalisCtx ========================================> importCtx <============================");
			String ctxFile = "arbres_thabor.ctx";
			CamelisHttp camelisHttp1 =  new CamelisHttp(activeUser1, activeLisService1);
			ImportCtxReponse importReponse = camelisHttp1.importCtx(ctxFile);
			assertTrue("la réponse doit être OK : "+importReponse, importReponse.getStatus().equals(XmlIdentifier.OK));
			
			LOGGER.info("\nPortalisCtx ========================================> zoom <============================");
			ZoomReponse zoomReponse = camelisHttp1.zoom("Magnolia", "gps ?");
			assertTrue("la réponse doit être OK : "+zoomReponse, zoomReponse.getStatus().equals(XmlIdentifier.OK));
			int nbMagnoliaWithGps = 49;
			assertTrue("la reponse doit contenir "+nbMagnoliaWithGps+ " Magnolia avec gps. La reponse est = "+zoomReponse, zoomReponse.getIncrements().getIncrements().size() == nbMagnoliaWithGps);
			
			LOGGER.info("\nPortalisCtx ========================================> extent <============================");
			ExtentReponse extentReponse = camelisHttp1.extent();
			assertTrue("la réponse doit être OK : "+extentReponse, extentReponse.getStatus().equals(XmlIdentifier.OK));
			int nbArbresTotal = 1099;
			assertTrue("la reponse doit contenir "+nbArbresTotal+ " arbres. La reponse est = "+zoomReponse, extentReponse.getExtent().card() == nbArbresTotal);
			
			LOGGER.info("\nPortalisCtx ========================================> stopCamelis <============================");
			VoidReponse stopReponse = AdminHttp.stopCamelis(activeUser1, activeLisService1);
			assertTrue("la réponse doit être OK : "+stopReponse, stopReponse.getStatus().equals(XmlIdentifier.OK));

			LOGGER.info("\n====================================== AdminHttp.logout ====================================");
			VoidReponse pietLogoutReponse = AdminHttp.logout(activeUser1);
			assertTrue("la réponse doit être OK : "+pietLogoutReponse, pietLogoutReponse.getStatus().equals(XmlIdentifier.OK));

			
		} catch (Exception e) {
			fail(e.getClass().getCanonicalName()
					+ " rencontrée lors du test : " + e.getMessage()
					+ "\nStack :\n" + CommonsUtil.stack2string(e));
		}
	}
	
	@Test @Ignore
	public void testExtentERRBis() {
		LOGGER.info("\nPortalisCtx **************************************************** @Test testExtentERRBis ***********************************\n");
		try {

			LOGGER.info("\nPortalisCtx ========================================> login <============================");
			LoginReponse loginReponse = AdminHttp.login("bekkers@irisa.fr", "yves");
			assertTrue("la réponse doit être OK : "+loginReponse, loginReponse.getStatus().equals(XmlIdentifier.OK));
			ActiveUser activeUser1 = loginReponse.getActiveUser();

			LOGGER.info("\nPortalisCtx ========================================> startCamelis <============================");
			StartReponse startReponse = AdminHttp.startCamelis(activeUser1, CoreTestConstants.SERVICE_THABOR_ARBRES_THABOR);
			assertTrue("Le démarrage de Camelis n'a pas marché", startReponse!=null);
			assertTrue("la réponse doit être OK : "+startReponse, startReponse.getStatus().equals(XmlIdentifier.OK));
			ActiveLisService activeLisService1 = startReponse.getActiveLisService();
			assertTrue("pas de service en retour du démarage de Camelis : "+startReponse, activeLisService1!=null);

			LOGGER.info("\nPortalisCtx ========================================> getActiveLisServicesOnPortalis <============================");
			PingReponse pingReponse = AdminHttp.getActiveLisServicesOnPortalis(activeUser1);
			assertTrue("la réponse doit être OK : "+pingReponse, pingReponse.getStatus().equals(XmlIdentifier.OK));
			assertTrue("La réponse doit contenir le service actif : "+activeLisService1+
					"\nreponse = "+pingReponse, pingReponse.contains(activeLisService1));
			
			LOGGER.info("\nPortalisCtx ========================================> importCtx <============================");
			String ctxFile = "arbres_thabor.ctx";
			CamelisHttp camelisHttp1 = new CamelisHttp(activeUser1, activeLisService1);
			ImportCtxReponse importReponse = camelisHttp1.importCtx(ctxFile);
			assertTrue("la réponse doit être OK : "+importReponse, importReponse.getStatus().equals(XmlIdentifier.OK));
			
			LOGGER.info("\nPortalisCtx ========================================> getValuedFeatures <============================");
			int[] oidArray={};
			String[] feat = {"gps"};
			GetValuedFeaturesReponse valuedFeatures = camelisHttp1.getValuedFeatures(oidArray, feat);
			assertTrue("la réponse doit être OK : "+valuedFeatures, valuedFeatures.getStatus().equals(XmlIdentifier.OK));
			
			LOGGER.info("\nPortalisCtx ========================================> stopCamelis <============================");
			VoidReponse stopReponse = AdminHttp.stopCamelis(activeUser1, activeLisService1);
			assertTrue("la réponse doit être OK : "+stopReponse, stopReponse.getStatus().equals(XmlIdentifier.OK));
			
			LOGGER.info("\n====================================== AdminHttp.logout ====================================");
			VoidReponse pietLogoutReponse = AdminHttp.logout(activeUser1);
			assertTrue("la réponse doit être OK : "+pietLogoutReponse, pietLogoutReponse.getStatus().equals(XmlIdentifier.OK));
			
		} catch (Exception e) {
			fail(e.getClass().getCanonicalName()
					+ " rencontrée lors du test : " + e.getMessage()
					+ "\nStack :\n" + CommonsUtil.stack2string(e));
		}
	}
	
	
	
	@Test
	public void testPaginatedExtentReponse() {
		LOGGER.info("\nPortalisCtx ********** @Test testPaginatedExtentReponse **********\n");
		try {
			String lisQuery = "all";
			ExtentReponse reponse = camelisHttp.extent(lisQuery, 1, 2);
			assertNotNull(reponse);
			assertTrue("l'extension ne doit doit pas être en erreur :"+ reponse, reponse.isOk());
			LisExtent receivedExtent = reponse.getExtent();
			assertTrue("Il doit y avoir deux éléments dans l'extension :"+ reponse,
						receivedExtent != null && receivedExtent.card() == 2);
			// page 1 of extent(all) should contain those two objects: mars and mercury
			String ctxDir = CoreTestConstants.WEBAPPS_DIR + "/planets/planets/";
			LisObject[] expectedObjs = { new LisObject(3, "mars", ctxDir + "images/mars.jpg", ""),
					  					 new LisObject(4, "mercury", ctxDir + "images/mercury.jpg", "") };
			LisExtent expectedExtent = new LisExtent(expectedObjs);
			assertTrue("Received extent\n"+receivedExtent+"does not match expected extent\n"+expectedExtent,
						expectedExtent.equals(receivedExtent));
		} catch (Exception e) {
			fail(e.getClass().getCanonicalName()
					+ " rencontrée lors du test : " + e.getMessage()
					+ "\nStack :\n" + CommonsUtil.stack2string(e));
		}
	}
	
	
	@Test
	public void testIntentReponse() {
		LOGGER.info("\nPortalisCtx **************************************************** @Test testIntentReponse ***********************************\n");
		try {
			String[] oids = { "1", "2", "5", "8" };
			IntentReponse reponse = camelisHttp.intent(oids);
			assertNotNull(reponse);
			LisIntent receivedIntent = reponse.getIntent();
			// intent({1,2,5,8}) should contain 3 features: Distance, satellite, Size
			LisIntent expectedIntent = new LisIntent();
			expectedIntent.add(new Property("Distance"));
			expectedIntent.add(new Property("satellite"));
			expectedIntent.add(new Property("Size"));
			assertTrue(String.format("Received intent %s\ndoes not match expected intent %s", receivedIntent, expectedIntent),
						expectedIntent.equals(receivedIntent));
		} catch (Exception e) {
			fail(e.getClass().getCanonicalName()
					+ " rencontrée lors du test : " + e.getMessage()
					+ "\nStack :\n" + CommonsUtil.stack2string(e));
		}
	}

	
	

	@Test
	public void testZoomReponse() {
		LOGGER.info("\nPortalisCtx **************************************************** @Test testZoomReponse ***********************************\n");
		try {
			String wq = "Medium";
			String feat = "all";
			ZoomReponse reponse = camelisHttp.zoom(wq, feat);
			assertNotNull(reponse);
			assertTrue("Zoom should not return an error:"+ reponse, reponse.isOk());
			LisIncrementSet receivedIncrs = reponse.getIncrements();
			assertTrue("There should be 3 increments: "+ reponse,
						receivedIncrs != null && receivedIncrs.card() == 3);
			// the increments should be as follow:
			LisIncrement[] incrs = { new LisIncrement("Distance", 2, null),
					  				 new LisIncrement("satellite", 2, null),
					  				 new LisIncrement("Size", 2, null) };
			LisIncrementSet expectedIncrs = new LisIncrementSet(incrs);
			assertTrue("Received increments does not match expected increments",
						expectedIncrs.equals(receivedIncrs));
		} catch (Exception e) {
			fail(e.getClass().getCanonicalName()
					+ " rencontrée lors du test : " + e.getMessage()
					+ "\nStack :\n" + CommonsUtil.stack2string(e));
		}
	}


	@Test
	public void testPaginatedZoomReponse() {
		LOGGER.info("\nPortalisCtx **************************************************** @Test testZoomReponse ***********************************\n");
		try {
			String wq = "all";
			String feat = "all";
			ZoomReponse reponse = camelisHttp.zoom(wq, feat, 1, 2);
			assertNotNull(reponse);
			assertTrue("Zoom should not return an error:"+ reponse, reponse.isOk());
			LisIncrementSet receivedIncrs = reponse.getIncrements();
			assertTrue("There should be 2 increments: "+ reponse,
						receivedIncrs != null && receivedIncrs.card() == 2);
			// the increments should be as follow:
			LisIncrement[] incrs = { new LisIncrement("satellite", 7, null),
					  				 new LisIncrement("bar ?", 2, null) };
			LisIncrementSet expectedIncrs = new LisIncrementSet(incrs);
			assertTrue("Received increments does not match expected increments",
						expectedIncrs.equals(receivedIncrs));
		} catch (Exception e) {
			fail(e.getClass().getCanonicalName()
					+ " rencontrée lors du test : " + e.getMessage()
					+ "\nStack :\n" + CommonsUtil.stack2string(e));
		}
	}


	@Test
	public void testGetValuedFeatures() {
		LOGGER.info("\nPortalisCtx **************************************************** @Test testGetValuedFeatures ***********************************\n");
		try {
			int[] oids = {1, 2};
			String[] feats = {"foo", "bar"};
			GetValuedFeaturesReponse reponse = camelisHttp.getValuedFeatures(oids, feats);
			assertNotNull(reponse);
			assertTrue("getValuedFeatures should not return an error: " + reponse, reponse.isOk());

			// check response is correct
			assertTrue(reponse.getValues(1, "foo").contains("toto"));
			assertTrue(reponse.getValues(1, "bar").contains("42"));
			assertTrue(reponse.getValues(1, "bar").contains("12"));
			assertTrue(reponse.getValues(2, "foo").contains("tata"));
			assertTrue(reponse.getValues(2, "bar").contains("1618"));
		} catch (Exception e) {
			fail(e.getClass().getCanonicalName()
					+ " rencontrée lors du test : " + e.getMessage()
					+ "\nStack :\n" + CommonsUtil.stack2string(e));
		}
	}

	@Test
	public void testGetResource() {
		LOGGER.info("\nPortalisCtx ********** @Test testGetResources **********\n");
		try {
			java.net.URL reponse = camelisHttp.getResource("images/earth.jpg");
			assertNotNull(reponse);

			// check response is correct
			assertEquals(reponse.getProtocol(), "http");
			assertEquals(reponse.getHost(), activeLisService.getHost());
			assertEquals(reponse.getPort(), activeLisService.getPort());
			assertEquals(reponse.getPath(), "/getResource");
			String query = reponse.getQuery();
			assertTrue(query.matches("userKey=[a-f0-9]{64}&file="+URLEncoder.encode("images/earth.jpg", "utf-8")));

			// try to download the file
			File res = UtilTest.downloadTo(reponse);
			assertTrue(res.length() > 0);
		} catch (Exception e) {
			fail(e.getClass().getCanonicalName()
					+ " rencontrée lors du test : " + e.getMessage()
					+ "\nStack :\n" + CommonsUtil.stack2string(e));
		}
	}

	@Test
	public void testGetThumbnail() {
		LOGGER.info("\nPortalisCtx ********** @Test testGetThumbnail **********\n");
		try {
			java.net.URL reponse = camelisHttp.getThumbnail("images/earth.jpg", 64, 64);
			assertNotNull(reponse);

			// check response is correct
			assertEquals(reponse.getProtocol(), "http");
			assertEquals(reponse.getHost(), activeLisService.getHost());
			assertEquals(reponse.getPort(), activeLisService.getPort());
			assertEquals(reponse.getPath(), "/getThumbnail");
			String query = reponse.getQuery();
			assertTrue(query.matches("userKey=[a-f0-9]{64}&file="+URLEncoder.encode("images/earth.jpg", "utf-8")+"&width=64&height=64"));

			// try to download the file
			File res = UtilTest.downloadTo(reponse);
			assertTrue(res.length() > 0);
		} catch (Exception e) {
			fail(e.getClass().getCanonicalName()
					+ " rencontrée lors du test : " + e.getMessage()
					+ "\nStack :\n" + CommonsUtil.stack2string(e));
		}
	}
}

	package fr.irisa.lis.portalis.camelis.http;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.nio.channels.Channels;
import java.nio.channels.FileChannel;
import java.nio.channels.ReadableByteChannel;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.irisa.lis.portalis.shared.admin.PortalisException;
import fr.irisa.lis.portalis.shared.admin.RequestException;
import fr.irisa.lis.portalis.shared.admin.RightValue;
import fr.irisa.lis.portalis.shared.admin.data.ActiveLisService;
import fr.irisa.lis.portalis.shared.admin.data.ActiveUser;
import fr.irisa.lis.portalis.shared.admin.data.UserData;
import fr.irisa.lis.portalis.shared.admin.http.AdminHttp;
import fr.irisa.lis.portalis.shared.admin.reponse.LoginReponse;
import fr.irisa.lis.portalis.shared.admin.reponse.PidReponse;
import fr.irisa.lis.portalis.shared.admin.reponse.PortsActifsReponse;
import fr.irisa.lis.portalis.shared.admin.reponse.VoidReponse;
import fr.irisa.lis.portalis.shared.camelis.http.CamelisHttp;
import fr.irisa.lis.portalis.shared.camelis.reponse.PingReponse;
import fr.irisa.lis.portalis.shared.camelis.reponse.RegisterKeyReponse;
import fr.irisa.lis.portalis.shared.camelis.reponse.StartReponse;
import fr.irisa.lis.portalis.test.CoreTestConstants;

public class UtilTest {private static final Logger LOGGER = LoggerFactory.getLogger(UtilTest.class.getName());


	public static ActiveLisService startPlanetsPhoto(ActiveUser activeUser)
            throws PortalisException, IOException, InterruptedException {
		StartReponse startReponse = AdminHttp.startCamelis(activeUser,
				CoreTestConstants.SERVICE_PLANETS_DISTANCE);
		assertTrue("Le retour ne doit pas être en erreur startReponse="
				+ startReponse, startReponse.isOk());

		ActiveLisService activeLisService = startReponse.getActiveLisService();
		int port = activeLisService.getPort();

		PingReponse pingReponse = CamelisHttp.ping(activeLisService);
		LOGGER.debug("startOneCamelis() activeUser =" + activeUser + "activeLisService ="
				+ activeLisService + "pingReponse =" + pingReponse);
		assertTrue("Le retour ne doit pas être en erreur pingReponse="
				+ pingReponse, pingReponse.isOk());
		assertEquals(activeLisService, pingReponse.getFirstService());
		assertEquals(port, pingReponse.getFirstService().getPort());
		return activeLisService;
	}

	public static ActiveUser startOneCamelis(ActiveUser activeUser, String serviceFullName)
            throws PortalisException, IOException, InterruptedException {
		StartReponse startReponse = AdminHttp.startCamelis(
				activeUser, serviceFullName);
		assertTrue("Le retour ne doit pas être en erreur :"+startReponse, startReponse.isOk());

		ActiveLisService activeLisService = startReponse.getActiveLisService();
		int port = activeLisService.getPort();

		PingReponse pingReponse = CamelisHttp.ping(activeLisService);
		LOGGER.debug("startOneCamelis() activeUser =" + activeUser + "activeLisService ="
				+ activeLisService + "pingReponse =" + pingReponse);
		assertTrue("Le retour ne doit pas être en erreur :"+pingReponse, pingReponse.isOk());
		assertEquals(activeLisService, pingReponse.getFirstService());
		assertEquals(port, pingReponse.getFirstService().getPort());

		return activeUser;
	}

	public static void registerUser(ActiveUser activeUser, ActiveLisService service, String userKey,
			RightValue rightValue) throws PortalisException {
		RegisterKeyReponse camelisUserResponse = new CamelisHttp(activeUser, service).registerKey(userKey, rightValue);
		LOGGER.info("CamelisUserResponse = " + camelisUserResponse);
		assertTrue("Le retour ne doit pas être en erreur\nreponse = "
				+ camelisUserResponse, camelisUserResponse.isOk());
		assertTrue("Un utilisateur doit être là. La réponse est :\n"
				+ camelisUserResponse, camelisUserResponse.getUser() != null);

		assertEquals(rightValue, camelisUserResponse.getUser().getRole());
	}



	public static StartReponse startCamelis(ActiveUser activeUser) throws PortalisException {
		StartReponse reponse = AdminHttp.startCamelis(activeUser,
				CoreTestConstants.SERVICE_PLANETS_DISTANCE);
		assertTrue("La reponse ne doit pas être en erreur, reponse=" + reponse,
				reponse.isOk());
		assertTrue("Le service Camelis ne doit pas être nul",
				reponse.getActiveLisService() != null);

		PortsActifsReponse actifs = AdminHttp.getActifsPorts(activeUser);
		assertTrue("actifs ne doit pas être null", actifs != null);
		assertTrue("actifs ne doit pas être en erreur, il vaut "+actifs, actifs != null);
		return reponse;
	}

	public static StartReponse startCamelis(ActiveUser activeUser, int port) throws PortalisException {
		StartReponse startReponse = AdminHttp.startCamelis(activeUser,
				CoreTestConstants.SERVICE_PLANETS_DISTANCE, port);
		assertTrue("La reponse ne doit pas être nulle, startReponse=" + startReponse,
				startReponse != null);
		assertTrue("La reponse ne doit pas être en erreur, startReponse=" + startReponse,
				startReponse.isOk());
		assertTrue("Le service Camelis ne doit pas être nul",
				startReponse.getActiveLisService() != null);

		PortsActifsReponse actifs = AdminHttp.getActifsPorts(activeUser);
		assertTrue("actifs ne doit pas être null", actifs != null);
		assertTrue("actifs ne doit pas être en erreur, il vaut "+actifs, actifs != null);
		return startReponse;
	}

	public static void isPresent(int pid, PidReponse reponse) {
		Set<Integer> pidList = reponse.getPids();
		assertTrue(
				"Le processus " + pid + " devrait être présent dans la liste "
						+ reponse.getPidsAsString(), pidList.contains(pid));
	}
	public static LoginReponse loginHttpAndCheck(String email,
			String password) throws PortalisException {
		LoginReponse reponse = AdminHttp.login(email, password);
		assertTrue ("loginReponse ne doit doit pas être en erreur :"+ reponse, reponse.isOk());
		ActiveUser activeUser = reponse.getActiveUser();
		assertTrue(activeUser!=null);
		UserData UserCore = activeUser.getUserCore();
		assertTrue(UserCore!=null);
		assertEquals(email,UserCore.getEmail());

		return reponse;
	}

	private static void copyFile(File sourceFile, File destFile) throws IOException {
	    if (!destFile.exists()) destFile.createNewFile();
	    FileChannel source = null;
	    FileChannel destination = null;
	    try {
	        source = new FileInputStream(sourceFile).getChannel();
	        destination = new FileOutputStream(destFile).getChannel();
	        destination.transferFrom(source, 0, source.size());
	    } finally {
	        if (source != null) source.close();
	        if (destination != null) destination.close();
	    }
	}

	private static File getTestResource(String name) {
		ClassLoader cl = Thread.currentThread().getContextClassLoader();
		return new File(cl.getResource(name).getPath());
	}

	public static void installTestResource(String src, String dest) throws IOException {
		copyFile(getTestResource(src), new File(dest));
	}

	public static void installResourcesForFreshPlanetsCtx() throws Exception {
		// ----- copy fresh ctx file
		// TODO: remove hardcoded path and filename
		LOGGER.debug("Copying fresh planets.ctx to the planets:planets service");
		// first, create directory if needed
		File planetsDir = new File(CoreTestConstants.WEBAPPS_DIR + "/planets/planets/images");
		if (!planetsDir.exists()) {
			planetsDir.mkdirs();
		}
		// clean out if necessary
		File oldLis = new File(CoreTestConstants.WEBAPPS_DIR + "/planets/planets/planets.lis");
		if (oldLis.exists()) {
			boolean success = oldLis.delete();
			if (!success) throw new Exception("Deleting .lis file in installFreshPlanetsCtx failed.");
		}
		// install a ctx file
		installTestResource(
			"planets-test.ctx",
			CoreTestConstants.WEBAPPS_DIR + "/planets/planets/planets.ctx");
		// install images
		String imgDir = CoreTestConstants.WEBAPPS_DIR + "/planets/planets/images/";
		installTestResource("images/earth.jpg",   imgDir + "earth.jpg");
		installTestResource("images/jupiter.gif", imgDir + "jupiter.gif");
		installTestResource("images/mars.jpg",    imgDir + "mars.jpg");
		installTestResource("images/mercury.jpg", imgDir + "mercury.jpg");
		installTestResource("images/neptune.jpg", imgDir + "neptune.jpg");
		installTestResource("images/pluto.gif",   imgDir + "pluto.gif");
		installTestResource("images/saturn.jpg",  imgDir + "saturn.jpg");
		installTestResource("images/uranus.jpg",  imgDir + "uranus.jpg");
		installTestResource("images/venus.jpg",   imgDir + "venus.jpg");
	}


	public static void killAllCamelisProcess() throws PortalisException {
		LOGGER.info("\nPortalisCtx ---------> killing Camelis Processes if any\n");
		LOGGER.info("\nPortalisCtx ---------> login bekkers");
		LoginReponse loginReponse = AdminHttp.login(CoreTestConstants.MAIL_YVES, CoreTestConstants.PASSWORD_YVES);
		assertNotNull(loginReponse);
		assertTrue("loginReponse ne doit doit pas être en erreur :"+ loginReponse, loginReponse.isOk());
		ActiveUser bekkers = loginReponse.getActiveUser();

		LOGGER.info("\nPortalisCtx ---------> bekkers kills AllCamelisProcess");
		PingReponse pingReponse = AdminHttp.killAllCamelisProcess(bekkers);
		assertNotNull(pingReponse);
		assertTrue("pingReponse ne doit doit pas être en erreur :"+ pingReponse, pingReponse.isOk());
		
		LOGGER.info("\nPortalisCtx ---------> logout bekkers");
		VoidReponse logoutReponse = AdminHttp.logout(bekkers);
		assertNotNull(logoutReponse);
		assertTrue("logoutReponse ne doit doit pas être en erreur :"+ logoutReponse, logoutReponse.isOk());
		
		LOGGER.info("\nPortalisCtx ---------> fin killing Camelis Processes if any\n");

	}

	public static void killAllCamelisProcessAndClearUsers() throws PortalisException, RequestException {
		LOGGER.debug("killAllCamelisProcessAndClearUsers");
		LoginReponse loginReponse = AdminHttp.login(CoreTestConstants.MAIL_YVES, CoreTestConstants.PASSWORD_YVES);
		assertNotNull(loginReponse);
		assertTrue("loginReponse ne doit doit pas être en erreur :"+ loginReponse, loginReponse.isOk());
		ActiveUser bekkers = loginReponse.getActiveUser();

		PingReponse pingReponse = AdminHttp.killAllCamelisProcess(bekkers);
		assertNotNull(pingReponse);
		assertTrue("pingReponse ne doit doit pas être en erreur :"+ pingReponse, pingReponse.isOk());

		VoidReponse voidReponse = AdminHttp.clearActifUsers(bekkers);
		assertNotNull(voidReponse);
		assertTrue("voidReponse ne doit doit pas être en erreur :"+ voidReponse, voidReponse.isOk());
	}

	public static void downloadTo(URL src, String dest) throws IOException {
		ReadableByteChannel rbc = Channels.newChannel(src.openStream());
		@SuppressWarnings("resource")
		FileOutputStream fos = new FileOutputStream(dest);
		fos.getChannel().transferFrom(rbc, 0, Long.MAX_VALUE);
	}

	public static File downloadTo(URL src) throws IOException {
		File f = File.createTempFile("portalis", null);
		downloadTo(src, f.getPath());
		return f;
	}

}

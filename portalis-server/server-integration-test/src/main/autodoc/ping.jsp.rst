Afficher le service à l'écoute du port considéré.

Fonction Java
-------------

**Class** : ``fr.irisa.lis.portalis.camelis.http.CameCorelisHttp``

**Fonction static** : ``ping``

.. code-block:: java

   public static PingReponse ping(Session sess, String host, int port) {};
   public static PingReponse ping(Session sess, String host, String port) {};
   public static PingReponse ping(Session sess, int port) {};
   public static PingReponse ping(Session sess, String port) {};
   public static PingReponse ping(Session sess) {};
   
         
Paramêtres
----------

============  ===================================================================  ========  ========================
Name          Description                                                          Required  Defaults
============  ===================================================================  ========  ========================
session       Session avec son utilisateur et son service courant                  *yes*
host          Nom du serveur                                                       *no*      ``session.getHost()``
port          Numéro de port                                                       *no*      ``session.getPort()``
============  ===================================================================  ========  ========================

Paramêtres http
'''''''''''''''

Pour chaque requête Java une requête http sans paramètre est envoyée sur le port du serveur considéré.

Résultat
--------

``PingReponse`` **extends** ``LisReponse`` **extends** ``VoidReponse``

.. image:: ../images/PingReponse.gif

Pour le service actif sur le port considéré, déclare un nouvel utilisateur et lui donne des droits. 
L'utilsateur ne doit pas déjà être avoir fait l'objet d'un enregistrement par ``registerKey``.

Paramêtres
----------

============  ===================================================================  ========  =========================
Name          Description                                                          Required  Defaults
============  ===================================================================  ========  =========================
adminKey      admin's key                                                          *yes*
userKey       user's key to which a role is assigned                               *yes*
role          the assigned role                                                    *yes*
============  ===================================================================  ========  =========================

Résultat
--------

``RegisterKeyReponse`` **extends**``CamelisUserReponse`` **extends** ``LisReponse`` **extends** ``VoidReponse``


.. image:: ../images/CamelisUserReponse.gif

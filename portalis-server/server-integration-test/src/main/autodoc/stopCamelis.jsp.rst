Arrêter un service.

Fonction Java
-------------

**Class** : ``fr.irisa.lis.portalis.camelis.http.CamelisHttp``

**Fonction static** : ``stopCamelis``

.. code-block:: java

   public static PingReponse stopCamelis(Session sess, String port) {};
   public static PingReponse stopCamelis(Session sess, int port) {};


Paramêtres
----------

============  ===================================================================  ========  =========================
Name          Description                                                          Required  Defaults
============  ===================================================================  ========  =========================
session       Session avec son utilisateur et son service courant                  *yes*
port          port number on which the service is listening                        *yes*
============  ===================================================================  ========  =========================

Paramêtres http
'''''''''''''''

Pour chaque requête Java une requête http est envoyée dans laquelle le paramêtre ``session`` est
remplacé automatiquement par les deux paramètre suivants (les autres paramêtres sont identiques) :

============  ===================================================================  ========  =========================
Name          Description                                                          Required  Defaults
============  ===================================================================  ========  =========================
creator       email of the service creator (that's YOU)                            *yes*
key           current key of the creator. He has admin right for this service      *yes*
============  ===================================================================  ========  =========================

Résultat
--------

``PingReponse`` **extends** ``LisReponse`` **extends** ``VoidReponse``

.. image:: ../images/PingReponse.gif

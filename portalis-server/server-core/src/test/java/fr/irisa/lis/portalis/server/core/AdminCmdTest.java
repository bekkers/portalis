package fr.irisa.lis.portalis.server.core;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.net.InetAddress;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import fr.irisa.lis.portalis.admin.db.DataBase;
import fr.irisa.lis.portalis.admin.db.DataBaseSimpleImpl;
import fr.irisa.lis.portalis.commons.core.CommonsUtil;
import fr.irisa.lis.portalis.shared.admin.ClientConstants;
import fr.irisa.lis.portalis.shared.admin.ErrorMessages;
import fr.irisa.lis.portalis.shared.admin.PortalisException;
import fr.irisa.lis.portalis.shared.admin.Util;
import fr.irisa.lis.portalis.shared.admin.XmlIdentifier;
import fr.irisa.lis.portalis.shared.admin.data.ActiveSite;
import fr.irisa.lis.portalis.shared.admin.data.PortalisService;
import fr.irisa.lis.portalis.shared.camelis.reponse.PingReponse;
import fr.irisa.lis.portalis.test.CoreTestConstants;

public class AdminCmdTest {

	private static final Logger LOGGER = LoggerFactory
			.getLogger(AdminCmdTest.class.getName());
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		final String host = InetAddress.getLocalHost().getCanonicalHostName();
		PortalisService.getInstance().init(host, 8080, "portalis");
		Util.clientInitLogPropertiesFile();
		LOGGER.info(""
				+ "\n            ----------------------------------------------------"
				+ "\n            |            @BeforeClass : AdminCmdTest        |"
				+ "\n            ----------------------------------------------------\n");
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		LOGGER.info(""
				+ "\n            ----------------------------------------------------"
				+ "\n            |            @AfterClass : AdminCmdTest         |"
				+ "\n            ----------------------------------------------------\n");
	}
	
	static DataBaseSimpleImpl bd = DataBaseSimpleImpl.getInstance();

	
	private final static String badFullName1 = "toto";
	private final static String badFullName2 = "toto:titi:tutu";
	private final static String knownFullName = CoreTestConstants.SERVICE_PLANETS_DISTANCE;
	private final static String unknownFullName1 = "toto:titi";
	private final static String unknownFullName2 = CoreTestConstants.SERVICE_PLANETS_DISTANCE_ERR;
	
	@Test
	public void testCheckServiceNameString1() {
		try {
			AdminCmd.checkServiceName(badFullName1);
		} catch (PortalisException e) {
			assertTrue("Le message d'erreur doit contenir : '"+ErrorMessages.TEMOIN_INCORRECT_SERVICE_NAME
					+"' sa valeur est '"
					+e.getMessage(), e.getMessage().contains(ErrorMessages.TEMOIN_INCORRECT_SERVICE_NAME));
		}
	}
	
	@Ignore @Test
	public void testGetSite() {
		fail("Not yet implemented");
	}

	@Ignore @Test
	public void testGetActiveSite() {
		fail("Not yet implemented");
	}

	@Ignore @Test
	public void testLinuxCmdGetActifsCamelisPortsOnPortalis() {
		fail("Not yet implemented");
	}



	@Test
	public void testSite() {
		try {
			PingReponse pingReponse = AdminCmd.getActiveLisServicesOnPortalis();
			DataBase dataBase = PortalisCtx.getInstance().getDataBase();
			ActiveSite activeSite = new ActiveSite(dataBase, pingReponse.getLesServices());
			Element elem = ClientConstants.PORTALIS_DATA_XML_WRITER
			.visit(activeSite);
			assertEquals(XmlIdentifier.ACTIVE_SITE(), elem.getNodeName());
			NodeList nodeList = elem.getElementsByTagName(XmlIdentifier.LIS_APPLICATION());
			assertTrue(nodeList.getLength()>0);
		} catch (Exception e) {
			assertTrue("Le message d'erreur doit contenir : '"+ErrorMessages.TEMOIN_INCORRECT_SERVICE_NAME
					+"' sa valeur est '"
					+e.getMessage(), e.getMessage().contains(ErrorMessages.TEMOIN_INCORRECT_SERVICE_NAME));
		}
	}

	@Test
	public void testCheckServiceNameString2() {
		try {
			AdminCmd.checkServiceName(badFullName2);
		} catch (PortalisException e) {
			assertTrue("Le message d'erreur doit contenir : '"+ErrorMessages.TEMOIN_INCORRECT_SERVICE_NAME
					+"' sa valeur est '"
					+e.getMessage(), e.getMessage().contains(ErrorMessages.TEMOIN_INCORRECT_SERVICE_NAME));
		}
	}

	@Test
	public void testCheckServiceNameString3() {
		try {
			AdminCmd.checkServiceName(knownFullName);
		} catch (Exception e) {
			fail(e.getClass().getCanonicalName()
					+ " rencontrée lors du test : " + e.getMessage()
					+ "\nStack :\n" + CommonsUtil.stack2string(e));
		}
	}

	@Test
	public void testCheckServiceNameString4() {
		try {
			AdminCmd.checkServiceName(unknownFullName1);
		} catch (PortalisException e) {
			assertTrue("Le message d'erreur doit contenir : '"+ErrorMessages.TEMOIN_UNKNOWN_APPLICATION_NAME
					+"' sa valeur est '"
					+e.getMessage(), e.getMessage().contains(ErrorMessages.TEMOIN_UNKNOWN_APPLICATION_NAME));
		}
	}

	@Test
	public void testCheckServiceNameString5() {
		try {
			AdminCmd.checkServiceName(unknownFullName2);
		} catch (PortalisException e) {
			assertTrue("Le message d'erreur doit contenir : '"+ErrorMessages.TEMOIN_UNKNOWN_SERVICE_NAME_FOR_APPLICATION
					+"' sa valeur est '"
					+e.getMessage(), e.getMessage().contains(ErrorMessages.TEMOIN_UNKNOWN_SERVICE_NAME_FOR_APPLICATION));
		}
	}

	@Ignore @Test
	public void testCheckServiceNameStringBoolean() {
		fail("Not yet implemented");
	}

	@Ignore @Test
	public void testGetLog() {
		fail("Not yet implemented");
	}

	@Ignore @Test
	public void testClearLog() {
		fail("Not yet implemented");
	}

	@Ignore @Test
	public void testAdminPropertyFilesCheck() {
		fail("Not yet implemented");
	}

	@Ignore @Test
	public void testGetCamelisVersion() {
		fail("Not yet implemented");
	}

	@Ignore @Test
	public void testPortIsBusy() {
		fail("Not yet implemented");
	}

	@Ignore @Test
	public void testGetCamelisProcessIdStringString() {
		fail("Not yet implemented");
	}

	@Ignore @Test
	public void testGetCamelisProcessIdStringInt() {
		fail("Not yet implemented");
	}

	@Ignore @Test
	public void testGetActifsPorts() {
		fail("Not yet implemented");
	}

	@Ignore @Test
	public void testGetActiveLisServicesOnPortalisString() {
		fail("Not yet implemented");
	}

	@Ignore @Test
	public void testGetActiveLisServicesOnPortalis() {
		fail("Not yet implemented");
	}

	@Ignore @Test
	public void testStartCamelisStringStringStringString() {
		fail("Not yet implemented");
	}

	@Ignore @Test
	public void testStartCamelisStringStringStringStringString() {
		fail("Not yet implemented");
	}

	@Ignore @Test
	public void testStartCamelisStringIntStringStringString() {
		fail("Not yet implemented");
	}

	@Ignore @Test
	public void testKillAllCamelisProcess() {
		fail("Not yet implemented");
	}

	@Ignore @Test
	public void testStopCamelis() {
		fail("Not yet implemented");
	}

}

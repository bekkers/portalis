package fr.irisa.lis.portalis.server.core;

import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

public class MySessionListener implements HttpSessionListener {

	@Override
	public void sessionCreated(HttpSessionEvent se) {
		PortalisCtx.getInstance().sessionCreated(se);
	}

	@Override
	public void sessionDestroyed(HttpSessionEvent se) {
		PortalisCtx.getInstance().sessionDestroyed(se);
	}

}

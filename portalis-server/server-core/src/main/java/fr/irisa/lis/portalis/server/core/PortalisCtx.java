package fr.irisa.lis.portalis.server.core;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionEvent;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.irisa.lis.portalis.admin.db.DataBase;
import fr.irisa.lis.portalis.admin.db.DataBaseSimpleImpl;
import fr.irisa.lis.portalis.commons.core.CommonsUtil;
import fr.irisa.lis.portalis.shared.admin.AdminProprietes;
import fr.irisa.lis.portalis.shared.admin.ErrorMessages;
import fr.irisa.lis.portalis.shared.admin.PortalisException;
import fr.irisa.lis.portalis.shared.admin.RightValue;
import fr.irisa.lis.portalis.shared.admin.XmlIdentifier;
import fr.irisa.lis.portalis.shared.admin.data.ActiveLisService;
import fr.irisa.lis.portalis.shared.admin.data.ActiveUser;
import fr.irisa.lis.portalis.shared.admin.data.PortalisService;
import fr.irisa.lis.portalis.shared.admin.data.Role;
import fr.irisa.lis.portalis.shared.admin.data.UserData;
import fr.irisa.lis.portalis.shared.camelis.http.CamelisHttp;
import fr.irisa.lis.portalis.shared.camelis.reponse.DelKeyReponse;
import fr.irisa.lis.portalis.shared.camelis.reponse.RegisterKeyReponse;
import fr.irisa.lis.portalis.shared.camelis.reponse.ResetCreatorReponse;
import fr.irisa.lis.portalis.shared.camelis.reponse.StartReponse;

public class PortalisCtx {

	private static final Logger LOGGER = LoggerFactory
			.getLogger(PortalisCtx.class.getName());

	public final static int ONE_USER_MAX = Integer
			.parseInt(AdminProprietes.portalis
					.getProperty("portalis.one.user.max"));

	public static int MAX_INACTIVE_INTERVAL;

	private DataBase dataBase = DataBaseSimpleImpl.getInstance();

	private int httpSessionCount;

	// key is portalisID
	private Map<String, ActiveUser> portalisUsers = new ConcurrentHashMap<String, ActiveUser>();
	private Map<String, HttpSession> httpSessions = new ConcurrentHashMap<String, HttpSession>();

	// key is user's email
	private Map<String, ArrayList<ActiveUser>> loggedUsers = new ConcurrentHashMap<String, ArrayList<ActiveUser>>();

	/** Holder */
	private static class SingletonHolder {
		/** Instance unique non préinitialisée */
		private final static PortalisCtx instance = new PortalisCtx();
	}

	/** Point d'accès pour l'instance unique du singleton */
	public static PortalisCtx getInstance() {
		return SingletonHolder.instance;
	}

	private PortalisCtx() {
		super();
		this.httpSessionCount = 0;
		String MAX_INACTIVE_INTERVAL_STRING = AdminProprietes.portalis
				.getProperty("portalis.max.inactive.interval");
			MAX_INACTIVE_INTERVAL = CommonsUtil
					.evaluateExpr(MAX_INACTIVE_INTERVAL_STRING);
			LisServerChecker.getInstance();
			LOGGER.info("\nPortalisCtx initialisé : " + this);
	}

	public void init(String hostName, int port, String portalisAppliName) {
		PortalisService.getInstance().init(hostName, port, portalisAppliName);
	}

	public void sessionCreated(HttpSessionEvent se) {
		final HttpSession session = se.getSession();

		// session 5mn
		session.setMaxInactiveInterval(MAX_INACTIVE_INTERVAL);
		synchronized (this) {
			httpSessionCount++;
		}
		String id = session.getId();
		String message = new StringBuffer(
				"\nPortalisCtx New J2EE Session created ").append("ID: ")
				.append(id).append(" ").append("There are now ")
				.append("" + httpSessionCount).append(" live sessions")
				.toString();

		LOGGER.info(message);
	}

	public void sessionDestroyed(HttpSessionEvent se) {
		final HttpSession session = se.getSession();
		String httpSessionId = session.getId();
		destroyBothSessionsByHttpId(httpSessionId);
	}

	private synchronized void destroyBothSessionsByHttpId(String httpSessionId) {
		--httpSessionCount;
		for (String portalisId : httpSessions.keySet()) {
			if (httpSessions.get(portalisId).getId().equals(httpSessionId)) {
				try {
					destroyBothSessionsByPortalisId(portalisId);
				} catch (PortalisException e) {
					LOGGER.error("error destroying portalisId " + portalisId);
				}
				break;
			}
		}
		String message = new StringBuffer(
				"\nPortalisCtx J2EE Session destroyed" + "ID = ")
				.append(httpSessionId).append(" ").append("There are now ")
				.append("" + httpSessionCount).append(" live sessions")
				.toString();
		LOGGER.info(message);
	}

	public synchronized void clearActiveUsers() throws PortalisException {
		LOGGER.info("\nPortalisCtx clearActiveUsers");
		for (String portalisId : portalisUsers.keySet()) {
			destroyBothSessionsByPortalisId(portalisId);
		}
		portalisUsers.clear();
		httpSessions.clear();
		LOGGER.debug("\nPortalisCtx fin clearActiveUsers");
	}

	public synchronized void userLogout(String portalisId)
			throws PortalisException {
		destroyBothSessionsByPortalisId(portalisId);
	}

	private ActiveUser destroyBothSessionsByPortalisId(String portalisId)
			throws PortalisException {
		LOGGER.info("\nPortalisCtx removing "
				+ portalisId
				+ " user="
				+ (portalisUsers.get(portalisId) != null ? portalisUsers
						.get(portalisId).getUserCore().getEmail() : "null"));
		HttpSession session = httpSessions.get(portalisId);
		ActiveUser user = portalisUsers.remove(portalisId);
		removeUserFromLoggedUserLists(user);
		broadcastUserKeyDeletion(user);
		// remove the three stateValues
		session.removeAttribute(XmlIdentifier.CAMELIS_SESSION_ID);
		httpSessions.remove(portalisId);
		session.invalidate();
		return user;
	}

	private void removeUserFromLoggedUserLists(ActiveUser user) {
		ArrayList<ActiveUser> users = loggedUsers.get(user.getUserCore()
				.getEmail());
		users.remove(user);
	}

	public synchronized void userLogin(ActiveUser activeUser,
			HttpSession session, String maxInactiveInterval)
			throws PortalisException {
		if (maxInactiveInterval!=null)
			session.setMaxInactiveInterval(CommonsUtil.evaluateExpr(maxInactiveInterval));
		userLogin(activeUser, session);
	}

	public synchronized void userLogin(ActiveUser activeUser,
			HttpSession session) throws PortalisException {
		LOGGER.info("\nPortalisCtx registering " + activeUser.toShortString());
		String portalisId = activeUser.getPortalisSessionId();
		// set the three stateValues

		session.setAttribute(XmlIdentifier.CAMELIS_SESSION_ID, portalisId);
		LOGGER.debug("session[" + XmlIdentifier.CAMELIS_SESSION_ID + "]="
				+ portalisId);
		portalisUsers.put(portalisId, activeUser);
		httpSessions.put(portalisId, session);
		broadcastUserKeyCreation(activeUser);

		// add to loggedUser list
		String email = activeUser.getUserCore().getEmail();
		ArrayList<ActiveUser> thisUserList = loggedUsers.get(email);
		if (thisUserList == null) {
			thisUserList = new ArrayList<ActiveUser>();
			loggedUsers.put(email, thisUserList);
		}
		if (thisUserList.size() < ONE_USER_MAX) {
			thisUserList.add(activeUser);
		} else {
			String mess = new StringBuffer("Error ").append(email)
					.append(ErrorMessages.TEMOIN_IS_ALREADY_LOGGED)
					.append(ONE_USER_MAX + " times").toString();
			LOGGER.warn(mess);
			throw new PortalisException(mess);
		}
	}

	public synchronized ActiveUser getUser(String portalisId) {
		return portalisUsers.get(portalisId);
	}

	public synchronized ActiveUser[] getUsers() {
		return portalisUsers.values().toArray(
				new ActiveUser[portalisUsers.size()]);
	}

	private void broadcastUserKeyCreation(ActiveUser activeUser)
			throws PortalisException {
		LOGGER.debug(String.format(
				"\nPortalisCtx broadcastUserKeyCreation() : %s",
				activeUser.toShortString()));

		// 347 On calcule dynamiquement les services courramment actifs
		List<ActiveLisService> services = AdminCmd
				.getActiveLisServicesOnPortalis().getLesServices();

		// boucle sur tous les services actifs
		for (ActiveLisService service : services) {
			registerUserOnService(activeUser, service);
		}
		LOGGER.info(String.format(
				"\nPortalisCtx fin broadcastUserKeyCreation() : %s",
				activeUser.toShortString()));
	}

	public synchronized void broadCastStartCamelisToUsers(
			StartReponse startReponse, ActiveUser whoIsCreator)
			throws PortalisException {
		LOGGER.info("\nPortalisCtx broadCastStartCamelisToUsers creator "
				+ whoIsCreator.toShortString());

		String creatorKey = whoIsCreator.getPortalisSessionId();
		for (String key : portalisUsers.keySet()) {
			if (!key.equals(creatorKey)) {
				ActiveUser user = portalisUsers.get(key);
				LOGGER.debug("\nPortalisCtx broadCastStartCamelisToUsers user "
						+ user.toShortString());
				registerUserOnService(user, startReponse.getActiveLisService());
			}
		}
		LOGGER.info("\nPortalisCtx fin broadCastStartCamelisToUsers");
	}

	private void registerUserOnService(ActiveUser activeUser,
			ActiveLisService service) throws PortalisException {
		UserData user = activeUser.getUserCore();
		LOGGER.debug(String.format("\nPortalisCtx registerUserOnService %s %s",
				user.getEmail(), service.getFullName()));
		String userKey = activeUser.getPortalisSessionId();
		String creator = service.getCreator();
		// création d'un actif user anonyme temporaire
		ActiveUser tempoUser = ActiveUser.createAnonymousActiveUser();

		// on prend la main sur le service
		ResetCreatorReponse resetCreatorReponse = new CamelisHttp(tempoUser, service).resetCreator(creator);
		if (!resetCreatorReponse.isOk()) {
			String mess = resetCreatorReponse.getMessagesAsString();
			LOGGER.error(mess);
			throw new PortalisException(mess);
		}

		Role userRole = dataBase
				.getRole(user.getEmail(), service.getFullName());
		RegisterKeyReponse registerKeyResponse = new CamelisHttp(tempoUser, service).registerKey(
				userKey, userRole.getRight()
						.name());
		if (!registerKeyResponse.isOk()) {
			String mess = registerKeyResponse.getMessagesAsString();
			LOGGER.error(mess);
			throw new PortalisException(mess);
		}
	}

	private void broadcastUserKeyDeletion(ActiveUser activeUser)
			throws PortalisException {
		LOGGER.debug(String.format(
				"\nPortalisCtx broadcastUserKeyDeletion() activeUser = %s",
				activeUser.toShortString()));
		String userKey = activeUser.getPortalisSessionId();

		// On calcule dynamiquement les services courramment actifs
		List<ActiveLisService> services = AdminCmd
				.getActiveLisServicesOnPortalis().getLesServices();

		// boucle sur tous les services actifs
		for (ActiveLisService service : services) {
			String creator = service.getCreator();

			// création d'une session admin temporaire
			ActiveUser tempoUser = ActiveUser.createAnonymousActiveUser();

			// on prend la main sur le service
			ResetCreatorReponse resetCreatorReponse = new CamelisHttp(tempoUser, service).resetCreator(creator);
			if (!resetCreatorReponse.isOk()) {
				String mess = resetCreatorReponse.getMessagesAsString();
				LOGGER.error(mess);
				throw new PortalisException(mess);
			}

			DelKeyReponse delKeyReponse = new CamelisHttp(tempoUser, service).delKey(userKey);
			if (!delKeyReponse.isOk()) {
				String mess = delKeyReponse.getMessagesAsString();
				LOGGER.error(mess);
				throw new PortalisException(mess);
			}
		}
		LOGGER.debug(String.format(
				"\nPortalisCtx fin broadcastUserKeyDeletion() activeUser = %s",
				activeUser.toShortString()));
	}

	public DataBase getDataBase() {
		return dataBase;
	}

	public void setDataBase(DataBase dataBase) {
		this.dataBase = dataBase;
	}

	public void roleCheck(String camelisSessionID, String fullName,
			RightValue requiredRole) throws PortalisException {
		ActiveUser activeUser = portalisUsers.get(camelisSessionID);
		roleCheck(requiredRole, activeUser, fullName);
	}

	public void roleCheck(RightValue requiredRole, ActiveUser activeUser,
			String fullName) throws PortalisException {
		if (activeUser == null) {
			String mess = "Impossible de calculer les droits, activeUser is null";
			LOGGER.error(mess);
			throw new PortalisException(mess);
		}
		if (fullName == null) {
			String mess = "Impossible de calculer les droits, fullName is null";
			LOGGER.error(mess);
			throw new PortalisException(mess);
		}
		UserData user = activeUser.getUserCore();

		dataBase.isAuthorized(user, fullName, requiredRole);
	}

}

package fr.irisa.lis.portalis.server.core;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.irisa.lis.portalis.commons.core.CommonsUtil;
import fr.irisa.lis.portalis.shared.admin.AdminProprietes;
import fr.irisa.lis.portalis.shared.admin.PortalisError;
import fr.irisa.lis.portalis.shared.admin.PortalisException;
import fr.irisa.lis.portalis.shared.admin.Util;
import fr.irisa.lis.portalis.shared.admin.data.ActiveLisService;
import fr.irisa.lis.portalis.shared.camelis.reponse.PingReponse;
import fr.irisa.lis.portalis.shared.camelis.reponse.StartReponse;

public class LisServerChecker {
	private static final Logger LOGGER = LoggerFactory.getLogger(LisServerChecker.class
			.getName());

	static final String emailYves = "bekkers@irisa.fr";
	static final String emailBenjamin = "benjamin@dromaludaire.info";

	private static final String logFileName = AdminProprietes.portalis
			.getProperty("admin.lisServerChecker.logFilePath");
	private static final boolean killProcessesOnStartup = AdminProprietes.portalis
			.getProperty("admin.lisServerChecker.killProcessesOnStartup").equals(
					"true");

	private static final Set<String> serviceIds = new HashSet<String>();


	/** Holder */
	private static class SingletonHolder {
		/** Instance unique non préinitialisée */
		private final static LisServerChecker instance = new LisServerChecker();
	}

	/** Point d'accès pour l'instance unique du singleton */
	public static LisServerChecker getInstance() {
		return SingletonHolder.instance;
	}

	private LisServerChecker() {

		try {
			BufferedWriter logFile = new BufferedWriter(new FileWriter(
					new File(logFileName), true));
			final String headers = "action,port,service,date,host,creator,creationDate\n";
			logFile.write(headers);
			logFile.close();

			initChecker();
		} catch (Exception e) {
			String message = "problème lors de l'initialisation du ServerLogger "
					+ CommonsUtil.stack2string(e);
			LOGGER.error(message);
			throw new PortalisError(message);
		}

	}

	private void initChecker() throws PortalisException {
		PingReponse pingReponse = AdminCmd.getActiveLisServicesOnPortalis();
		List<ActiveLisService> actifs = pingReponse.getLesServices();
		ActiveLisService[] actifTable = actifs
				.toArray(new ActiveLisService[actifs.size()]);
		LOGGER.debug("initChecher() actifs.list.size = " + actifs.size()
				+ ", actifs.tab.lenght = " + actifTable.length + "\n"
				+ pingReponse.toString());
		if (actifs.size() > 0) {
			if (killProcessesOnStartup) {
				StringBuffer buff = new StringBuffer("Portalis start : killing existing active lis process(es) on port(s) :");
				for (ActiveLisService actif : actifTable) {
					LinuxCmd.killCamelisProcessByPid(actif.getPid());
					putMessage(actif, "killed on Portalis startup");
					buff.append(" "+actif.getPort());
				}
				LOGGER.info(buff.toString());
			} else {
				StringBuffer buff = new StringBuffer("Portalis start : registering existing active lis process(es) on port(s) :");
				for (ActiveLisService actif : actifTable) {
					serviceIds.add(actif.getActiveId());
					putMessage(actif, "added on Portalis startup");
					buff.append(" "+actif.getPort());
					LOGGER.info(buff.toString());
				}
			}
		}
	}

	public String toString() {
		return Arrays
				.toString(serviceIds.toArray(new String[serviceIds.size()]));
	}

	public void checkFailure() {
		try {
			PingReponse pingReponse = AdminCmd.getActiveLisServicesOnPortalis();
			List<ActiveLisService> lesServices = pingReponse.getLesServices();
			// checking for unregistered services
			for (ActiveLisService service : lesServices) {
				String id = service.getActiveId();
				if (!serviceIds.contains(id)) {
					LOGGER.warn("On vient de trouver un processus LIS pas enregistré : "
							+ id + " Ids = " + serviceIds.toString());
					// processus actif qui n'est pas connu
					serviceIds.add(id);
					putMessage(service,
							"added on a périodical check although this service should allready be there");
				}
			}

			List<String> result = new ArrayList<String>();
			for (String serviceId : serviceIds.toArray(new String[serviceIds
					.size()])) {
				boolean isThere = false;
				for (ActiveLisService service : lesServices) {
					String id = service.getActiveId();
					if (serviceId.equals(id)) {
						isThere = true;
					}
				}
				if (!isThere) {
					LOGGER.error("On vient de trouver un processus LIS mort de mort subite : "
							+ serviceId);
					result.add(serviceId);
				}
			}
			if (result.size() != 0) {
				String subject = "Disparition de serveur(s) Camelis";
				StringBuffer buff = new StringBuffer(
						"Attention : le (les) processus Camelis :");
				for (String serviceId : result) {
					buff.append("\n   - ").append(serviceId);
					serviceIds.remove(serviceId);
				}
				buff.append("\nest (sont) mort(s) d'une mort subite");
				String content = buff.toString();
				;
				new Mail().send(emailYves, emailYves, subject, content);
				new Mail().send(emailYves, emailBenjamin, subject, content);
			}

		} catch (Exception e) {
			String mess = "problème lors de l'écriture dans le fichier de log des création/destruction de serveurs "
					+ e.getMessage();
			LOGGER.error(mess);

		}
	}

	public synchronized void starting(StartReponse startReponse)
			throws PortalisException {
		ActiveLisService service = startReponse.getActiveLisService();
		putMessage(service, "start");
		pushId(service);
	}

	public synchronized void stopServices(PingReponse pingReponse) throws PortalisException {
		for (ActiveLisService service : pingReponse.getLesServices()) {
			stop(service);
		}
		
	}

	private void stop(ActiveLisService service) throws PortalisException {
		LinuxCmd.killCamelisProcessByPid(service.getPid());
		putMessage(service, "stop");
		pullId(service);
	}
	
	private void pushId(ActiveLisService service) {
		serviceIds.add(service.getActiveId());
	}

	private void pullId(ActiveLisService service) {
		serviceIds.remove(service.getActiveId());
	}

	private void putMessage(ActiveLisService service, String action)
			throws PortalisException {
		String fullName = service.getFullName();
		String host = service.getHost();
		String port = service.getPort() + "";
		String date = Util.writeDate(service.getActivationDate());
		String creator = service.getCreator();

		String thisTime = Util.writeDate(new Date());

		String logMess = String.format("%s,%s,%s,%s,%s,%s,%s\n", action,
				port, fullName, thisTime, host, creator, date);
		try {
			BufferedWriter logFile = new BufferedWriter(new FileWriter(
					new File(logFileName), true));
			logFile.write(logMess);
			logFile.close();
		} catch (IOException e) {
			String errorMess = "Ecriture impossible sur le fichier de log des servers lis "
					+ e.getMessage();
			LOGGER.error(errorMess + CommonsUtil.stack2string(e));
			throw new PortalisException(errorMess, e);
		}
	}


}

package fr.irisa.lis.portalis.server.core;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.irisa.lis.portalis.commons.core.CommonsUtil;
import fr.irisa.lis.portalis.shared.admin.AdminProprietes;
import fr.irisa.lis.portalis.shared.admin.Err;
import fr.irisa.lis.portalis.shared.admin.ErrorMessages;
import fr.irisa.lis.portalis.shared.admin.PortalisException;
import fr.irisa.lis.portalis.shared.admin.Precondition;
import fr.irisa.lis.portalis.shared.admin.RightValue;
import fr.irisa.lis.portalis.shared.admin.Util;
import fr.irisa.lis.portalis.shared.admin.XmlIdentifier;
import fr.irisa.lis.portalis.shared.admin.data.ActiveLisService;
import fr.irisa.lis.portalis.shared.admin.data.ActiveSite;
import fr.irisa.lis.portalis.shared.admin.data.ActiveUser;
import fr.irisa.lis.portalis.shared.admin.data.AnonymousUser;
import fr.irisa.lis.portalis.shared.admin.data.LoginErr;
import fr.irisa.lis.portalis.shared.admin.data.LoginResult;
import fr.irisa.lis.portalis.shared.admin.data.PortActif;
import fr.irisa.lis.portalis.shared.admin.data.PortalisService;
import fr.irisa.lis.portalis.shared.admin.reponse.ActiveSiteReponse;
import fr.irisa.lis.portalis.shared.admin.reponse.BooleanReponse;
import fr.irisa.lis.portalis.shared.admin.reponse.LogReponse;
import fr.irisa.lis.portalis.shared.admin.reponse.LoginReponse;
import fr.irisa.lis.portalis.shared.admin.reponse.PidReponse;
import fr.irisa.lis.portalis.shared.admin.reponse.PortsActifsReponse;
import fr.irisa.lis.portalis.shared.admin.reponse.SiteReponse;
import fr.irisa.lis.portalis.shared.admin.reponse.VersionReponse;
import fr.irisa.lis.portalis.shared.admin.reponse.VoidReponse;
import fr.irisa.lis.portalis.shared.admin.validation.CoreServiceNameValidator;
import fr.irisa.lis.portalis.shared.camelis.http.CamelisHttp;
import fr.irisa.lis.portalis.shared.camelis.reponse.PingReponse;
import fr.irisa.lis.portalis.shared.camelis.reponse.StartReponse;
import fr.irisa.lis.portalis.system.linux.LinuxInteractor;

public class AdminCmd {

	private static final Logger LOGGER = LoggerFactory.getLogger(AdminCmd.class
			.getName());

	private static PortalisCtx ctx = PortalisCtx.getInstance();

	public final static String WEBAPP_PATH = AdminProprietes.portalis
			.getProperty("admin.webappsDirPath");
	public static final String LOG_FILE_PATH = AdminProprietes.portalis
			.getProperty("admin.camelis.logFilePath");
	private static final File logFile = new File(LOG_FILE_PATH);

	static {
		try {
			// test de la présence de WEBAPP_PATH
			File webapp = new File(WEBAPP_PATH);
			if (!webapp.exists()) {
				LOGGER.warn("pas de répertoire '" + WEBAPP_PATH
						+ "' on essaie de le créer");
				if (!webapp.mkdirs())
					throw new PortalisException("répertoire " + WEBAPP_PATH
							+ " impossible à créer");
			}
			// test de la présence de LOG_FILE_PATH
			File logFile = new File(LOG_FILE_PATH);
			File logPath = logFile.getParentFile();
			if (!logPath.exists()) {
				LOGGER.warn("pas de répertoire '" + logPath.getAbsolutePath()
						+ "' on essaie de le créer");
				if (!logPath.mkdirs())
					throw new PortalisException("répertoire "
							+ logPath.getAbsolutePath() + " impossible à créer");
			}

		} catch (Exception e) {
			LOGGER.error("Erreur lors de l'initialisation de la classes AdminCmd"
					+ CommonsUtil.stack2string(e));
		}

	}

	public static String checkServiceName(String serviceFullName)
			throws PortalisException {
		return checkServiceName(serviceFullName, true);
	}

	public static String checkServiceName(String serviceFullName,
			boolean isKnown) throws PortalisException {
		String result = null;
		if (serviceFullName != null) {
			if (!CoreServiceNameValidator.getInstance().validate(
					serviceFullName)) {
				String mess = ErrorMessages.TEMOIN_INCORRECT_SERVICE_NAME
						+ serviceFullName;
				throw new PortalisException(mess);
			}
			if (isKnown) {
				PortalisCtx.getInstance().getDataBase()
						.getService(serviceFullName);
			}
			result = serviceFullName;
		}
		return result;
	}

	public static LogReponse getLog(String camelisSessionID) {
		LogReponse logReponse = new LogReponse();
		try {
			// vérification du rôle
			PortalisCtx.getInstance().roleCheck(camelisSessionID, PortalisService.getInstance().getFullName(),
					RightValue.ADMIN);
			BufferedReader reader = new BufferedReader(new FileReader(logFile));
			String line = reader.readLine();
			while (line != null) {
				logReponse.addLine(line);
				line = reader.readLine();
			}
			reader.close();
			return logReponse;
		} catch (PortalisException e) {
			String mess = "getLog error";
			LOGGER.warn(mess, e);
			return new LogReponse(new Err(mess, e.getMessage()));
		} catch (Throwable t) {
			String mess = "getLog error";
			LOGGER.error(mess, t);
			return new LogReponse(new Err(mess, t));
		}
	}

	public static VoidReponse clearLog(String camelisSessionID) {
		try {
			// vérification du rôle
			PortalisCtx.getInstance().roleCheck(camelisSessionID, PortalisService.getInstance().getFullName(),
					RightValue.ADMIN);
			return LinuxCmd.clearLog(logFile);
		} catch (PortalisException e) {
			String mess = "clearLog error";
			LOGGER.warn(mess, e);
			return new LogReponse(new Err(mess, e.getMessage()));
		} catch (Throwable t) {
			String mess = "clearLog error";
			LOGGER.error(mess, t);
			return new LogReponse(new Err(mess, t));
		}
	}

	public static SiteReponse getSite(String camelisSessionID) {
		try {
			// vérification du rôle
			PortalisCtx.getInstance().roleCheck(camelisSessionID, PortalisService.getInstance().getFullName(),
					RightValue.READER);
			return new SiteReponse(ctx.getDataBase());
		} catch (PortalisException e) {
			String mess = "getSite error";
			LOGGER.warn(mess, e);
			return new SiteReponse(new Err(mess, e.getMessage()));
		} catch (Throwable t) {
			String mess = "getSite error";
			LOGGER.error(mess, t);
			return new SiteReponse(new Err(mess, t));
		}
	}

	public static ActiveSiteReponse getActiveSite(String camelisSessionID) {
		LOGGER.info("getActiveSite(" + camelisSessionID + ")");
		try {
			// vérification du rôle
			PortalisCtx.getInstance().roleCheck(camelisSessionID, PortalisService.getInstance().getFullName(),
					RightValue.READER);
			PingReponse pingReponse = getActiveLisServicesOnPortalis(camelisSessionID);
			ActiveSite activeSite = new ActiveSite(ctx.getDataBase(), pingReponse.getLesServices());

			return new ActiveSiteReponse(activeSite);
		} catch (PortalisException e) {
			String mess = "getActiveSite error";
			LOGGER.warn(mess, e);
			return new ActiveSiteReponse(new Err(mess, e.getMessage()));
		} catch (Throwable t) {
			String mess = "getActiveSite error";
			LOGGER.error(mess, t);
			return new ActiveSiteReponse(new Err(mess, t));
		}
	}

	public static VoidReponse adminPropertyFilesCheck(String camelisSessionID) {
		LOGGER.info("adminPropertyFilesCheck(" + camelisSessionID + ")");

		try {
			// vérification du rôle
			PortalisCtx.getInstance().roleCheck(camelisSessionID, PortalisService.getInstance().getFullName(),
					RightValue.READER);
			AdminProprietes.test();
			return new VoidReponse();
		} catch (PortalisException e) {
			String mess = "adminPropertyFilesCheck error";
			LOGGER.warn(mess, e);
			return new VoidReponse(new Err(mess, e.getMessage()));
		} catch (Throwable t) {
			String mess = "adminPropertyFilesCheck error";
			LOGGER.error(mess, t);
			return new VoidReponse(new Err(mess, t));
		}
	}

	public static VersionReponse getCamelisVersion(String camelisSessionID) {
		try {
			// vérification du rôle
			PortalisCtx.getInstance().roleCheck(camelisSessionID, PortalisService.getInstance().getFullName(),
					RightValue.READER);
			return new VersionReponse(LinuxCmd.getPortalisVersion());
		} catch (PortalisException e) {
			String mess = "getCamelisVersion error";
			LOGGER.warn(mess, e);
			return new VersionReponse(new Err(mess, e.getMessage()));
		} catch (Throwable t) {
			String mess = "getCamelisVersion error";
			LOGGER.error(mess, t);
			return new VersionReponse(new Err(mess, t));
		}

	}
	
	public static BooleanReponse portIsBusy(String camelisSessionID, int port) {
		try {
			// vérification du rôle
			PortalisCtx.getInstance().roleCheck(camelisSessionID, PortalisService.getInstance().getFullName(),
					RightValue.ADMIN);
			LOGGER.info("camelisSessionID = "+camelisSessionID+" port"+port);
			BooleanReponse reponse =  new BooleanReponse(!Util.available(port));
			LOGGER.info("reponse = "+reponse);
			return reponse;
		} catch (PortalisException e) {
			String mess = "portIsBusy exception : "+e.getMessage();
			LOGGER.warn(mess);
			return new BooleanReponse(new Err(mess, e.getMessage()));
		} catch (Throwable t) {
			String mess = "portIsBusy error";
			LOGGER.error(mess, t);
			return new BooleanReponse(new Err(mess, t));
		}
	}

	public static PidReponse getCamelisProcessId(String camelisSessionID,
			String fullName) {

		try {
			// vérification du rôle
			PortalisCtx.getInstance().roleCheck(camelisSessionID, PortalisService.getInstance().getFullName(),
					RightValue.ADMIN);
			PidReponse pidReponse = new PidReponse();
			for (ActiveLisService service : getActiveLisServicesOnPortalis()
					.getLesServices()) {
				if (service.getFullName().equals(fullName)) {
					pidReponse.getPids().add(service.getPid());
				}
			}
			return pidReponse;
		} catch (PortalisException e) {
			String mess = "getCamelisProcessId error";
			LOGGER.warn(mess, e);
			return new PidReponse(new Err(mess, e.getMessage()));
		} catch (Throwable t) {
			String mess = "getCamelisProcessId error";
			LOGGER.error(mess, t);
			return new PidReponse(new Err(mess, t));
		}
	}

	public static PidReponse getCamelisProcessId(String camelisSessionId,
			int port) {
		LOGGER.info("==== AdminCmd ==== >> getCamelisProcessId(" + port + ")");

		PidReponse pidReponse = new PidReponse();
		try {
			// vérification du rôle
			PortalisCtx.getInstance().roleCheck(camelisSessionId, PortalisService.getInstance().getFullName(),
					RightValue.ADMIN);

			Set<Integer> pids = new HashSet<Integer>();
			int pid = LinuxCmd.getCamelisProcessId(port);
			if (pid != 0)
				pids.add(pid);
			pidReponse.setPids(pids);
			return pidReponse;
		} catch (PortalisException e) {
			return new PidReponse(new Err(e.getMessage()));
		} catch (Throwable t) {
			String mess = "getCamelisProcessId error";
			LOGGER.error(mess + t.getMessage());
			return new PidReponse(new Err(mess, t));
		}

	}

	public static PortsActifsReponse getActifsPorts(String camelisSessionID) {
		try {
			// vérification du rôle
			PortalisCtx.getInstance().roleCheck(camelisSessionID, PortalisService.getInstance().getFullName(),
					RightValue.ADMIN);

			PortsActifsReponse reponse = new PortsActifsReponse(
					LinuxCmd.getActifsCamelisPortsOnPortalis());
			LOGGER.info("==== AdminCmd ==== >> getActifsPorts() =\n" + reponse);
			return reponse;
		} catch (PortalisException e) {
			return new PortsActifsReponse(new Err(e.getMessage()));
		} catch (Throwable t) {
			String mess = "getActifsPorts error";
			LOGGER.error(mess + t.getMessage());
			return new PortsActifsReponse(new Err(mess, t));
		}
	}

	public static LoginReponse login(String email, String password,
			HttpSession httpSession) {
		return login(email, password, httpSession, null);
	}
		public static LoginReponse login(String email, String password,
				HttpSession httpSession, String maxInactiveInterval) {
		try {
			Precondition precondition = new Precondition();
			precondition.checkJavaArg(email != null, "Parameter %s is missing",
					XmlIdentifier.EMAIL());
			precondition.checkJavaArg(password != null,
					"Parameter %s is missing", XmlIdentifier.PASSWORD());

			if (!precondition.isOk()) {
				LOGGER.warn(precondition.getMessagesAsString());
				return new LoginReponse(new Err(precondition.getMessages()));
			}

			LoginResult loggingCheck = ctx.getDataBase().getUserInfo(email, password);

			if (loggingCheck.getLoginErr().equals(LoginErr.ok)) {

				LOGGER.info(new StringBuffer("email = ").append(email)
						.append(" password = ").append(password).append(")")
						.toString());

				ActiveUser activeUser = new ActiveUser(loggingCheck.getUserCore(),
						httpSession.getId());

				// creation de la session portalis associée
				if (maxInactiveInterval!=null) {
					ctx.userLogin(activeUser, httpSession, maxInactiveInterval);
				} else {
					ctx.userLogin(activeUser, httpSession);
					
				}
				return new LoginReponse(activeUser);
			} else {
				String mess = new StringBuffer("login impossible : ")
						.append(loggingCheck.getLoginErr().name()).append(" email = ")
						.append(email).append("password = ").append(password)
						.toString();

				LOGGER.warn(mess);
				return new LoginReponse(new Err(mess));
			}

		} catch (PortalisException e) {
			String mess = new StringBuffer("login impossible : ")
					.append(e.getMessage()).append(" email = ").append(email)
					.append("password = ").append(password)
					.toString();

			LOGGER.warn(mess, e);
			return new LoginReponse(new Err(mess, e.getMessage()));
		} catch (Throwable t) {
			String mess = new StringBuffer("login impossible : ")
					.append(t.getMessage()).append(" email = ").append(email)
					.append("password = ").append(password).append(")")
					.toString();

			LOGGER.warn(mess + CommonsUtil.stack2string(t));
			return new LoginReponse(new Err(mess, t));
		}
	}

		public static LoginReponse anonymousLogin(HttpSession httpSession) {
			return anonymousLogin(httpSession, null);
		}
			
		public static LoginReponse anonymousLogin(HttpSession httpSession, String maxInactiveInterval) {
		try {

			ActiveUser activeUser = new ActiveUser(new AnonymousUser(),
					httpSession.getId());

			// creation de la session portalis associée
			ctx.userLogin(activeUser, httpSession, maxInactiveInterval);
			return new LoginReponse(activeUser);

		} catch (PortalisException e) {
			return new LoginReponse(new Err(e.getMessage()));
		} catch (Throwable t) {
			String mess = "anonymousLogin error";
			LOGGER.error(mess + t.getMessage());
			return new LoginReponse(new Err(mess, t));
		}
	}

	public static PingReponse getActiveLisServicesOnPortalis(
			String camelisSessionID) {
		LOGGER.info("==== AdminCmd ==== >> getActiveLisServicesOnPortalis("
				+ camelisSessionID + ")");
		try {
			// vérification du rôle
			PortalisCtx.getInstance().roleCheck(camelisSessionID, PortalisService.getInstance().getFullName(),
					RightValue.READER);
			return getActiveLisServicesOnPortalis();
		} catch (PortalisException e) {
			String mess = "getActiveLisServicesOnPortalis error";
			LOGGER.warn(mess, e);
			return new PingReponse(new Err(mess, e.getMessage()));
		} catch (Throwable t) {
			String mess = "getActiveLisServicesOnPortalis error";
			LOGGER.error(mess, t);
			return new PingReponse(new Err(mess, t));
		}
	}

	public static PingReponse getActiveLisServicesOnPortalis() {
		try {
			Set<PortActif> PortActifs = LinuxCmd
					.getActifsCamelisPortsOnPortalis();
			if (PortActifs.size()>0) {
				PortActif portActif = PortActifs.toArray(new PortActif[PortActifs.size()])[0];
				String host = portActif.getServer();
				assert(host!=null);
			}
			List<ActiveLisService> actifs = new ArrayList<ActiveLisService>();
			for (PortActif portActif : PortActifs) {
				int port = portActif.getPort();
				String host = portActif.getServer();
				PingReponse pingReponse = CamelisHttp.ping(host, port);
				if (pingReponse.getStatus().equals(XmlIdentifier.OK)) {
					ActiveLisService ActiveLisService = pingReponse
							.getFirstService();
					if (ActiveLisService == null) {
						String mess = "Erreur interne, il devrait y avoir un service camelis : "
								+ pingReponse;
						throw new PortalisException(mess);
					}
					actifs.add(ActiveLisService);
				} else {
					LOGGER.info("==== AdminCmd ==== >> fin de getActiveLisServicesOnPortalis() reponse :\n"
							+ pingReponse);
					return pingReponse;
				}
			}
			PingReponse pingReponse = new PingReponse(actifs);
			LOGGER.info("==== AdminCmd ==== >> fin de getActiveLisServicesOnPortalis() reponse :\n"
					+ pingReponse);
			return pingReponse;
		} catch (PortalisException e) {
			String mess = "getActiveLisServicesOnPortalis error";
			LOGGER.warn(mess, e);
			return new PingReponse(new Err(mess, e.getMessage()));
		} catch (Throwable t) {
			String mess = "getActiveLisServicesOnPortalis error";
			LOGGER.error(mess, t);
			return new PingReponse(new Err(mess, t));
		}
	}

	/*
	 * ------------------------------ startCamelis
	 */

	public static StartReponse startCamelis(String camelisSessionID,
			String serviceName, String webApplicationDataPath, String logFile) {
		int port;
		try {
			port = PortService.choosePort();
		} catch (PortalisException e) {
			String mess = "startCamelis error";
			LOGGER.warn(mess, e);
			return new StartReponse(new Err(mess, e.getMessage()));
		}

		StartReponse rep = startCamelis(camelisSessionID, port, serviceName,
				webApplicationDataPath, logFile);
		return rep;
	}

	public static StartReponse startCamelis(String camelisSessionID,
			String port, String serviceName, String webApplicationDataPath,
			String logFile) {
		Precondition errors = new Precondition();
		int portNum = errors.checkIntArg(port,
				"Le parametre %s doit être un entier, sa valeur est %s",
				XmlIdentifier.PORT(), port);
		if (!errors.isOk()) {
			LOGGER.warn(errors.getMessagesAsString());
			return new StartReponse(new Err(errors.getMessages()));
		}
		return startCamelis(camelisSessionID, portNum, serviceName,
				webApplicationDataPath, logFile);
	}

	public static StartReponse startCamelis(String camelisSessionID, int port,
			String serviceName, String webApplicationDataPath, String logFile) {
		StartReponse startReponse = new StartReponse();
		try {
			// vérification du rôle
			PortalisCtx.getInstance().roleCheck(camelisSessionID, PortalisService.getInstance().getFullName(),
					RightValue.ADMIN);
			
			if (!Util.available(port)) {
				return new StartReponse(new Err("port "+port+" déjà occupé"));
			}

			LOGGER.info(String.format(
					"==== AdminCmd ==== >> startCamelis(%s, %d, %s, %s, %s)",
					camelisSessionID, port, serviceName,
					webApplicationDataPath, logFile));
			Precondition precond = new Precondition();
			precond.checkJavaArg(
					// TODO validation
					CoreServiceNameValidator.getInstance()
							.validate(serviceName),
					"Incorrect service name : %s", serviceName);
			ActiveUser activeUser = PortalisCtx.getInstance().getUser(
					camelisSessionID);
			precond.checkJavaArg(activeUser != null, "Utilisateur actif null");
			LOGGER.debug(String.format(
					"startCamelis1(%s, %d, %s, %s+Util.stack2string( %s))",
					camelisSessionID, port, serviceName,
					webApplicationDataPath, logFile));
			LOGGER.debug("startCamelis1 precond="
					+ precond.getMessagesAsString());

			if (!precond.isOk()) {
				LOGGER.warn("erreur de paramêtre : "
						+ precond.getMessagesAsString());
				return new StartReponse(new Err(precond.getMessages()));
			} else {

				final String creatorEmail = activeUser.getUserCore().getEmail();
				final String adminKey = activeUser.getPortalisSessionId();

				if (port == 0) {
					port = PortService.choosePort();
				}

				LOGGER.debug(String.format(
						"startCamelis2(%s, %s, %s+Util.stack2string( %s))",
						camelisSessionID, serviceName, webApplicationDataPath,
						logFile));
				if (logFile == null || logFile.length() == 0) {
					logFile = LOG_FILE_PATH;
					if (logFile == null || logFile.length() == 0) {
						return new StartReponse(new Err("La propriété logFilePath doit être initialisée"));
					}
				}

				LOGGER.debug(String.format(
						"startCamelis3(%s, %s, %s+Util.stack2string( %s))",
						camelisSessionID, serviceName, webApplicationDataPath,
						logFile));
				if (webApplicationDataPath == null
						|| webApplicationDataPath.length() == 0) {
					webApplicationDataPath = WEBAPP_PATH;
					if (webApplicationDataPath == null
							|| webApplicationDataPath.length() == 0) {
						return new StartReponse(new Err("La propriété webApplicationDataPath doit être initialisée"));
					}
				}
				LOGGER.debug(String.format(
						"startCamelis4(%s, %s, %s+Util.stack2string( %s))",
						camelisSessionID, serviceName, webApplicationDataPath,
						logFile));

				checkWebappsDir(webApplicationDataPath);

				String host = PortalisService.getInstance().getHost();
				String ActiveLisServiceId = host + ":" + port + "::"
						+ serviceName;
				LOGGER.debug("startCamelis\n(" + adminKey + ", " + creatorEmail
						+ ", " + serviceName + ", " + port + ", "
						+ webApplicationDataPath + ", " + logFile + ")");

				String[][] httpReqArgs = {
						{ XmlIdentifier.CREATOR(), creatorEmail },
						{ XmlIdentifier.PORT(), port + "" },
						{
								XmlIdentifier.KEY(),
								(activeUser != null ? activeUser
										.getPortalisSessionId() : "none") },
						{ XmlIdentifier.ACTIVE_SERVICE_ID(), ActiveLisServiceId },
						{ XmlIdentifier.LOG(), logFile },
						{ XmlIdentifier.DATADIR(), webApplicationDataPath } };
				precond.setHttpReqArgs(httpReqArgs);

				if (!precond.isOk()) {
					LOGGER.warn("erreur de paramêtre : "
							+ precond.getMessagesAsString());
					startReponse = new StartReponse(new Err(
							precond.getMessages()));
				} else {

					final String cmd = ServerConstants.CAMELIS_APPLICATION_NAME
							+ precond.getArgsAsLinuxCommandString();

					LinuxInteractor.executeCommand(cmd, false);
					// ici on ne peut pas attendre la fin du processus car, par
					// définition,
					// il ne va pas s'arrèter ...

					PingReponse pingReponse = CamelisHttp.ping(PortalisService
							.getInstance().getHost(), port);

					ActiveLisService ActiveLisService = pingReponse
							.getFirstService();
					if (ActiveLisService == null) {
						String mess = "Erreur interne, il devrait y avoir un ActiveLisServices sur le port "
								+ port;
						LOGGER.debug(mess);
						startReponse = new StartReponse(new Err(mess));

					} else {

						startReponse = new StartReponse(ActiveLisService);
						ctx.broadCastStartCamelisToUsers(startReponse,
								activeUser);
						LisServerChecker.getInstance().starting(startReponse);
					}
				}
			}
		} catch (PortalisException e) {
			String mess = "startCamelis error";
			LOGGER.warn(mess, e);
			return new StartReponse(new Err(mess, e.getMessage()));
		} catch (Throwable t) {
			String mess = "startCamelis error";
			LOGGER.error(mess, t);
			return new StartReponse(new Err(mess, t));
		}
		LOGGER.info("==== AdminCmd ==== >> fin de startCamelis() Camelis is started, reponse :\n"
				+ startReponse);
		return startReponse;
	}
	


	public static PingReponse killAllCamelisProcess(String camelisSessionID) {
		LOGGER.debug("-------------------> cleaning up all CamelisProcess("
				+ camelisSessionID + ")\n");
		try {
			// vérification du rôle
			PortalisCtx.getInstance().roleCheck(camelisSessionID, PortalisService.getInstance().getFullName(),
					RightValue.ADMIN);

			PingReponse pingReponse = getActiveLisServicesOnPortalis(camelisSessionID);
			LisServerChecker.getInstance().stopServices(pingReponse);
			
			return getActiveLisServicesOnPortalis(camelisSessionID);
		} catch (PortalisException e) {
			String mess = "killAllCamelisProcess error";
			LOGGER.warn(mess, e);
			return new PingReponse(new Err(mess, e.getMessage()));
		} catch (Throwable t) {
			String mess = "killAllCamelisProcess error";
			LOGGER.error(mess, t);
			return new PingReponse(new Err(mess, t));
		}
	}

	public static PingReponse stopCamelis(String camelisSessionID,
			String dynamicServiceId) {
		LOGGER.info("==== AdminCmd ==== >> stopCamelis(" + dynamicServiceId
				+ ")");
		try {
			// vérification du rôle
			PortalisCtx.getInstance().roleCheck(camelisSessionID, PortalisService.getInstance().getFullName(),
					RightValue.ADMIN);

			String host = ActiveLisService.extractHost(dynamicServiceId);
			int port = ActiveLisService.extractPort(dynamicServiceId);

			PingReponse pingReponse = CamelisHttp.ping(host, port); 
			
			LisServerChecker.getInstance().stopServices(pingReponse);

			return getActiveLisServicesOnPortalis(camelisSessionID);
		} catch (PortalisException e) {
			String mess = "stopCamelis error";
			LOGGER.warn(mess, e);
			return new PingReponse(new Err(mess, e.getMessage()));
		} catch (Throwable t) {
			String mess = "stopCamelis error";
			LOGGER.error(mess, t);
			return new PingReponse(new Err(mess, t));
		}
	}

	private static void checkWebappsDir(String webApplicationDataPath)
			throws PortalisException {
		if (webApplicationDataPath == null) {
			String mess = "checkWebappsDir() : WebApplicationDataPath is null";
			LOGGER.error(mess);
			throw new PortalisException(mess);
		}
		File webappsDir = new File(webApplicationDataPath);
		if (!webappsDir.exists()) {
			LOGGER.warn("directory " + webApplicationDataPath
					+ " does not exists, creating it !");
			if (!webappsDir.mkdirs()) {
				String mess = "checkWebappsDir() : Cannot create directory "
						+ webApplicationDataPath;
				LOGGER.error(mess);
				throw new PortalisException(mess);
			}
		}
	}

	/* gestion des utilisateurs */

}

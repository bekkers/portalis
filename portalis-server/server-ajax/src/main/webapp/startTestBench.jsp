<?xml version="1.0" encoding="UTF-8" ?>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page errorPage="showError.jsp"%>


<%!private static String URL = "http://linkedgeodata.org/sparql";%>

<%
	session.setAttribute("parcours", "testBench");
	String action = request.getParameter("action");
	if (action == null) {
		action = "testBenchUpload";
	} else {
		action = "testBenchReload";
	}
%>
<!doctype html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>

<!-- JSP XML -->
<jsp:directive.page contentType="text/html; charset=UTF-8" />

<!-- JSF/Facelets XHTML -->
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />

<meta name="viewport"
	content="width=device-width, initial-scale=1.0, user-scalable=no">
<link rel="stylesheet" type="text/css" href="css/camelis.css">
<title>Portalis test bench</title>
<!--[if lt IE 9]-->
<script type="text/javascript" src="http://code.jquery.com/jquery-1.10.2.js"></script>
<!--[endif]-->
<!--[if gte IE 9]-->
<script type="text/javascript" src="http://code.jquery.com/jquery-2.0.3.js"></script>
<!--[endif]-->
<script type="text/javascript" src="./js/switchrequest.js"></script>

<link rel="stylesheet" type="text/css" href="./css/jqtree.css" />
<script type="text/javascript" src="./js/jQuery/tree.jquery.js"></script>
<script type="text/javascript">
	function serialize(form) {
		if (!form || !form.elements)
			return;

		var serial = [], i, j, first;
		var add = function(name, value) {
			serial.push(encodeURIComponent(name) + '='
					+ encodeURIComponent(value));
		}

		var elems = form.elements;
		for (i = 0; i < elems.length; i += 1, first = false) {
			if (elems[i].name.length > 0) { /* don't include unnamed elements */
				switch (elems[i].type) {
				case 'select-one':
					first = true;
				case 'select-multiple':
					for (j = 0; j < elems[i].options.length; j += 1)
						if (elems[i].options[j].selected) {
							add(elems[i].name, elems[i].options[j].value);
							if (first)
								break; /* stop searching for select-one */
						}
					break;
				case 'checkbox':
				case 'radio':
					if (!elems[i].checked)
						break; /* else continue */
				default:
					add(elems[i].name, elems[i].value);
					break;
				}
			}
		}

		return serial.join('&');
	}
</script>
</head>
<body>
	<header id="main-header">
		<div id="logo-container">
			<img id="logo" alt="" src="images/logo-lis-60.gif" /> <label
				id="logo-label">Portalis</label>
		</div>
		<div id="header-login-title">
			<h4>Démarrer Camelis</h4>
		</div>
	</header>

	<section id="divLogin">
		<form id="queryForm" onsubmit="serialize(document.forms['queryForm'])"
			action="<%=action%>" method="post" enctype="multipart/form-data">
			<fieldset id="formLogin">

				<label for="query">Charger un fichier '.ctx'</label> <br />
				<div id="file">
					<input type="text" id="fileName" name="fileName" size="18" /> <input
						type="button" id="button" value="Browse ..." />
				</div>
				<input type="file" name="file" /> <input type="submit"
					value="Submit" />

			</fieldset>
		</form>
		<strong class="erreur"></strong>
	</section>
	<script type="text/javascript">
		var wrapper = $('<div/>').css({
			height : 0,
			width : 0,
			'overflow' : 'hidden'
		});
		var fileInput = $(':file').wrap(wrapper);

		fileInput.change(function() {
			$this = $(this);
			$('#file').children('#fileName').attr('value', $this.val());
		})

		$('#file').click(function() {
			fileInput.click();
		}).show();
	</script>
</body>
</html>
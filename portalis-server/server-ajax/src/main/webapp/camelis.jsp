<?xml version="1.0" encoding="UTF-8" ?>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page errorPage="showError.jsp"%>


<%@ page import="org.slf4j.Logger"%>
<%@ page import="org.slf4j.LoggerFactory"%>

<%@ page
	import="fr.irisa.lis.portalis.shared.admin.data.ActiveLisService"%>
<%@ page import="fr.irisa.lis.portalis.shared.admin.data.ActiveUser"%>
<%@ page import="fr.irisa.lis.portalis.shared.admin.data.ActivePortalisService"%>
<%@ page import="fr.irisa.lis.portalis.shared.camelis.http.CamelisHttp"%>
<%@ page import="javax.servlet.http.HttpServletRequest"%>

<%@ page
	import="fr.irisa.lis.portalis.shared.admin.reponse.ServiceCoreReponse"%>
<%@ page
	import="fr.irisa.lis.portalis.shared.camelis.reponse.ExtentReponse"%>
<%@ page
	import="fr.irisa.lis.portalis.shared.camelis.reponse.ZoomReponse"%>
<%@ page import="fr.irisa.lis.portalis.shared.camelis.data.LisExtent"%>
<%@ page import="fr.irisa.lis.portalis.shared.camelis.data.LisObject"%>
<%@ page import="fr.irisa.lis.portalis.shared.camelis.data.LisIncrement"%>
<%@ page import="fr.irisa.lis.portalis.shared.admin.http.AdminHttp"%>
<%@ page
	import="fr.irisa.lis.portalis.shared.camelis.data.LisIncrementSet"%>
<%@ page import="fr.irisa.lis.portalis.shared.admin.data.Property"%>
<%@ page
	import="fr.irisa.lis.portalis.shared.camelis.data.LisIncrementSet"%>
<%@ page import="fr.irisa.lis.portalis.server.ajax.Constant"%>
<%@ page import="fr.irisa.lis.portalis.server.ajax.UserContext"%>
<%@ page import="fr.irisa.lis.portalis.shared.admin.data.UIPreference"%>

<%@ page import="org.codehaus.jackson.JsonGenerationException"%>
<%@ page import="org.codehaus.jackson.map.JsonMappingException"%>
<%@ page import="org.codehaus.jackson.map.ObjectMapper"%>



<%!private static final String jspName = "admin.camelis.jsp";
	private static final Logger LOGGER = LoggerFactory
			.getLogger("fr.irisa.lis.portalis.admin." + jspName);
	private static final int DEFAULT_PAGE_SIZE = 30;
	private static final Property PROPERTY_HOME = new Property("all");
	
	%>
<%
	UserContext userContext = (UserContext) session
			.getAttribute(Constant.USER_CONTEXT);
	if (userContext == null) {
		String mess = "Pas de session utilisateur";
		LOGGER.error(mess);
		response.sendError(
				HttpServletResponse.SC_INTERNAL_SERVER_ERROR, mess);
		return;
	}

	CamelisHttp camelisHttp = userContext.getCamelisHttp();
	if (camelisHttp == null) {
		String mess = "Pas de camelis dans la session utilisateur";
		LOGGER.error(mess);
		response.sendError(
				HttpServletResponse.SC_INTERNAL_SERVER_ERROR, mess);
		return;
	}

	String userPseudo = camelisHttp.getActiveUser().getUserCore()
			.getPseudo();

	ActiveLisService activeService = camelisHttp.getActiveService();
	if (activeService == null) {
		throw new Exception(
				"Pas d'activeService dans la session utilisateur");
	}
	LOGGER.debug(userPseudo + " is using activeService "
			+ activeService);

	String activeServiceId = activeService.getActiveId();
	ServiceCoreReponse serviceCoreReponse = AdminHttp.getServiceCore(
			camelisHttp.getActiveUser(), activeServiceId);
	if (!serviceCoreReponse.isOk()) {
		String mess = "Impossible d'obtenir le serviceCore : "
				+ serviceCoreReponse.getMessagesAsString();
		LOGGER.error(mess);
		response.sendError(
				HttpServletResponse.SC_INTERNAL_SERVER_ERROR, mess);
		return;
	}
	
	UIPreference uiPreference = serviceCoreReponse.getServiceCore().getUiPreference();
	if (uiPreference.getEntryShown()>activeService.getNbObject()) {
		uiPreference.setEntryShown(activeService.getNbObject());
	}
	ObjectMapper mapper = new ObjectMapper();
	String uiPreferenceAsJson = mapper.writeValueAsString(uiPreference);
	LOGGER.debug("uiPreferenceAsJson = " + uiPreferenceAsJson);


	String parcours = (String) session.getAttribute("parcours");
	if (parcours == null) {
		throw new Exception(
				"Pas de type {defaut | testBench} dans la session");
	}

	String query = request.getParameter("query");
	Property wq = null;
	Property feature = PROPERTY_HOME;
	if (query == null) {
		wq = new Property("all");
	} else {
		wq = new Property(query);
	}

	ZoomReponse zoomReponse = camelisHttp.zoom(wq.getName(),
			feature.getName());
	if (!zoomReponse.isOk()) {
		throw new Exception(zoomReponse.getMessagesAsString());
	}
	LisIncrementSet increments = zoomReponse.getIncrements();

	ExtentReponse extentReponse = camelisHttp.extent(wq.getName());
	if (!extentReponse.isOk()) {
		throw new Exception(extentReponse.getMessagesAsString());
	}
	int nbObjects = extentReponse.getNbObjects();
	LisExtent extent = extentReponse.getExtent();

	int minimumSupport = userContext.getUiPreference()
			.getMinimumSupport();
	
%>

<!DOCTYPE html>
<html lang="fr" manifest="./cache.manifest">
<head>

<!-- JSP XML -->
<jsp:directive.page contentType="text/html; charset=UTF-8" />

<!-- JSF/Facelets XHTML -->
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />

<!-- Generation des metadatas, css header etc ... -->
<meta name="viewport"
	content="width=device-width, initial-scale=1.0, user-scalable=no">
<meta name="author" content="">
<meta name="description" content="">
<meta name="keywords" content="">
<meta name="generator" content="Eclipse Java EE 3.7">
<title>Portalis</title>


<link href="css/ui-lightness/jquery-ui-1.10.3.custom.css"
	rel="stylesheet" type="text/css" />
<link href="css/camelis.css" rel="stylesheet" type="text/css"
	media="screen" />
<link href="css/TableTools.css" rel="stylesheet" type="text/css"/>
<!--[if lt IE 9]-->
<script type="text/javascript"
	src="http://code.jquery.com/jquery-1.10.2.js"></script>
<!--[endif]-->
<!--[if gte IE 9]-->
<script type="text/javascript"
	src="http://code.jquery.com/jquery-2.0.3.js"></script>
<!--[endif]-->
<script type="text/javascript" src="./js/switchrequest.js"></script>

<link rel="stylesheet" type="text/css" href="./css/jqtree.css" />
<link rel="stylesheet" type="text/css"
	href="./css/jquery.dataTables.css" />
<link rel="stylesheet" type="text/css"
	href="./css/jquery.dataTables_themeroller.css" />

<script type="text/javascript"
	src="js/jQuery/jquery-ui-1.10.3.custom.js"></script>
<script type="text/javascript" src="./js/jQuery/tree.jquery.js"></script>
<script type="text/javascript" src="./js/jQuery/jquery.dataTables.js"></script>
<script type="text/javascript" src="./js/jQuery/TableTools.js"></script>
<script type="text/javascript"
	src="./js/jQuery/dataTables.reloadAjax.js"></script>
<script type="text/javascript" src="./js/camelis.js"></script>
</head>
<body>

	<!-- Generation du header -->
	<header id="main-header">
		<div id="logo-container">
			<img id="logo" alt="" src="images/logo-lis-60.gif" /> <label
				id="logo-label">Portalis</label>
		</div>
		<table style="width:100%">
		<tr>
		<td>
			<span>
				<textarea style="width: 91%; " name="requete" rows="4" cols="2" id="request"><%=wq.getName()%></textarea>
			</span>
			<span class="rightAlign" style="width: 8%; ">
				<em><b>Service</b></em>&nbsp;:
				<br /><%=activeService.getActivePartOfId()%>
				<br /><%=activeService.getFullName()%>
				<br />
				<em><b>User</b></em>&nbsp;:
				<br /><span id="leftInfo"><%=userPseudo%></span>

			</span>
		</td>
		</tr>
		<tr>
		<td>
			<span id="buttonsForRequest">
				<img id="refresh"
						src="images/page-refresh-reload-icone-5333-16.png" alt=""  onclick="ajaxRefresh_AttrAndObjects()"/>
			</span>
			<span style="float: right;">
				<img id="toolsBotton"
				src="images/Android-Assistant-24.png" alt="Accéder aux outils"
				title="Propriétés" onclick="showToolDiv()" />
				<img
				id="annuler" style="display: none;"
				src="images/annuler-icone-24.png" alt="annuler" title="Annuler"
				onclick="showMain()" />
				<img id="show-caddy" src="images/icone-caddy-24.png" alt=""
						onclick="showCaddy()" title="Press papier"/>
				<a href="clientLogout"><img style="vertical-align:top"
					src="images/gnome-logout-icone-3872-16.png" alt="" title="logout" /></a>
			<%
				if (parcours.equals("default")) {
			%>
			<a id="changeService" href="chooseService.jsp"><img style="vertical-align:top"
				src="images/upload-16.png" alt="" title="Choose new service" /></a>&nbsp;
			<%
				} else {
			%>
			<a id="changeService"
				href="startTestBench.jsp?action=testBenchReload"><img style="vertical-align:top"
				src="images/upload-16.png" alt="" title="Reload service" /></a>&nbsp;
			<%
				} 

			%>
			</span>
		</td>
		</tr>
		</table>
	</header>

	<section id="logError" style="display:none;">
		<div class="log" style="background-color: #FAC5D2;"></div>
		<button onclick="$('#logError').hide()">Hide</button>
	</section>

	<section id="main-content">
		<section id="left-section">
			<header id="sub-header">
					<img id="minize-left-img" src="images/toggle-expand-alt_basic_blue-24.png"
						alt="" onclick="toggleCamelisJsp('left')"/>
				<span style="position: absolute; top: 2px; left-margin: 3px;">
					<label id="left-label">Attributs</label> <label
					for="minSupportSpinner">Minimum Support</label> <input
					id="minSupportSpinner" name="value" value="<%=minimumSupport%>" style="width: 24px;" />
				</span> <span id="zoomSelection"> <select id="zoomSelector">
						<option value="or">... and (or &lt;select&gt;)</option>
						<option value="except">... except &lt;select&gt;</option>
						<option value="not">not (or &lt;select&gt;)</option>
				</select>
						<img onclick="zoom()" id="navig" src="images/icone-validation-24.jpg" alt="" />
				</span>

			</header>
			<article>
				<div id="attributs" data-url="zoom"></div>
			</article>
		</section>

		<section id="right-section">

			<header id="sub-header">
					<img id="minize-right-img" src="images/toggle-expand-alt_basic_blue-24.png"
						alt="" onclick="toggleCamelisJsp('right')"/>
				<label id="right-label">Objets</label> &nbsp;<span
					id="currentObjectCount"><%=nbObjects%></span>/<span
					id="objectCountLabel"><%=nbObjects%></span>
				<%
					if (uiPreference.isHasPictures()) {
				%>
				<!--
	 			<select
					id="objectPageSelector" onchange="objectPageChange(this)">
					<option value="objectList">list</option>
					<option value="objectListIcon">listIcon</option>
					<option value="objectTabIcon">tabIcon</option>
				</select>
	 -->
				<%
					}
				%>
			</header>
			<article id="rightSectionContainer">
				<div id="objectList">
					<table id="tableObjectList">
						<thead>
						</thead>
						<tbody></tbody>
					</table>
				</div>
			</article>
		</section>
		<section id="UIPreference">
		</section>
	</section>
	<script type="text/javascript">
//	<!--
	// Retrieve preferences from storage
	var activeServiceId = '<%=activeServiceId%>';
	var headers = new Array();
	var uiPreference = {};

	var retrievedObject;
	
	if(typeof(window.localStorage) != 'undefined'){ 
		retrievedObject = localStorage.getItem('uiPreference/'+activeServiceId);
	} 
	else{ 
		throw "window.localStorage, not defined"; 
	}
	if (retrievedObject==null || retrievedObject== 'undefined' ) {
		uiPreference = <%=uiPreferenceAsJson%>;
		display('default uiPreference', uiPreference);
	} else {
		uiPreference = JSON.parse(retrievedObject);
		display('client uiPreference', uiPreference);
		// send preferences to server
		$.ajax({
			"url" : 'preference?callback=?',
			"dataType" : 'json',
	        "type": "POST",
			"data" : {
				preferences: JSON.stringify(uiPreference)
			},
	        "error": showError,
			"succes": function(data) {
				display('data', data);
	    		initHeaders(data.headers);
				display('new headers', headers);
	    		ajaxRefresh_AttrAndObjects();
			},
		});
	}

		var $objectTable;
		

		$(document).ready(function() {
		    $("#UIPreference").load("UIPreference.html", function() {
				document.getElementById('toolDiv').style.display = "none";
				initHtmlPreferences();
				
		    }); 

		    $("#request").val("all");
			ajaxZoom("all");

			$("#minSupportSpinner").spinner({
				stop : refreshMinimumSupport,
				min : 0,
			});
			
			$(function() {
				//use jQuery AJAX to get column definitions for the objects table
				$.ajax({
					"url" : 'preference?callback=?',
					"dataType" : 'json',
			        "error": showError,
					"success" : function(data) {
			    		initHeaders(data.headers);
						uiPreference = data.uiPreferences;
						display("drawing object table, headers",headers);
						$objectTable = $('#tableObjectList').dataTable({
							"aoColumns" : headers,
							"bServerSide": true,
							"iDisplayLength" : uiPreference.entryShown,
							"sAjaxSource" : getObjectUrl(),
							"bAutoWidth" : false,
						    "fnInitComplete": installObjectSpinner,
						    // this is to check errors comming back from server
			                "fnServerData": getExtentFromServer,
						});
					},
				});
				
			});

			

			objectPageChange();
			

			document.getElementById('annuler').style.display = "none";
		});


		window.onload = setHeight;
		window.onresize = setHeight;

		$body = $("body");

		$(document).on({
			ajaxStart : function() {
				$body.addClass("loading");
			},
			ajaxStop : function() {
				$body.removeClass("loading");
			}
		});
		
		function showMain() {
			document.getElementById("right-section").style.display = "table-cell";
			document.getElementById("left-section").style.display = "table-cell";
			// toolDiv = page de preference
			document.getElementById('toolDiv').style.display = "none";
			// annuler = page de preference
			document.getElementById('annuler').style.display = "none";
			// toolsBotton = appel de page de preference
			document.getElementById('toolsBotton').style.display = "table-cell";
		}
		
//-->
	</script>
	<div class="modal"></div>


</body>
</html>


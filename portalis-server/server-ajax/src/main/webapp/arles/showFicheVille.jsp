<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page errorPage="showError.jsp" %>

<%@ page import="org.slf4j.Logger"%>
<%@ page import="org.slf4j.LoggerFactory"%>

<%@ page import="fr.irisa.lis.portalis.service.patrimoine.arles.db.PatrimoineArlesDataBase"%>
<%@ page import="fr.irisa.lis.portalis.shared.admin.PortalisException"%>
<%@ page import="fr.irisa.lis.portalis.shared.admin.XmlIdentifier"%>
<%@ page import="fr.irisa.lis.portalis.server.ajax.Constant"%>



<%!
	private static final String jspName = "admin.showDecrVille.jsp";
	private static final Logger LOGGER = LoggerFactory
			.getLogger("fr.irisa.lis.portalis.admin." + jspName);

%>

<%
	PatrimoineArlesDataBase patrimoineArlesDataBase = new PatrimoineArlesDataBase();
	short id = 0;
	String sId = request.getParameter("id");
	if (sId!=null) {
	try {
		id = Short.parseShort(sId);
	} catch (Exception e) {
		String mess = "Parametre 'id' non numérique : "+sId;
		LOGGER.error(mess);
		throw new PortalisException(mess);
	}
	}
	
	response.setHeader("Expires", "0");
	response.setContentType(Constant.CONTENT_TYPE_HTML);

%>
<!doctype html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
	<link rel="stylesheet" type="text/css" href="../../css/portalis.css">
</head>
<body>
	<jsp:include page="${request.getContextPath()}/arles/header.jsp" />

<% if (sId==null) {
		out.print(patrimoineArlesDataBase.getHtmlFicheVille());
	} else {
		out.print(patrimoineArlesDataBase.getHtmlFicheVille(id));
	}%>
}
</body>
</html>
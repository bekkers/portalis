
function setHeight() {
	var height = 0;
	if (document.body && document.body.offsetHeight) {
		height = document.body.offsetHeight;
	}
	if (document.compatMode == 'CSS1Compat' && document.documentElement
			&& document.documentElement.offsetHeight) {
		height = document.documentElement.offsetHeight;
	}
	if (window.innerHeight) {
		height = window.innerHeight;
	}
	height = height - 233;
	if (height < 100) {
		height = 100;
	} // dimension minimum = 100
	document.getElementById("rightSectionContainer").style.height = height
			+ "px";
	document.getElementById("attributs").style.height = height + "px";
}

function toggleCamelisJsp(sideName) {
	toggle(sideName);
	$objectTable.fnAdjustColumnSizing(true);
}

function toggle(name) {
	if (typeof window.currentSection == 'undefined') {
		window.currentSection = 'both';
	}
	if (window.currentSection == 'both') {
		window.currentSection = name;
		if (name == 'right') {
			$('#minize-right-img').attr('src',
					'images/toggle-collapse-alt_basic_blue-24.png');
			document.getElementById("right-section").style.width = "100%";
			document.getElementById("left-section").style.display = "none";
		} else if (name == 'left') {
			$('#minize-left-img').attr('src',
					'images/toggle-collapse-alt_basic_blue-24.png');
			document.getElementById("left-section").style.width = "100%";
			document.getElementById("right-section").style.display = "none";
		}
	} else if (window.currentSection == 'right') {
		$('#minize-right-img').attr('src',
				'images/toggle-expand-alt_basic_blue-24.png');
		document.getElementById("right-section").style.width = "50%";
		document.getElementById("left-section").style.width = "50%";
		document.getElementById("left-section").style.display = "table-cell";
		window.currentSection = 'both';
	} else if (window.currentSection == 'left') {
		$('#minize-left-img').attr('src',
				'images/toggle-expand-alt_basic_blue-24.png');
		document.getElementById("left-section").style.width = "50%";
		document.getElementById("right-section").style.width = "50%";
		document.getElementById("right-section").style.display = "table-cell";
		window.currentSection = 'both';
	}
}

/* gestion des objets */
/* ================== */

function objectPageChange() {
	var selector = $('#objectPageSelector').val();
	if (selector == 'objectList') {
		$("#objectListIcon").css('display', 'none');
		$("#objectList").css('display', 'table-cell');
		$("#objectTabIcon").css('display', 'none');
	} else if (selector == 'objectListIcon') {
		$("#objectList").css('display', 'none');
		$("#objectTabIcon").css('display', 'none');
		$("#objectListIcon").css('display', 'table-cell');
	} else if (selector == 'objectTabIcon') {
		$("#objectList").css('display', 'none');
		$("#objectListIcon").css('display', 'none');
		$("#objectTabIcon").css('display', 'table-cell');
	}

}

function getObjectUrl() {
	console.log('getObjectUrl(' + $("#request").val() + ')');
	return 'extent?request=' + encodeURIComponent($("#request").val())
			+ '&callback=?&aaData=true';
}

function buildOjectTabIcons(objects) {
	$('#objectTabIcon').empty();

	var section = $('<section></section>');
	$('#objectTabIcon').append(section);
	jQuery
			.each(
					objects,
					function(i, field) {
						var table = $('<table class="objectTabContainer"><tr><td><center style="width: 140px;">'
								+ '<img class="objectTabIcon" src="'
								+ field.picture
								+ '" width="80" height="80" /><br />'
								+ field.id
								+ '<br />'
								+ '<label class="objectTabLabel">'
								+ field.label
								+ '</label>'
								+ '</center></td></tr></table>');
						section.append(table);
					});
}

/* gestion des attributs */
/* ===================== */

var putNodes = function(node, $li) {
	if (node.color) {
		var $title = $li.find('.jqtree-title');
		$title.addClass(node.color);
	}
	if (node.color != 'gray') {
		$($li.children().last().children().last()).before(
				'<input type="checkbox" name="choisir" onchange="updateAttrCaddy(&quot;'
						+ replaceAll("'", "\\x27", replaceAll('"', "\\x22",
								node.id)) + '&quot;, this)"/>');
	}
};

function replaceAll(find, replace, str) {
	return str.replace(new RegExp(find, 'g'), replace);
}

var loadFailedFunction = function(response) {
	$('#attributs').tree('destroy');
	$('#attributs').append(response.responseText);
};

function ajaxZoom(req) {
	var url = 'zoom?minSupport=' + minSupportSpinner.value + '&request='
			+ encodeURIComponent(req) + "&callback=?";
	$('#attributs').tree('destroy');
	jQuery.getJSON(url, function(result) {
		$('#attributs').tree({
			data : result,
			onCreateLi : putNodes,
			onLoadFailed : loadFailedFunction,
			dataUrl : function(node) {
				var result = {
					'url' : url,
					'type' : 'post'
				};

				if (node) {
					// -> load on demand
					result['data'] = {
						'node' : node.id
					};
				}

				return result;
			}
		});
	});
}

var objCaddy = [];
var attrCaddy = [];

function resetCheckedNodes() {

	for (var i = 0; i < objCaddy.length; i = i + 1) {
		console.log('resetCheckedNodes() : '
				+ $('#attributs').tree().getNodeById(objCaddy[i]));
	}
}

function showCaddy() {
	alert(attrCaddy);
}

function updateAttrCaddy(featureName, input) {
	console.log('before feature = ' + featureName + ' checked = '
			+ input.checked + ' attrCaddy = ' + attrCaddy);
	if (input.checked) {
		attrCaddy.push(featureName);
	} else {
		attrCaddy.pop(input);
	}
	console.log('before attrCaddy = ' + attrCaddy);
}

function installObjectSpinner() {
	$('#tableObjectList_length>label').empty();
	$('#tableObjectList_length>label').append('Show&nbsp;');
	$('#tableObjectList_length>label')
			.append(
					'<input id="entryShownSpinner" name="value" value="'
							+ uiPreference.entryShown
							+ '" style="width: 24px; font-size: 16px; margin-top: 0; height: 15px" />');
	$('#tableObjectList_length>label').append('&nbsp;entries');
	$('#entryShownSpinner').spinner({
		min : 0,
		step : 10,
		max : $('#objectCountLabel').text() * 1,
		stop : function() {
			refreshEntryShown();
		},
	});
}

function getExtentFromServer(sSource, aoData, fnCallback) {
	// debugger;
	$.ajax({
		"dataType" : 'json',
		"type" : "POST",
		"url" : sSource,
		"data" : aoData,
		"cache" : true,
		"timeout" : 40000,
		"success" : function(data) {
			$('#currentObjectCount').text(data.iTotalRecords);
			fnCallback(data);
		},
		"error" : showError,
	});
}

function showError(request, textStatus, thrownError) {
	$('div.log').empty();
	$('div.log').append(request.responseText);
	$('#logError').show();
}
// this function is called when user click on "zoomButton" in left part
function zoom() {
	if (attrCaddy.length == 0) {
		return;
	}
	var oldQuery = $("#request").val();

	var newQuery = "";
	for (var i = 0; i < attrCaddy.length; i++) {
		if (i == 0) {
			newQuery = attrCaddy.pop();
		} else {
			newQuery = newQuery + " or " + attrCaddy.pop();
		}
		;
	}

	var query = "";

	var selector = $('#zoomSelector').val();
	if (oldQuery == "all") {
		if (selector == 'or') {
			query = newQuery;
		} else {
			query = 'not(' + newQuery + ')';
		}
		;
	} else {
		if (selector == 'or') {
			query = oldQuery + " and " + newQuery;
		} else if (selector == 'except') {
			query = oldQuery + ' and not(' + newQuery + ')';
		} else if (selector == 'not') {
			query = 'not(' + newQuery + ')';
		}
		;
	}
	$("#request").val(query);
	console.log('zoom req=' + $("#request").val());
	ajaxRefresh_AttrAndObjects();
}

function refreshEntryShown() {
	uiPreference.entryShown = $('#entryShownSpinner').val();
	storePreferences();
	console.log('refreshEntryShown', uiPreference.entryShown);
	ajaxRefresh_AttrAndObjects();
}

function refreshMinimumSupport() {
	uiPreference.minimumSupport = $('#minSupportSpinner').val();
	console.log('refreshMinimumSupport', uiPreference.minimumSupport);
	storePreferences();
	ajaxRefresh_AttrAndObjects();
}

/* clic sur le bouton refresh de la requête */

function ajaxRefresh_AttrAndObjects() {
	var req = $("#request").val();
	console.log('ajaxRefresh_AttrAndObjects req=' + req);
	ajaxZoom(req);
	console.log('après ajaxZoom req=' + req);
	var oSettings = $objectTable.fnSettings();
	oSettings._iDisplayStart = 0;
	oSettings._iDisplayLength = uiPreference.entryShown;
	reDrawObjectTable();
	// $objectTable.fnReloadAjax(getObjectUrl());
}

function reDrawObjectTable() {
	display("redrawing object table, headers", headers);
	$('#tableObjectList > thead').empty();
	$objectTable.fnSettings().oInit.aoColumns = [];
	$objectTable = $('#tableObjectList').dataTable({
		"bDestroy" : true,
		"aoColumns" : headers,
		"bServerSide" : true,
		"iDisplayLength" : uiPreference.entryShown,
		"sAjaxSource" : getObjectUrl(),
		"bAutoWidth" : false,
		"fnInitComplete" : installObjectSpinner,
		// this is to check errors comming back from server
		"fnServerData" : getExtentFromServer,
	});
}

/* preferences */
/* =========== */

function initHtmlPreferences() {

	if (uiPreference.hasPictures)
		$('#UIPref_hasPictures').attr('checked', 'checked');
	else if ($('#UIPref_hasPictures').attr('checked'))
		$('#UIPref_hasPictures').removeAttr('checked');

	if (uiPreference.camelisID)
		$('#UIPref_CamelisIds').attr('checked', 'checked');
	else if ($('#UIPref_CamelisIds').attr('checked'))
		$('#UIPref_CamelisIds').removeAttr('checked');

	if (uiPreference.camelisLabel)
		$('#UIPref_CamelisLabels').attr('checked', 'checked');
	else if ($('#UIPref_CamelisLabels').attr('checked'))
		$('#UIPref_CamelisLabels').removeAttr('checked');

	$("#UIPref_minSupportSpinner").spinner({
		min : 0,
		max : $('#objectCountLabel').text() * 1,
	});
	$("#UIPref_minSupportSpinner").val(uiPreference.minimumSupport);
	$("#UIPref_entryShownSpinner").spinner({
		min : 0,
		step : 10,
		max : $('#objectCountLabel').text() * 1,
	});
	$("#UIPref_entryShownSpinner").val(uiPreference.entryShown);
}

function defaultPreferences() {
	$.ajax({
		"url" : 'preference?callback=?&default=true',
		"type" : 'POST',
		"dataType" : 'json',
		"error" : showError,
		"success" : function(preferencesAndHeaders) {
			uiPreference = {};
			uiPreference = preferencesAndHeaders.uiPreferences;
			initHeaders(preferencesAndHeaders.headers);
			display('defaultPreferences preferencesAndHeaders',
					preferencesAndHeaders);
			display('after reseting default preferences, headers=', headers);
			storePreferences();
		}
	});
}

function savePreferences() {
	if ($('#UIPref_hasPictures').prop('checked')) {
		uiPreference.hasPictures = true;
	} else {
		uiPreference.hasPictures = false;
	}
	if ($('#UIPref_CamelisIds').prop('checked')) {
		uiPreference.camelisID = true;
	} else {
		uiPreference.camelisID = false;
	}
	if ($('#UIPref_CamelisLabels').prop('checked')) {
		uiPreference.camelisLabel = true;
	} else {
		uiPreference.camelisLabel = false;
	}
	uiPreference.entryShown = $("#UIPref_entryShownSpinner").spinner("value") * 1;
	$('#entryShownSpinner').val(uiPreference.entryShown);
	uiPreference.minimumSupport = $("#UIPref_minSupportSpinner").spinner(
			"value") * 1;
	if ($('#UIPref_hiddenAttribute').val() == null) {
		uiPreference.hiddenAttribute = [];
	} else {
		uiPreference.hiddenAttribute = $('#UIPref_hiddenAttribute').val();
	}
	uiPreference.extraColumn = {};
	var extraColumns = $('#UIPref_extraColumn').val();
	// extraColumns is a map indexed by the name of the column, the value
	// is '?local?' if the column is a Camelis attribut, otherwise its an url to
	// get the column values
	// this url is called by the servlet Extent

	if (extraColumns != null) {
		for (var i = 0; i < extraColumns.length; i++) {
			uiPreference.extraColumn[extraColumns[i]] = '?local?';
		}
	}
	display("savePreferences", uiPreference);
	storePreferences();
}

function storePreferences() {
	// store preferences into client storage
	localStorage.setItem('uiPreference/' + activeServiceId, JSON
			.stringify(uiPreference));
	console.log("stringify=" + JSON.stringify(uiPreference));
	// send preferences to server
	$.ajax({
		"url" : 'preference?callback=?',
		"type" : 'POST',
		"dataType" : 'json',
		"data" : {
			"preferences" : JSON.stringify(uiPreference)
		},
		"error" : showError,
		"success" : function(data) {
			display('storePreferences', data);
			initHeaders(data.headers);
			display('after sending preferences, headers=', headers);
			showMain();
			ajaxRefresh_AttrAndObjects();
		}
	});
}

function initHeaders(newHeaders) {
	headers.splice(0, headers.length);
	headers.length = 0;
	for (x in newHeaders) {
		headers.push(newHeaders[x]);
	}
}

function initHtmlFeaturesPreferencesFromServer() {
	$.ajax({
		"url" : 'feature?callback=?',
		"dataType" : 'json',
		"error" : showError,
		"success" : function(features) {
			currentFeatures = features;
			$('#UIPref_hiddenAttribute').empty();
			for (var i = 0; i < currentFeatures.length; i++) {
				var currentFeature = currentFeatures[i];
				if (contains(uiPreference.hiddenAttribute, currentFeature)) {
					$('#UIPref_hiddenAttribute').append(
							"<option selected=\"selected\" >" + currentFeature
									+ "</option>");
				} else {
					$('#UIPref_hiddenAttribute').append(
							"<option>" + currentFeature + "</option>");
				}
			}
			$('#UIPref_extraColumn').empty();
			for (var i = 0; i < currentFeatures.length; i++) {
				var currentFeature = currentFeatures[i];
				if (uiPreference.extraColumn[currentFeature] === "?local?") {
					$('#UIPref_extraColumn').append(
							"<option selected=\"selected\" >" + currentFeature
									+ "</option>");
				} else {
					$('#UIPref_extraColumn').append(
							"<option>" + currentFeature + "</option>");
				}
			}
		},
	});
}

function showToolDiv() {
	initHtmlFeaturesPreferencesFromServer();
	initHtmlPreferences();
	document.getElementById("right-section").style.display = "none";
	document.getElementById("left-section").style.display = "none";
	document.getElementById('toolDiv').style.display = "table-cell";
	document.getElementById('annuler').style.display = "table-cell";
	document.getElementById('toolsBotton').style.display = "none";
}

/* services fonctions */
/* ================== */

function contains(a, obj) {
	if (getClass(a) != "Array")
		return false;
	for (var i = 0; i < a.length; i++) {
		if (a[i] === obj) {
			return true;
		}
	}
	return false;
}

function display(label, o) {
	console.log(label + " ::\n" + prettyStringify(o));
}

function prettyStringify(data) {
	return JSON.stringify(data, undefined, 4);
}

function getClass(x) {
	var str = Object.prototype.toString.call(x);
	return /^\[object (.*)\]$/.exec(str)[1];
}

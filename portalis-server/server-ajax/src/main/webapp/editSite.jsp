<?xml version="1.0" encoding="UTF-8" ?>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%@ page import="org.slf4j.Logger"%>
<%@ page import="org.slf4j.LoggerFactory"%>

<%@ page errorPage="showError.jsp"%>
<%@ page import="fr.irisa.lis.portalis.shared.camelis.http.CamelisHttp"%>
<%@ page import="fr.irisa.lis.portalis.server.ajax.Constant"%>
<%@ page import="fr.irisa.lis.portalis.server.ajax.UserContext"%>

<%!private static final String jspName = "admin.camelis.jsp";
	private static final Logger LOGGER = LoggerFactory
			.getLogger("fr.irisa.lis.portalis.admin." + jspName);
%>

<%
	UserContext userContext = (UserContext) session
			.getAttribute(Constant.USER_CONTEXT);
	if (userContext == null) {
		String mess = "Pas de session utilisateur";
		LOGGER.error(mess);
		response.sendError(
				HttpServletResponse.SC_INTERNAL_SERVER_ERROR, mess);
		return;
	}

	CamelisHttp camelisHttp = userContext.getCamelisHttp();
	if (camelisHttp == null) {
		String mess = "Pas de camelis dans la session utilisateur";
		LOGGER.error(mess);
		response.sendError(
				HttpServletResponse.SC_INTERNAL_SERVER_ERROR, mess);
		return;
	}

	String userPseudo = camelisHttp.getActiveUser().getUserCore()
			.getPseudo();;
%>
<!doctype html>
<html>
<head>

<!-- JSP XML -->
<jsp:directive.page contentType="text/html; charset=UTF-8" />

<!-- JSF/Facelets XHTML -->
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />

<meta name="viewport"
	content="width=device-width, initial-scale=1.0, user-scalable=no">
<link rel="stylesheet" type="text/css" href="css/portalis.css">
<link rel="stylesheet" type="text/css" href="css/camelis.css"
	media="screen" />
<title>Insert title here</title>
<!--[if lt IE 9]-->
<script type="text/javascript" src="http://code.jquery.com/jquery-1.10.2.js"></script>
<!--[endif]-->
<!--[if gte IE 9]-->
<script type="text/javascript" src="http://code.jquery.com/jquery-2.0.3.js"></script>
<!--[endif]-->
<script type="text/javascript" src="./js/camelis.js"></script>
<link href="css/jquery.treetable.css" rel="stylesheet" type="text/css" />
<script src="js/jquery.treetable.js"></script>
</head>
<body>
	<header id="main-header">
		<div id="logo-container">
			<img id="logo" alt="" src="images/logo-lis-60.gif" /> <label
				id="logo-label">Portalis</label>
		</div>
		<div id="header-title">
			<h4>Administration des services</h4>
		</div>
		<div class="rightAlign" >
			<a id="logout" href="clientLogout" title="Logout"> <img
				src="images/logout-icone-6625-24.png" alt="Logout" />
			</a> user :<br /><%=userPseudo%><br /> <a href="showUsers.html"
				title="Edit Users"> <img
				src="images/Alarm-Arrow-Right-icon-24.png" alt="Show users" />
			</a>
		</div>

	</header>

	<section>
		<div class="log" style="color: red;"></div>
	</section>

	<section id="main-content">
		<section id="left-section">
			<header id="sub-header">
					<img class="toggler" onclick="toggle('left')" id="minize-left-img" src="images/toggle-expand-alt_basic_blue-24.png"
						alt="" />
				<label id="left-label">Création/lancement de services</label>
			</header>
			<article>
				<div id="staticSite"></div>
			</article>
		</section>
		<section id="right-section">
			<header id="sub-header">
					<img class="toggler" onclick="toggle('left')" id="minize-right-img" src="images/toggle-expand-alt_basic_blue-24.png"
						alt="" />
				<label id="right-label">Services actifs</label>
			</header>
			<article>
				<div id="actifSite"></div>
			</article>
		</section>
	</section>

	<script type="text/javascript">
		//<[CDATA[
		$(document)
				.ready(
						function() {
							$('#staticSite')
									.load(
											'editSite?site=static',
											function(response, status, xhr) {
												if (status == "error") {
													$("#staticSite").html(
															response);
													var msg = "Sorry but there was an error: "
															+ xhr.status
															+ " "
															+ xhr.statusText;
													$("#div.log").html(msg);
													console.log(msg);
													alert(msg);
												}
												;
											});
							$('#actifSite')
									.load(
											'editSite?site=actif',
											function(response, status, xhr) {
												if (status == "error") {
													$("#actifSite").html(
															reponse);
													var msg = "Sorry but there was an error: "
															+ xhr.status
															+ " "
															+ xhr.statusText;
													$("#div.log").html(msg);
													console.log(msg);
													alert(msg);
												}
												;
											});
						});

		$body = $("body");

		$(document).on({
			ajaxStart : function() {
				$body.addClass("loading");
			},
			ajaxStop : function() {
				$body.removeClass("loading");
			}
		});
		//]]>
	</script>
	<div class="modal"></div>

</body>
</html>
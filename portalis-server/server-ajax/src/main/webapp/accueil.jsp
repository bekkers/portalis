<?xml version="1.0" encoding="UTF-8" ?>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page errorPage="showError.jsp"%>


<% 
	session.setAttribute("parcours", "default");

%>

<!doctype html>
<html>
<head>

<!-- JSP XML -->
<jsp:directive.page contentType="text/html; charset=UTF-8" />

<!-- JSF/Facelets XHTML -->
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />

<meta name="viewport"
	content="width=device-width, initial-scale=1.0, user-scalable=no">
<link rel="stylesheet" type="text/css" href="css/portalis.css">
<link rel="stylesheet" type="text/css" href="css/camelis.css"
	media="screen" />
<title>Insert title here</title>
<!--[if lt IE 9]-->
<script type="text/javascript" src="http://code.jquery.com/jquery-1.10.2.js"></script>
<!--[endif]-->
<!--[if gte IE 9]-->
<script type="text/javascript" src="http://code.jquery.com/jquery-2.0.3.js"></script>
<!--[endif]-->
<script type="text/javascript" src="./js/camelis.js"></script>
</head>
<body>
	<header id="main-header">
		<div id="logo-container">
			<img id="logo" alt="" src="images/logo-lis-60.gif" /> <label id="logo-label">Portalis</label>
		</div>
		<div id="header-title">
			<h4>Etat des services</h4>
		</div>
		<div class="rightAlign">
			<span id="annuler" style="display: none;">
				<em><b>Not connected</b></em> <br/>Annuler login :<br/><img
					src="images/annuler-icone-24.png" alt="annuler" title="Annuler login"
					onclick="showMain()" />
			</span>
			<span id="login">
				<em><b>Not connected</b></em> <br/><img
					src="images/login-computer-button-24.jpg" alt="login" title="Login"
					onclick="showLoginDiv()" />
			</span>
		</div>

	</header>

	<section>
		<div class="log" style="color: red;"></div>
	</section>
	<section id="main-content">
		<section id="left-section">
			<header id="sub-header">
					<img class="toggler" onclick="toggle('left')" id="minize-left-img" src="images/toggle-expand-alt_basic_blue-24.png" alt="" />
				<label id="left-label">Modèles de service</label>
			</header>
			<article>
				<div id="staticSite"></div>
			</article>
		</section>
		<section id="right-section">
			<header id="sub-header">
					<img class="toggler" onclick="toggle('right')" id="minize-right-img" src="images/toggle-expand-alt_basic_blue-24.png" alt="" />
				<label id="right-label">Services actifs</label>
				<div class="rightAlign">
					<form action="startTestBench.jsp?parcours=default">
						<span>Charger et tester votre propre service :</span><input
							type="submit" value="Ok" />
					</form>
				</div>
			</header>
			<article>
				<div id="actifSite"></div>
			</article>
		</section>
		<section id="divLogin">
			<form action="execLogin" method="post">
				<div class="centered">
					<table>
						<tr>
							<th colspan="2">Accéder aux services lis</th>
						</tr>
						<tr>
							<td><label for="email">Identifiant</label></td>
							<td><input type="email" id="email" name="email" /></td>
						</tr>
						<tr>
							<td><label for="password">Mot de passe</label></td>
							<td><input type="password" id="password" name="password" /></td>
						</tr>
						<tr>
							<td colspan="2"><div>
									<input type="radio" name="admin" value="camelis"
										checked="checked" />Utiliser Camelis <input type="radio"
										name="admin" value="site" />Admin Site <input type="radio"
										name="admin" value="users" />Admin Users
								</div></td>
						</tr>
						<tr>
							<td colspan="2"><span class="centrage"><input
									type="submit" name="connexionType" value="anonymousConnexion"
									id="valid-form" /><input type="submit" name="connexionType"
									value="userConnexion" id="valid-form" /></span></td>
						</tr>
					</table>
				</div>
			</form>
		</section>
	</section>

	<script type="text/javascript">
		//<[CDATA[
		$(document).ready(
				function() {
					$('#staticSite').load('showSite?site=static',
							function(response, status, xhr) {
								if (status == "error") {
									$("#staticSite").html(response);
									var msg = "Sorry but there was an error: "
										+ xhr.status
										+ " "
										+ xhr.statusText;
								$("#div.log").html(msg);
								console.log(msg);
								alert(msg);
								}
								;
							});
					$('#actifSite').load('showSite?site=actif',
							function(response, status, xhr) {
								if (status == "error") {
									$("#actifSite").html(reponse);
									var msg = "Sorry but there was an error: "
										+ xhr.status
										+ " "
										+ xhr.statusText;
								$("#div.log").html(msg);
								console.log(msg);
								alert(msg);
								}
								;
							});
					document.getElementById('divLogin').style.display = "none";
					document.getElementById('annuler').style.display = "none";

				});


		function showLoginDiv() {
			document.getElementById("right-section").style.display = "none";
			document.getElementById("left-section").style.display = "none";
			document.getElementById('divLogin').style.display = "table-cell";
			document.getElementById('annuler').style.display = "table-cell";
			document.getElementById('login').style.display = "none";
		}
		function showMain() {
			document.getElementById("right-section").style.display = "table-cell";
			document.getElementById("left-section").style.display = "table-cell";
			document.getElementById('divLogin').style.display = "none";
			document.getElementById('annuler').style.display = "none";
			document.getElementById('login').style.display = "table-cell";
		}
		
		$body = $("body");

		$(document).on({
		    ajaxStart: function() { 
		        $body.addClass("loading"); 
		    },
		    ajaxStop: function() { 
		        $body.removeClass("loading"); 
		    }    
		});
		//]]>
	</script>
	<div class="modal"></div>

</body>
</html>
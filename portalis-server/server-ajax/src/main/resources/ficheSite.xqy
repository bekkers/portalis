declare namespace local = "http://www.irisa.fr/bekkers";
declare variable $root as node() external;

declare function local:getSite() as  element()* {
		<section> 
		{
	for $application in $root/activeSite/lisApplication
	return(
		<h3 class="name">{concat('',$application/@applicationId)}</h3>,
		<ol class="list">
		{for $service in $application/serviceCore
		return
		<li>{concat($service/@serviceName,' (', $service/@role, 
			', ', $service/@language, ')')}</li>
		}
		</ol>)
		}
		
		</section>
};

declare function local:getActifSite() as  element()* {
		<section> 
		{
	for $sparqlEndPoints in $root/activeSite/sparqlEndPoints
	return(
		for $sparqlEndPoint in $sparqlEndPoints/sparqlEndPoint
		return(
		<h3 class="name">
		{concat('sparqlEndPoint - ',$sparqlEndPoint/@serviceName)}
		</h3>,
		concat(' (', $sparqlEndPoint/@url, ')'),
		<ol>
		{for $query in $sparqlEndPoint/query
		return
		<form action="query.jsp" method="POST">
		<li>
		<input type="submit" value="Go"/>{concat($query/@label, '')}
		<input type="hidden" name="serviceName" value="{$sparqlEndPoint/@serviceName}"/>
		<input type="hidden" name="url" value="{$sparqlEndPoint/@url}"/>
		<input type="hidden" name="query" value="{$query/node()/text()}"/>		
		<input type="hidden" name="label" value="{$query/@label}"/>		
		</li>
		</form>
		}
		</ol>)
	),
	for $camelis in $root/activeSite/camelis
	let $fullName := substring-after($camelis/@serviceId, '::')
	let $appliName := substring-before($fullName, ':')
	let $serviceName := substring-after($fullName, ':')
	let $activationDate := substring-before($camelis/@activationDate, 'T')
	let $activationTime := substring-before(substring-after($camelis/@activationDate, 'T'), '.')
	let $lastDate := if ($camelis/@lastUpdate='none') then 'none' else concat(substring-before($camelis/@lastUpdate, 'T'), ' at ',
	 substring-before(substring-after($camelis/@lastUpdate, 'T'), '.'))
	return (
		<h3 class="name">{concat($appliName, ' - ', $serviceName)}</h3>
		,
		<ul class="list">
		<li><em>Creator</em>{concat(' : ',$camelis/@creator)}</li>
		<li><em>Activation date</em>{concat(' : ',$activationDate)}</li>
		<li><em>Activation time</em>{concat(' : ',$activationTime)}</li>
		<li><em>Last update</em>{concat(' : ',$camelis/@lastUpdate)}</li>
		<li><em>Host</em>{concat(' : ',$camelis/@host)}</li>
		<li><em>Port</em>{concat(' : ',$camelis/@port)}</li>
		<li><em>Nb objects</em>{concat(' : ',$camelis/@nbObject)}</li>
		</ul>)
		}
		
		
		</section>
};

declare function local:getActifSiteChooser() as  element()* {
		<section> 
		{
	for $sparqlEndPoints in $root/activeSite/sparqlEndPoints
	return(
		for $sparqlEndPoint in $sparqlEndPoints/sparqlEndPoint
		return(
		<h3 class="name">
		{concat('sparqlEndPoint - ',$sparqlEndPoint/@serviceName)}
		</h3>,
		concat(' (', $sparqlEndPoint/@url, ')'),
		<ol>
		{for $query in $sparqlEndPoint/query
		return
		<form action="query.jsp" method="POST">
		<li>
		<input type="submit" value="Go"/>{concat($query/@label, '')}
		<input type="hidden" name="serviceName" value="{$sparqlEndPoint/@serviceName}"/>
		<input type="hidden" name="url" value="{$sparqlEndPoint/@url}"/>
		<input type="hidden" name="query" value="{$query/node()/text()}"/>		
		<input type="hidden" name="label" value="{$query/@label}"/>		
		</li>
		</form>
		}
		</ol>)
	),
	for $camelis in $root/activeSite/camelis
	let $fullName := substring-after($camelis/@serviceId, '::')
	let $appliName := substring-before($fullName, ':')
	let $serviceName := substring-after($fullName, ':')
	let $activationDate := substring-before($camelis/@activationDate, 'T')
	let $activationTime := substring-before(substring-after($camelis/@activationDate, 'T'), '.')
	let $lastDate := if ($camelis/@lastUpdate='none') then 'none' else concat(substring-before($camelis/@lastUpdate, 'T'), ' at ',
	 substring-before(substring-after($camelis/@lastUpdate, 'T'), '.'))
	return 
	<article>
		<h3 class="name">{concat($appliName, ' - ', $serviceName)}</h3>
		<ul class="list">
		<li><em>Creator</em>{concat(' : ',$camelis/@creator)}</li>
		<li><em>Activation date</em>{concat(' : ',$activationDate)}</li>
		<li><em>Activation time</em>{concat(' : ',$activationTime)}</li>
		<li><em>Last update</em>{concat(' : ',$camelis/@lastUpdate)}</li>
		<li><em>Host</em>{concat(' : ',$camelis/@host)}</li>
		<li><em>Port</em>{concat(' : ',$camelis/@port)}</li>
		<li><em>Nb objects</em>{concat(' : ',$camelis/@nbObject)}</li>
		<li>
			<form action="setService">
				<input type="submit" value="Go"/>
				<input type="hidden" name="serviceId" value="{
					concat($camelis/@serviceId,'')}"/>
			</form>
		</li>
		</ul>
		</article>
		}
		
		
		</section>
};

()
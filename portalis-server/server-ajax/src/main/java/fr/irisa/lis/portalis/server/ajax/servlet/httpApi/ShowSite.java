package fr.irisa.lis.portalis.server.ajax.servlet.httpApi;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Element;

import fr.irisa.lis.portalis.commons.XqueryUtil;
import fr.irisa.lis.portalis.server.ajax.Constant;
import fr.irisa.lis.portalis.shared.admin.ClientConstants;
import fr.irisa.lis.portalis.shared.admin.data.ActiveSite;
import fr.irisa.lis.portalis.shared.admin.data.ActiveUser;
import fr.irisa.lis.portalis.shared.admin.http.AdminHttp;
import fr.irisa.lis.portalis.shared.admin.reponse.ActiveSiteReponse;
import fr.irisa.lis.portalis.shared.admin.reponse.LoginReponse;

@SuppressWarnings("serial")
public class ShowSite extends HttpServlet {

	private static final Logger LOGGER = LoggerFactory.getLogger(ShowSite.class
			.getName());

	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		try {
			request.setCharacterEncoding("UTF-8");
			response.setCharacterEncoding("UTF-8");
			
			
			String site = request.getParameter("site");
			if (site==null) {
				String mess = "Pas de paramêtre 'site'";
				LOGGER.error(mess);
				response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, mess);
				return;
			}
			if (! (site.equals("actif") || site.equals("static") || site.equals("actifChoose"))) {
				String mess = "Paramêtre 'site' incorrect : "+site;
				LOGGER.error(mess);
				response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, mess);
				return;
			}
			
			LoginReponse loginReponse = AdminHttp.anonymousLogin();
			if (!loginReponse.isOk()) {
				String mess = "la réponse de getActifSite est en erreur "
						+ loginReponse.getMessagesAsString();
				LOGGER.error(mess);
				response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, mess);
				return;
			}
			ActiveUser activeUser = loginReponse.getActiveUser();
			
			ActiveSiteReponse siteRep = AdminHttp.getActiveSite(activeUser);
			
			LOGGER.info("siteRep = " + siteRep.toString());
			if (!siteRep.isOk()) {
				String mess = "la réponse de getActifSite est en erreur "
						+ siteRep.getMessagesAsString();
				LOGGER.error(mess);
				response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, mess);
				return;
			}
			ActiveSite activeSite = siteRep.getSite();
			if (activeSite == null) {
				String mess = "la réponse de getActifSite ne contient pas de siteActif";
				LOGGER.error(mess);
				response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, mess);
				return;
			}
			Element elem = ClientConstants.PORTALIS_DATA_XML_WRITER.visit(activeSite);
			LOGGER.debug("activeSite = "+activeSite);
			
			response.setHeader("Expires", "0");
			response.setContentType(Constant.CONTENT_TYPE_HTML);
			
			PrintWriter out = response.getWriter();
			if (site.equals("static")) {
				out.println(new XqueryUtil().toStringEval(elem.getOwnerDocument(),
					"ficheSite.xqy", "getSite"));
			} else if (site.equals("actif")) {
				out.println(new XqueryUtil().toStringEval(elem.getOwnerDocument(),
					"ficheSite.xqy", "getActifSite"));				
			} else if (site.equals("actifChoose")) {
				out.println(new XqueryUtil().toStringEval(elem.getOwnerDocument(),
					"ficheSite.xqy", "getActifSiteChooser"));				
			}

		} catch (Throwable t) {
			response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, t.getMessage());
		}
	}
}

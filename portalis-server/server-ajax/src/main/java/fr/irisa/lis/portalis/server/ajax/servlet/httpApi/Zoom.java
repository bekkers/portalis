package fr.irisa.lis.portalis.server.ajax.servlet.httpApi;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Set;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.irisa.lis.portalis.server.ajax.Constant;
import fr.irisa.lis.portalis.server.ajax.UserContext;
import fr.irisa.lis.portalis.shared.admin.data.ActiveService;
import fr.irisa.lis.portalis.shared.admin.http.AdminHttp;
import fr.irisa.lis.portalis.shared.admin.reponse.ServiceCoreReponse;
import fr.irisa.lis.portalis.shared.camelis.data.LisIncrement;
import fr.irisa.lis.portalis.shared.camelis.data.LisIncrementSet;
import fr.irisa.lis.portalis.shared.camelis.http.CamelisHttp;
import fr.irisa.lis.portalis.shared.camelis.reponse.ZoomReponse;

@SuppressWarnings("serial")
public class Zoom extends HttpServlet {

	private static final Logger LOGGER = LoggerFactory.getLogger(Zoom.class
			.getName());

	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws IOException {
		try {

			request.setCharacterEncoding("UTF-8");
			response.setCharacterEncoding("UTF-8");

			String callback = request.getParameter("callback");

			HttpSession session = request.getSession(true);
			UserContext userContext = (UserContext) session
					.getAttribute(Constant.USER_CONTEXT);
			if (userContext == null) {
				String mess = "Pas de session utilisateur";
				LOGGER.error(mess);
				response.sendError(
						HttpServletResponse.SC_INTERNAL_SERVER_ERROR, mess);
				return;
			}

			CamelisHttp camelisHttp = userContext.getCamelisHttp();
			if (camelisHttp == null) {
				String mess = "Pas de camelis dans la session utilisateur";
				LOGGER.error(mess);
				response.sendError(
						HttpServletResponse.SC_INTERNAL_SERVER_ERROR, mess);
				return;
			}

			ActiveService activeService = camelisHttp.getActiveService();
			if (activeService == null) {
				String mess = "Pas de serviceActif dans camelis";
				LOGGER.error(mess);
				response.sendError(
						HttpServletResponse.SC_INTERNAL_SERVER_ERROR, mess);
				return;
			}

			ServiceCoreReponse serviceCoreReponse = AdminHttp.getServiceCore(
					camelisHttp.getActiveUser(), activeService.getActiveId());
			if (!serviceCoreReponse.isOk()) {
				String mess = "Impossible d'obtenir le serviceCore : "
						+ serviceCoreReponse.getMessagesAsString();
				LOGGER.error(mess);
				response.sendError(
						HttpServletResponse.SC_INTERNAL_SERVER_ERROR, mess);
				return;
			}
			
			Set<String> hiddenAttributes = serviceCoreReponse.getServiceCore().getUiPreference().getHiddenAttribute();

			String minSupport = request.getParameter("minSupport");
			if (minSupport == null) {
				String mess = "Pas de parametre minSupport";
				LOGGER.error(mess);
				response.sendError(
						HttpServletResponse.SC_INTERNAL_SERVER_ERROR, mess);
				return;
			}
			int minimumSupport = 0;
			try {
				minimumSupport = Integer.parseInt(minSupport);
			} catch (NumberFormatException e) {
				String mess = "Parametre minSupport incorrect : " + minSupport;
				LOGGER.error(mess);
				response.sendError(
						HttpServletResponse.SC_INTERNAL_SERVER_ERROR, mess);
				return;
			}

			String feature = request.getParameter("node");
			String req = request.getParameter("request");
			if (req == null) {
				String mess = "Pas de paramêtre 'request'";
				LOGGER.error(mess);
				response.sendError(
						HttpServletResponse.SC_INTERNAL_SERVER_ERROR, mess);
				return;
			}

			if (feature == null) {
				feature = "all";
			}
			LOGGER.debug(String.format("request = %s, feature = %s", req,
					feature));

			ZoomReponse reponse = camelisHttp.zoom(req, feature, true);
			LOGGER.debug("début zoom " + reponse);

			if (!reponse.isOk()) {
				String mess = reponse.getMessagesAsString();
				LOGGER.error(mess);
				response.sendError(
						HttpServletResponse.SC_INTERNAL_SERVER_ERROR, mess);
				return;
			}

			Integer nbObjects = reponse.getNbObjects();

			LisIncrementSet receivedIncrs = reponse.getIncrements();
			response.setContentType(Constant.CONTENT_TYPE_JSON);
			PrintWriter out = response.getWriter();
			if (callback != null) {
				out.println(callback + "(");
			}
			out.println("[");
			int last = receivedIncrs.card();
			int i = 0;
			for (LisIncrement increment : receivedIncrs.toArray()) {
				if (increment.getCard() > minimumSupport && !contains(hiddenAttributes,increment.getName())) {
					String name = increment.getName();
					out.print("   { \"id\" : \"");
					String param = name.replace("\"", "\\\"");
					out.print(param);
					out.print("\", \"label\" : \"");
					out.print(param);
					out.print(" (");
					out.print(increment.getCard());

					// test s'il y a des descendants de cet incrément
					
					if (!increment.isLeafUnknown() &&  increment.getIsLeaf().equals("true")) {
						out.print(")\"");
					} else {
						out.print(")\", \"load_on_demand\" : \"true\"");
					}
					int card = increment.getCard();
					String color = "black";
					if (card == 0) {
						// useless attribute light light green
						color = "lightgreen";
					} else if (card == nbObjects) {
						// non discriminating attribute, gray
						color = "gray";
					} else if (card <= 0.1 * nbObjects) {
						// very specific attribute, green
						color = "green";
					}
					out.print(", \"color\" : \"");
					out.print(color);
					;
					out.print("\"");

					i++;
					if (i == last) {
						out.println(" }");
					} else {
						out.println(" },");
					}
				}
			}
			out.println("]");
			if (callback != null) {
				out.println(")");
			}

			LOGGER.debug("fin zoom");

		} catch (Throwable t) {
			response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR,
					t.getMessage());
		}

	}

	private boolean contains(Set<String> hiddenAttributes, String name) {
		for (String hiddenAttr : hiddenAttributes) {
			if (name.startsWith(hiddenAttr))
				return true;
		}
		return false;
	}

	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws IOException {
		LOGGER.debug("post");
		doGet(request, response);
	}

}

package fr.irisa.lis.portalis.server.ajax;

public class Constant {
	public static final String CONTENT_TYPE_XML = "text/xml; charset=UTF-8";
	public static final String CONTENT_TYPE_HTML = "text/html; charset=UTF-8";
	public static final String CONTENT_TYPE_JSON = "application/json; charset=UTF-8";
	public static final String USER_CONTEXT = "userContext";
	public static final String WEBAPPS_DIR = "/srv/webapps/";
	public static final String REQUEST_HOME = "all";
	public static final int DEFAULT_PAGE_SIZE = 30;
	public static final String PREFERENCE = "preferences/";
}

package fr.irisa.lis.portalis.server.ajax;

import java.io.InputStream;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.irisa.lis.portalis.shared.admin.Util;

public class LoggerInitialiser implements ServletContextListener {

	@SuppressWarnings("unused")
	private static final Logger LOGGER = LoggerFactory.getLogger(LoggerInitialiser.class
			.getName());

	private static final String propFileName = "/WEB-INF/logging.properties";
	
	

	@Override
	public void contextInitialized(ServletContextEvent event) {
		ServletContext context = event.getServletContext();
		InputStream loggingConfFile = context.getResourceAsStream(propFileName);

		String propName = "org.apache.juli.FileHandler.level";
		Util.initLogPropertiesFile(loggingConfFile, propFileName, propName);
	}



	@Override
	public void contextDestroyed(ServletContextEvent sce) {
		// TODO Auto-generated method stub
		
	}



}
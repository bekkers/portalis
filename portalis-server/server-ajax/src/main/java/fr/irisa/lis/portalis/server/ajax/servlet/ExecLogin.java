package fr.irisa.lis.portalis.server.ajax.servlet;

import java.io.IOException;
import java.net.InetAddress;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.irisa.lis.portalis.server.ajax.Constant;
import fr.irisa.lis.portalis.server.ajax.UserContext;
import fr.irisa.lis.portalis.server.core.PortalisCtx;
import fr.irisa.lis.portalis.shared.admin.PortalisException;
import fr.irisa.lis.portalis.shared.admin.data.ActiveUser;
import fr.irisa.lis.portalis.shared.admin.http.AdminHttp;
import fr.irisa.lis.portalis.shared.admin.reponse.LoginReponse;
import fr.irisa.lis.portalis.shared.camelis.http.CamelisHttp;

@SuppressWarnings("serial")
public class ExecLogin extends HttpServlet {

	private static final Logger LOGGER = LoggerFactory.getLogger(ExecLogin.class
			.getName());


	public void init() {
	
		String hostName = "";
		try {
			hostName = InetAddress.getLocalHost().getCanonicalHostName();
		} catch (java.net.UnknownHostException e) {
			hostName = getInitParameter("portalis.host");
		}
		String port = getInitParameter("portalis.port");
		String portalisAppliName = getInitParameter("portalis.application.name");
		int hostPort = Integer.parseInt(port);
		PortalisCtx.getInstance().init(hostName, hostPort, portalisAppliName);;

	}
	
	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		try {
			HttpSession session = request.getSession();
			if (session == null) {
				String mess = "Pas de session utilisateur";
				LOGGER.error(mess);
				throw new ServletException(mess);

			}


			String connectUser = request.getParameter("connexionType");
			String admin = request.getParameter("admin");
			LoginReponse reponse = null;
			if (connectUser != null && connectUser.equals("userConnexion")) {
				// attention il faut passer par une requête http pour que
				// l'utilisateur aie sa session camelis ...
				// et ses cookies qui la certifient
				String email = request.getParameter("email");
				String password = request.getParameter("password");
				if (email == null || email.length() == 0 || password == null
						|| password.length() == 0) {
					String mess = "Error : email or password parameters are missing";
					LOGGER.error(mess);
					throw new ServletException(mess);
				}
				reponse = AdminHttp.login(email, password);
			} else if (connectUser != null && connectUser.equals("anonymousConnexion")) {
				reponse = AdminHttp.anonymousLogin();
			} else {
				String mess = "Error : login mal paramêtré, manque le type de connexion";
				LOGGER.error(mess);
				throw new ServletException(mess);
			}
			if (!reponse.isOk()) {
				String mess = "Error : " + reponse.getMessagesAsString();
				LOGGER.error(mess);
				throw new PortalisException(mess);
			}

			ActiveUser activeUser = reponse.getActiveUser();
			CamelisHttp camelisHttp = new CamelisHttp(activeUser, null);
			session.setAttribute(Constant.USER_CONTEXT, new UserContext(camelisHttp));

			if (admin == null) {
				String mess = "Error : manque le paramêtre de choix d'édition";
				LOGGER.error(mess);
				throw new ServletException(mess);
			}

			if (admin.equals("site")) {
				response.sendRedirect("editSite.jsp");
			} else if (admin.equals("users")) {
				response.sendRedirect("showUsers.html");
			} else if (admin.equals("camelis")) {
				response.sendRedirect("chooseService.jsp");
			}

		} catch (ServletException e) {
			throw e;
		} catch (IOException e) {
			throw e;
		} catch (Exception e) {
			throw new ServletException(e);
		}
	}

	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doGet(request, response);
	}

}

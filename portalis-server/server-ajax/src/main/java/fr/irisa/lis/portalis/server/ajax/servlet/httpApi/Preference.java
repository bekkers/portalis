package fr.irisa.lis.portalis.server.ajax.servlet.httpApi;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.codehaus.jackson.map.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.irisa.lis.portalis.server.ajax.Constant;
import fr.irisa.lis.portalis.server.ajax.UserContext;
import fr.irisa.lis.portalis.shared.admin.data.ActiveLisService;
import fr.irisa.lis.portalis.shared.admin.data.LisServiceCore;
import fr.irisa.lis.portalis.shared.admin.data.UIPreference;
import fr.irisa.lis.portalis.shared.admin.http.AdminHttp;
import fr.irisa.lis.portalis.shared.admin.reponse.ServiceCoreReponse;
import fr.irisa.lis.portalis.shared.camelis.http.CamelisHttp;

/**
 * 
 * @author bekkers
 * 
 *         Servlet qui revoie les headers correspondant aux préférences.
 * 
 * 
 *         trois modes d'appel :
 * 
 *         un mode push qui permet à l'utilisateur de pousser ses préférenes et
 *         deux modes pull selon que le client utilise les preférences par
 *         défaut ou utilise ses propres préférences
 * 
 *         <ul>
 *         <li>preference?[callback=name&]default=true : pour obtenir les
 *         preferences par défaut et les headers</li>
 *         <li>preference?[callback=name&] : pour obtenir les preferences de
 *         l'utilisateur et les headers</li>
 *         <li>preference?[callback=name&]preferences=... : pour envoyer les
 *         préférences du client et obtenir les headers</li>
 * 
 */

@SuppressWarnings("serial")
public class Preference extends HttpServlet {

	private static final Logger LOGGER = LoggerFactory
			.getLogger(Preference.class.getName());

	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws IOException {
		try {

			request.setCharacterEncoding("UTF-8");
			response.setCharacterEncoding("UTF-8");

			String callback = request.getParameter("callback");

			LOGGER.debug("1");
			HttpSession session = request.getSession(true);
			UserContext userContext = (UserContext) session
					.getAttribute(Constant.USER_CONTEXT);
			if (userContext == null) {
				String mess = "Pas de session utilisateur";
				LOGGER.error(mess);
				response.sendError(
						HttpServletResponse.SC_INTERNAL_SERVER_ERROR, mess);
				return;
			}

			LOGGER.debug("1");
			CamelisHttp camelisHttp = userContext.getCamelisHttp();
			if (camelisHttp == null) {
				String mess = "Pas de camelis dans la session utilisateur";
				LOGGER.error(mess);
				response.sendError(
						HttpServletResponse.SC_INTERNAL_SERVER_ERROR, mess);
				return;
			}

			LOGGER.debug("2");
			ActiveLisService activeLisService = camelisHttp.getActiveService();
			if (activeLisService == null) {
				String mess = "Pas de serviceActif dans camelis";
				LOGGER.error(mess);
				response.sendError(
						HttpServletResponse.SC_INTERNAL_SERVER_ERROR, mess);
				return;
			}

			LOGGER.debug("3");
			PrintWriter out = response.getWriter();
			response.setContentType(Constant.CONTENT_TYPE_JSON);

			if (callback != null) {
				out.println(callback + "(");
			}
			out.println("{");

			UIPreference uiPreferences = null;

			String JsonPreferences = request.getParameter("preferences");
			if (JsonPreferences == null) {
				String defaultParam = request.getParameter("default");
				ServiceCoreReponse serviceCoreReponse = AdminHttp
						.getServiceCore(camelisHttp.getActiveUser(),
								activeLisService.getActiveId());
				if (!serviceCoreReponse.isOk()) {
					String mess = "Impossible d'obtenir le serviceCore : "
							+ serviceCoreReponse.getMessagesAsString();
					LOGGER.error(mess);
					response.sendError(
							HttpServletResponse.SC_INTERNAL_SERVER_ERROR,
							mess);
					return;
				}
				LisServiceCore serviceCore = serviceCoreReponse
						.getServiceCore();
				if (defaultParam != null && defaultParam.equals("true")) {

					LOGGER.debug("41");
					// pour revenir aux preferences par défaut du service et ses headers correspondants
					uiPreferences = serviceCore.getUiPreference();
					session.setAttribute(Constant.PREFERENCE + activeLisService.getActiveId(), uiPreferences);
				} else {
					LOGGER.debug("42");
					// pour obtenir les prefences de l'utilisateur et ses headers correspondants
					uiPreferences = (UIPreference) session.getAttribute(Constant.PREFERENCE + activeLisService.getActiveId());
					if (uiPreferences == null) {
						LOGGER.debug("42bis");
						// c'est la première fois que l'on regarde les préférences de l'utilisateur
						uiPreferences = serviceCore.getUiPreference();
						session.setAttribute(Constant.PREFERENCE + activeLisService.getActiveId(), uiPreferences);
					}
				}
				session.setAttribute(
						Constant.PREFERENCE
								+ activeLisService.getActiveId(),
						uiPreferences);

				out.println("\"uiPreferences\": ");

				ObjectMapper mapper = new ObjectMapper();
				out.println(mapper.writeValueAsString(uiPreferences));
				out.println(",");
			} else {

				LOGGER.debug("43");
				// pour recevoir les préférences du client
				LOGGER.debug("JsonPreferences = " + JsonPreferences);
				uiPreferences = UIPreference.fromJson(JsonPreferences);
				session.setAttribute(
						Constant.PREFERENCE + activeLisService.getActiveId(),
						uiPreferences);
			}
			// génération des headers
			generateHeaders(out, uiPreferences);
			out.println("}");
			if (callback != null) {
				out.println(")");
			}
			return;

		} catch (Throwable t) {
			response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR,
					t.getMessage());
		}

	}

	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws IOException {
		LOGGER.debug("post");
		doGet(request, response);
	}

	private void generateHeaders(PrintWriter out, UIPreference uiPreferences)
			throws IOException {

		List<String> feats = uiPreferences.getCamelisInfo();
		feats.addAll(uiPreferences.getCamelisFeatures());

		out.println("\"headers\": [");
		int max = feats.size() - 1;
		// generating aoColumns
		for (int i = 0; i < feats.size(); i++) {
			String feat = feats.get(i);
			out.println("{\"sTitle\":\"" + feat.replace("\"", "\\\"") + "\"}");
			if (i < max) {
				out.println(",");
			}
		}
		// generating aoColumnDefs
//		for (int i = 0; i < feats.size(); i++) {
//			String feat = feats.get(i);
//			out.println(String.format("{\"sTitle\":\"%s\",\"aTargets\":[%d]}", feat.replace("\"", "\\\""), i));
//			if (i < max) {
//				out.println(",");
//			}
//		}
		out.println("]");

	}

}

declare namespace local = "http://www.irisa.fr/bekkers";
declare variable $root as node() external;

declare function local:getFicheSite() as  element()* {
		<section> 
				<h1 class="header">Services disponibles</h1>
		{
	for $application in $root/activeSite/application
	return
	<article>
		<h3 class="name">{concat('',$application/@applicationId)}</h3>
		<ul class="list">
		{for $service in $application/serviceCore
		return	
		<li>
		<form action="startCamelis.jsp">
		<input type="submit" value="Start"/>{concat(' ', $service/@serviceId, ' (', $service/@role, ')')}
		<input type="hidden" name="serviceName" value="{
		concat($application/@applicationId, ':', $service/@serviceId)}"/>
		
		</form>
		</li>
		}
		</ul>
		</article>
		}
				<h1 class="header">Services actifs</h1>
		{
	for $camelis in $root/activeSite/camelis
	let $fullName := substring-after($camelis/@serviceId, '::')
	let $appliName := substring-before($fullName, ':')
	let $serviceName := substring-after($fullName, ':')
	let $activationDate := substring-before($camelis/@activationDate, 'T')
	let $activationTime := substring-before(substring-after($camelis/@activationDate, 'T'), '.')
	let $lastDate := if ($camelis/@lastUpdate='none') then 'none' else concat(substring-before($camelis/@lastUpdate, 'T'), ' at ',
	 substring-before(substring-after($camelis/@lastUpdate, 'T'), '.'))
	return 
	<article>
		<form action="stopCamelis.jsp">
		<h3 class="name">{concat($appliName, ' - ', $serviceName)}</h3>
		<ul class="list">
		<li><em>Creator</em>{concat(' : ',$camelis/@creator)}</li>
		<li><em>Activation date</em>{concat(' : ',$activationDate)}</li>
		<li><em>Activation time</em>{concat(' : ',$activationTime)}</li>
		<li><em>Last update</em>{concat(' : ',$camelis/@lastUpdate)}</li>
		<li><em>Host</em>{concat(' : ',$camelis/@host)}</li>
		<li><em>Port</em>{concat(' : ',$camelis/@port)}</li>
		<li><em>Nb objects</em>{concat(' : ',$camelis/@nbObject)}</li>
		<li><input type="submit" value="Stop"/></li>
		</ul>
		<input type="hidden" name="serviceId" value="{
		concat($camelis/@serviceId,'')}"/>
		</form>
		</article>
		}
		
	<div id="divLogout">
		<form action="showUsers.jsp" method="post">
			<fieldset id="formLogin">
				<input type="submit" name="connectUser" value="Edit Users" />
			</fieldset>
		</form>
		<form action="clientLogout.jsp" method="post">
			<fieldset id="formLogin">
				<input type="submit" name="connectUser" value="Logout" />
			</fieldset>
		</form>
	</div>
		
		</section>
};

()
<?xml version="1.0" encoding="UTF-8" ?>
<%@ page language="java" contentType="text/xml; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%@ page import="org.slf4j.Logger"%>
<%@ page import="org.slf4j.LoggerFactory"%>
<%@ page import="java.util.logging.LogManager"%>

<%@ page import="fr.irisa.lis.portalis.shared.admin.Util"%>
<%@ page import="fr.irisa.lis.portalis.shared.admin.Err"%>
<%@ page import="fr.irisa.lis.portalis.shared.admin.XmlIdentifier"%>
<%@ page import="fr.irisa.lis.portalis.server.Config"%>
<%@ page import="fr.irisa.lis.portalis.server.core.PortalisCtx"%>
<%@ page import="fr.irisa.lis.portalis.shared.admin.PortalisException"%>
<%@ page
	import="fr.irisa.lis.portalis.shared.admin.data.PortalisService"%>

<%@ page import="fr.irisa.lis.portalis.server.core.AdminCmd"%>
<%@ page import="fr.irisa.lis.portalis.shared.admin.AdminProprietes"%>

<%@ page
	import="fr.irisa.lis.portalis.shared.admin.reponse.LoginReponse"%>

<%@ page import="java.io.File"%>
<%@ page import="java.io.InputStream"%>
<%@ page import="java.net.InetAddress"%>


<%!private static final String jspName = "login.jsp";
private static final Logger LOGGER = LoggerFactory
.getLogger("fr.irisa.lis.portalis.admin." + jspName);

	public void jspInit() {
		
		String hostName = "";
		try {
			hostName = InetAddress.getLocalHost().getCanonicalHostName();
		} catch (java.net.UnknownHostException e) {
			hostName = getInitParameter("portalis.host");
		}
		String port = getInitParameter("portalis.port");
		String portalisAppliName = getServletContext(). getContextPath().substring(1);
		int hostPort = Integer.parseInt(port);
		PortalisCtx.getInstance().init(hostName, hostPort, portalisAppliName);;

	}%>

<%
	response.setContentType(Config.CONTENT_TYPE_XML);
	response.setHeader("Expires", "0");

	LOGGER.info("\nPortalisCtx ====== " + jspName + " ====== "
	+ session.getId());

	LoginReponse reponse = null;
	try {

			String email = request.getParameter(XmlIdentifier.EMAIL());
			reponse = AdminCmd.login(email,
			request.getParameter(XmlIdentifier.PASSWORD()),
				session, request.getParameter(XmlIdentifier.MAX_INACTIVE_INTERVAL()));

	} catch (Throwable t) {
		String mess = "Error "+jspName;
		LOGGER.error(mess, t);
		reponse = new LoginReponse(new Err(mess, t));
	}
%>

<%=reponse.toString()%>

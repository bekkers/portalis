<?xml version="1.0" encoding="UTF-8" ?>
<%@ page language="java" contentType="text/xml; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%@ page import="org.slf4j.Logger"%>
<%@ page import="org.slf4j.LoggerFactory"%>
<%@ page import="java.util.logging.LogManager"%>

<%@ page import="fr.irisa.lis.portalis.shared.admin.Err"%>
<%@ page import="fr.irisa.lis.portalis.shared.admin.XmlIdentifier"%>
<%@ page import="fr.irisa.lis.portalis.server.Config"%>
<%@ page import="fr.irisa.lis.portalis.server.core.PortalisCtx"%>
<%@ page import="fr.irisa.lis.portalis.shared.admin.data.ActiveUser"%>
<%@ page import="fr.irisa.lis.portalis.shared.admin.PortalisException"%>

<%@ page import="fr.irisa.lis.portalis.shared.admin.reponse.VoidReponse"%>

<%!private static final String jspName = "logout.jsp";
	private static final Logger LOGGER = LoggerFactory
			.getLogger("fr.irisa.lis.portalis.admin." + jspName);
	private PortalisCtx camelisCtx = PortalisCtx.getInstance();%>

<%
	response.setContentType(Config.CONTENT_TYPE_XML);
	response.setHeader("Expires", "0");

	LOGGER.debug("\nPortalisCtx ====== " + jspName + " ====== "
			+ session.getId());

	VoidReponse reponse = null;
	try {
		String camelisSessionId = (String) session
				.getAttribute(XmlIdentifier.CAMELIS_SESSION_ID);
		LOGGER.debug("CamelisSessionId = " + camelisSessionId);

		if (camelisSessionId == null) {
			String mess = jspName + " : Sorry no user id logged";
			LOGGER.warn(mess);
			reponse = new VoidReponse(new Err(mess));
		} else {
			ActiveUser activeUser = PortalisCtx.getInstance().getUser(
					camelisSessionId);
			if (activeUser == null) {
				String message = "impossible d'effectuer le logout, la session "
						+ camelisSessionId + " n'est pas connue";
				LOGGER.error(message);
				reponse = new VoidReponse(new Err(message));
			} else {
				String email = camelisCtx.getUser(camelisSessionId)
						.getUserCore().getEmail();
				camelisCtx.userLogout(camelisSessionId);
				LOGGER.debug(new StringBuffer(jspName).append("(")
						.append(camelisSessionId).append(")")
						.append(" email = ").append(email).toString());
				reponse = new VoidReponse();
			}
		}
		// 	} catch (PortalisException e) {
		// 		String mess = "Error " + jspName;
		// 		LOGGER.warn(mess, e);
		// 		reponse = new VoidReponse(new Err(mess, e.getMessage()));
	} catch (Throwable t) {
		String mess = "Error " + jspName;
		LOGGER.error(mess, t);
		reponse = new VoidReponse(new Err(mess, t));
	}
%>

<%=reponse%>

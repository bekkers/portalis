package fr.irisa.lis.portalis.server;

import java.io.InputStream;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Element;

import fr.irisa.lis.portalis.admin.db.DataBaseSimpleImpl;
import fr.irisa.lis.portalis.commons.DOMUtil;
import fr.irisa.lis.portalis.commons.XqueryUtil;
import fr.irisa.lis.portalis.server.core.AdminCmd;
import fr.irisa.lis.portalis.server.core.PortalisCtx;
import fr.irisa.lis.portalis.service.patrimoine.arles.db.PatrimoineArlesDataBase;
import fr.irisa.lis.portalis.shared.admin.ClientConstants;
import fr.irisa.lis.portalis.shared.admin.Util;
import fr.irisa.lis.portalis.shared.admin.data.ActiveSite;
import fr.irisa.lis.portalis.shared.admin.data.Site;
import fr.irisa.lis.portalis.shared.camelis.reponse.PingReponse;

public class Config implements ServletContextListener {

	@SuppressWarnings("unused")
	private static final Logger LOGGER = LoggerFactory.getLogger(Config.class
			.getName());

	private static final String propFileName = "/WEB-INF/logging.properties";
	
	public static final String CONTENT_TYPE_HTML = "text/html; charset=UTF-8";
	public static final String CONTENT_TYPE_XML = "text/xml; charset=UTF-8";
	
	private ScheduledExecutorService scheduler;

	@Override
	public void contextInitialized(ServletContextEvent event) {
		scheduler = Executors.newSingleThreadScheduledExecutor();
		scheduler.scheduleAtFixedRate(new ServerCheckerCmd(), 0, 1,
				TimeUnit.MINUTES);
		ServletContext context = event.getServletContext();
		InputStream loggingConfFile = context.getResourceAsStream(propFileName);

		String propName = "org.apache.juli.FileHandler.level";
		Util.initLogPropertiesFile(loggingConfFile, propFileName, propName);
	}

	@Override
	public void contextDestroyed(ServletContextEvent event) {
		scheduler.shutdownNow();
	}

	public static void main(String[] args) throws Exception {
		testFicheVille((short) 14);
	}
	
	public static void testFicheVille() throws Exception {
		PatrimoineArlesDataBase patrimoineArlesDataBase = new PatrimoineArlesDataBase();
		System.out.print(patrimoineArlesDataBase.getHtmlFicheVille());
	}

	public static void testFicheVille(short id) throws Exception {
		PatrimoineArlesDataBase patrimoineArlesDataBase = new PatrimoineArlesDataBase();
		System.out.print(patrimoineArlesDataBase.getHtmlFicheVille(id));
	}

	public static void testSite() throws Exception {
		PingReponse pingReponse = AdminCmd.getActiveLisServicesOnPortalis();
		System.out.println(pingReponse);
		
		ActiveSite activeSite = new ActiveSite(PortalisCtx.getInstance()
				.getDataBase(),
				pingReponse.getLesServices());
		Element elem = ClientConstants.PORTALIS_DATA_XML_WRITER
				.visit(activeSite);
		System.out.println(DOMUtil.prettyXmlString(elem));
		XqueryUtil xqueryUtil = new XqueryUtil();
		System.out.println(xqueryUtil.toStringEval(elem.getOwnerDocument(),
				"ficheEditSite.xqy", "getFicheSite"));
	}
	
	public static void testSite2() throws Exception {
		Site site = DataBaseSimpleImpl.getInstance();
		Element elem = ClientConstants.PORTALIS_DATA_XML_WRITER
				.visit(site);
		System.out.println(DOMUtil.prettyXmlString(elem));
		XqueryUtil xqueryUtil = new XqueryUtil();
		System.out.println(xqueryUtil.toStringEval(elem.getOwnerDocument(),
				"ficheUsers.xqy", "getFicheSite"));
	}

}
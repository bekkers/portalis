package fr.irisa.lis.portalis.taxonomie.insee.communes;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import fr.irisa.lis.portalis.taxonomy.insee.communes.CommunesInfo;
import fr.irisa.lis.portalis.taxonomy.insee.communes.circonscription.Circonscription;
import fr.irisa.lis.portalis.taxonomy.insee.communes.circonscription.CirconscriptionEuropeenne;
import fr.irisa.lis.portalis.taxonomy.insee.communes.circonscription.CirconscriptionsEuropeennes;
import fr.irisa.lis.portalis.taxonomy.insee.communes.circonscription.Commune;
import fr.irisa.lis.portalis.taxonomy.insee.communes.circonscription.Departement;
import fr.irisa.lis.portalis.taxonomy.insee.communes.circonscription.HierachieEurope;
import fr.irisa.lis.portalis.taxonomy.insee.communes.circonscription.Region;
import fr.irisa.lis.portalis.taxonomy.insee.communes.cog.COG;
import fr.irisa.lis.portalis.taxonomy.insee.communes.cog.Communes;

public class TestCommunes {

	@Rule
	public TemporaryFolder testFolder = new TemporaryFolder();

	@Test
	public void testCommunes_csvToDocument() {
		try {
			FileInputStream inputStream = new FileInputStream(CommunesInfo.cogCsvFileName);
			COG cog = new COG(inputStream);
			Element root = cog.getDocument().getDocumentElement();

			System.out
					.println("nbCommunes " + root.getChildNodes().getLength());
			int nbCommunes = 36701;
			assertEquals(nbCommunes, root.getChildNodes().getLength());

		} catch (Exception e) {
			fail("Exception rencontrée lors du test : " + e.getMessage());
		}
	}

	@Test
	public void testCommunes_loadFromCsv() {
		try {
			FileInputStream inputStream = new FileInputStream(CommunesInfo.cogCsvFileName);
			COG cog = new COG(inputStream);
			Communes communes = cog.getBean().getBeanValue();

			int tailleListe = 36700;
			assertEquals(tailleListe, communes.getCommuneCount());

		} catch (Exception e) {
			fail("Exception rencontrée lors du test : " + e.getMessage());
		}
	}

	@Test
	public void testCommunes_writeRead() {
		try {
			FileInputStream inputStream = new FileInputStream(CommunesInfo.cogCsvFileName);
			COG cog = new COG(inputStream);
			Communes communes = cog.getBean().getBeanValue();

			int tailleListe = 36700;
			assertEquals(tailleListe, communes.getCommuneCount());

			String fileName = "file.xml";
			cog.getBean().storeBeanToFile(testFolder.newFile(fileName));

			cog.getBean().loadBeanFromFile(testFolder.newFile(fileName));
			assertTrue("taille de list non égale à " + tailleListe,
					communes.getCommuneCount() == tailleListe);

		} catch (Exception e) {
			fail("Exception rencontrée lors du test : " + e.getMessage());
		}
	}

	@Test
	public void testEuroCirco_csvToDocument() {
		try {
			File file = new File(CommunesInfo.circonscriptionGpsCsvFileName);
			assertTrue(file.exists());
			FileInputStream inputStream = new FileInputStream(CommunesInfo.circonscriptionGpsCsvFileName);
			assertTrue(inputStream.available()>0);

			HierachieEurope hierachieEurope = new HierachieEurope(inputStream);
			Document doc = hierachieEurope.getDocument();

			int tailleListe = 8;
			Element root = doc.getDocumentElement();
			assertEquals(tailleListe, root.getChildNodes().getLength());

		} catch (Exception e) {
			fail("Exception rencontrée lors du test : " + e.getMessage());
		}
	}

	@Test
	public void testEuroCirco_loadFromCsv() {
		try {
			FileInputStream inputStream = new FileInputStream(CommunesInfo.circonscriptionGpsCsvFileName);
			HierachieEurope hierachieEurope = new HierachieEurope(inputStream);
			CirconscriptionsEuropeennes circonscriptionsEuropeennes = hierachieEurope.getBean()
					.getBeanValue();

			int tailleListe = 8;
			assertEquals(tailleListe, circonscriptionsEuropeennes.getCirconscriptionEuropeenne().length);

		} catch (Exception e) {
			fail("Exception rencontrée lors du test : " + e.getMessage());
		}
	}

	@Test
	public void testEuroCirco_writeRead() {
		try {
			FileInputStream inputStream = new FileInputStream(CommunesInfo.circonscriptionGpsCsvFileName);
			HierachieEurope hierachieEurope = new HierachieEurope(inputStream);
			CirconscriptionsEuropeennes circonscriptionsEuropeennes = hierachieEurope.getBean()
					.getBeanValue();

			int tailleListe = 8;
			assertTrue(
					"taille de liste non égale à " + tailleListe,
					circonscriptionsEuropeennes.getCirconscriptionEuropeenne().length == tailleListe);
			checkFirstCommune(circonscriptionsEuropeennes);

			String fileName = "file.xml";
			hierachieEurope.getBean().storeBeanToFile(testFolder.newFile(fileName));

			hierachieEurope.getBean().loadBeanFromFile(testFolder.newFile(fileName));
			assertEquals(tailleListe, circonscriptionsEuropeennes.getCirconscriptionEuropeenne().length);

		} catch (Exception e) {
			fail("Exception rencontrée lors du test : " + e.getMessage());
		}
	}

	private void checkFirstCommune(
			CirconscriptionsEuropeennes circonscriptionsEuropeennes) {
		CirconscriptionEuropeenne circons = circonscriptionsEuropeennes.getCirconscriptionEuropeenne()[0];
		assertEquals("Sud-Est", circons.getName());
		Region region = circons.getRegion(0);
		assertEquals("Rhône-Alpes", region.getNom());
		assertEquals(82, region.getCode());
		assertEquals("Lyon", region.getChefLieu());
		Departement departement = region.getDepartement(0);
		assertEquals("Ain", departement.getNom());
		assertEquals("01", departement.getNumero());
		assertEquals("Bourg-en-Bresse", departement.getPrefecture());
		Circonscription circonsDept = departement.getCirconscription(0);
		assertEquals(1, circonsDept.getNumero());
		Commune commune = circonsDept.getCommune(0);
		assertEquals("Attignat", commune.getNom());
		assertEquals("Ain-1", commune.getCirconscription());
		assertEquals(1024, commune.getCodeInsee());
		assertEquals("46.283333", commune.getLatitude());
		assertEquals("5.166667", commune.getLongitude());
		assertEquals("1.21", commune.getEloignement());
		assertEquals("01340", commune.getCodesPostaux());
	}
	
	@Test
	public void testCommunesInfo() {
		try {
			CommunesInfo communesInfo = new CommunesInfo();
			HierachieEurope hierachieEurope = communesInfo.getHierachieEurope();
			
			CirconscriptionsEuropeennes circonscriptionsEuropeennes = hierachieEurope.getBean()
					.getBeanValue();

			int tailleListe = 8;
			assertTrue(
					"taille de liste non égale à " + tailleListe,
					circonscriptionsEuropeennes.getCirconscriptionEuropeenne().length == tailleListe);
			checkFirstCommune(circonscriptionsEuropeennes);


		} catch (Exception e) {
			fail("Exception rencontrée lors du test : " + e.getMessage());
		}
	}


}

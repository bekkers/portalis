package fr.irisa.lis.portalis.taxonomy.insee.communes.iris;

import java.io.FileInputStream;
import java.io.InputStream;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import fr.irisa.lis.portalis.commons.DOMUtil;
import fr.irisa.lis.portalis.commons.XmlBeanDAO;
import fr.irisa.lis.portalis.commons.core.CommonsUtil;
import fr.irisa.lis.portalis.commons.csv.File2DocumentCsvParser;
import fr.irisa.lis.portalis.taxonomy.insee.communes.CommunesInfo;
import fr.irisa.lis.portalis.taxonomy.insee.communes.cog.Communes;

public class Iris  extends File2DocumentCsvParser {
	

	private static final String rowName = "commune";
	private static final String cvsSplitBy = ",";
	private static final String encoding = CommonsUtil.DEFAULT_ENCODING;
	private static final int headersPos = 5;
	private static final int longHeadersPos = 4;

	XmlBeanDAO<Communes> bean = new XmlBeanDAO<Communes>(new Communes());

	public Iris(FileInputStream inputStream) throws Exception {
		super(inputStream, rowName, cvsSplitBy, headersPos, longHeadersPos, encoding);
	}
	
	public static void main(String[] args) throws Exception {
		FileInputStream inputStream = new FileInputStream(CommunesInfo.irisCsvFileName);
		Iris cog = new Iris(inputStream);
		Document doc = cog.parse();

//		System.out
//				.println("nbCommunes " + root.getChildNodes().getLength());
		String fileName = "iris.xml";
		DOMUtil.prettyPrintXml(doc, fileName);
		
	}
}

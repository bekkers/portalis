/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.3.1</a>, using an XML
 * Schema.
 * $Id$
 */

package fr.irisa.lis.portalis.taxonomy.insee.communes.circonscription;

import javax.xml.bind.annotation.XmlAttribute;

/**
 * Class CommuneType.
 * 
 * @version $Revision$ $Date$
 */
@SuppressWarnings("serial")
public class CommuneType implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * internal content storage
     */
    private java.lang.String _content = "";

    /**
     * Field _circonscription.
     */
	@XmlAttribute
    private java.lang.String _circonscription;

    /**
     * Field _codeInsee.
     */
	@XmlAttribute
    private int _codeInsee;

    /**
     * keeps track of state for field: _codeInsee
     */
    private boolean _has_codeInsee;

    /**
     * Field _codesPostaux.
     */
	@XmlAttribute
    private java.lang.String _codesPostaux;

    /**
     * Field _eloignement.
     */
	@XmlAttribute
    private java.lang.String _eloignement;

    /**
     * Field _latitude.
     */
	@XmlAttribute
    private java.lang.String _latitude;

    /**
     * Field _longitude.
     */
	@XmlAttribute
    private java.lang.String _longitude;

    /**
     * Field _nom.
     */
	@XmlAttribute
    private java.lang.String _nom;


      //----------------/
     //- Constructors -/
    //----------------/

    public CommuneType() {
        super();
        setContent("");
    }

    public CommuneType(final java.lang.String defaultValue) {
        try {
            setContent( new java.lang.String(defaultValue));
         } catch(Exception e) {
            throw new RuntimeException("Unable to cast default value for simple content!");
         } 
    }


      //-----------/
     //- Methods -/
    //-----------/

    /**
     */
    public void deleteCodeInsee(
    ) {
        this._has_codeInsee= false;
    }

    /**
     * Returns the value of field 'circonscription'.
     * 
     * @return the value of field 'Circonscription'.
     */
    public java.lang.String getCirconscription(
    ) {
        return this._circonscription;
    }

    /**
     * Returns the value of field 'codeInsee'.
     * 
     * @return the value of field 'CodeInsee'.
     */
    public int getCodeInsee(
    ) {
        return this._codeInsee;
    }

    /**
     * Returns the value of field 'codesPostaux'.
     * 
     * @return the value of field 'CodesPostaux'.
     */
    public java.lang.String getCodesPostaux(
    ) {
        return this._codesPostaux;
    }

    /**
     * Returns the value of field 'content'. The field 'content'
     * has the following description: internal content storage
     * 
     * @return the value of field 'Content'.
     */
    public java.lang.String getContent(
    ) {
        return this._content;
    }

    /**
     * Returns the value of field 'eloignement'.
     * 
     * @return the value of field 'Eloignement'.
     */
    public java.lang.String getEloignement(
    ) {
        return this._eloignement;
    }

    /**
     * Returns the value of field 'latitude'.
     * 
     * @return the value of field 'Latitude'.
     */
    public java.lang.String getLatitude(
    ) {
        return this._latitude;
    }

    /**
     * Returns the value of field 'longitude'.
     * 
     * @return the value of field 'Longitude'.
     */
    public java.lang.String getLongitude(
    ) {
        return this._longitude;
    }

    /**
     * Returns the value of field 'nom'.
     * 
     * @return the value of field 'Nom'.
     */
    public java.lang.String getNom(
    ) {
        return this._nom;
    }

    /**
     * Method hasCodeInsee.
     * 
     * @return true if at least one CodeInsee has been added
     */
    public boolean hasCodeInsee(
    ) {
        return this._has_codeInsee;
    }

    /**
     * Sets the value of field 'circonscription'.
     * 
     * @param circonscription the value of field 'circonscription'.
     */
    public void setCirconscription(
            final java.lang.String circonscription) {
        this._circonscription = circonscription;
    }

    /**
     * Sets the value of field 'codeInsee'.
     * 
     * @param codeInsee the value of field 'codeInsee'.
     */
    public void setCodeInsee(
            final int codeInsee) {
        this._codeInsee = codeInsee;
        this._has_codeInsee = true;
    }

    /**
     * Sets the value of field 'codesPostaux'.
     * 
     * @param codesPostaux the value of field 'codesPostaux'.
     */
    public void setCodesPostaux(
            final java.lang.String codesPostaux) {
        this._codesPostaux = codesPostaux;
    }

    /**
     * Sets the value of field 'content'. The field 'content' has
     * the following description: internal content storage
     * 
     * @param content the value of field 'content'.
     */
    public void setContent(
            final java.lang.String content) {
        this._content = content;
    }

    /**
     * Sets the value of field 'eloignement'.
     * 
     * @param eloignement the value of field 'eloignement'.
     */
    public void setEloignement(
            final java.lang.String eloignement) {
        this._eloignement = eloignement;
    }

    /**
     * Sets the value of field 'latitude'.
     * 
     * @param latitude the value of field 'latitude'.
     */
    public void setLatitude(
            final java.lang.String latitude) {
        this._latitude = latitude;
    }

    /**
     * Sets the value of field 'longitude'.
     * 
     * @param longitude the value of field 'longitude'.
     */
    public void setLongitude(
            final java.lang.String longitude) {
        this._longitude = longitude;
    }

    /**
     * Sets the value of field 'nom'.
     * 
     * @param nom the value of field 'nom'.
     */
    public void setNom(
            final java.lang.String nom) {
        this._nom = nom;
    }

}

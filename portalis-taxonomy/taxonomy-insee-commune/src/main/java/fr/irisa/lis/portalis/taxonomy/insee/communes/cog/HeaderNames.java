package fr.irisa.lis.portalis.taxonomy.insee.communes.cog;
/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.3.1</a>, using an XML
 * Schema.
 * $Id$
 */

/**
 * Class HeaderNames.
 * 
 * @version $Revision$ $Date$
 */
@SuppressWarnings("serial")
public class HeaderNames extends CommuneType 
implements java.io.Serializable
{


      //----------------/
     //- Constructors -/
    //----------------/

    public HeaderNames() {
        super();
    }

}

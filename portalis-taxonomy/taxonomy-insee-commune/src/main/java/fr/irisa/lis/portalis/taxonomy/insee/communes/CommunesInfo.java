package fr.irisa.lis.portalis.taxonomy.insee.communes;

import java.io.File;
import java.io.FileInputStream;

import javax.xml.bind.JAXBException;

import org.w3c.dom.Document;

import fr.irisa.lis.portalis.commons.XmlBeanDAO;
import fr.irisa.lis.portalis.taxonomy.insee.communes.circonscription.CirconscriptionsEuropeennes;
import fr.irisa.lis.portalis.taxonomy.insee.communes.circonscription.HierachieEurope;
import fr.irisa.lis.portalis.taxonomy.insee.communes.cog.COG;
import fr.irisa.lis.portalis.taxonomy.insee.communes.cog.Communes;

public class CommunesInfo {
	
	private static final String WEBAPPS_DIR = "/srv/webapps";
	private static final String VILLES_DIR = WEBAPPS_DIR + "/taxonomies/villes";

	public final static String circonscriptionGpsCsvFileName = VILLES_DIR+"/circonscriptionGps/communesGps.csv";
	public final static String circonscriptionGpsXmlFileName = VILLES_DIR+"/circonscriptionGps/communesGps.xml";
	
	public final static String cogCsvFileName = VILLES_DIR+"/COG/table-appartenance-geo-communes-au-01-01-2012.csv";
	public final static String cogXmlFileName = VILLES_DIR+"/COG/table-appartenance-geo-communes-au-01-01-2012.xml";

	public final static String irisCsvFileName = VILLES_DIR+"/IRIS/reference_IRIS_geo2012.csv";
	public final static String irisXmlFileName = VILLES_DIR+"/IRIS/reference_IRIS_geo2012.xml";
	
	private HierachieEurope hierachieEurope;
	private COG cog;

	private CirconscriptionsEuropeennes circonscriptionsEuropeennes;
	private Communes communesGps;

	public CommunesInfo() throws Exception {
		FileInputStream circonscriptionFile = new FileInputStream(circonscriptionGpsCsvFileName);
		hierachieEurope = new HierachieEurope(circonscriptionFile);
		circonscriptionsEuropeennes = hierachieEurope.getBean().getBeanValue();

		FileInputStream communesGpsFile = new FileInputStream(cogCsvFileName);
		cog = new COG(communesGpsFile);
		communesGps = cog.getBean().getBeanValue();
}

	public CirconscriptionsEuropeennes getCirconscriptionsEuropeennes() {
		return circonscriptionsEuropeennes;
	}
	
	public void generateXmlFiles() throws JAXBException {
		hierachieEurope.getBean().storeBeanToFile(new File(circonscriptionGpsXmlFileName));
		cog.getBean().storeBeanToFile(new File(cogXmlFileName));
	}
	
	public Communes getCommunesGps() {
		return communesGps;
	}

	public HierachieEurope getHierachieEurope() {
		return hierachieEurope;
	}

	public COG getCog() {
		return cog;
	}
	
	public static void main(String[] args) throws JAXBException, Exception {
		new CommunesInfo().generateXmlFiles();
	}

}

/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.3.1</a>, using an XML
 * Schema.
 * $Id$
 */

package fr.irisa.lis.portalis.taxonomy.insee.communes.circonscription;

import javax.xml.bind.annotation.XmlAttribute;

/**
 * Class CirconscriptionEuropeenneType.
 * 
 * @version $Revision$ $Date$
 */
@SuppressWarnings("serial")
public class CirconscriptionEuropeenneType implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _name.
     */
	@XmlAttribute
    private java.lang.String _name;

    /**
     * Field _regionList.
     */
    private java.util.Vector<fr.irisa.lis.portalis.taxonomy.insee.communes.circonscription.Region> _regionList;


      //----------------/
     //- Constructors -/
    //----------------/

    public CirconscriptionEuropeenneType() {
        super();
        this._regionList = new java.util.Vector<fr.irisa.lis.portalis.taxonomy.insee.communes.circonscription.Region>();
    }


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * 
     * 
     * @param vRegion
     * @throws java.lang.IndexOutOfBoundsException if the index
     * given is outside the bounds of the collection
     */
    public void addRegion(
            final fr.irisa.lis.portalis.taxonomy.insee.communes.circonscription.Region vRegion)
    throws java.lang.IndexOutOfBoundsException {
        this._regionList.addElement(vRegion);
    }

    /**
     * 
     * 
     * @param index
     * @param vRegion
     * @throws java.lang.IndexOutOfBoundsException if the index
     * given is outside the bounds of the collection
     */
    public void addRegion(
            final int index,
            final fr.irisa.lis.portalis.taxonomy.insee.communes.circonscription.Region vRegion)
    throws java.lang.IndexOutOfBoundsException {
        this._regionList.add(index, vRegion);
    }

    /**
     * Method enumerateRegion.
     * 
     * @return an Enumeration over all
     * fr.irisa.lis.portalis.service.patrimoine.arles.Region element
     */
    public java.util.Enumeration<? extends fr.irisa.lis.portalis.taxonomy.insee.communes.circonscription.Region> enumerateRegion(
    ) {
        return this._regionList.elements();
    }

    /**
     * Returns the value of field 'name'.
     * 
     * @return the value of field 'Name'.
     */
    public java.lang.String getName(
    ) {
        return this._name;
    }

    /**
     * Method getRegion.
     * 
     * @param index
     * @throws java.lang.IndexOutOfBoundsException if the index
     * given is outside the bounds of the collection
     * @return the value of the
     * fr.irisa.lis.portalis.service.patrimoine.arles.Region at the
     * given index
     */
    public fr.irisa.lis.portalis.taxonomy.insee.communes.circonscription.Region getRegion(
            final int index)
    throws java.lang.IndexOutOfBoundsException {
        // check bounds for index
        if (index < 0 || index >= this._regionList.size()) {
            throw new IndexOutOfBoundsException("getRegion: Index value '" + index + "' not in range [0.." + (this._regionList.size() - 1) + "]");
        }

        return (fr.irisa.lis.portalis.taxonomy.insee.communes.circonscription.Region) _regionList.get(index);
    }

    /**
     * Method getRegion.Returns the contents of the collection in
     * an Array.  <p>Note:  Just in case the collection contents
     * are changing in another thread, we pass a 0-length Array of
     * the correct type into the API call.  This way we <i>know</i>
     * that the Array returned is of exactly the correct length.
     * 
     * @return this collection as an Array
     */
    public fr.irisa.lis.portalis.taxonomy.insee.communes.circonscription.Region[] getRegion(
    ) {
        fr.irisa.lis.portalis.taxonomy.insee.communes.circonscription.Region[] array = new fr.irisa.lis.portalis.taxonomy.insee.communes.circonscription.Region[0];
        return (fr.irisa.lis.portalis.taxonomy.insee.communes.circonscription.Region[]) this._regionList.toArray(array);
    }

    /**
     * Method getRegionCount.
     * 
     * @return the size of this collection
     */
    public int getRegionCount(
    ) {
        return this._regionList.size();
    }

    /**
     */
    public void removeAllRegion(
    ) {
        this._regionList.clear();
    }

    /**
     * Method removeRegion.
     * 
     * @param vRegion
     * @return true if the object was removed from the collection.
     */
    public boolean removeRegion(
            final fr.irisa.lis.portalis.taxonomy.insee.communes.circonscription.Region vRegion) {
        boolean removed = _regionList.remove(vRegion);
        return removed;
    }

    /**
     * Method removeRegionAt.
     * 
     * @param index
     * @return the element removed from the collection
     */
    public fr.irisa.lis.portalis.taxonomy.insee.communes.circonscription.Region removeRegionAt(
            final int index) {
        java.lang.Object obj = this._regionList.remove(index);
        return (fr.irisa.lis.portalis.taxonomy.insee.communes.circonscription.Region) obj;
    }

    /**
     * Sets the value of field 'name'.
     * 
     * @param name the value of field 'name'.
     */
    public void setName(
            final java.lang.String name) {
        this._name = name;
    }

    /**
     * 
     * 
     * @param index
     * @param vRegion
     * @throws java.lang.IndexOutOfBoundsException if the index
     * given is outside the bounds of the collection
     */
    public void setRegion(
            final int index,
            final fr.irisa.lis.portalis.taxonomy.insee.communes.circonscription.Region vRegion)
    throws java.lang.IndexOutOfBoundsException {
        // check bounds for index
        if (index < 0 || index >= this._regionList.size()) {
            throw new IndexOutOfBoundsException("setRegion: Index value '" + index + "' not in range [0.." + (this._regionList.size() - 1) + "]");
        }

        this._regionList.set(index, vRegion);
    }

    /**
     * 
     * 
     * @param vRegionArray
     */
    public void setRegion(
            final fr.irisa.lis.portalis.taxonomy.insee.communes.circonscription.Region[] vRegionArray) {
        //-- copy array
        _regionList.clear();

        for (int i = 0; i < vRegionArray.length; i++) {
                this._regionList.add(vRegionArray[i]);
        }
    }

}

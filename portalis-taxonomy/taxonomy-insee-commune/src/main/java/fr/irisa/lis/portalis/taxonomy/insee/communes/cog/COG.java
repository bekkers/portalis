package fr.irisa.lis.portalis.taxonomy.insee.communes.cog;

import java.io.FileInputStream;
import java.io.InputStream;

import org.w3c.dom.Document;

import fr.irisa.lis.portalis.commons.DOMUtil;
import fr.irisa.lis.portalis.commons.XmlBeanDAO;
import fr.irisa.lis.portalis.commons.core.CommonsUtil;
import fr.irisa.lis.portalis.commons.csv.File2DocumentCsvParser;
import fr.irisa.lis.portalis.taxonomy.insee.communes.circonscription.CirconscriptionsEuropeennes;


/**
 * 
 * @author bekkers
 *
 */
public class COG extends File2DocumentCsvParser {
	
	private XmlBeanDAO<Communes> bean = new XmlBeanDAO<Communes>(new Communes());

	private static String rowName = "commune";
	private static String csvSplitBy = ",";
	private static int HEADERS_POS = 5;
	private static int LONG_HEADERS_POS = 4;
	private static String encoding = CommonsUtil.DEFAULT_ENCODING;

	private Document doc;
		
	public COG(FileInputStream inputStream) throws Exception {
		super(inputStream, rowName, csvSplitBy, HEADERS_POS, LONG_HEADERS_POS, encoding);
		doc = parse();
		// compile (unmarshall)
		bean.loadBeanFromDom(doc);
	}
	
	public XmlBeanDAO<Communes> getBean() {
		return bean;
	}

	public Document getDocument() {
		return doc;
	}

		
}

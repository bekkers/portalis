package fr.irisa.lis.portalis.taxonomy.insee.communes.cog;

import javax.xml.bind.annotation.XmlRootElement;
/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.3.1</a>, using an XML
 * Schema.
 * $Id$
 */

/**
 * Class Communes.
 * 
 * @version $Revision$ $Date$
 */
@SuppressWarnings("serial")
@XmlRootElement
public class Communes implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _headerNames.
     */
    private HeaderNames _headerNames;

    /**
     * Field _communeList.
     */
    private java.util.Vector<Commune> _communeList;


      //----------------/
     //- Constructors -/
    //----------------/

    public Communes() {
        super();
        this._communeList = new java.util.Vector<Commune>();
    }


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * 
     * 
     * @param vCommune
     * @throws java.lang.IndexOutOfBoundsException if the index
     * given is outside the bounds of the collection
     */
    public void addCommune(
            final Commune vCommune)
    throws java.lang.IndexOutOfBoundsException {
        this._communeList.addElement(vCommune);
    }

    /**
     * 
     * 
     * @param index
     * @param vCommune
     * @throws java.lang.IndexOutOfBoundsException if the index
     * given is outside the bounds of the collection
     */
    public void addCommune(
            final int index,
            final Commune vCommune)
    throws java.lang.IndexOutOfBoundsException {
        this._communeList.add(index, vCommune);
    }

    /**
     * Method enumerateCommune.
     * 
     * @return an Enumeration over all Commune elements
     */
    public java.util.Enumeration<? extends Commune> enumerateCommune(
    ) {
        return this._communeList.elements();
    }

    /**
     * Method getCommune.
     * 
     * @param index
     * @throws java.lang.IndexOutOfBoundsException if the index
     * given is outside the bounds of the collection
     * @return the value of the Commune at the given index
     */
    public Commune getCommune(
            final int index)
    throws java.lang.IndexOutOfBoundsException {
        // check bounds for index
        if (index < 0 || index >= this._communeList.size()) {
            throw new IndexOutOfBoundsException("getCommune: Index value '" + index + "' not in range [0.." + (this._communeList.size() - 1) + "]");
        }

        return (Commune) _communeList.get(index);
    }

    /**
     * Method getCommune.Returns the contents of the collection in
     * an Array.  <p>Note:  Just in case the collection contents
     * are changing in another thread, we pass a 0-length Array of
     * the correct type into the API call.  This way we <i>know</i>
     * that the Array returned is of exactly the correct length.
     * 
     * @return this collection as an Array
     */
    public Commune[] getCommune(
    ) {
        Commune[] array = new Commune[0];
        return (Commune[]) this._communeList.toArray(array);
    }

    /**
     * Method getCommuneCount.
     * 
     * @return the size of this collection
     */
    public int getCommuneCount(
    ) {
        return this._communeList.size();
    }

    /**
     * Returns the value of field 'headerNames'.
     * 
     * @return the value of field 'HeaderNames'.
     */
    public HeaderNames getHeaderNames(
    ) {
        return this._headerNames;
    }

    /**
     */
    public void removeAllCommune(
    ) {
        this._communeList.clear();
    }

    /**
     * Method removeCommune.
     * 
     * @param vCommune
     * @return true if the object was removed from the collection.
     */
    public boolean removeCommune(
            final Commune vCommune) {
        boolean removed = _communeList.remove(vCommune);
        return removed;
    }

    /**
     * Method removeCommuneAt.
     * 
     * @param index
     * @return the element removed from the collection
     */
    public Commune removeCommuneAt(
            final int index) {
        java.lang.Object obj = this._communeList.remove(index);
        return (Commune) obj;
    }

    /**
     * 
     * 
     * @param index
     * @param vCommune
     * @throws java.lang.IndexOutOfBoundsException if the index
     * given is outside the bounds of the collection
     */
    public void setCommune(
            final int index,
            final Commune vCommune)
    throws java.lang.IndexOutOfBoundsException {
        // check bounds for index
        if (index < 0 || index >= this._communeList.size()) {
            throw new IndexOutOfBoundsException("setCommune: Index value '" + index + "' not in range [0.." + (this._communeList.size() - 1) + "]");
        }

        this._communeList.set(index, vCommune);
    }

    /**
     * 
     * 
     * @param vCommuneArray
     */
    public void setCommune(
            final Commune[] vCommuneArray) {
        //-- copy array
        _communeList.clear();

        for (int i = 0; i < vCommuneArray.length; i++) {
                this._communeList.add(vCommuneArray[i]);
        }
    }

    /**
     * Sets the value of field 'headerNames'.
     * 
     * @param headerNames the value of field 'headerNames'.
     */
    public void setHeaderNames(
            final HeaderNames headerNames) {
        this._headerNames = headerNames;
    }

}

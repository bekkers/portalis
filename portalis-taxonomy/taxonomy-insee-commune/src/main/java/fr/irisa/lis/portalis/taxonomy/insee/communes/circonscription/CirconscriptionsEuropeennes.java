/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.3.1</a>, using an XML
 * Schema.
 * $Id$
 */

package fr.irisa.lis.portalis.taxonomy.insee.communes.circonscription;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * Class CirconscriptionsEuropeennes.
 * 
 * @version $Revision$ $Date$
 */
@SuppressWarnings("serial")
@XmlRootElement
public class CirconscriptionsEuropeennes extends CirconscriptionsEuropeennesType 
implements java.io.Serializable
{


      //----------------/
     //- Constructors -/
    //----------------/

    public CirconscriptionsEuropeennes() {
        super();
    }

}

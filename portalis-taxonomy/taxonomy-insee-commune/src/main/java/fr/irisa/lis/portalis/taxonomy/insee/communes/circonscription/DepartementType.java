/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.3.1</a>, using an XML
 * Schema.
 * $Id$
 */

package fr.irisa.lis.portalis.taxonomy.insee.communes.circonscription;

import javax.xml.bind.annotation.XmlAttribute;

/**
 * Class DepartementType.
 * 
 * @version $Revision$ $Date$
 */
@SuppressWarnings("serial")
public class DepartementType implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _nom.
     */
	@XmlAttribute
    private java.lang.String _nom;

    /**
     * Field _numero.
     */
	@XmlAttribute
    private java.lang.String _numero;

    /**
     * Field _prefecture.
     */
	@XmlAttribute
    private java.lang.String _prefecture;

    /**
     * Field _circonscriptionList.
     */
    private java.util.Vector<fr.irisa.lis.portalis.taxonomy.insee.communes.circonscription.Circonscription> _circonscriptionList;


      //----------------/
     //- Constructors -/
    //----------------/

    public DepartementType() {
        super();
        this._circonscriptionList = new java.util.Vector<fr.irisa.lis.portalis.taxonomy.insee.communes.circonscription.Circonscription>();
    }


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * 
     * 
     * @param vCirconscription
     * @throws java.lang.IndexOutOfBoundsException if the index
     * given is outside the bounds of the collection
     */
    public void addCirconscription(
            final fr.irisa.lis.portalis.taxonomy.insee.communes.circonscription.Circonscription vCirconscription)
    throws java.lang.IndexOutOfBoundsException {
        this._circonscriptionList.addElement(vCirconscription);
    }

    /**
     * 
     * 
     * @param index
     * @param vCirconscription
     * @throws java.lang.IndexOutOfBoundsException if the index
     * given is outside the bounds of the collection
     */
    public void addCirconscription(
            final int index,
            final fr.irisa.lis.portalis.taxonomy.insee.communes.circonscription.Circonscription vCirconscription)
    throws java.lang.IndexOutOfBoundsException {
        this._circonscriptionList.add(index, vCirconscription);
    }

    /**
     * Method enumerateCirconscription.
     * 
     * @return an Enumeration over all
     * fr.irisa.lis.portalis.service.patrimoine.arles.Circonscription
     * elements
     */
    public java.util.Enumeration<? extends fr.irisa.lis.portalis.taxonomy.insee.communes.circonscription.Circonscription> enumerateCirconscription(
    ) {
        return this._circonscriptionList.elements();
    }

    /**
     * Method getCirconscription.
     * 
     * @param index
     * @throws java.lang.IndexOutOfBoundsException if the index
     * given is outside the bounds of the collection
     * @return the value of the
     * fr.irisa.lis.portalis.service.patrimoine.arles.Circonscription
     * at the given index
     */
    public fr.irisa.lis.portalis.taxonomy.insee.communes.circonscription.Circonscription getCirconscription(
            final int index)
    throws java.lang.IndexOutOfBoundsException {
        // check bounds for index
        if (index < 0 || index >= this._circonscriptionList.size()) {
            throw new IndexOutOfBoundsException("getCirconscription: Index value '" + index + "' not in range [0.." + (this._circonscriptionList.size() - 1) + "]");
        }

        return (fr.irisa.lis.portalis.taxonomy.insee.communes.circonscription.Circonscription) _circonscriptionList.get(index);
    }

    /**
     * Method getCirconscription.Returns the contents of the
     * collection in an Array.  <p>Note:  Just in case the
     * collection contents are changing in another thread, we pass
     * a 0-length Array of the correct type into the API call. 
     * This way we <i>know</i> that the Array returned is of
     * exactly the correct length.
     * 
     * @return this collection as an Array
     */
    public fr.irisa.lis.portalis.taxonomy.insee.communes.circonscription.Circonscription[] getCirconscription(
    ) {
        fr.irisa.lis.portalis.taxonomy.insee.communes.circonscription.Circonscription[] array = new fr.irisa.lis.portalis.taxonomy.insee.communes.circonscription.Circonscription[0];
        return (fr.irisa.lis.portalis.taxonomy.insee.communes.circonscription.Circonscription[]) this._circonscriptionList.toArray(array);
    }

    /**
     * Method getCirconscriptionCount.
     * 
     * @return the size of this collection
     */
    public int getCirconscriptionCount(
    ) {
        return this._circonscriptionList.size();
    }

    /**
     * Returns the value of field 'nom'.
     * 
     * @return the value of field 'Nom'.
     */
    public java.lang.String getNom(
    ) {
        return this._nom;
    }

    /**
     * Returns the value of field 'numero'.
     * 
     * @return the value of field 'Numero'.
     */
    public java.lang.String getNumero(
    ) {
        return this._numero;
    }

    /**
     * Returns the value of field 'prefecture'.
     * 
     * @return the value of field 'Prefecture'.
     */
    public java.lang.String getPrefecture(
    ) {
        return this._prefecture;
    }

    /**
     */
    public void removeAllCirconscription(
    ) {
        this._circonscriptionList.clear();
    }

    /**
     * Method removeCirconscription.
     * 
     * @param vCirconscription
     * @return true if the object was removed from the collection.
     */
    public boolean removeCirconscription(
            final fr.irisa.lis.portalis.taxonomy.insee.communes.circonscription.Circonscription vCirconscription) {
        boolean removed = _circonscriptionList.remove(vCirconscription);
        return removed;
    }

    /**
     * Method removeCirconscriptionAt.
     * 
     * @param index
     * @return the element removed from the collection
     */
    public fr.irisa.lis.portalis.taxonomy.insee.communes.circonscription.Circonscription removeCirconscriptionAt(
            final int index) {
        java.lang.Object obj = this._circonscriptionList.remove(index);
        return (fr.irisa.lis.portalis.taxonomy.insee.communes.circonscription.Circonscription) obj;
    }

    /**
     * 
     * 
     * @param index
     * @param vCirconscription
     * @throws java.lang.IndexOutOfBoundsException if the index
     * given is outside the bounds of the collection
     */
    public void setCirconscription(
            final int index,
            final fr.irisa.lis.portalis.taxonomy.insee.communes.circonscription.Circonscription vCirconscription)
    throws java.lang.IndexOutOfBoundsException {
        // check bounds for index
        if (index < 0 || index >= this._circonscriptionList.size()) {
            throw new IndexOutOfBoundsException("setCirconscription: Index value '" + index + "' not in range [0.." + (this._circonscriptionList.size() - 1) + "]");
        }

        this._circonscriptionList.set(index, vCirconscription);
    }

    /**
     * 
     * 
     * @param vCirconscriptionArray
     */
    public void setCirconscription(
            final fr.irisa.lis.portalis.taxonomy.insee.communes.circonscription.Circonscription[] vCirconscriptionArray) {
        //-- copy array
        _circonscriptionList.clear();

        for (int i = 0; i < vCirconscriptionArray.length; i++) {
                this._circonscriptionList.add(vCirconscriptionArray[i]);
        }
    }

    /**
     * Sets the value of field 'nom'.
     * 
     * @param nom the value of field 'nom'.
     */
    public void setNom(
            final java.lang.String nom) {
        this._nom = nom;
    }

    /**
     * Sets the value of field 'numero'.
     * 
     * @param numero the value of field 'numero'.
     */
    public void setNumero(
            final java.lang.String numero) {
        this._numero = numero;
    }

    /**
     * Sets the value of field 'prefecture'.
     * 
     * @param prefecture the value of field 'prefecture'.
     */
    public void setPrefecture(
            final java.lang.String prefecture) {
        this._prefecture = prefecture;
    }

}

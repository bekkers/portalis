<circonscriptionsEuropeennes>{

for $euCirco in distinct-values(/communes/commune/EU_circo)
return 
		<circonscriptionEuropeenne name="{$euCirco}"> {
		for $region in distinct-values(/communes/commune[EU_circo=$euCirco]/code_region)
		let $regionInfo := /communes/commune[code_region=$region]
		return
			<region code="{$region}" nom="{$regionInfo[1]/nom_region}" chefLieu="{$regionInfo[1]/chef_lieu_region}"> {
				for $departement in distinct-values($regionInfo/numero_departement)
				let $departementInfo := $regionInfo[numero_departement=$departement]
				return
					<departement numero="{$departement}" nom="{$departementInfo[1]/nom_departement}" prefecture="{$departementInfo[1]/prefecture}"> {
						for $circonscription in distinct-values($departementInfo/numero_circonscription)
						let $circonscriptionInfo := $departementInfo[numero_circonscription=$circonscription]
						return
							<circonscription numero="{$circonscription}" > {
								for $commune in $circonscriptionInfo
								return
									<commune nom="{$commune/nom_commune}" codesPostaux="{$commune/codes_postaux}"
									codeInsee="{$commune/code_insee}" latitude="{$commune/latitude}" longitude="{$commune/longitude}" eloignement="{$commune/eloignement}"
									circonscription="{$commune/nom_departement}-{$commune/numero_circonscription}"/>
							}
							</circonscription>
					}
					</departement>
			}
			</region>
		}
		</circonscriptionEuropeenne>
}
</circonscriptionsEuropeennes>
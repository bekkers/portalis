package com.androlis.fr.smartphone.modele.data.observer;

public interface Observer {

	public void notifyFromSuject(Subject subject);
	
}

package com.androlis.fr.smartphone.modele.data;

import com.androlis.fr.core.Core;
import com.androlis.fr.smartphone.controleur.communication.CommunicationServeur;

public class Client {

	private static Client client;
	private Core core;
	private CommunicationServeur handler;
	private String login;
	private String email;
	
	private Client(){
		this.core = new Core();
		this.handler = new CommunicationServeur(null);
	}
	
	public static Client getInstance(){
		if( client == null ){
			client = new Client();
		}
		return client;
	}

	public Core getCore(){
		return core;
	}
	
	public CommunicationServeur getHandler(){
		return handler;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
		
	}
	
}

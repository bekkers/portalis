package com.androlis.fr.smartphone.modele.data.lis;

import com.androlis.fr.smartphone.modele.data.memento.Commande;
import com.androlis.fr.smartphone.modele.data.memento.Memento;

public abstract class DataLoader implements Runnable, Commande, Memento {

	protected Requete requete;
	public static final int IS_OBJECT = -1;
	
	public DataLoader(Requete requete){
		this.requete = requete;
	}
	
	public abstract void run();
	
	@Override
	public void execute() {
		Thread th = new Thread(this);
		th.start();
		//Log.d("","Axiome loader execute : " + ((Requete) memento).getRequete().replace("%20", " "));
	}
	
	public void setMemento(Memento memento){
		this.requete.setState(memento);
	}
}

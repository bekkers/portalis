package com.androlis.fr.smartphone.modele.data;

import android.os.Parcel;
import android.os.Parcelable;

public interface ParcelableData extends Parcelable{
	
	public void writeToParcel(Parcel dest, int flags);
	public void lire(Parcel in);
	
}

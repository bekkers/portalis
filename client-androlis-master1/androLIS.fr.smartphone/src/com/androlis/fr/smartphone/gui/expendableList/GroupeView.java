package com.androlis.fr.smartphone.gui.expendableList;

import java.util.ArrayList;

import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.androlis.fr.smartphone.gui.ActivityRecherche;
import com.androlis.fr.smartphone.gui.list.ListViewer;
import com.androlis.fr.smartphone.gui.Viewer;
import com.androlis.fr.smartphone.modele.data.DataModel;
import com.androlis.fr.smartphone.modele.data.lis.Groupe;
import com.fr.lis.androlis.smartphone.main.R;

public class GroupeView implements ViewExpendableList{

	private static final int LAYOUT = R.layout.entete_groupe;
	
	private ArrayList<Viewer> axiomesView;
	private Groupe groupe;
	
	public GroupeView(Groupe groupe, ActivityRecherche activity){
		this.groupe = groupe;
		
		axiomesView = new ArrayList<Viewer>();
		ArrayList<DataModel> axiomes = groupe.getAxiomes();
		for(DataModel ax : axiomes){
			Viewer v = new AxiomeView(ax, activity);
			axiomesView.add(v);
		}
	}
	
	public GroupeView(Groupe groupe, ArrayList<Viewer> axiomesView){
		this.groupe = groupe;
		this.axiomesView = axiomesView;
	}
	
	public GroupeView(Groupe groupe){
		this.groupe = groupe;
		this.axiomesView = new ArrayList<Viewer>();
	}
	
	public void addAxiomeView(Viewer axiome){
		this.axiomesView.add(axiome);
	}
	
	@Override
	public View getView(LayoutInflater inflater, View convertView) {

		TextView label = null;
		
		if( convertView == null ){
			convertView = inflater.inflate(LAYOUT, null);
		}
		
		label = (TextView) convertView.findViewById(R.id.labelGroupe);
		label.setText(groupe.getName());
		
		return convertView;
	}

	public Viewer getAxiomeView(int index){
		return axiomesView.get(index);
	}
	
	public int getNbAxiomes(){
		return axiomesView.size();
	}
	
	@Override
	public int getLayout() {
		return LAYOUT;
	}

	@Override
	public DataModel getDataModel() {
		return groupe;
	}


	@Override
	public void onCreate(ListViewer list) {
		// TODO Auto-generated method stub
	}

}

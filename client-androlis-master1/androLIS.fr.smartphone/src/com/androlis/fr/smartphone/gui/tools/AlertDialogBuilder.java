package com.androlis.fr.smartphone.gui.tools;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;

import com.fr.lis.androlis.smartphone.main.R;

public class AlertDialogBuilder {


	public static void showAlertDialog(String message, String titre, Context ctx){

		final AlertDialog.Builder alertbox = new AlertDialog.Builder(ctx);

		try{
			alertbox.setMessage(message);
			alertbox.setTitle(titre);
			alertbox.setPositiveButton(R.string.ok,new DialogInterface.OnClickListener(){
				public void onClick(DialogInterface arg0, int arg1){

				}
			});
			/*alertbox.setNegativeButton(R.id.button_not,new DialogInterface.OnClickListener(){
				public void onClick(DialogInterface arg0, int arg1){
				}
			});*/
			alertbox.show();
			alertbox.setCancelable(false);
		}catch(Exception e){
			//Handle BadTokenException.
			e.printStackTrace();
		}
	} 
	
	public static AlertDialog.Builder buildAlertDialog(String message, String titre, Context ctx){

		final AlertDialog.Builder alertbox = new AlertDialog.Builder(ctx);

		try{
			alertbox.setMessage(message);
			alertbox.setTitle(titre);
			alertbox.setPositiveButton(R.string.ok,new DialogInterface.OnClickListener(){
				public void onClick(DialogInterface arg0, int arg1){

				}
			});
			/*alertbox.setNegativeButton(R.id.button_not,new DialogInterface.OnClickListener(){
				public void onClick(DialogInterface arg0, int arg1){
				}
			});*/
			alertbox.setCancelable(false);
			return alertbox;
		}catch(Exception e){
			//Handle BadTokenException.
			e.printStackTrace();
			return null;
		}
	} 
}


package com.androlis.fr.smartphone.gui.list;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.androlis.fr.smartphone.gui.Viewer;

public class ListViewer extends BaseAdapter{

	private List<Viewer> listeVue = new ArrayList<Viewer>();
	private LayoutInflater inflater;
	
	private Context context;
	
	public ListViewer(Context context){
		this.inflater = LayoutInflater.from(context);
		this.context = context;
	}
	
	public void add(Viewer view){
		this.listeVue.add(view);
		view.onCreate(this);
	}
	
	@Override
	public int getCount() {
		return listeVue.size();
	}

	@Override
	public Object getItem(int arg0) {
		return listeVue.get(arg0);
	}

	@Override
	public long getItemId(int arg0) {
		return listeVue.size();
	}

	public void startActivity(Intent intent){
		context.startActivity(intent);
	}
	
	@Override
	public View getView(int index, View convertView, ViewGroup parent) {
		Viewer vue = listeVue.get(index);
		return vue.getView(inflater , convertView);
	}

	public Context getContext() {
		return context;
	}

	public void setContext(Context context) {
		this.context = context;
	}	
}

package com.androlis.fr.smartphone.gui;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Message;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.androlis.fr.smartphone.controleur.communication.UpdateView;
import com.androlis.fr.smartphone.controleur.listeners.ListenerLogin;
import com.androlis.fr.smartphone.modele.data.Client;
import com.fr.lis.androlis.smartphone.main.R;

public class ActivityLogin extends Activity implements UpdateView {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activty_login);

		Client.getInstance().getHandler().setView(this);

		Button bouton = (Button) findViewById(R.id.connexion);
		bouton.setOnClickListener(new ListenerLogin(this));

		/*bouton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(getApplicationContext(), LocalisationMap.class);
				intent.putParcelableArrayListExtra("points", new ArrayList<Parcelable>());
				startActivity(intent);
			}
		});*/
	}

	public void onResume(){
		Client.getInstance().getHandler().setView(this);
		super.onResume();
	}

	@Override
	public void onUpdateReceive(Message msg) {
		Toast.makeText(this, "Message reçu : "+msg.obj.toString(), Toast.LENGTH_LONG).show();
		boolean connection = (Boolean) msg.obj;
		if( connection ){
			EditText login = (EditText)findViewById(R.id.username);
			Client.getInstance().setLogin(login.getText().toString());
			Intent intent = new Intent(this, ApplicationActivity.class);
			this.startActivity(intent);
		}else{
			Toast.makeText(this, "Echec de connexion", Toast.LENGTH_LONG).show();
		}
	}

}

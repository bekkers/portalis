package com.androlis.fr.smartphone.controleur.listeners;

import android.app.Activity;
import android.content.Intent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;

import com.androlis.fr.smartphone.gui.ActivityRecherche;
import com.androlis.fr.smartphone.gui.list.ApplicationView;
import com.androlis.fr.smartphone.gui.list.ListViewer;
import com.androlis.fr.smartphone.modele.data.DataModel;

public class ListenerListViewer implements OnItemClickListener{

	private ListViewer listViewer;
	private Activity activity;
	
	public ListenerListViewer(ListViewer listView, Activity activity){
		this.listViewer = listView;
		this.activity = activity;
	}

	public void onItemClick(AdapterView<?> arg0, View arg1, int index, long arg3) {
		ApplicationView appView = (ApplicationView)listViewer.getItem(index);
		DataModel data = appView.getDataModel();
		// D�marer l'activity pour naviguer dans l'application
		Intent intent = new Intent(activity, ActivityRecherche.class ); 
		intent.putExtra("Application", data.getName());
		activity.startActivity(intent);
	}

}

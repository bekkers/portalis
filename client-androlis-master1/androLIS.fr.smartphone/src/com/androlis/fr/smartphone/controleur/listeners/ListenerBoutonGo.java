package com.androlis.fr.smartphone.controleur.listeners;

import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;

import com.androlis.fr.smartphone.modele.data.lis.AxiomeLoader;
import com.androlis.fr.smartphone.modele.data.lis.Requete;
import com.androlis.fr.smartphone.modele.data.memento.CareTaker;
import com.androlis.fr.smartphone.modele.data.memento.Paire;
import com.androlis.fr.smartphone.modele.data.memento.RequeteMemento;

public class ListenerBoutonGo implements OnClickListener{

	private Requete requete;
	private AxiomeLoader axiomeLoader;
	private TextView chemin;
	
	public ListenerBoutonGo(Requete requete, AxiomeLoader axiomeLoader, TextView chemin){
		this.requete = requete;
		this.axiomeLoader = new AxiomeLoader(requete);
		this.chemin = chemin;
	}
	
	@Override
	public void onClick(View v) {
		Log.d("","click ok");
		RequeteMemento rq = new RequeteMemento();
		rq.setState(requete.getSource());
		CareTaker ct = CareTaker.getInstance();
		ct.addMemento(new Paire(axiomeLoader, rq));
		
		axiomeLoader.execute();
		chemin.setText(requete.getRequete().replace("%20", " "));
	}

}

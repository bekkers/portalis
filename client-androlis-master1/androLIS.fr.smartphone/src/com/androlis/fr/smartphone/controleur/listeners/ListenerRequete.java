package com.androlis.fr.smartphone.controleur.listeners;

import android.content.Context;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;

import com.androlis.fr.smartphone.gui.tools.AlertDialogBuilder;
import com.androlis.fr.smartphone.modele.data.lis.Requete;

public class ListenerRequete implements OnClickListener{

	private Requete requete;
	private Context ctx;
	
	public ListenerRequete(Requete requete, Context context){
		this.requete = requete;
		this.ctx = context;
		Log.d("create onclick", "ok");
	}
	
	@Override
	public void onClick(View v) {
		Log.d("","onclick ok");
		String requete = this.requete.getRequete();
		requete = requete.replace("%20", " ");
		
		AlertDialogBuilder.showAlertDialog(requete, "", ctx);
		
	}

}

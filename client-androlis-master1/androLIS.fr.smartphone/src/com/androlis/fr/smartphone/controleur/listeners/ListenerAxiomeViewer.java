package com.androlis.fr.smartphone.controleur.listeners;

import com.androlis.fr.smartphone.gui.ActivityRecherche;
import com.androlis.fr.smartphone.gui.expendableList.AxiomeView;
import com.androlis.fr.smartphone.modele.data.lis.Requete;

import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.CheckBox;

public class ListenerAxiomeViewer implements OnClickListener{

	private CheckBox checkBox;
	private AxiomeView axiome;
	private ActivityRecherche recherche;
	
	public ListenerAxiomeViewer(AxiomeView axiome, CheckBox checkbox, ActivityRecherche activity){
		this.axiome = axiome;
		this.checkBox = checkbox;
		this.recherche = activity;
	}
	
	@Override
	public void onClick(View v) {
		Requete req = axiome.getActivity().getRequete();
		if ( axiome.isChecked() ){
			axiome.setChecked(false);
			req.remove(axiome.getDataModel().getName());
		}else{
			axiome.setChecked(true);
			if( recherche.andIsChecked() ){
				if( recherche.notIsChecked() ){
					req.andNotAxiome(axiome.getDataModel().getName());
				}else{
					req.andAxiome(axiome.getDataModel().getName());
				}
			}else if (recherche.orIsChecked()){
				if( recherche.notIsChecked() ){
					req.orNotAxiome(axiome.getDataModel().getName());
				}else{
					req.orAxiome(axiome.getDataModel().getName());
				}
			}
		}
		checkBox.setChecked(axiome.isChecked());
		Log.d("", "'"+req.getRequete()+"'");
	}
	
}

package com.androlis.map;

import java.util.ArrayList;

import android.util.Log;

import com.androlis.fr.smartphone.cartographie.map.configuration.Configuration;
import com.androlis.fr.smartphone.cartographie.map.point.Circle;
import com.androlis.fr.smartphone.cartographie.map.point.PointGraphique;
import com.androlis.fr.smartphone.cartographie.map.point.Zone;
import com.google.android.maps.GeoPoint;


public class Cadrillage {

	public static final int RAYON_TERRE = 6371;

	//private static ArrayList<PointGeographique> points = new ArrayList<PointGeographique>();
	private ArrayList<Zone> zones;

	private CustomMap mapView;

	private double pente;
	private double b;

	/**
	 * @param mapView
	 * - Gère les points dans la zone de visualisation (regroupement par zone)
	 * - Calcul l'interpolation linéaire pour determiner le nombre de zone à créer.
	 */
	public Cadrillage(CustomMap mapView){
		this.mapView = mapView;
		calculVariables();
	}

	/**
	 * Calcul des paramètres de l'interpolation linéaire y=ax+b
	 */
	private void calculVariables(){
		//y = ax + b
		//interpolation lineaire por obtenir
		//le nombre de case du quadrillage de la map :

		// f(zoom) = decoupage

		//Dans ce cas :
		// p1 (2, 10) <=> p1( MIN_ZOOM, MAX_DECOUPAGE)
		// p2 (21, 1) <=> p2( MAX_ZOOM, MIN_DECOUPAGE)
		// pX ( x, y)
		// 1) 1 = 21a + b
		// 2) 10 = 2a + b
		// l1 - l2 ) -9 = 19a <=> a = -9/19 <=> a = -0,47
		// 10 = 2 * -0,47 + b <=> b = 10 + 2*0,47 = 10, 9
		// <=> y = -0.47x + 10.9

		//calcul de la pente :
		pente = (double)(Configuration.MIN_DECOUPAGE - Configuration.MAX_DECOUPAGE) /
				(Configuration.MAX_ZOOM - Configuration.MIN_ZOOM);
		b = -(pente*Configuration.MIN_ZOOM) * Configuration.MAX_DECOUPAGE;
	}
	/**
	 * 
	 * @param zoom
	 * @return int, le nombre de découpage de la carte visible
	 * Ex. getDecoupage(x) return 3, cadrillage de 3*3 zones
	 */
	public int getDecoupage(int zoom){
		//Toast.makeText(mapView.getContext(), "decoupe : " + (int)(pente*zoom + b), Toast.LENGTH_LONG).show();
		return (int)(pente*zoom + b);
	}

	/**
	 * @param decoupage nombre de zone a calculé sur la map
	 * @param latitudeSpan (axe des y) espace visible sur la map
	 * @param longitudeSpan (axe des x) espace visible sur la map 
	 * @param center centre de la map
	 * @param in points dans la zone visible 
	 * @param out points dans la zone non visible
	 * Ajoute les points ou les cercles à la map
	 */
	public void decoupeV2(int decoupage, double latitudeSpan,
			double longitudeSpan, GeoPoint center,
			ArrayList<PointGraphique> in, ArrayList<PointGraphique> out){

		zones = new ArrayList<Zone>();
		mapView.getOverlays().clear();

		double deltaLatitude = latitudeSpan;
		double deltaLongitude = longitudeSpan;

		double deltaDecoupageLatitude = deltaLatitude / decoupage;
		double deltaDecoupageLongitude = deltaLongitude / decoupage;

		GeoPoint hautGauche = new GeoPoint( (int) (center.getLatitudeE6() + latitudeSpan/2), (int)(center.getLongitudeE6() - longitudeSpan/2));

		//Cr�ation des zones � partir du coin haut gauche de la zone
		for( int i = 0 ; i < decoupage ; i++ ){
			for( int j = 0 ; j < decoupage ; j++ ){
				GeoPoint p = new GeoPoint(hautGauche.getLatitudeE6() - (int) (j * deltaDecoupageLatitude) ,
						(int)(hautGauche.getLongitudeE6() + i*deltaDecoupageLongitude) );
				Zone z = new Zone( p, deltaDecoupageLatitude, deltaDecoupageLongitude);
				zones.add(z);
			}
		}

		//Ajout des points � la zone :
		for( PointGraphique gp : in ){
			for( Zone z : zones ){
				if( z.pointAppartientZone(gp.getPoint())){
					z.addPoint(gp);
					gp.setZone(z);
				}
			}
		}

		for(Zone z : zones ){
			if( z.isCercle() ){
				//dessiner un cercle
				Circle c = new Circle(mapView.getContext(), z);
				mapView.getOverlays().add(c);
				Log.d("LIS","dessine cercle : " + z.getColorZone());
			}else{
				if( !z.isEmpty() ){
					//dessiner le point
					//Toast.makeText(mapView.getContext(), "Dessin d'un point", Toast.LENGTH_LONG).show();
					//GeoPoint gp = z.getPoint().getPoint();
					PointGraphique pg = z.getPoint();
					mapView.getOverlays().add(pg);
					Log.d("LIS","dessine");
				}
			}
		}
		for( PointGraphique gp : out ){
			mapView.getOverlays().add(gp);
		}
		mapView.invalidate();
	}

	/*public void decoupe(int decoupage, double latitudeSpan,
			double longitudeSpan, GeoPoint center){
		/*
		^ HG o______________________o Center + (latSpan, longSpan) | HD
		|	 |						|
		|	 |						|
		|	 |						|
		|	 |						|
delta	|	 |						|
long.	|	 |						|
		|	 |						|
		|	 |			+ Center	|
		|    |						|
		|	 |						|
		|	 |						|
		|	 |						|
		|	 |						|
		|	 |						|
		|	 |						|
		v BG o______________________o BD
		 	 <----delta latitude---->
		 */

		//Ajout des points (temporaire) :
		/*if( points.isEmpty() ){
			distance2();
		}

		zones = new ArrayList<Zone>();
		mapView.getOverlays().clear();

		ArrayList<PointGraphique> points = mapView.getPoints();

		for(PointGraphique pg : points){
			pg.setZone(null);
		}
		Log.d("LIS","decoupage " + decoupage);
		/*if( decoupage < 2 ){
			dessinePoints();
			return;
		}
		
		GeoPoint hautGauche = new GeoPoint( (int) (center.getLatitudeE6() + latitudeSpan/2), (int)(center.getLongitudeE6() - longitudeSpan/2));
		/*GeoPoint hautDroit = new GeoPoint( (int) (center.getLatitudeE6() + latitudeSpan/2), (int)(center.getLongitudeE6() + longitudeSpan/2));
		GeoPoint basDroit = new GeoPoint( (int) (center.getLatitudeE6() - latitudeSpan/2), (int)(center.getLongitudeE6() + longitudeSpan/2));
		GeoPoint basGauche = new GeoPoint( (int) (center.getLatitudeE6() - latitudeSpan/2), (int)(center.getLongitudeE6() - longitudeSpan/2));
		
		mapView.getOverlays().add(new PointGraphique(mapView.getContext(), hautGauche, R.drawable.rennes_image));
		mapView.getOverlays().add(new PointGraphique(mapView.getContext(), hautDroit, R.drawable.punaise));
		mapView.getOverlays().add(new PointGraphique(mapView.getContext(), basDroit, R.drawable.planet));
		mapView.getOverlays().add(new PointGraphique(mapView.getContext(), basGauche, R.drawable.paca));
		 
		mapView.invalidate();

		/*
		double deltaLatitude = hautDroit.getLatitudeE6() - hautGauche.getLatitudeE6();
		double deltaLongitude = hautDroit.getLongitudeE6() - basDroit.getLongitudeE6();
		 
		double deltaLatitude = latitudeSpan;
		double deltaLongitude = longitudeSpan;

		double deltaDecoupageLatitude = deltaLatitude / decoupage;
		double deltaDecoupageLongitude = deltaLongitude / decoupage;

		//Toast.makeText(mapView.getContext(), deltaDecoupageLatitude + " ; " + deltaDecoupageLongitude, Toast.LENGTH_LONG).show();

		//Cr�ation des zones � partir du coin haut gauche de la zone
		for( int i = 0 ; i < decoupage ; i++ ){
			for( int j = 0 ; j < decoupage ; j++ ){
				GeoPoint p = new GeoPoint(hautGauche.getLatitudeE6() - (int) (j * deltaDecoupageLatitude) ,
						(int)(hautGauche.getLongitudeE6() + i*deltaDecoupageLongitude) );
				Zone z = new Zone( p, deltaDecoupageLatitude, deltaDecoupageLongitude);
				zones.add(z);
			}
		}

		//Ajout des points � la zone :
		for( PointGraphique gp : points ){
			for( Zone z : zones ){
				if( z.pointAppartientZone(gp.getPoint())){
					z.addPoint(gp);
					gp.setZone(z);
				}
			}
		}

		for(Zone z : zones ){
			if( z.isCercle() ){
				//dessiner un cercle
				Circle c = new Circle(mapView.getContext(), z);
				mapView.getOverlays().add(c);
				Log.d("LIS","dessine cercle : " + z.getColorZone());
			}else{
				if( !z.isEmpty() ){
					//dessiner le point
					//Toast.makeText(mapView.getContext(), "Dessin d'un point", Toast.LENGTH_LONG).show();
					//GeoPoint gp = z.getPoint().getPoint();
					PointGraphique pg = z.getPoint();
					mapView.getOverlays().add(pg);
					Log.d("LIS","dessine");
				}
			}
		}
		for(PointGraphique pg : points){
			if( ! pg.aUneZone() ){
				//PointGraphique point = new PointGraphique(mapView.getContext(), pg, R.drawable.punaise2);
				mapView.getOverlays().add(pg);
			}
		}
		mapView.invalidate();

		// Test de dessin du cadrillage :
		/*for(Zone z : zones ){
			//z.testDessineZone(mapView);
		}
		mapView.invalidate();
	}*/

	public void dessinePoints(){
		ArrayList<PointGraphique> points = mapView.getPoints();
		for(PointGraphique pg : points){
			mapView.getOverlays().add(pg);
		}
	}

	/*public static void distance(Context ctx, MapView mapView){

		GeoPoint p = new GeoPoint(1, 1);

		double longitude, latitude;

		latitude = 48.100f * 1E6;
		longitude = -1.667 * 1E6;
		PointGeographique rennes = new PointGeographique((int)latitude,(int) longitude);
		points.add(rennes);


		latitude = 48.000f * 1E6;
		longitude = -4.100 * 1E6;
		PointGeographique quimper = new PointGeographique((int)latitude,(int) longitude);
		points.add(quimper);

		latitude = 48.383f * 1E6;
		longitude = -4.500 * 1E6;
		PointGeographique brest = new PointGeographique((int)latitude,(int) longitude);
		points.add(brest);

		latitude = 49.183 * 1E6;
		longitude =  -0.367 * 1E6;
		PointGeographique caen = new PointGeographique((int)latitude,(int) longitude);
		points.add(caen);

		latitude = 49.1466 * 1E6;
		longitude = 0.2293 * 1E6;
		PointGeographique lisieux = new PointGeographique((int)latitude,(int) longitude);
		points.add(lisieux);

		latitude = 48.900f * 1E6;
		longitude = -0.200 * 1E6;
		PointGeographique falaise = new PointGeographique((int)latitude,(int) longitude);
		points.add(falaise);

		/*float result[] = new float[3];

		Location.distanceBetween(rennes.getLatitudeE6()/1E6, rennes.getLongitudeE6()/1E6,
				caen.getLatitudeE6()/1E6, caen.getLongitudeE6()/1E6, result);

		int i = 0;
		for(Float f : result ){
			//Toast.makeText(ctx, "result["+i+"]="+f, Toast.LENGTH_LONG).show();
			i++;
		}

		int lat = mapView.getLatitudeSpan();
		int longi =  mapView.getLongitudeSpan();
		int zoom = mapView.getZoomLevel();
	 */
	//Toast.makeText(ctx, "lat : "+lat, Toast.LENGTH_LONG).show();
	//Toast.makeText(ctx, "long "+longi, Toast.LENGTH_LONG).show();
	//Toast.makeText(ctx, "zoom "+zoom, Toast.LENGTH_LONG).show();

	/* 
		double latitude1, longitude1;
		double latitude2, longitude2;

		double distance = 0;

		latitude1 = 48.1;
		longitude1 = -1.667;

		latitude2 = 49.183;
		longitude2 = -0.367;

		distance = RAYON_TERRE * Math.abs( Math.cosh( Math.sin(latitude1) * Math.sin(latitude2) 
				+ Math.cos( latitude1 )*Math.cos(latitude2) *Math.cos(longitude1 - longitude2)  ) );
		System.out.println(distance);
	}	*/
	/*public static void distance2(){

		double longitude, latitude;

		latitude = 48.100f * 1E6;
		longitude = -1.667 * 1E6;
		PointGeographique rennes = new PointGeographique((int)latitude,(int) longitude);
		points.add(rennes);


		latitude = 48.000f * 1E6;
		longitude = -4.100 * 1E6;
		PointGeographique quimper = new PointGeographique((int)latitude,(int) longitude);
		points.add(quimper);

		latitude = 48.383f * 1E6;
		longitude = -4.500 * 1E6;
		PointGeographique brest = new PointGeographique((int)latitude,(int) longitude);
		points.add(brest);

		latitude = 49.183 * 1E6;
		longitude =  -0.367 * 1E6;
		PointGeographique caen = new PointGeographique((int)latitude,(int) longitude);
		points.add(caen);

		latitude = 49.1466 * 1E6;
		longitude = 0.2293 * 1E6;
		PointGeographique lisieux = new PointGeographique((int)latitude,(int) longitude);
		points.add(lisieux);

		latitude = 48.900f * 1E6;
		longitude = -0.200 * 1E6;
		PointGeographique falaise = new PointGeographique((int)latitude,(int) longitude);
		points.add(falaise);

		latitude = 47.8667 * 1E6;
		longitude = -3.9167 * 1E6;
		PointGeographique concarneau = new PointGeographique((int)latitude,(int) longitude);
		points.add(concarneau);

		latitude = 48.0954 * 1E6;
		longitude = -4.329 * 1E6;
		PointGeographique douarnenez = new PointGeographique((int)latitude,(int) longitude);
		points.add(douarnenez);

		/*float result[] = new float[3];

		Location.distanceBetween(rennes.getLatitudeE6()/1E6, rennes.getLongitudeE6()/1E6,
				caen.getLatitudeE6()/1E6, caen.getLongitudeE6()/1E6, result);

		int i = 0;
		for(Float f : result ){
			//Toast.makeText(ctx, "result["+i+"]="+f, Toast.LENGTH_LONG).show();
			i++;
		}

		int lat = mapView.getLatitudeSpan();
		int longi =  mapView.getLongitudeSpan();
		int zoom = mapView.getZoomLevel();
	 */
	//Toast.makeText(ctx, "lat : "+lat, Toast.LENGTH_LONG).show();
	//Toast.makeText(ctx, "long "+longi, Toast.LENGTH_LONG).show();
	//Toast.makeText(ctx, "zoom "+zoom, Toast.LENGTH_LONG).show();

	/* 
		double latitude1, longitude1;
		double latitude2, longitude2;

		double distance = 0;

		latitude1 = 48.1;
		longitude1 = -1.667;

		latitude2 = 49.183;
		longitude2 = -0.367;

		distance = RAYON_TERRE * Math.abs( Math.cosh( Math.sin(latitude1) * Math.sin(latitude2) 
				+ Math.cos( latitude1 )*Math.cos(latitude2) *Math.cos(longitude1 - longitude2)  ) );
		System.out.println(distance);
	}	*/
}

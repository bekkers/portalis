import java.util.ArrayList;
public class Main {
	/**
	  * méthode qui calcule si le point se situe dans le polygone
	  * @param tab tableau de points du polygone
	  * @param M point recherché
	  * @return true si le point est situé dans le polygone
	  */
	
	public static double min(double a, double b) {
		if (a<b) {
			return a;
		}
		else {
			return b;
		}
	}
	
	public static double max(double a, double b) {
		if (b<a) {
			return a;
		}
		else {
			return b;
		}
	}
	
	public static boolean cos_positif(Point M, Point A1, Point A2) {
		
		double prodscal = (A2.x-A1.x)*(M.x-A1.x)+(A2.y-A1.y)*(M.y-A1.y);
		return (prodscal>0);
	}
	public static double distance_segment(Point M, Point A, Point B) {
		// Etape 1 : on calcule la distance de M à (AB)
		
		if (!(cos_positif(M,A,B))) {
			double dist1 = Math.sqrt((M.y-A.y)*(M.y-A.y)+(M.x-A.x)*(M.x-A.x));
			return dist1;
		}
		else if (!(cos_positif(M,B,A))) {
			double dist2 = Math.sqrt((M.y-B.y)*(M.y-B.y)+(M.x-B.x)*(M.x-B.x));
			return dist2;
		}
		else {
			double a = B.x-A.x;
			double b = A.y-B.y;
			double c = -a*B.y - b*B.x;
			
			double dist = (Math.abs(a*M.y + b*M.x + c)/(Math.sqrt(a*a+b*b)));
			return dist;
		}
		
	}
	
	 public static boolean appartient_polygone(ArrayList<Point> tab,Point M) {
	  int compteur = 0;
	  double y;
	  int n = tab.size();
	  double coefdir;
	  double ordori;
	  for (int i = 0; i < n ;i++) {

	   if (i < (n-1)) {
		   coefdir = ((tab.get(i+1).y-tab.get(i).y)/(tab.get(i+1).x-tab.get(i).x));
		   ordori = tab.get(i).y-coefdir*tab.get(i).x;
		   System.out.println("Coef directeur" + coefdir + " ordonnée à l'origine : " +ordori);
	       y =  (coefdir*M.x) + ordori; 
	       System.out.println(y);
	    
	    if ((M.x < max(tab.get(i+1).x,tab.get(i).x)) && (M.x > min(tab.get(i+1).x,tab.get(i).x)) && (y >= M.y)) {
	     compteur++;
	    }
	   }
	   else {
		   coefdir = ((tab.get(0).y-tab.get(i).y)/(tab.get(0).x-tab.get(i).x));
		   ordori = tab.get(i).y-coefdir*tab.get(i).x;
		   System.out.println("Coef directeur" + coefdir + " ordonnée à l'origine : " +ordori);
	       y =  (coefdir*M.x) + ordori; 
	       System.out.println(y);
	    if ((M.x < max(tab.get(0).x,tab.get(i).x)) && (M.x > min(tab.get(0).x,tab.get(i).x)) && (y >= M.y)) {
	     compteur++;
	    }
	   }

	  }
	  return ((compteur % 2) == 1);

	 }
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		ArrayList<ArrayList<ArrayList<Double>>> tab = new ArrayList<ArrayList<ArrayList<Double>>>();
		double[][] segments_a_inserer = {{1.0,1.0,4.0,1.0},{4,1,4,2},{1,1,1,4},{1,4,7,4},{1,4,1,7},{1,7,4,5},{4,5,7,7},{7,4,7,7},{7,7,9,7}};
		for (int i = 0; i < 9; i++) {
			tab.add(new ArrayList<ArrayList<Double>>());	
			tab.get(i).add(new ArrayList<Double>());
			tab.get(i).add(new ArrayList<Double>());
			tab.get(i).get(0).add(segments_a_inserer[i][0]);
			tab.get(i).get(0).add(segments_a_inserer[i][1]);
			tab.get(i).get(1).add(segments_a_inserer[i][2]);
			tab.get(i).get(1).add(segments_a_inserer[i][3]);
		}
		/* tab.add(new ArrayList<ArrayList<Double>>());
		tab.add(new ArrayList<ArrayList<Double>>());
		tab.add(new ArrayList<ArrayList<Double>>());
		tab.add(new ArrayList<ArrayList<Double>>());
		tab.get(0).add(new ArrayList<Double>());
		tab.get(0).add(new ArrayList<Double>());
		tab.get(1).add(new ArrayList<Double>());
		tab.get(1).add(new ArrayList<Double>());
		tab.get(2).add(new ArrayList<Double>());
		tab.get(2).add(new ArrayList<Double>());
		tab.get(3).add(new ArrayList<Double>());
		tab.get(3).add(new ArrayList<Double>());
		tab.get(0).get(0).add(1.0);
		tab.get(0).get(0).add(1.0);
		tab.get(0).get(1).add(3.0);
		tab.get(0).get(1).add(3.0);
		tab.get(1).get(0).add(1.0);
		tab.get(1).get(0).add(1.0);
		tab.get(1).get(1).add(4.0);
		tab.get(1).get(1).add(2.0);
		tab.get(2).get(0).add(3.0);
		tab.get(2).get(0).add(3.0);
		tab.get(2).get(1).add(4.0);
		tab.get(2).get(1).add(2.0);
		tab.get(3).get(0).add(3.0);
		tab.get(3).get(0).add(3.0);
		tab.get(3).get(1).add(6.0);
		tab.get(3).get(1).add(6.0); 
		tab.add(new ArrayList<ArrayList<Double>>());
		tab.add(new ArrayList<ArrayList<Double>>());
		tab.add(new ArrayList<ArrayList<Double>>());
		tab.add(new ArrayList<ArrayList<Double>>()); */
		Graph g =new Graph(tab);
		System.out.println(g.toString());
		System.out.println(distance_segment(new Point(-1,-5), new Point(-1,-1),new Point(0,0)));
		Segment s1 = new Segment(new Point(4.0,1.0), new Point(4.0,2.0));
		Segment s2 = new Segment(new Point(1.0,4.0), new Point(1.0,7.0));
		Segment s3 = new Segment(new Point(7.0,7.0), new Point(9.0,7.0));
		ArrayList<Segment> ls = new ArrayList<Segment>();
		ls.add(s1);
		ls.add(s2);
		ls.add(s3);
		Graph g2 = g.reduire(ls);
		System.out.println(g2);
	}

}

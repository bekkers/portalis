import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.text.Normalizer;
import java.text.Normalizer.Form;
import java.util.ArrayList;
import java.util.Scanner;

import javax.swing.JOptionPane; 

/**
 * /!\ Cette classe est sp�cifique au traitement des fichiers pour le parc du Thabor /!\
 * 
 * Cette classe modifie le fichier ctx pass� en param�tre et y ajoute la taxonomie (la vraie^^)
 * 	- en regardant les familles g�n�rales pr�sentes (Marronier, cerisier etc) (Cette taxo sera nomm�e taxo g�n�rale dans les commentaires)
 * 	- en ajoutant la notion de famille, un rang au dessus de l'�tape pr�c�dente (Rosac�es->cerisier->cerisier du japon) (cette taxo sera nomm�e taxo familiale dans les commentaires)
 * 
 * @author Laurent ODERMATT
 * 
 */



public class Taxonomyer {
	/**
	 * fichier contenant la taxonomie des diff�rentes esp�ces, familles etc
	 */
	private String taxo;
	/**
	 * le fichier ctx dans lequel sera implant� la taxo
	 */
	private String ctx;
	/**
	 * liste des noms communs et des noms g�n�raux
	 */
	private ArrayList<String> list, listGen;
	private FileWriter fw;
	private boolean first;
	private BufferedWriter output;
	private boolean finished;
	/**
	 * liste des noms g�n�raux d�j� trait�s
	 */
	private ArrayList<String> listGen2;
	private CsvToCtx2 parent;
	

	/**
	 * constructeur
	 * @param taxo le fichier contenant la taxonomie des diff�rentes esp�ces, familles etc
	 * @param ctx le fichier ctx dans lequel sera implant� la taxo
	 */
	public Taxonomyer(CsvToCtx2 parent,String taxo, String ctx){
		this.taxo = taxo;
		this.parent = parent;
		this.ctx = ctx;
		list = new ArrayList<String>();
		listGen = new ArrayList<String>();
		listGen2 = new ArrayList<String>();
		finished = false;
		first = true;

	}
	

	
	/**
	 * cr�ation de la taxonomie g�n�rale : on analyse les diff�rents noms d'arbres et on regarde s'ils ont une racine en commun (ex : cerisier � fleurs et cerisier du japon -> cerisier)
	 * @param str ligne lue dans le csv
	 * @param type type de la taxo gen : scientifique ou commune
	 * @return 
	 */
	String taxoGen(String str, int type) {
		String ret="";
		
		String[] sstr = str.split(";");
		for(int i = 0; i < sstr.length; i++)
			if(sstr[i].contains("?"))sstr[i] = ""; // suppression de toutes les occurences de "??"
		if(type == 0){
			if(!sstr[4].equals("")){
				String str2 = sstr[4];
				sstr = sstr[4].split("_");//la premi�re case de ce tableau contiendra le nom "g�n�ral"
				
				if(!sstr[0].equals(null)){
					//ret = "axiom "+str2+","+parent.getMapR().taxoSect(str)+"\n"; //str2 = nom commun
					//System.out.println(str2);
					if(!list.contains(str2)){//on ajoute la taxo g�n�rale de str2 (le nom commun) � la liste
						if(!listGen.contains(sstr[0])){
							listGen.add(sstr[0]);
							ret = "axiom "+sstr[0]+","+"Nom_commun"+"\n";
						}
						return (ret+="axiom "+str2+","+sstr[0]+"\n");
						
					}
					else
						return "";
				}
			}
		}else{
			if(!sstr[3].equals("")){
				String str2 = sstr[3];
		
				sstr = sstr[3].split("_");//la premi�re case de ce tableau contiendra le nom "g�n�ral"
	
				if(!sstr[0].equals(null)){
					//ret = "axiom "+str2+","+parent.getMapR().taxoSect(str)+"\n";

					if(!list.contains(str2)){//on ajoute la taxo g�n�rale de str2 (le nom scientifique) � la liste
						list.add(str2);
						if(!listGen.contains(sstr[0]))
							listGen.add(sstr[0]);
						if(!sstr[0].equals(""))
							//ret = "axiom "+sstr[0]+","+parent.getMapR().taxoSect(str)+"\n";
							return (ret+"axiom "+str2+","+sstr[0]+"\naxiom "+sstr[0]+",Nom_scientifique\n");
					}
	
					else
						return "";
				}
			}
		}	
		return "";
		
		
	}


	/**
	 * lecture du fichier de taxonomie et ajout dans le ctx de la taxo familiale
	 * @param str 
	 * 
	 */
	private void taxoFam(String str){
		
		String[] sstr = str.split(";");
		//tableau contenant le nom de la plante uniquement
		String[] sstr2 = sstr[0].split(" ");
		
		String str2 = null;
		//removeDiacriticalMarks(sstr2[0].split(" ")[0]).toUpperCase() = le nom de l'arbre (nom g�n�ral) en majuscules et sans accent
		// si l'arbre a �t� trouv� dans la liste de ceux mis dans le fichier ctx ET qu'on ne l'a pas encore "mis avec" sa famille, on le fait
		if(listGen.contains(removeDiacriticalMarks(sstr2[0].split(" ")[0]).toUpperCase()) && !listGen2.contains(removeDiacriticalMarks(sstr2[0].split(" ")[0]).toUpperCase())){

			str2 = "axiom "+removeDiacriticalMarks(sstr2[0]).toUpperCase()+","+sstr[2]+"\n"; // si le nom "g�n�rique commun" ("marronier") est pr�sent dans la database, on ajoute la famille correspondante. ex : mk "MARRONNIER" gallinac�e (exemple s�rement faux xD)
			str2+= "axiom "+sstr[2]+", Famille\n";
			listGen2.add(removeDiacriticalMarks(sstr2[0].split(" ")[0]).toUpperCase());
			
			writeFile(str2);

		}	
		//fermeture du fichier
		if(finished)
			try {
				output.close();
				JOptionPane.showMessageDialog(Converter.getInstance(), "conversion termin�e", "Message", JOptionPane.INFORMATION_MESSAGE);

			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	}
	
	/**
	 * enl�ve les accents du string pass� en param�tre
	 * @param string texte � modifier
	 * @return string sans les accents
	 */
	public static String removeDiacriticalMarks(String string) {
	    return Normalizer.normalize(string, Form.NFD).replaceAll("\\p{InCombiningDiacriticalMarks}+", "");
	}
	
	/**
	 * lit le fichier ctx et ajoute la taxonomie familiale
	 * 
	 */
	void readTaxoFam(){
		Scanner scanner = null;
		String str = null, tmp = null;
		try {
			
			scanner = new Scanner(new FileReader(this.taxo));
			while (scanner.hasNextLine()) {
				str = scanner.nextLine();
				if(!scanner.hasNextLine()){
					finished = true;
				}
				if(!str.contains("coordinates"))
					taxoFam(str);
			}
			
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * �crit text dans le fichier ctx
	 * @param text
	 */
	void writeFile(String text){	
		try
		{

			if(first){
				fw = new FileWriter(ctx, true);
				first = false;
				this.output = new BufferedWriter(fw);
			}
			
			//on marque dans le fichier ou plutot dans le BufferedWriter qui sert comme un tampon(stream)
			output.write(removeDiacriticalMarks(text)); // � retirer d�s qu'on aura trouv� la cause des conflits d'accents
			//on peut utiliser plusieurs fois methode write
			
			output.flush();	//ensuite flush envoie dans le fichier, ne pas oublier cette methode pour le BufferedWriter
			
			
		}
		catch(IOException ioe){
			System.out.print("Erreur : ");
			ioe.printStackTrace();
		}

	}
	
	
}

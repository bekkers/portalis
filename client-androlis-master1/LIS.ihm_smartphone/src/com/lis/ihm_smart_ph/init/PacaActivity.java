package com.lis.ihm_smart_ph.init;

import com.lis.activity.R;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

public class PacaActivity extends Activity {
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        
        Button connexion = (Button) findViewById(R.id.connexion);
        connexion.setOnClickListener(new OnClickListener() {
			
			public void onClick(View v) {
				//tester la connexion au serveur
				//Si le serveur repond connexion ok alors
				//afficher les services camelis
				Intent intent = new Intent(PacaActivity.this, ServiceActivity.class);
				startActivityForResult(intent, 0);
				finish();
				return;
			}
		});
        
    }
}
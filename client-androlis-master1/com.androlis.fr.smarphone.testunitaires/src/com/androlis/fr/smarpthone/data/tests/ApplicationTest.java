package com.androlis.fr.smarpthone.data.tests;

import android.test.AndroidTestCase;

import com.androlis.fr.smartphone.modele.data.Application;

public class ApplicationTest extends AndroidTestCase {

	private Application app;
	private String nom;
	
	@Override
	protected void setUp() throws Exception {
		super.setUp();
		nom = "test application";
		app = new Application(nom);
	}
	
	public void testNom() throws Exception {
		assertEquals(app.getName(), nom);
	}
	
	public void testImageApplication() throws Exception {
		assertEquals(app.getImage(), Application.NO_PICTURE);
	}
	
}

package com.androlis.fr.smarpthone.data.lis.tests;

import java.util.ArrayList;

import com.androlis.fr.smartphone.modele.data.lis.Axiome;
import com.androlis.fr.smartphone.modele.data.lis.Groupe;

import android.test.AndroidTestCase;

public class TaxonomieTest extends AndroidTestCase {

	private ArrayList<Groupe> groupes;
	private static final int NB_GROUPES = 10;
	private static final int NB_AXIOMES = 10;
	
	@Override
	protected void setUp() throws Exception {
		super.setUp();
		groupes = new ArrayList<Groupe>();

		for(int i = 0 ; i < NB_GROUPES ; i++ ){
			Groupe g = new Groupe("Groupe " + i);
			for(int j = 0 ; j < NB_AXIOMES ; j++ ){
				g.addAxiome(new Axiome(g, "axiome " + j));
			}
			groupes.add(g);
		}
	}

	public void testNbGroupe() throws Exception {
		assertTrue(groupes.size() == NB_GROUPES);
	}
	
	public void testNbAxiomeParGroupe() throws Exception {
		for(Groupe g : groupes){
			assertTrue(g.getAxiomes().size() == NB_AXIOMES);
		}
	}
	
	public void testAxiomePossedeGruope() throws Exception {
		for(Groupe g : groupes){
			ArrayList<Axiome> axiomes = g.getAxiomes();
			for(Axiome ax : axiomes){
				assertTrue(g == ax.getGroupe());
			}
		}
	}

	public void testRemoveAxiome() throws Exception {
		Axiome ax = groupes.get(NB_GROUPES/2).getAxiome(NB_AXIOMES/2);
		groupes.get(NB_GROUPES/2).removeAxiome(ax);
		for(Groupe g : groupes){
			assertFalse(g.getAxiomes().contains(ax));
		}
	}
	
	public void testAjoutAxiome() throws Exception {
		Axiome ax = new Axiome(null, "Nouvel axiome");
		groupes.get(NB_GROUPES/2).addAxiome(ax);
		ax.setGroupe(groupes.get(NB_GROUPES/2));
	
		for(Groupe g : groupes){
			if( g.equals(groupes.get(NB_GROUPES/2))){
				assertTrue(g.getAxiomes().contains(ax));
				assertTrue(ax.getGroupe() == g);
			}else{
				assertFalse(g.getAxiomes().contains(ax));
			}
		}
	}
}

package com.androlis.fr.smarpthone.data.lis.tests;

import com.androlis.fr.smartphone.modele.data.lis.Requete;

import android.test.AndroidTestCase;

public class RequeteTest extends AndroidTestCase {

	private Requete requete;
	private String axiome1;
	private String axiome2;
	private String axiome3;
	
	@Override
	protected void setUp() throws Exception {
		requete = new Requete();
		axiome1 = "axiome:1";
		axiome2 = "axiome:2";
		axiome3 = "axiome:3";
		super.setUp();
	}
	
	public void testInitialisation() throws Exception {
		assertEquals(requete.getRequete(), "all");
	}
	
	public void testAddAndAxiomeDebut() throws Exception {
		requete.andAxiome( axiome1 );
		assertEquals( requete.getRequete(), axiome1);
	}

	public void testOrAndAxiomeDebut() throws Exception {
		requete.orAxiome(axiome1);
		assertEquals( requete.getRequete(), axiome1);
	}
	
	public void testAddAndNotAxiomeDebut() throws Exception {
		requete.andNotAxiome(axiome1);
		assertEquals( requete.getRequete(), "not%20"+axiome1);
	}
	
	public void testAddOrNotAxiomeDebut() throws Exception {
		requete.orNotAxiome("axiome:1");
		assertEquals( requete.getRequete(), "not%20"+axiome1);
	}
	
	public void testAdd2Axiome() throws Exception {
		requete.andAxiome(axiome1);
		requete.andAxiome(axiome2);
		assertEquals(requete.getRequete(), axiome1+"%20and%20"+axiome2);
	}
	
	public void testAdd2NotAxiome() throws Exception {
		requete.andAxiome(axiome1);
		requete.andNotAxiome(axiome2);
		assertEquals(requete.getRequete(), axiome1+"%20and%20not%20"+axiome2);
	}
	
	public void testOr2Axiome() throws Exception{
		requete.orAxiome(axiome1);
		requete.orAxiome(axiome2);
		assertEquals(requete.getRequete(), axiome1+"%20or%20"+axiome2);
	}
	
	public void testOr2NotAxiome() throws Exception{
		requete.orAxiome(axiome1);
		requete.orNotAxiome(axiome2);
		assertEquals(requete.getRequete(), axiome1+"%20or%20not%20"+axiome2);
	}
	
	public void testClear() throws Exception{
		requete.clear();
		assertEquals(requete.getRequete(), "all");
	}
	
	public void testRemoveAnd() throws Exception {
		requete.andAxiome(axiome1);
		requete.andAxiome(axiome2);
		requete.andAxiome(axiome3);
		
		String res = axiome1+"%20and%20"+axiome2+"%20and%20"+axiome3;
		
		assertEquals(requete.getRequete(), res);
		requete.remove(axiome2);
		
		assertEquals(requete.getRequete(), axiome1+"%20and%20"+axiome3);
	}
	
	public void testRemoveOr() throws Exception {
		requete.orAxiome(axiome1);
		requete.orAxiome(axiome2);
		requete.orAxiome(axiome3);
		
		String res = axiome1+"%20or%20"+axiome2+"%20or%20"+axiome3;
		
		assertEquals(requete.getRequete(), res);
		requete.remove(axiome2);
		
		assertEquals(requete.getRequete(), axiome1+"%20or%20"+axiome3);
	}
	
	public void testRemoveAndNot() throws Exception {
		requete.andAxiome(axiome1);
		requete.andNotAxiome(axiome2);
		requete.andAxiome(axiome3);
		
		String res = axiome1+"%20and%20not%20"+axiome2+"%20and%20"+axiome3;
		
		assertEquals(requete.getRequete(), res);
		requete.remove(axiome2);
		
		assertEquals(requete.getRequete(), axiome1+"%20and%20"+axiome3);
	}
	
	public void testRemoveOrNot() throws Exception {
		requete.andAxiome(axiome1);
		requete.orNotAxiome(axiome2);
		requete.andAxiome(axiome3);
		
		String res = axiome1+"%20or%20not%20"+axiome2+"%20and%20"+axiome3;
		
		assertEquals(requete.getRequete(), res);
		requete.remove(axiome2);
		
		assertEquals(requete.getRequete(), axiome1+"%20and%20"+axiome3);
	}
	
	public void testGetAxiomes() throws Exception {
		requete.andAxiome(axiome1);
		requete.orNotAxiome(axiome2);
		requete.andAxiome(axiome3);
		
		String[] axiomes = requete.getAxiomes();
		assertEquals(axiome1, axiomes[0]);
		assertEquals(axiome2, axiomes[1]);
		assertEquals(axiome3, axiomes[2]);
		assertTrue(axiomes.length == 3);
	}
}
